# REWIND

Rewind is a puzzle game with unique game mechanics that challenges your mind. The spirits of the Jungle have been caught by some evil force.
Your mission as a mystical creature of the jungle is to free them again so that the Jungle can prosper.
To do that, you have to solve the puzzles and open the cages that keeps the spirits captive. To solve the puzzle you must use the Rewind ability
First, you record your actions, then the actions will be replayed by a copy of yourself. This copy is also a physical being so it will also have an impact on the environment.
After performing your actions you can choose to continue with either your copy or the initial character. You will explore amazing Jungle landscapes, dead forests and a cave.

![REWIND](https://bitbucket.org/four_rhonkies/rewind/raw/publish/images/rewind_impression.PNG)

## Trailer
[![REWIND Trailer](http://img.youtube.com/vi/ZseN123a1-I/1.jpg)](http://www.youtube.com/watch?v=ZseN123a1-I)

## Team
Tunay Bora, Per N�slund, Niclas Scheuing, Magenta Zeng

##Controll
This section is valid during devolepment only
D: move right
A: move left
Space: jump
R: start recording actions or replay actions
1: Dismiss ghost character
2: Dismiss original character
P: print a line into output console
Arrows: move camera
I: zoom in
O: zoom out
U: toggle debug rendering

## Install
- Install Visual Studio 2017, C# version 7.0 is required
- Install Monogame 3.6

### Install Monogame.Extended
1. in your terminal: cd gamelab-2017-team8/Project/external
2. git clone https://github.com/craftworkgames/MonoGame.Extended.git
3. Start team8 Visual Studio project
4. In the solution explorer right click on the Solution 'team8'
5. Add/existing project...
6. navigate to external/MonoGame.Extended/Source/MonoGame.Extended
7. select MonoGame.Extended.scproj
11. Right click on MonoGame.Extended (portable) and build.
If it fails, goto 8. else goto 12.
8. In solution explorer right click on MonoGame.Extended (portable) and select ManageNuGetPackages and go to Installed
9. Uninstall MonoGame.Framework.Protable and Newtonsoft.Json
10. Reinstall MonoGame.Framework.Protable and Newtonsoft.Json
11. Right click on MonoGame.Extended (portable) and build. If it fails, fix it somehow.
12. Rebuild and done!

### Install Monogame.Extended.SceneGraphs
This works the same as installing MonoGame.Extended and is just some sort of submodule of Monogame.Extended.
It works the same for all the submodules used (such as SceneGraphs) and in the following <modul_name> will refer to the submoduls name (e.g. <modul_name> is SceneGraphs)

1. In the solution explorer right click on the Solution 'team8'
2. Add/existing project...
3. navigate to external/MonoGame.Extended/Source/MonoGame.Extended.<modul_name>
4. select MonoGame.Extended.<modul_name>.scproj
5. Right click on MonoGame.Extended (portable) and build.

### Install Farseer Physics engine
1. In the solution explorer go to team8/References
2. right click on refernce, select "Manage NuGet packages"
3. Install FarsserPhysicsMonGameWindows8

### Trouble shooting
#### During build
Error message: _Unable to load DLL 'FreeImage': The specified module could not be found._
Try downloading https://www.microsoft.com/en-us/download/details.aspx?id=40784 or check in VS Tools/Options, search for nuGet, enable "allow nuget to download missing packages" and "Automatically check formissing packages during build in Visual Studio"

## Level Editor
### Tools
Tiled is used for editing levels. Download from http://www.mapeditor.org/

### Create levels
Any layer that should get rendered needs the 'type' and 'depth' custom attribute set.

#### Tiled Layers
__Attributes:__

|Name  |type  |Valid values|
|------|------|------------|
|type  |string|'action','foreground','background') |
|depth |flaot |<0 foreground, =0 action layer, >0 background|

Used for prototying only.
They result in a very large tmx file.
Only for displaying, no physical objects possible.

#### Object Layer
__Attributes:__

|Name  |type  |Valid values|
|------|------|------------|
|type  |string|'action','foreground','background') |
|depth |float |<0 foreground, =0 action layer, >0 background|

These attributes are passed down to all objects in this layer. If the objects define the same attributes, the object overwrites the layer.

|Name      |type  |Valid values|
|----------|------|------------|
|ground |bool  |true to set this as a ground collidor|
|wall      |bool  |true to set this as a wall object. 'collision' must be true as well|
|collision|bool|true to make this a collidor|
|rendergroupid|string|Any string. A pair of image object and geometry object must have the same _rendergroupid_.|
|jointgroupid|string|Any string|
|hidden|bool|true hides the object or disables its collision|
|bodytype|string|"dynamic" or "static"|
|fixrotation|bool|false makes the object rotate freely according to phyisics. true keeps it in its initial rotation|
|lethal|bool|marks elements that are dying zone. When they get touched, the character dies.|
|spirit|bool|Just as the spawn-point, but used to respawn ofter dying|

#### Objects
__Attributes:__

|Name      |type  |Valid values|
|----------|------|------------|
|ground |bool  |true to set this as a ground collidor|
|wall      |bool  |true to set this as a wall object. 'collision' must be true as well|
|collision|bool|true to make this a collidor|
|spawnpoint|bool  |true sets the first point as spawn-point. The geometry must be a polyline|
|rendergroupid|string|Any string. A pair of image object and geometry object must have the same _rendergroupid_.|
|bodytype|string|"dynamic", "kinematic" or "static"|
|fixrotation|bool|false makes the object rotate freely according to phyisics. true keeps it in its initial rotation|
|jointtype|string|"distance", "revolute", "prismatic". revolute must be a single point, the others a polyline with two points. The points must intersect the objects labeled with the same jointgroupid|
|jointgroupid|string|Any string. All objects involved with a joint must have this set.|
|frequency|float|>0, used for distance joints|
|damping|float|0<x<1, used for distance joints|
|density|float|>0, the density of a physical object. If you set it to 0, the object will get the mass of 1 and not rotate!|
|switchid|string|Any string. Used to identify switches|
|hidden|bool|true hides the object or disables its collision|
|parallaxorigin|bool|Object must be unique. It is the point, where all �arallax layers are aligned.|
|sensor|bool|true makes this object a sensor. This means OnCollision and OnSeparation are triggered, but no physical collisions are taking place.|
|angulardamping|float|The angular velocity of this object is dampend by this factor.|
|friction|bool|define the friction of this object|
|lethal|bool|marks elements that are dying zone. When they get touched, the character dies.|
|spirit|bool|Just as the spawn-point, but used to respawn ofter dying|
|trigger_rendergroupid|string|the rendergroupid of the cage and its collidor. Only used if spirit==true|
|music_region_id|string|marks polygon or regtangle as a music region|
|background_region_id|string|marks polygon or regtangle as a background region|
|end|bool|Collision with this object ends the game|
|alpha|float|alpha (color) value for images|

For animation objects

|Name      |type  |Valid values|
|----------|------|------------|
|animation|string|path of animation that should be shown instead of the image. Relative to Content Manager. Needs to be added to the Content Manager|
|column_count|int| number of columns in the sprite atlas|
|row_count|int| number of rows in the sprite atlas|
|frame_duration|float|The duration of a frame in seconds|
|start_frame|int|the frame to start the animation with|
|loop|bool|is true, the animation starts automatically and loops|
|reversed|bool|Play animation in revers order]

For Rope objects

|Name      |type  |Valid values|
|----------|------|------------|
|rope|bool|makes this polyline a rope, or rather a chain, with the points as joints|
|width|float|width of rope texture|
|fix_start|bool|make the beginning of the rope static|
|fix_end|bool|make the end of the rope static|

### Import levels
The level tmx file has to be added to the Visual Studio project. In the _Properties_ tab, set 'Build Action': Content and 'Copy to Output Directory': Copy if newer.

All image files must be added in the MonoGame Content manager.


## Install the AppPackage
see install.md

## Licence
Copyright 2017 Niclas Scheuing, Per N�slung, Tunay Bora, Xiaowei Zeng
This file is part of REWIND.

REWIND is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

REWIND is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with REWIND. If not, see <http://www.gnu.org/licenses/>.

## Attributions
Our thanks to the creators of the wonderful sounds we were able to use for this game.
https://www.freesound.org/people/Motion_S/sounds/221765/ - Landing squishy
https://www.freesound.org/people/speedygonzo/sounds/235718/ - Wooden landing
https://www.freesound.org/people/tommy_mooney/sounds/386707/ - running soft ground
https://www.freesound.org/people/ceberation/sounds/235521/ - Landing leaves

## BiBTex
@misc{rewind,
	author = {Tunay Bora, Per Naslund, Niclas Scheuing and Magenta Zeng},
	title = {REWIND},
	type = {Video Game},
	institution = {Game Technology Lab ETH Zürich, Game Design Department ZHDK},
	year = {2017},
	abstract = {Rewind is a puzzle 2D platformer game with unique game mechanics that challenges your mind. Developed with the Game Technology Lab ETHZ in collaboration with the game design department of ZHDK. Audience award winner.},
	url = {https://bitbucket.org/ley/REWIND}
	howpublished = {\url{https://bitbucket.org/ley/REWIND}}
}