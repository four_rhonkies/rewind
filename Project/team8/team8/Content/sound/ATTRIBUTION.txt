----------------------------------
ATTRIBUTION
----------------------------------
----------------------------------
All music by Janik Scheuing

This game uses many sounds from freesound
Title						Author
Wood Stomp Mud					https://freesound.org/people/Motion_S/
Footsteps_sneakers on wood			https://freesound.org/people/speedygonzo/
footsteps running on soft ground		https://freesound.org/people/tommy_mooney/
feet hitting the ground after jump		https://freesound.org/people/ceberation/
DoorCreak.wav					https://freesound.org/people/0XMUSEX0/
Tree stump breaks and falls			https://freesound.org/people/speedygonzo/
Stone dropping to ground_northern87		https://freesound.org/people/northern87/
Metalliputket, hel�hdys / Small metal tubes, chimes, a short tinkle...	https://freesound.org/people/YleArkisto/
furnace creaks.wav				https://freesound.org/people/sforsman/
Flagstones scraping.wav	https://freesound.org/people/Metzik/
cartoon style footsteps and peeking through bushes	https://freesound.org/people/elektroproleter/
Black Casqued Hornbill 5.wav			https://freesound.org/people/ERH/
Monkey screaming.wav				https://freesound.org/people/Archeos/
2012_05_2012_Siamang_gibbon.wav			https://freesound.org/people/reinsamba/
amazon 04.wav					https://freesound.org/people/volivieri/
Noisy Cricket					https://freesound.org/people/bspiller5/
Landing squishy					https://www.freesound.org/people/Motion_S/sounds/221765/
Wooden landing					https://www.freesound.org/people/Motion_S/sounds/221765/
running soft ground				https://www.freesound.org/people/tommy_mooney/sounds/386707/
Landing leaves					https://www.freesound.org/people/ceberation/sounds/235521/