Copyright 2017 Niclas Scheuing, Per N�slung, Tunay Bora, Xiaowei Zeng
All files in this directory are part of REWIND.

These files are part of a free software: you can redistribute it and/or modify
them under the terms of the GNU Free Documentation License  as published by
the Free Software Foundation, either version  1.3 of the License, or
(at your option) any later version.

These files are distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
NU General Public License for more details.

You should have received a copy of the GNU Free Documentation License
along with these files. If not, see <http://www.gnu.org/licenses/>.

