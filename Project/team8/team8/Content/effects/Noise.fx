﻿#if OPENGL
	#define SV_POSITION POSITION
	#define VS_SHADERMODEL vs_3_0
	#define PS_SHADERMODEL ps_3_0
#else
	#define VS_SHADERMODEL vs_4_0_level_9_1
	#define PS_SHADERMODEL ps_4_0
#endif

Texture2D SpriteTexture;
float RandomSeed;
float2 CharacterPosition;
float EffectAmount;

sampler2D SpriteTextureSampler = sampler_state
{
	Texture = <SpriteTexture>;
};

struct VertexShaderOutput
{
	float4 Position : SV_POSITION;
	float4 Color : COLOR0;
	float2 TextureCoordinates : TEXCOORD0;
};
float rand_1_05(in float2 uv)
{
    float2 noise = (frac(sin(dot(uv, float2(12.9898, 78.233) * 2.0)) * 43758.5453));
    return abs(noise.x + noise.y) * 0.5;
}
float4 MainPS(VertexShaderOutput input) : COLOR
{
    float distance = length(input.Position.xy - CharacterPosition);
    float2 direction = normalize(input.Position.xy - CharacterPosition);
	float intDistance = float(int(distance.x) / 50);

    float2 intDirection = float2(int(direction.x * 100) / 10, int(direction.y * 100) / 10);
	//rand_1_05(intDirection)
    float random = (sin(intDistance - 4 * RandomSeed) + 1) * EffectAmount; //+dddddistance * 0.001;
    return tex2D(SpriteTextureSampler, input.TextureCoordinates) - float4(random, random, random, 0);
}


technique SpriteDrawing
{
	pass P0
	{
		PixelShader = compile PS_SHADERMODEL MainPS();
	}
};