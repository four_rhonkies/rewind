﻿#if OPENGL
	#define SV_POSITION POSITION
	#define VS_SHADERMODEL vs_3_0
	#define PS_SHADERMODEL ps_3_0
#else
	#define VS_SHADERMODEL vs_4_0_level_9_1
	#define PS_SHADERMODEL ps_4_0
#endif

Texture2D SpriteTexture;
float2 CharacterPosition;
float Depth;
float FogStart;
float FogEnd;
float FogIntensity;
float ScreenDiagonal;

sampler2D SpriteTextureSampler = sampler_state
{
    Texture = <SpriteTexture>;
};

struct VertexShaderOutput
{
    float4 Position : SV_POSITION;
    float4 Color : COLOR0;
    float2 TextureCoordinates : TEXCOORD0;
};

float4 MainPS(VertexShaderOutput input) : COLOR
{
    float4 texColor = tex2D(SpriteTextureSampler, input.TextureCoordinates);
    float3 fogColor = float3(0.5, 0.5, 0.5);
    float depth = (Depth - FogStart) / (FogEnd - FogStart);
    float distanceToCharacter = length(CharacterPosition - input.Position.xy);
    distanceToCharacter = distanceToCharacter / ScreenDiagonal;
    float fogIntensity = ((0.15 * distanceToCharacter) + (depth * 0.85f)) * FogIntensity;
    //distanceToCharacter = (Depth * 0.5f);
    
    return float4(fogIntensity * fogColor + (1.0 - fogIntensity) * texColor.rgb*input.Color.rgb, texColor.w) * texColor.w;
}

technique SpriteDrawing
{
    pass P0
    {
        PixelShader = compile PS_SHADERMODEL MainPS();
    }
};