﻿#if OPENGL
	#define SV_POSITION POSITION
	#define VS_SHADERMODEL vs_3_0
	#define PS_SHADERMODEL ps_3_0
#else
	#define VS_SHADERMODEL vs_4_0_level_9_1
	#define PS_SHADERMODEL ps_4_0
#endif

Texture2D SpriteTexture;
float FadeCorrection;
float BlurWeights[21];
int BlurStrength;
float2 CharacterPosition;

sampler2D SpriteTextureSampler = sampler_state
{
	Texture = <SpriteTexture>;
};

struct VertexShaderOutput
{
	float4 Position : SV_POSITION;
	float4 Color : COLOR0;
	float2 TextureCoordinates : TEXCOORD0;
};

float4 MainPSHoirzontal(VertexShaderOutput input) : COLOR
{
    
	float4 color = { 0, 0, 0, 0 };
    float2 textureCoord = input.TextureCoordinates;
    float2 direction = input.Position.xy-CharacterPosition;
    int center = (BlurStrength-1)/2;
    int blurStrength = (BlurStrength - 1) / 2;
    for (int i = 0; i < BlurStrength; i++)
    {
        color += tex2D(SpriteTextureSampler, textureCoord + direction * (i - center) * FadeCorrection) * BlurWeights[i];
    }
    color.w = tex2D(SpriteTextureSampler, textureCoord).w;
    if (input.TextureCoordinates.x % 1 == 0)
    {
		color.gb = float2(0,0);
    }
	return color;
}

//float4 MainPSHoirzontal(VertexShaderOutput input) : COLOR
//{
//    return blur(input.TextureCoordinates, ScreenWidth, 1);
//}
//float4 MainPSVertical(VertexShaderOutput input) : COLOR
//{
//    return blur(input.TextureCoordinates, ScreenHeight,1);
//}
//float4 MainPSHoirzontal2(VertexShaderOutput input) : COLOR
//{
//    return blur(input.TextureCoordinates, ScreenWidth, 100);
//}
//float4 MainPSVertical2(VertexShaderOutput input) : COLOR
//{
//    return blur(input.TextureCoordinates, ScreenHeight, 100);
//}
technique SpriteDrawing1
{
    pass P0
    {
        PixelShader = compile PS_SHADERMODEL MainPSHoirzontal();
    }
};
//technique SpriteDrawing
//{
//    pass P1
//    {
//        PixelShader = compile PS_SHADERMODEL MainPSVertical2();
//    }
//};