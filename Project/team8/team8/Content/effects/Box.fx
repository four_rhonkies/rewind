﻿#if OPENGL
	#define SV_POSITION POSITION
	#define VS_SHADERMODEL vs_3_0
	#define PS_SHADERMODEL ps_3_0
#else
	#define VS_SHADERMODEL vs_4_0_level_9_1
	#define PS_SHADERMODEL ps_4_0
#endif

Texture2D SpriteTexture;
float BoxSize;
float ScreenWidth;
float ScreenHeight;

sampler2D SpriteTextureSampler = sampler_state
{
	Texture = <SpriteTexture>;
};

struct VertexShaderOutput
{
	float4 Position : SV_POSITION;
	float4 Color : COLOR0;
	float2 TextureCoordinates : TEXCOORD0;
};

float4 MainPS(VertexShaderOutput input) : COLOR
{
    float2 blur;
	float4 color = { 0, 0, 0, 0 }; 
	float4 finalColor = { 0, 0, 0, 0 };
    float weight = 1/(BoxSize * BoxSize);
    int center = (BoxSize-1)/2;
    
	[unroll(121)]
    for (int i = -center; i <= center; i++)
    {
        blur.x = input.TextureCoordinates.x + i/ ScreenWidth;
        for (int j = -center; j <= center; j++)
        {
            blur.y = input.TextureCoordinates.y + j/ ScreenHeight;
            color += tex2D(SpriteTextureSampler, blur.xy) * weight;
        }
    }
	finalColor.x = color.x;
	finalColor.y = color.y;
	finalColor.z = color.z;
	finalColor.w = tex2D(SpriteTextureSampler, input.TextureCoordinates).w;
	return finalColor*input.Color;
}

technique SpriteDrawing
{
	pass P0
	{
		PixelShader = compile PS_SHADERMODEL MainPS();
	}
};