﻿#if OPENGL
	#define SV_POSITION POSITION
	#define VS_SHADERMODEL vs_3_0
	#define PS_SHADERMODEL ps_3_0
#else
	#define VS_SHADERMODEL vs_4_0_level_9_1
	#define PS_SHADERMODEL ps_4_0
#endif

Texture2D SpriteTexture;
float2 CharacterPosition;
float DiagonalLength;
float Radius;
bool Inverted=false;

sampler2D SpriteTextureSampler = sampler_state
{
    Texture = <SpriteTexture>;
};

struct VertexShaderOutput
{
    float4 Position : SV_POSITION;
    float4 Color : COLOR0;
    float2 TextureCoordinates : TEXCOORD0;
};

float4 MainPS(VertexShaderOutput input) : COLOR
{
	float4 finalColor = tex2D(SpriteTextureSampler, input.TextureCoordinates);
    float distanceToCharacter = length(CharacterPosition - input.Position.xy);
    float alpha = max(0,distanceToCharacter-Radius)/ DiagonalLength;
    alpha = min(alpha, 1);
	if(Inverted)
    {
        alpha = 1 - alpha;
    }
    return finalColor*alpha;
}

technique SpriteDrawing
{
    pass P0
    {
        PixelShader = compile PS_SHADERMODEL MainPS();
    }
};