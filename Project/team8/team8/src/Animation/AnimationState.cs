﻿/**
*    Copyright 2017 Niclas Scheuing, Per Näslung, Tunay Bora, Xiaowei Zeng    
*    This file is part of REWIND.
*
*    REWIND is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    REWIND is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with REWIND. If not, see <http://www.gnu.org/licenses/>.
*/

using FarseerPhysics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using MonoGame.Extended.Animations.SpriteSheets;
using MonoGame.Extended.Sprites;
using MonoGame.Extended.TextureAtlases;
using System;
using System.Collections.Generic;
using System.Linq;
using static team8.Physics.PhysicsObject;

namespace team8.Animation
{
    public class AnimationState
    {
        private TextureAtlas atlas;
        public SpriteSheetAnimation Animation;
        private Sprite sprite;
        public Sprite Sprite
        {
            get
            {
                return sprite;
            }
        }
        private Texture2D texture;
        public int FrameWidth;
        public int FrameHeight;

        public delegate void FrameChangedDelegate(int frameIndex);
        public FrameChangedDelegate OnFrameChange;
        private ContentManager ContentManager;
        private int LastFrameIndex;
        private Vector2 Scale;
        public Vector2 AnimationScale
        {
            get
            {
                return Scale;
            }
        }

        public int CurrentFrameIndex
        {
            get
            {
                return Animation.CurrentFrameIndex;
            }
        }
        


        public AnimationState(ContentManager contentManager, string texturePath, int rowCount, int columnCount, float frameDuration, bool loop = true, bool reversed = false, bool pingPong=false, (int,int)? startAndEndIndex=null)
        {
            this.ContentManager = contentManager;
            this.SetAnimation(texturePath: texturePath, rowCount: rowCount, columnCount: columnCount, frameDuration: frameDuration, loop: loop, reversed: reversed, pingPong: pingPong, startAndEndIndex: startAndEndIndex);
            
        }

        public void SetAnimation(string texturePath, int rowCount, int columnCount, float frameDuration, bool loop = true, bool reversed = false, bool pingPong = false, (int, int)? startAndEndIndex = null)
        {
            LastFrameIndex = 0;
            this.texture = ContentManager.Load<Texture2D>(texturePath);
            FrameWidth = texture.Width / columnCount;
            FrameHeight = texture.Height / rowCount;
            this.atlas = TextureAtlas.Create(name: texturePath, texture: texture, regionWidth: FrameWidth, regionHeight: FrameHeight);
            TextureRegion2D[] regions = atlas.Regions.ToArray();
            if (startAndEndIndex.HasValue)
            {
                regions = atlas.Regions.ToList().GetRange(startAndEndIndex.Value.Item1, (startAndEndIndex.Value.Item2 - startAndEndIndex.Value.Item1 + 1) ).ToArray();
            }
            this.Animation = CreateAnimation(name: texturePath, regions: regions, frameDuration: frameDuration, loop: loop, pingPong: pingPong, reversed: reversed);
            sprite = new Sprite(Animation.CurrentFrame);
        }

        private SpriteSheetAnimation CreateAnimation(string name, TextureRegion2D[] regions, float frameDuration, bool loop, bool pingPong, bool reversed)
        {
            var animation = new SpriteSheetAnimation(name: name, keyFrames: regions);
            animation.FrameDuration = frameDuration;
            animation.IsLooping = loop;
            animation.IsPingPong = pingPong;
            animation.IsReversed = reversed;
            return animation;
        }

        public void SetFrameDuration(float duration)
        {
            Animation.FrameDuration = duration;
        }

        public void SetScale(float scale)
        {
            this.Scale = new Vector2(scale, scale);
            sprite.Scale = this.Scale;
        }

        public void SetScale(Vector2 scale)
        {
            this.Scale = scale;
            sprite.Scale = this.Scale;
        }

        public void MultiplayScale(float factor)
        {
            this.Scale = this.Scale * factor;
            sprite.Scale = this.Scale;
        }

        public void Update(GameTime gameTime, Vector2 position, float? rotation=null)
        {
            var deltaSeconds = (float)gameTime.ElapsedGameTime.TotalSeconds;
            Animation.Update(deltaSeconds);
            sprite.TextureRegion = Animation.CurrentFrame;
            sprite.Position = position;
            sprite.Rotation = rotation.GetValueOrDefault(sprite.Rotation);
            //check if frame has changed. If yes invoke OnFrameChange
            if (LastFrameIndex != Animation.CurrentFrameIndex)
            {
                OnFrameChange?.Invoke(LastFrameIndex);
                LastFrameIndex = Animation.CurrentFrameIndex;
            }
        }
        public void ResetLastFrameIndex()
        {
            this.LastFrameIndex = 0;// this.Animation.KeyFrames.Count();
        }
        public void SetFrame(int frameIndex, Vector2 position, float? rotation = null)
        {
            var frames = Animation.KeyFrames;
            if (Animation.IsReversed)
            {
                frames.Reverse();
            }
            var frame = frames[frameIndex];
            sprite.Position = position;
            sprite.Rotation = rotation.GetValueOrDefault(sprite.Rotation);
            sprite.TextureRegion = frame;
        }
        
        public void SetAnimationDirection(bool forward)
        {
            this.Animation.IsReversed = !forward;
        }

        public void InvertAnimationDirection()
        {
            this.Animation.IsReversed = !this.Animation.IsReversed;
        }

        public void Rewind()
        {
            this.Animation = CreateAnimation(name: Animation.Name, regions: Animation.KeyFrames, frameDuration: Animation.FrameDuration, loop: Animation.IsLooping, pingPong: Animation.IsPingPong, reversed: Animation.IsReversed);
        }
        
        public void Dispose()
        {
            texture.Dispose();
        }
        public void SetDirection(Direction direction)
        {
            switch (direction)
            {
                case Direction.Right:
                    Sprite.Effect = SpriteEffects.None;
                    break;
                case Direction.Left:
                    Sprite.Effect = SpriteEffects.FlipHorizontally;
                    break;
            }
        }
    }
}
