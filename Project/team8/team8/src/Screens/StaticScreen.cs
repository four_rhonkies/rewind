﻿/**
* Copyright 2017 Niclas Scheuing, Per Näslung, Tunay Bora, Xiaowei Zeng
* This file is part of REWIND.
*
* REWIND is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* REWIND is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
* GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with REWIND.If not, see<http://www.gnu.org/licenses/>.
*/
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace team8.Screens
{
    public abstract class StaticScreen
    {
        protected SpriteFont TitleFont;
        protected Texture2D BackgroundTexture;
        protected Vector2 TitlePosition = new Vector2(0.5f, 0.45f);

        virtual public void LoadContent(ContentManager contentManager)
        {
            BackgroundTexture = contentManager.Load<Texture2D>(StaticScreenConstants.BackgroundTexturePath);
            TitleFont = contentManager.Load<SpriteFont>(FontConstants.TitleFont);
        }

        public void Draw(SpriteBatch spritebatch, GameTime gameTime)
        {
            spritebatch.Begin(blendState: BlendState.AlphaBlend);
            DrawContent(spriteBatch: spritebatch, gameTime: gameTime);
            spritebatch.End();
        }
        protected virtual void DrawContent(SpriteBatch spriteBatch, GameTime gameTime)
        {
            spriteBatch.Draw(texture: BackgroundTexture, destinationRectangle: spriteBatch.GraphicsDevice.Viewport.Bounds);

            var screenBounds = spriteBatch.GraphicsDevice.Viewport.Bounds;
            var fontSize = TitleFont.MeasureString(StaticScreenConstants.Text.Title);
            var textPosition = new Vector2((screenBounds.Width - fontSize.X) * TitlePosition.X, (screenBounds.Height - fontSize.Y) * TitlePosition.Y);
            spriteBatch.DrawString(spriteFont: TitleFont, text: StaticScreenConstants.Text.Title, position: textPosition, color: StaticScreenConstants.Colors.TitleFontColor);
        }
    }
}
