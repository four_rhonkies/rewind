﻿/**
* Copyright 2017 Niclas Scheuing, Per Näslung, Tunay Bora, Xiaowei Zeng
* This file is part of REWIND.
*
* REWIND is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* REWIND is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
* GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with REWIND.If not, see<http://www.gnu.org/licenses/>.
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace team8.Screens
{
    public class EndScreen: StaticScreen
    {
        //private Vector2 TextPosition;
        private SpriteFont Font;
        private Vector2 ThankYouPosition = new Vector2(0.5f, 0.2f);
        public override void LoadContent(ContentManager contentManager)
        {
            base.LoadContent(contentManager);
            Font = contentManager.Load<SpriteFont>(FontConstants.EndScreenFont);
        }

        protected override void DrawContent(SpriteBatch spriteBatch, GameTime gameTime)
        {
            base.DrawContent(spriteBatch: spriteBatch, gameTime: gameTime);
            var screenBounds = spriteBatch.GraphicsDevice.Viewport.Bounds;
            var fontSize = Font.MeasureString(StaticScreenConstants.Text.EndText);
            var textPosition = new Vector2((screenBounds.Width - fontSize.X) * ThankYouPosition.X, screenBounds.Height * ThankYouPosition.Y);
            spriteBatch.DrawString(spriteFont: Font, text: StaticScreenConstants.Text.EndText, position: textPosition, color: StaticScreenConstants.Colors.EndFontColor);
        }

    }
}
