﻿/**
* Copyright 2017 Niclas Scheuing, Per Näslung, Tunay Bora, Xiaowei Zeng
* This file is part of REWIND.
*
* REWIND is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* REWIND is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
* GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with REWIND.If not, see<http://www.gnu.org/licenses/>.
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace team8.Screens
{
    public class InstructionScreen: StaticScreen
    {
        private SpriteFont Font;
        public bool Visible = false;
        private Texture2D InstructionTexture;

        private Vector2 MoveTextPosition = new Vector2(0.1f, 0.2f); 
        private float LineSpace = 1.2f;

        public override void LoadContent(ContentManager contentManager)
        {
            base.LoadContent(contentManager);
            Font = contentManager.Load<SpriteFont>(FontConstants.StartScreenFont);
            var texturePath = StaticScreenConstants.InstructionKeyboardTexturePath;
            if (GamePad.GetState(playerIndex: PlayerIndex.One).IsConnected) //There is only one player
            {
                texturePath = StaticScreenConstants.InstructionControllerTexturePath;
            }
            InstructionTexture = contentManager.Load<Texture2D>(texturePath);
        }

        protected override void DrawContent(SpriteBatch spriteBatch, GameTime gameTime)
        {
            spriteBatch.Draw(texture: InstructionTexture, destinationRectangle: spriteBatch.GraphicsDevice.Viewport.Bounds);

            //var screenBounds = spriteBatch.GraphicsDevice.Viewport.Bounds;
            //var fontHeight = Font.MeasureString(InstructionConstants.Text.Move).Y;

            ////move
            //var textPosition = new Vector2((screenBounds.Width - Font.MeasureString(InstructionConstants.Text.Move).X) * MoveTextPosition.X, (screenBounds.Height - Font.MeasureString(InstructionConstants.Text.Move).Y) * MoveTextPosition.Y);
            //var text = InstructionConstants.Text.Move;
            //spriteBatch.DrawString(spriteFont: Font, text: text, position: textPosition, color: StaticScreenConstants.Colors.InstructionFontColor);

            ////jump
            //textPosition = textPosition + new Vector2(0, LineSpace* fontHeight);
            //text = InstructionConstants.Text.Jump;
            //spriteBatch.DrawString(spriteFont: Font, text: text, position: textPosition, color: StaticScreenConstants.Colors.InstructionFontColor);

            ////record
            //textPosition = textPosition + new Vector2(0, LineSpace * fontHeight);
            //text = InstructionConstants.Text.Record;
            //spriteBatch.DrawString(spriteFont: Font, text: text, position: textPosition, color: StaticScreenConstants.Colors.InstructionFontColor);

            ////replay
            //textPosition = textPosition + new Vector2(0, LineSpace * fontHeight);
            //text = InstructionConstants.Text.Replay;
            //spriteBatch.DrawString(spriteFont: Font, text: text, position: textPosition, color: StaticScreenConstants.Colors.InstructionFontColor);

            ////Mark
            //textPosition = textPosition + new Vector2(0, LineSpace * fontHeight);
            //text = InstructionConstants.Text.MarkCharacter;
            //spriteBatch.DrawString(spriteFont: Font, text: text, position: textPosition, color: StaticScreenConstants.Colors.InstructionFontColor);

            ////Choose
            //textPosition = textPosition + new Vector2(0, LineSpace * fontHeight);
            //text = InstructionConstants.Text.ChooseCharacter;
            //spriteBatch.DrawString(spriteFont: Font, text: text, position: textPosition, color: StaticScreenConstants.Colors.InstructionFontColor);

            ////reset
            //textPosition = textPosition + new Vector2(0, LineSpace * fontHeight);
            //text = InstructionConstants.Text.Reset;
            //spriteBatch.DrawString(spriteFont: Font, text: text, position: textPosition, color: StaticScreenConstants.Colors.InstructionFontColor);
        }
    }
}
