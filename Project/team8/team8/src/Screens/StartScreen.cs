﻿/**
* Copyright 2017 Niclas Scheuing, Per Näslung, Tunay Bora, Xiaowei Zeng
* This file is part of REWIND.
*
* REWIND is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* REWIND is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
* GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with REWIND.If not, see<http://www.gnu.org/licenses/>.
*/
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace team8.Screens
{
    public class StartScreen: StaticScreen
    {
        private SpriteFont Font;
        private SpriteFont CopyrightFont;
        private Vector2 StartPosition = new Vector2(0.5f, 0.6f);
        private Vector2 InstructionPosition = new Vector2(0.5f, 0.95f);
        private Vector2 CopyrightPosition = new Vector2(0.5f, 1f);
        private bool Loading = false;
        private float AnimationSpeed = 2.5f;

        public override void LoadContent(ContentManager contentManager)
        {
            base.LoadContent(contentManager);
            Font = contentManager.Load<SpriteFont>(FontConstants.StartScreenFont);
            CopyrightFont = contentManager.Load<SpriteFont>(FontConstants.CopyrightFont);
        }

        public void StartLoading()
        {
            this.Loading = true;
        }

        protected override void DrawContent(SpriteBatch spriteBatch, GameTime gameTime)
        {
            base.DrawContent(spriteBatch: spriteBatch, gameTime: gameTime);

            var screenBounds = spriteBatch.GraphicsDevice.Viewport.Bounds;
            var text = (Loading ? StaticScreenConstants.Text.Loading: GetPressToStartText());
            var fontSize = Font.MeasureString(text);
            var textPosition = new Vector2((screenBounds.Width - fontSize.X) * StartPosition.X, (screenBounds.Height - fontSize.Y) * StartPosition.Y);
            float alpha = (float) ((Math.Sin(gameTime.TotalGameTime.TotalSeconds*AnimationSpeed)+1)*0.5*0.8 + 0.2);
            spriteBatch.DrawString(spriteFont: Font, text: text, position: textPosition, color: StaticScreenConstants.Colors.EndFontColor * alpha);

            //copyright
            text = StaticScreenConstants.Text.Copyright;
            fontSize = CopyrightFont.MeasureString(text);
            textPosition = new Vector2((screenBounds.Width - fontSize.X) * CopyrightPosition.X, (screenBounds.Height - fontSize.Y) * CopyrightPosition.Y);
            spriteBatch.DrawString(spriteFont: CopyrightFont, text: text, position: textPosition, color: StaticScreenConstants.Colors.EndFontColor);

            if (Loading)
            {
                var instructionText = GetInstructionText();
                var instructionFontSize = Font.MeasureString(instructionText);
                var instructionTextPosition = new Vector2((screenBounds.Width - instructionFontSize.X) * InstructionPosition.X, (screenBounds.Height - instructionFontSize.Y) * InstructionPosition.Y);
                spriteBatch.DrawString(spriteFont: Font, text: instructionText, position: instructionTextPosition, color: StaticScreenConstants.Colors.EndFontColor);
            }
        }
        private string GetPressToStartText()
        {
            //If a controller is connected, show the controller text, otherwise the keyboard text
            var controllerConnected = GamePad.GetState(playerIndex: PlayerIndex.One).IsConnected; //There is only one player

            if (controllerConnected)
            {
                return StaticScreenConstants.Text.PressToStartController;
            }
            else
            {
                return StaticScreenConstants.Text.PressToStartKeyboard;
            }
        }

        private string GetInstructionText()
        {
            //If a controller is connected, show the controller text, otherwise the keyboard text
            var controllerConnected = GamePad.GetState(playerIndex: PlayerIndex.One).IsConnected; //There is only one player

            if (controllerConnected)
            {
                return StaticScreenConstants.Text.InstructionController;
            }
            else
            {
                return StaticScreenConstants.Text.InstructionKeyboard;
            }
        }

    }
}
