﻿/**
* Copyright 2017 Niclas Scheuing, Per Näslung, Tunay Bora, Xiaowei Zeng
* This file is part of REWIND.
*
* REWIND is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* REWIND is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
* GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with REWIND.If not, see<http://www.gnu.org/licenses/>.
*/
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using FarseerPhysics.Dynamics;
using FarseerPhysics;
using System.Diagnostics;
using team8.Replay;
using team8.Control;
using team8.Rendering;
using team8.LevelImporter;
using team8.Level;
using Microsoft.Xna.Framework.Content;
using System.IO;
using team8.Physics;
using team8.Sound;
using System.Collections.Generic;
using static team8.Control.InputController;
using team8.Screens;
using System.Threading;
using System.Threading.Tasks;
using System;

namespace team8
{

    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class GameMain : Game, ReloadGameDelegate, EndGameDelegate, ChangeScreenDelegate
    {
        private SpriteBatch spriteBatch;
        private GraphicsDeviceManager GraphicsDeviceManager;

        //private Texture2D background;
        //private Camera2D camera;
        private Renderer renderer;
        private PhysicalWorld World;
        private CameraController CameraController;
        private SoundManager SoundManager;

        //Class constants
        private const float WorldGravity = 15f;
        private const int PixelsPerMeter = 100;

        private const string ContentRootDirectory = "Content";

        //TODO: clean up and put into seperate file
        //Constants for testing
        private const float mainCharacterHeigth = 1.1f;

        private StartScreen StartScreen;
        private StaticScreen EndScreen;
        private InstructionScreen InstructionScreen;

        private InputController InputController;

        private ReplayEngine replayEngine;
        //private RecordingMovementController RecordingMovementController;

        //private MainCharacter mainCharacter;

        private Timer ShowEndScreenDelayTimer;

        private enum GameState
        {
            Running, Start, End, Outro
        }
        private GameState CurrentGameState = GameState.Start;

        private Map Map;
        private Outro Outro;

        public GameMain()
        {
            this.Window.Title = "Rewind";
            GraphicsDeviceManager = new GraphicsDeviceManager(this);
            GraphicsDeviceManager.IsFullScreen = Configuration.FullScreen;
            GraphicsDeviceManager.ApplyChanges();
            Content.RootDirectory = ContentRootDirectory;
            
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            base.Initialize();
            this.IsFixedTimeStep = false;
            
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            //create camera
            this.CameraController = new CameraController(window: Window, graphicsDevice: GraphicsDevice);
            this.CameraController.OnWindowSizeChanged += OnWindowSizeChanged;

            var path = Path.GetFullPath(ContentRootDirectory);
            LoadEngine();
            PreLoadInstructionScreen();
            LoadStartScreen();
            //LoadGame();
            //EndGame();
            //ShowInstructionScreen();

        }
        private void LoadStartScreen()
        {
            InputController.BlockAllKeys(exceptKeys: new List<KeyDefinition>() { KeyDefinition.ChooseCharacterKey, KeyDefinition.ShowInstructionsKey });
            StartScreen = new StartScreen();
            StartScreen.LoadContent(contentManager: Content);

            KeyPressDelegate keyDelegate = delegate (GameTime gameTime)
            {
                StartScreen.StartLoading();
                InputController.ReleaseHijackedControl(key: KeyDefinition.JumpKey);
                InputController.BlockAllKeys(exceptKeys: new List<KeyDefinition>() {KeyDefinition.ShowInstructionsKey });
                Task.Run(()=> {
                    LoadGame();
                    InputController.UnblockKeys();
                    if (InstructionScreen != null)
                    {
                        InstructionScreen.Visible = false;
                        World?.Resume();
                        InputController.ReleaseHijackedControl(key: KeyDefinition.ShowInstructionsKey);
                    }
                });
                
            };
            InputController.HijackControl(key: KeyDefinition.JumpKey, keyDelegate: keyDelegate, blocking: true);

            SoundManager.SetSoundtrack(track: SoundManager.SoundFile.StartScreen, trackFiltered: null, trackGhost: null, volume: Configuration.Sound.StartScreenMusicVolume, fadeOutInMs: 200);
        }

        //private void LoadEndScreen()
        //{
        //    EndScreen = new EndScreen();
        //    EndScreen.LoadContent(contentManager: Content);
        //    SoundManager.SetSoundtrack(track: SoundManager.SoundFile.EndScreen, trackFiltered: null, trackGhost: null, volume: Configuration.Sound.EndScreenMusicVolume);
        //}

        private void PreLoadInstructionScreen()
        {
            InstructionScreen = new InstructionScreen();
            InstructionScreen.LoadContent(contentManager: Content);
            //SoundManager.SetSoundtrack(track: SoundManager.SoundFile.EndScreen, trackFiltered: null, volume: Configuration.Sound.EndScreenMusicVolume);
        }
        public void ShowInstructionScreen()
        {
            World?.Pause();
            InstructionScreen.Visible = true;
            InputController.BlockAllKeys(exceptKeys: new List<KeyDefinition>() { KeyDefinition.ShowInstructionsKey });
            KeyPressDelegate keyDelegate = delegate (GameTime gameTime)
            {
                World?.Resume();
                InputController.ReleaseHijackedControl(key: KeyDefinition.ShowInstructionsKey);
                InputController.UnblockKeys();
                InstructionScreen.Visible = false;
            };
            InputController.HijackControl(key: KeyDefinition.ShowInstructionsKey, keyDelegate: keyDelegate, blocking: true, singlePress: true);
        }

        private void LoadEngine()
        {
            //Create new ContentManager
            Content = new ContentManager(serviceProvider: Services, rootDirectory: ContentRootDirectory);

            //Sound
            SoundManager = new SoundManager(content: Content);
            SoundManager.DistanceDelegate = CameraController;

            this.InputController = new InputController(cameraController: CameraController, soundManager: SoundManager);
            this.InputController.ReloadDelegate = this;
            this.InputController.ChangeScreenDelegate = this;
            this.InputController.AddInputHandler(new KeyboardInputHandler());
            this.InputController.AddInputHandler(new XboxOneControllerInputHandler());
            this.InputController.AddInputHandler(new AltXboxOneControllerInputHandler());

            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

        }

        private void ReloadGame()
        {
            LoadEngine();
            PreLoadInstructionScreen();
            LoadGame();
        }

        private void LoadGame()
        {
            //create world
            World farseerWorld = new World(new Vector2(0f, WorldGravity));
            ConvertUnits.SetDisplayUnitToSimUnitRatio(PixelsPerMeter);
            
            this.World = new PhysicalWorld(world: farseerWorld);
            this.World.EndGameDelegate = this;
            this.World.DistanceDelegate = CameraController;

            SoundManager.MusicRegionDelegate = World;
            InputController.ChooseCharacterDelegate = World;
            CameraController.SetWorld(World);

            //load map
            //Map = new TMXImporter(contentRoot: ContentRootDirectory, levelDirectory: "levels/level2/").ReadMap(filename: "level2.tmx");
            Map = new TMXImporter(contentRoot: ContentRootDirectory, levelDirectory: "levels/level1/").ReadMap(filename: "level1.tmx");
            Map.LoadContent(contentManager: Content, world: World);

           
            //Create the renderer
            InstructionHandler.Instance.LoadContent(contentManager: Content, cameraController: CameraController);

            this.renderer = new Renderer(   spriteBatch: spriteBatch,
                                            graphicsDevice: GraphicsDevice,
                                            cameraController: CameraController);
            this.renderer.SetParallaxOrigin(World.ParallaxOrigin);
            this.renderer.SetMap(Map);
            this.renderer.SetCharacterPositionDelegate(this.CameraController);
            this.renderer.SetTextRenderLayer(InstructionHandler.Instance.GetTextRenderLayer());

            this.renderer.LoadContent(contentManager: Content);

            //main character
            var mainCharacter = new MainCharacter(contentManager: Content,
                                                       world: World,
                                                       worldHeight: mainCharacterHeigth,
                                                       bodyType: BodyType.Dynamic,
                                                       friction: 2f,
                                                       restitution: 0f,
                                                       objectDensity: 5f);
            mainCharacter.LoadContent(initialPosition: World.GetSpawnPointOnGroundOrCollision(),
                                        initialFacingDirection: World.SpawnFacingDirection,
                                        userData: Constants.UserDataMainCharacter);

            mainCharacter.OnJump += SoundManager.OnJump;
            mainCharacter.OnLanding += SoundManager.OnLanding;
            mainCharacter.OnStep += SoundManager.OnStep;
            mainCharacter.OnDeath += SoundManager.OnDeath;
            
            renderer.AddCharacterToCharacterLayer(character: mainCharacter);

            World.SetMainCharacter(mainCharacter);

            //create ReplayEngine
            replayEngine = new ReplayEngine(renderer: renderer, content: Content, world: World);
            replayEngine.OnGhostAppears += SoundManager.OnGhostAppears;
            replayEngine.OnGhostDisappears += SoundManager.OnGhostDisappears;
            //replayEngine.OnGhostAppears += SoundManager.OnStartRecording;
            //replayEngine.OnGhostDisappears += SoundManager.OnStopRecording;
            replayEngine.OnStartRecording += SoundManager.OnStartRecording;
            replayEngine.OnEndRecording += SoundManager.OnStopRecording;

            replayEngine.OnStartRecording += renderer.OnRecordingStarted;
            replayEngine.OnEndRecording += renderer.OnRecordingStoped;

            World.SetCharacterResetDelegate(characterResetDelegate: replayEngine);
            CameraController.OnGhostOutsideCamera += replayEngine.OnGhostOutsideCamera;
            
            SoundManager.ReplayDelegate = replayEngine;
            InputController.CharacterResetDelegate = replayEngine;

            //Movement Controller
            var recordingMovementController = new RecordingMovementController(controlledTarget: mainCharacter, replayEngine: replayEngine);
            replayEngine.SetControlledCharacterDelegate(controlledCharacterDelegate: recordingMovementController);
            InputController.SetMovementController(recordingMovementController);

            World.Init();

            //Show first instruction
            InstructionHandler.Instance.ShowText(text: InstructionConstants.Text.Move, forMilliSeconds: InstructionConstants.Duration.Move);

            //preload outro
            LoadOutro();

            CurrentGameState = GameState.Running;
        }

        private void OnWindowSizeChanged()
        {
            var size = this.spriteBatch.GraphicsDevice.Viewport.Bounds;
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            SoundManager.Reset();
            spriteBatch.Dispose();
            Map.Dispose();
            renderer.Dispose();
            Content.Dispose();
            SwitchEvent.Dispose();
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected async override void Update(GameTime gameTime)
        {
            //Task physicsTask = null;
            switch(CurrentGameState)
            {
                case GameState.Running:
                    World?.Update(gameTime: gameTime);
                    CameraController.Update();
                    SoundManager.Update(gameTime: gameTime);
                    break;
                case GameState.Outro:
                    Outro.Update(gameTime: gameTime);
                    World?.Update(gameTime: gameTime);
                    //SoundManager.Update(gameTime: gameTime);
                    break;
            }
            // We update the world
            InputController.HandleInput(gameTime: gameTime);
            


            if (replayEngine?.ReplayMode == ReplayMode.Replaying)
            {
                replayEngine?.Replay(gameTime: gameTime);
            }
            

            if (gameTime.ElapsedGameTime.TotalMilliseconds > Constants.FrameWarningLimit)
            {
                Debug.WriteLine("Game lagging. " + gameTime.ElapsedGameTime.TotalMilliseconds + "ms per Update.");
            }

            //World.Update(gameTime);
            
            //if (physicsTask != null)
            //{
            //    await physicsTask;
            //}
            base.Update(gameTime);

        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            if (gameTime.ElapsedGameTime.TotalMilliseconds > Constants.FrameWarningLimit)
            {
                Debug.WriteLine("Game lagging. " + gameTime.ElapsedGameTime.TotalMilliseconds + "ms per Draw.");
            }

            if(InstructionScreen != null && InstructionScreen.Visible)
            {
                InstructionScreen.Draw(spritebatch: spriteBatch, gameTime: gameTime);
                return;
            }

            switch (CurrentGameState)
            {
                case GameState.Running:
                    //Tell the renderer to draw everything
                    renderer?.Draw(gameTime: gameTime);
                    break;

                case GameState.Start:
                    StartScreen.Draw(spritebatch: spriteBatch, gameTime: gameTime);
                    break;
                case GameState.Outro:
                    renderer?.Draw(gameTime: gameTime);
                    Outro.DrawText(spriteBatch: spriteBatch, gameTime: gameTime);
                    break;
                case GameState.End:
                    EndScreen.Draw(spritebatch: spriteBatch, gameTime: gameTime);
                    break;
            }

        }
        
        

        public void ReloadWorld()
        {
            //TODO: fix unloading content. The tilesets are not drawn after reload for some reason.
            UnloadContent();
            ReloadGame();
        }

        private void LoadOutro()
        {
            
            this.Outro = new Outro(world: World, cameraController: CameraController, contentManager: Content);
            this.renderer.AddRenderObjectToActionLayer(Outro);
        }
        public void EndGame()
        {
            
            //stop player 
            InputController.BlockAllKeys();
            replayEngine.AbortRecording();
            replayEngine.SelectMainCharacter();
            Action setToOutro = delegate () { CurrentGameState = GameState.Outro; };
            Outro.StartOutro(setStateToOutro: setToOutro);

            //World.MainCharacter.SetWalkAnimationDuration(AnimationConstants.Duration.Run *1.5f );

            //OutroReplayMovementController = new ReplayMovementController(controlledTarget: World.MainCharacter);
            //OutroReplayMovementController.SetMaxVelocity(2f);
            //OutroReplayMovementController.Stop(new GameTime());
            //OutroGoalPosition = World.MainCharacter.Position + new Vector2(5.5f, 0);

            //SoundManager.Instance.SetSoundtrack(track: SoundManager.SoundFile.Outro, trackFiltered: null, trackGhost: null, volume: Configuration.Sound.OutroVolume);
            //OutroStartMovingTimer = new Timer(callback: (object state) => { OutroMove = true;}, state: null, dueTime: 2000, period: Timeout.Infinite);
            //StartOutroTimer = new Timer(callback: (object state) => { CurrentGameState = GameState.Outro; }, state: null, dueTime: 1500, period: Timeout.Infinite);

            //SoundManager.Instance.PlaySound(file: SoundManager.SoundFile.Outro, id: "outre", initialVolume: Configuration.Sound.OutroVolume);
            //TimerCallback end = delegate (object state)
            //{
            //    this.LoadEndScreen();
            //    this.CurrentGameState = GameState.End;
            //};
            //ShowEndScreenDelayTimer = new Timer(callback: end, state: null, dueTime: StaticScreenConstants.Duration.ShowEndScreenDelay, period: Timeout.Infinite);

        }
    }

    public interface ReloadGameDelegate
    {
        void ReloadWorld();
    }

    public interface CharacterResetDelegate
    {
        void CharacterDied(bool isMainCharacter);
        void RespawnAtLastSpirit();
        void SelectMainCharacter();
    }
    public interface EndGameDelegate
    {
        void EndGame();
    }
    public interface ChangeScreenDelegate
    {
        void ShowInstructionScreen();
    }
}