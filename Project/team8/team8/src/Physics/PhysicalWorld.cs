﻿/**
* Copyright 2017 Niclas Scheuing, Per Näslung, Tunay Bora, Xiaowei Zeng
* This file is part of REWIND.
*
* REWIND is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* REWIND is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
* GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with REWIND.If not, see<http://www.gnu.org/licenses/>.
*/

using FarseerPhysics.Dynamics;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using team8.Level;
using static team8.Physics.PhysicsObject;
using FarseerPhysics;
using team8.Control;
using team8.Sound;

namespace team8.Physics
{
    using Coordinate = Vector2;
    public class PhysicalWorld : ChooseCharacterDelegate, MusicRegionDelegate
    {

        public readonly World World;
        private List<PhysicsLevelObject> GroundObjects;
        private List<PhysicsLevelObject> WallObjects;
        private List<PhysicsLevelObject> OtherObjects;
        private List<RenderPhysicsGroup> RenderGroups;
        private List<SpiritLevelObject> SpiritObjects;
        private List<JointGroup> JointGroups;
        private List<MusicRegionLevelObject> MusicRegionObjects;
        private List<BackgroundRegionLevelObject> BackgroundRegionObjects;
        private List<VisualLevelObject> VisualLevelObjects;
        private List<SwitchLevelObject> SwitchLevelObjects;
        //private List<PhysicsLevelObject> PhysicsObjects;

        private MainCharacter mainCharacter;
        private MainCharacter ghostCharacter;
        public MainCharacter MainCharacter { get { return this.mainCharacter; } }
        public MainCharacter GhostCharacter { get { return this.ghostCharacter; } }
        public Coordinate MainCharacterPositionInDisplay
        {
            get
            {
                return ConvertUnits.ToDisplayUnits(this.MainCharacter.Position);
            }
        }
        public Coordinate? GhostCharacterPositionInDisplay
        {
            get
            {
                if (ghostCharacter == null)
                {
                    return null;
                }
                else
                {
                    return ConvertUnits.ToDisplayUnits(this.GhostCharacter.Position);
                }
            }
        }

        //Spawn Point in SIM units
        //Use GetSpawnPointOnGround instead of attribute
        private Coordinate SpawnPoint;
        public Direction SpawnFacingDirection = Direction.Right;

        public Coordinate ParallaxOrigin;

        private SpiritLevelObject LastCollectedSpiritObject;

        private readonly string SpiritSaveId = "spirit";
        private readonly string ReplaySaveId = "replay";
        private readonly string InitialSaveId = "init";

        //Ground properties
        private readonly float GroundFriction = 2f;
        private readonly float GroundRestitution = 0;
        //Wall properties
        private readonly float WallFriction = 0;
        private readonly float WallRestitution = 0;

        private readonly float CheckInScreenScale = 10f;

        private CharacterResetDelegate CharacterResetDelegate;
        public EndGameDelegate EndGameDelegate;
        public DistanceDelegate DistanceDelegate;

        public delegate void UpdateDelegate(GameTime gameTime);
        public UpdateDelegate OnUpdate;
        public Action OnRespawn;

        //private double EllapsedTime = 0;
        //private double MinStepInMs = 15;

        private bool Paused = false;

        public PhysicalWorld(World world)
        {
            this.GroundObjects = new List<PhysicsLevelObject>();
            this.WallObjects = new List<PhysicsLevelObject>();
            this.OtherObjects = new List<PhysicsLevelObject>();
            this.RenderGroups = new List<RenderPhysicsGroup>();
            this.JointGroups = new List<JointGroup>();
            this.SpiritObjects = new List<SpiritLevelObject>();
            this.MusicRegionObjects = new List<MusicRegionLevelObject>();
            this.BackgroundRegionObjects = new List<BackgroundRegionLevelObject>();
            VisualLevelObjects = new List<VisualLevelObject>();
            this.SwitchLevelObjects = new List<SwitchLevelObject>();
            //this.PhysicsObjects = new List<PhysicsLevelObject>();
            this.World = world;
            this.ParallaxOrigin = new Coordinate(0, 0);
            //CreateUpdateWorldTask();
        }

        public void SetCharacterResetDelegate(CharacterResetDelegate characterResetDelegate)
        {
            this.CharacterResetDelegate = characterResetDelegate;
        }

        public void Init()
        {
            SaveObjectState(id: InitialSaveId);

        }

        public RenderPhysicsGroup CreateAndAddRenderGroup(string renderGroupId)
        {
            var renderGroup = new RenderPhysicsGroup(renderGroupId: renderGroupId);
            this.RenderGroups.Add(renderGroup);
            return renderGroup;
        }

        public JointGroup CreateAndAddJointGroup(string jointGroupId)
        {
            var jointGroup = new JointGroup(jointId: jointGroupId);
            this.JointGroups.Add(jointGroup);
            return jointGroup;
        }


        public Body CreateAndAddGroundObject(PhysicsLevelObject obj, Body body)
        {
            body.Friction = GroundFriction;
            body.Restitution = GroundRestitution;
            body.CollisionCategories = Constants.CategoryGround;
            body.BodyType = BodyType.Static;
            //Set the UserData for all fixtures
            foreach (var fixture in body.FixtureList)
            {
                fixture.UserData = Constants.UserDataGround; ;
            }
            this.GroundObjects.Add(obj);
            return body;
        }

        public Body CreateAndAddWallObject(PhysicsLevelObject obj, Body body)
        {
            body.Friction = WallFriction;
            body.Restitution = WallRestitution;
            body.CollisionCategories = Constants.CategoryWall;
            body.BodyType = BodyType.Static;
            //Set the UserData for all fixtures
            foreach (var fixture in body.FixtureList)
            {
                fixture.UserData = Constants.UserDataWall; ;
            }
            this.WallObjects.Add(obj);
            return body;
        }

        public Body CreateAndAddCollisionObject(PhysicsLevelObject obj, Body body, float? Friction = null, float? Restitution = null)
        {
            body.Friction = Friction.GetValueOrDefault(GroundFriction);
            body.Restitution = Restitution.GetValueOrDefault(GroundRestitution);
            body.CollisionCategories = Constants.CategoryCollision;
            body.CollidesWith = Constants.CategoryCollision | Constants.CategoryMainCharacter | Constants.CategoryWall | Constants.CategoryGround;// | Constants.CategorySwitch;

            //Set the UserData for all fixtures
            foreach (var fixture in body.FixtureList)
            {
                fixture.UserData = Constants.UserDataCollision;
            }
            this.OtherObjects.Add(obj);
            return body;
        }

        public Body CreateAndAddOtherObject(PhysicsLevelObject obj, Body body, float Friction = 0, float Restitution = 0f)
        {
            body.Friction = Friction;
            body.Restitution = Restitution;
            body.CollisionCategories = Constants.CategoryOthers;

            //Set the UserData for all fixtures
            //foreach (var fixture in body.FixtureList)
            //{
            //fixture.UserData = Constants.; ;
            //}
            this.OtherObjects.Add(obj);
            return body;
        }



        public void RemoveGround(PhysicsLevelObject ground)
        {
            this.GroundObjects.Remove(ground);
            this.World.RemoveBody(ground.Body);
        }
        public void RemoveOther(PhysicsLevelObject other)
        {
            this.OtherObjects.Remove(other);
            this.World.RemoveBody(other.Body);
        }
        public void SetMainCharacter(MainCharacter newMainCharacter)
        {
            if (ghostCharacter != null && ghostCharacter == newMainCharacter)
            {
                var message = "Tried adding the Ghost as MainCharacter. This is not allowed";
                Debug.WriteLine(message);
                throw new Exception(message);
            }
            if (newMainCharacter.IsGhost)
            {
                var message = "Tried adding a Character with IsGhost==true as MainCharacter. This is not allowed";
                Debug.WriteLine(message);
                throw new Exception(message);
            }
            this.mainCharacter = newMainCharacter;
        }
        public void SetGhostCharacter(MainCharacter newGhostCharacter)
        {
            if (mainCharacter != null && newGhostCharacter == this.mainCharacter)
            {
                var message = "Tried adding MainCharacter as Ghost. This is not allowed";
                Debug.WriteLine(message);
                throw new Exception(message);
            }
            if (!newGhostCharacter.IsGhost)
            {
                var message = "Tried adding a Character with IsGhost==false as Ghost. This is not allowed";
                Debug.WriteLine(message);
                throw new Exception(message);
            }

            this.ghostCharacter = newGhostCharacter;
        }
        public void RemoveGhostCharacter()
        {
            this.ghostCharacter.Body.Dispose();
            //this.World.RemoveBody(this.ghostCharacter.Body);
            this.ghostCharacter = null;
        }
        private void RemoveMainCharacter()
        {
            //this.World.RemoveBody(this.mainCharacter.Body);
            this.mainCharacter.Body.Dispose();
            this.mainCharacter = null;
        }
        public void UnghostGhostCharacter()
        {
            RemoveMainCharacter();
            var newGhost = this.ghostCharacter;
            this.ghostCharacter = null;
            this.SetMainCharacter(newGhost);
        }

        public void SetSpawnPoint(Coordinate spawnPoint, Direction facingDirection)
        {
            this.SpawnPoint = spawnPoint;
            this.SpawnFacingDirection = facingDirection;
        }

        public void AddSpiritObject(SpiritLevelObject spiritObject)
        {
            this.SpiritObjects.Add(spiritObject);
        }

        public void SetLastSpiritObject(SpiritLevelObject spiritObject)
        {
            this.LastCollectedSpiritObject = spiritObject;
            SaveObjectState(id: SpiritSaveId);
        }

        

        //in SIM units
        public Coordinate GetSpawnPointOnGroundOrCollision()
        {
            return GetPointOnGroundOrCollision(point: this.SpawnPoint);
        }

        public Coordinate GetPointOnGroundOrCollision(Coordinate point)
        {
            //Find ground below point
            Intersection? groundOrCollisionIntersection = ElementBelowPosition(position: point, distance: 100f, ElementUserData: new List<string>() { Constants.UserDataCollision, Constants.UserDataGround });
            //if found, use it. If not, use raw point
            if (groundOrCollisionIntersection.HasValue)
            {
                return groundOrCollisionIntersection.Value.IntersectionPoint + Constants.SaveDistanceAboveGround;
            }
            else
            {
                return point;
            }
        }

        public void SetParallaxOrigin(Coordinate parallaxOrigin)
        {
            this.ParallaxOrigin = parallaxOrigin;
        }

        //public void AddEndLevelObject(EndLevelObject endLevelObject)
        //{
        //    this.EndLevelObjects.Add(endLevelObject);
        //}

        public void EndLevel()
        {
            EndGameDelegate.EndGame();
        }

        /*-----MUSIC REGIONS-----*/
        public void AddMusicRegionObject(MusicRegionLevelObject musicRegionObject)
        {
            this.MusicRegionObjects.Add(musicRegionObject);
        }

        public string MusicRegionObjectIdForMainCharacter()
        {
            foreach (var musicRegion in MusicRegionObjects)
            {
                if (musicRegion.PointWithinRegion(pointInDisplay: mainCharacter.PositionInDisplay))
                {
                    return musicRegion.MusicRegionId;
                }
            }
            return null;
        }

        /*-----BACKGROUND REGIONS-----*/
        public void AddBackgroundRegionObject(BackgroundRegionLevelObject backgroundRegion)
        {
            this.BackgroundRegionObjects.Add(backgroundRegion);
        }

        public void UpdateBackground()
        {
            foreach (var backgroundRegion in BackgroundRegionObjects)
            {
                var isVisible = backgroundRegion.PointWithinRegion(pointInDisplay: mainCharacter.PositionInDisplay);
                var background = this.GetVisualLevelObject(id: backgroundRegion.BackgroundRegionId);
                background.IsHidden = !isVisible;
            }
        }

        /*-----QUERIES-----*/
        public PhysicsLevelObject GetOtherObject(string id)
        {
            return this.OtherObjects.Find(obj => obj.Id == id);
        }
        public PhysicsLevelObject GetGroundObject(string id)
        {
            return this.GroundObjects.Find(obj => obj.Id == id);
        }

        public RenderPhysicsGroup GetRenderPhysicsGroup(string renderGroupId)
        {
            var renderGroup = this.RenderGroups.Find(obj => obj.RenderGroupId == renderGroupId);
            return renderGroup;
        }

        public JointGroup GetJointGroup(string jointGroupId)
        {
            var jointGroup = this.JointGroups.Find(obj => obj.JointId == jointGroupId);
            return jointGroup;
        }

        public PhysicsLevelObject GetWallObject(string id)
        {
            return this.WallObjects.Find(obj => obj.Id == id);
        }

        public void AddVisualLevelObject(VisualLevelObject visualObject)
        {
            this.VisualLevelObjects.Add(visualObject);
        }
        public VisualLevelObject GetVisualLevelObject(string id)
        {
            return this.VisualLevelObjects.Find(obj => obj.Id == id);
        }

        public void AddSwitchObject(SwitchLevelObject switchObject)
        {
            this.SwitchLevelObjects.Add(switchObject);
        }

        public SwitchLevelObject GetSwitchObject(string id)
        {
            return this.SwitchLevelObjects.Find(obj => obj.Id == id);
        }

       //public void AddPhysicsObject(PhysicsLevelObject physicsObject)
       // {
       //     this.PhysicsObjects.Add(physicsObject);
       // }

        /*-----Save and Load world state-----*/
        private void SaveObjectState(string id)
        {
            var allObjects = this.OtherObjects;
            foreach (var obj in allObjects)
            {
                obj.SaveState(id: id);
            }
            var allJoints = this.JointGroups;
            foreach (var joints in allJoints)
            {
                joints.JointObject.SaveState(id: id);
            }

            var allWalls = this.WallObjects;
            foreach (var wall in allWalls)
            {
                wall.SaveState(id: id);
            }
        }

        private void LoadLastObjectState(string id)
        {
            var allObjects = this.OtherObjects;
            foreach (var obj in allObjects)
            {
                obj.LoadLastState(id: id);
            }
            var allJoints = this.JointGroups;
            foreach (var joints in allJoints)
            {
                joints.JointObject.LoadLastState(id: id);
            }
            var allWalls = this.WallObjects;
            foreach (var wall in allWalls)
            {
                wall.LoadLastState(id: id);
            }
            foreach (var switchObject in SwitchLevelObjects)
            {
                switchObject.Reset();
            }
        }

        /*-----RESPAWN AND DEATH-----*/
        public void RespawnAtLastSpirit()
        {
            OnRespawn?.Invoke();
            EndChoosing();

            Vector2 projectedRespawnPoint;
            Direction facingDirection;
            if (LastCollectedSpiritObject == null)
            {
                LoadLastObjectState(id: InitialSaveId);
                projectedRespawnPoint = GetPointOnGroundOrCollision(this.SpawnPoint);
                facingDirection = SpawnFacingDirection;
            }
            else
            {
                LoadLastObjectState(id: SpiritSaveId);
                projectedRespawnPoint = GetPointOnGroundOrCollision(LastCollectedSpiritObject.SpawnPosition);
                facingDirection = LastCollectedSpiritObject.FacingDirection;
            }
            //MainCharacter.SetToPosition(projectedRespawnPoint);
            MainCharacter.SetSpawnPoint(position: projectedRespawnPoint, facingDirection: facingDirection);

        }

        public void CharacterDied(bool isMainCharacter)
        {
            //If ghost is dying and main character is still alive, don't do anything
            if (!isMainCharacter && !MainCharacter.Dead)
            {
                CharacterResetDelegate.CharacterDied(isMainCharacter: isMainCharacter);
            }

            //if only the main character die, unghost and continue
            else if (isMainCharacter && (GhostCharacter != null && !GhostCharacter.Dead))
            {
                CharacterResetDelegate.CharacterDied(isMainCharacter: isMainCharacter);
            }
            //Else, respawn
            else
            {
                CharacterResetDelegate.CharacterDied(isMainCharacter: isMainCharacter);
                RespawnAtLastSpirit();
                MainCharacter.Revive();
            }

            EndChoosing();
        }

        /*-----Replay-----*/
        public void StartedRecording()
        {
            this.SaveObjectState(id: ReplaySaveId);
        }
        //public void StoppedRecording()
        //{
        //    TODO: recording animation
        //    this.mainCharacter.ShowRecordingAnimation = false;
        //}
        public void StartedReplaying()
        {
            this.LoadLastObjectState(id: ReplaySaveId);
        }
        public void StopReplaying()
        {
            if (ghostCharacter != null && NoCharacterMarked())
            {
                this.ghostCharacter.ShowSelectionAnimation = true;
            }
        }
        /*-----PAUSE-----*/
        public void Pause()
        {
            this.Paused = true;
        }
        public void Resume()
        {
            this.Paused = false;
        }
        public void Update(GameTime gameTime)
        {
            //EllapsedTime = EllapsedTime + gameTime.ElapsedGameTime.TotalMilliseconds;
            //if (EllapsedTime > MinStepInMs)
            //{
            //UpdatePhysicsObjects();
            var timeStep = Paused ? 0 : (float)gameTime.ElapsedGameTime.TotalMilliseconds * 0.001f;
                //await UpdateWorld(timeStep: timeStep);
                World.Step(timeStep);
                //PhysicsThread.Start(timeStep);
                //Action action = delegate() { World.Step(timeStep); };
                //var thread = Task.Factory.StartNew(action);
                //PhysicsThread.
                UpdateBackground();
                OnUpdate?.Invoke(gameTime: gameTime);
                //PhysicsThread.Join();
                //return thread;
                //EllapsedTime = 0;
            //}
        }
       
        //private void UpdatePhysicsObjects()
        //{
        //    foreach (var physicsObject in PhysicsObjects)
        //    {
        //        physicsObject.SetEnabled(DistanceDelegate.IsVisible(physicsObject.DisplayPosition(), scaleBoundingRectangel: CheckInScreenScale));
        //    }
        //    foreach(var joint in JointGroups)
        //    {
        //        joint.JointObject.SetEnabled(DistanceDelegate.IsVisible(pointInDisplay: joint.JointObject.DisplayPosition(), scaleBoundingRectangel: CheckInScreenScale));
        //    }
        //}



        /*----CHOOSE CHARACTER----*/
        public void StartChoosing()
        {
            //
        }

        public void EndChoosing()
        {
            mainCharacter.ShowSelectionAnimation = false;
            if (ghostCharacter != null)
            {
                ghostCharacter.ShowSelectionAnimation = false;
            }
        }

        //public void MarkCharacter(bool right)
        //{
        //    var rightCharacter = MainCharacterPositionInDisplay.X > GhostCharacterPositionInDisplay.Value.X ? MainCharacter: GhostCharacter;
        //    var leftCharacter = MainCharacterPositionInDisplay.X > GhostCharacterPositionInDisplay.Value.X ? GhostCharacter: MainCharacter;
        //    rightCharacter.ShowSelectionAnimation = right;
        //    leftCharacter.ShowSelectionAnimation = !right;
        //}
        private bool NoCharacterMarked()
        {
            return !mainCharacter.ShowSelectionAnimation && !ghostCharacter.ShowSelectionAnimation;
        }
        private bool BothCharacterMarked()
        {
            return mainCharacter.ShowSelectionAnimation && ghostCharacter.ShowSelectionAnimation;
        }

        public void MarkOtherCharacter()
        {
            //If both or none is marked, mark one
            if (BothCharacterMarked ()|| NoCharacterMarked())
            {
                ghostCharacter.ShowSelectionAnimation = true;
                mainCharacter.ShowSelectionAnimation = false;
            }
            else
            {
                mainCharacter.ShowSelectionAnimation = !mainCharacter.ShowSelectionAnimation;
                ghostCharacter.ShowSelectionAnimation = !ghostCharacter.ShowSelectionAnimation;
            }
        }

        //public bool IsRightGhost()
        //{
        //    return GhostCharacterPositionInDisplay.Value.X > MainCharacterPositionInDisplay.X;
        //}

        //public bool IsRightMarked()
        //{
        //    return IsRightGhost() ? GhostCharacter.ShowSelectionAnimation : mainCharacter.ShowSelectionAnimation;
        //}

        public bool ReadyToSelect()
        {
            return GhostCharacter.ShowSelectionAnimation || mainCharacter.ShowSelectionAnimation;
        }
        public bool IsGhostMarked()
        {
            return this.GhostCharacter.ShowSelectionAnimation;
        }

        /*-----INTERSECTION TEST-----*/
        public Intersection? GroundBelowPosition(Coordinate position, float distance, Vector2? direction = null)
        {
            return ElementBelowPosition(position: position, distance: distance, ElementUserData: Constants.UserDataGround, direction: direction);
        }
        public Intersection? GroundOrCollisionBelowPosition(Coordinate position, float distance, Vector2? direction = null)
        {
            return ElementBelowPosition(position: position, distance: distance, ElementUserData: new List<string>() { Constants.UserDataGround, Constants.UserDataCollision }, direction: direction);
        }
        public Intersection? WallBelowPosition(Coordinate position, float distance, Vector2? direction = null)
        {
            return ElementBelowPosition(position: position, distance: distance, ElementUserData: Constants.UserDataWall, direction: direction);
        }

        private Intersection? ElementBelowPosition(Coordinate position, float distance, string ElementUserData, Vector2? direction = null)
        {
            return ElementBelowPosition(position: position, distance: distance, ElementUserData: new List<string>() { ElementUserData }, direction: direction);
        }

        private Intersection? ElementBelowPosition(Coordinate position, float distance, List<string> ElementUserData, Vector2? direction = null)
        {
            Intersection? nearestGroundIntersection = null;
            Func<Fixture, Vector2, Vector2, float, float> rayCastHandler = delegate (Fixture fixture, Coordinate intersectionPoint, Vector2 intersectionNormal, float fraction)
            {
                if (ElementUserData.Contains((string)fixture.UserData) && (!nearestGroundIntersection.HasValue || fraction < nearestGroundIntersection.Value.Fraction))
                {
                    nearestGroundIntersection = new Intersection(fixture: fixture, intersectionPoint: intersectionPoint, intersectionNormal: intersectionNormal, fraction: fraction);
                    return 1;
                }
                else
                {
                    return 1;
                }
            };
            var dir = direction.GetValueOrDefault(new Coordinate(0, -1));
            //reset nearest ground intersection
            //shoot a ray downwards to detect ground
            var point2 = position - distance * dir;
            var point1 = position;
            World.RayCast(callback: rayCastHandler, point1: point1, point2: point2);
            //List<Fixture> collisionFixtures = world.World.RayCast(point1: Position + new Coordinate(0, -1), point2: Position);

            return nearestGroundIntersection;
        }

        //public void Update(GameTime gameTime)
        //{
        //    if(mainCharacter != null)
        //    {
        //        mainCharacter.Update(gameTime);
        //    }
        //    if (ghostCharacter != null)
        //    {
        //        ghostCharacter.Update(gameTime);
        //    }
        //}
    }
    

    /*-----INTERSECTION TEST-----*/
    public struct Intersection
    {
        public Fixture Fixture;
        public Coordinate IntersectionPoint;
        public Vector2 IntersectionNormal;
        public float Fraction;
        public Intersection(Fixture fixture, Coordinate intersectionPoint, Vector2 intersectionNormal, float fraction)
        {
            this.Fixture = fixture;
            this.IntersectionNormal = intersectionNormal;
            this.IntersectionPoint = intersectionPoint;
            this.Fraction = fraction;
        }
    }


}
