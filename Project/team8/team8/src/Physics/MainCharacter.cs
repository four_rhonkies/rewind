﻿/**
* Copyright 2017 Niclas Scheuing, Per Näslung, Tunay Bora, Xiaowei Zeng
* This file is part of REWIND.
*
* REWIND is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* REWIND is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
* GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with REWIND.If not, see<http://www.gnu.org/licenses/>.
*/
using System;
using System.Linq;
using FarseerPhysics.Dynamics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using team8.Rendering;
using System.Diagnostics;
using FarseerPhysics;
using Microsoft.Xna.Framework;
using MonoGame.Extended.Sprites;
using MonoGame.Extended.TextureAtlases;
using team8.Animation;
using FarseerPhysics.Dynamics.Contacts;
using FarseerPhysics.Common;

namespace team8.Physics
{
    using Coordinate = Vector2;
    public class MainCharacter : MovableObject, RenderObject
    {
       
        private enum State
        {
            Running, Jumping, Idling
        }
        //If the object is ghost, use the ghost texture, else use the normal texture

        //public bool ShowRecordingAnimation;
        public bool ShowSelectionAnimation = false;
        public bool ShowLight = false;
        private bool isGhost;
        public bool IsGhost { get { return this.isGhost; } }
        /* fields used for cloning the object */
        private ContentManager contentManager;
        private float worldHeight;
        private float objectDensity;
        private BodyType bodyType;
        private float friction;
        private float restitution;

        private bool Hidden = false;
        public float Alpha = 1f;
        private bool dead = false;
        public bool Dead
        {
            get
            {
                return this.dead;
            }
        }

        //animation
        private AnimationState runAnimation;
        private AnimationState idleAnimation;
        private AnimationState jumpAnimation;
        private AnimationState CurrentAnimation;
        private State CurrentState;
        private Texture2D SelectionIndicationTexture;
        
        private Texture2D LightTexture;

        //Delegates
        public delegate void StepDelegate();
        public delegate void LandingDelegate();
        public delegate void JumpDelegate();
        public delegate void DeathDelegate();
        public StepDelegate OnStep;
        public LandingDelegate OnLanding;
        public JumpDelegate OnJump;
        public DeathDelegate OnDeath;


        private TrailingList Trailing;


        public MainCharacter(ContentManager contentManager, PhysicalWorld world, float worldHeight, BodyType bodyType, float friction, float restitution, float objectDensity = 1) : base(null, world)
        {
            this.dead = false;
            this.worldHeight = worldHeight;
            this.objectDensity = objectDensity;
            this.contentManager = contentManager;
            this.isGhost = false;
            this.bodyType = bodyType;
            this.friction = friction;
            this.restitution = restitution;
            int numberOfTrailingFrames = AnimationConstants.GhostTrailingFrames * AnimationConstants.GhostTrailingDuration /16  + 1;
            this.Trailing = new TrailingList(numberOfFrames: numberOfTrailingFrames);
        }

        public void SetHidden(bool hidden)
        {
            this.Hidden = hidden;
        }
        
        public void SetWalkAnimationDuration(float duration)
        {
            this.runAnimation?.SetFrameDuration(duration);
        }
        public void SetIdleAnimationDuration(float duration)
        {
            this.idleAnimation?.SetFrameDuration(duration);
        }

        public MainCharacter GetGhostObject(Coordinate initalPosition, Direction initialFacingDirection)
        {
            var newObject = new MainCharacter(  contentManager: this.contentManager,
                                                world: this.world,
                                                worldHeight: this.worldHeight,
                                                objectDensity: this.objectDensity,
                                                bodyType: this.bodyType,
                                                friction: this.friction,
                                                restitution: this.restitution);
            newObject.isGhost = true;
            newObject.LoadContent(initialPosition: initalPosition, initialFacingDirection: initialFacingDirection, userData: Constants.UserDataGhost);
            newObject.ShowLight = this.ShowLight;
            CopyDelegates(newObject: newObject);


            return newObject;
        }

        private void CopyDelegates(MainCharacter newObject)
        {
            foreach (var listener in this.OnJump.GetInvocationList())
            {
                newObject.OnJump = (JumpDelegate)listener;
            }
            foreach (var listener in this.OnLanding.GetInvocationList())
            {
                newObject.OnLanding = (LandingDelegate)listener;
            }
            foreach (var listener in this.OnStep.GetInvocationList())
            {
                newObject.OnStep = (StepDelegate)listener;
            }
            foreach (var listener in this.OnDeath.GetInvocationList())
            {
                newObject.OnDeath = (DeathDelegate)listener;
            }
        }

        public void LoadContent(Coordinate initialPosition, Direction initialFacingDirection, string userData)
        {
            base.LoadContent(   contentManager: this.contentManager,
                                initialPosition: initialPosition,
                                worldHeight: this.worldHeight,
                                userData: userData,
                                bodyType: this.bodyType,
                                friction: this.friction,
                                restitution: this.restitution,
                                objectDensity: this.objectDensity);

            this.LastDirection = initialFacingDirection;
            ScaleAnimation();
            LoadLight(contentManager: contentManager);
            SetAnimation(state: State.Idling, direction: RelativeCurrentDirection);
        }

        private void LoadLight(ContentManager contentManager)
        {
            this.LightTexture = contentManager.Load<Texture2D>(AnimationPath.Light);
        }
        public void SetSpawnPoint(Coordinate position, Direction facingDirection)
        {
            this.SetToPosition(position: position);
            this.LastDirection = facingDirection;
        }
        private void ScaleAnimation()
        {
            runAnimation.SetScale(scale: Scale);
            idleAnimation.SetScale(scale: Scale);
            jumpAnimation.SetScale(scale: Scale);
        }

        override protected (int,int) LoadTexture(ContentManager contentManager)
        {
            var (width, height) = CreateAnimations();
            return (width, height);
        }

        public void UnghostObject()
        {
            this.isGhost = false;
            world.UnghostGhostCharacter();
            CreateAnimations();
            ScaleAnimation();
            SetBodyFixtureUserData(userData: Constants.UserDataMainCharacter);
            

            //LoadContent(body.Position);
        }

        override public void Dispose()
        {
            runAnimation.Dispose();
            jumpAnimation.Dispose();
            idleAnimation.Dispose();
        }

        public override void Draw(SpriteBatch spriteBatch, GameTime gameTime, Matrix transform, Action beginAgain)
        {
            if (Hidden)
            {
                return;
            }
            base.Draw(spriteBatch, gameTime, transform: transform, beginAgain: beginAgain);

            var positionInDisplay = ConvertUnits.ToDisplayUnits(body.Position);

            if (ShowLight)
            {
                DrawLight(spriteBatch: spriteBatch, gameTime: gameTime);
            }

            CurrentAnimation.Update(gameTime: gameTime, position: positionInDisplay);
            CurrentAnimation.Sprite.Alpha = Alpha;
            spriteBatch.Draw( CurrentAnimation.Sprite);
            if (isGhost)
            {
                Trailing.Update(totalGameTime: gameTime.TotalGameTime, state: CurrentState, direction: CurrentDirection, isOnGround: IsOnGround,frameIndex: CurrentAnimation.CurrentFrameIndex, position: positionInDisplay);
                DrawTrailing(spriteBatch: spriteBatch, gameTime: gameTime, positionInDisplay: positionInDisplay);
            }
            if (ShowSelectionAnimation)
            {
                DrawSelectionIndicator(spriteBatch: spriteBatch, gameTime: gameTime);
            }
        }

        public void DrawSelectionIndicator(SpriteBatch spriteBatch, GameTime gameTime)
        {
            float alpha = (float)((Math.Sin(gameTime.TotalGameTime.TotalSeconds * AnimationConstants.Duration.SelectionIndicator) + 1) * 0.5 * 0.6+0.25);
            
            var position = ConvertUnits.ToDisplayUnits(Position +this.OverHeadPosition)- new Vector2 (SelectionIndicationTexture.Width/2, SelectionIndicationTexture.Height/2)*AnimationConstants.SelectionIndicatorScale;
            spriteBatch.Draw(texture: SelectionIndicationTexture, position: position, scale: AnimationConstants.SelectionIndicatorScale, color: AnimationConstants.SelectionIndicatorColor * alpha);
            
        }

        private void DrawTrailing(SpriteBatch spriteBatch, GameTime gameTime, Coordinate positionInDisplay)
        {
            Coordinate lastPosition = positionInDisplay;
            for (int i=0; i<AnimationConstants.GhostTrailingFrames; i++)
            {
                TrailingFrame? frame = Trailing.GetFrameForTime(totalGameTime: gameTime.TotalGameTime - new TimeSpan(days: 0, hours: 0, minutes: 0, seconds: 0, milliseconds: AnimationConstants.GhostTrailingDuration*(i+1)));
                if (!frame.HasValue || Math.Abs(frame.Value.Position.X - lastPosition.X) < AnimationConstants.GhostTrailingMinPixelDistance && Math.Abs(frame.Value.Position.Y - lastPosition.Y) < AnimationConstants.GhostTrailingMinPixelDistance)
                {
                    continue;
                }
                var state = frame.Value.State;
                var animation = AnimationForState(state: frame.Value.State, isOnGround: frame.Value.IsOnGround);
                float alpha = 1 /(float)(i + 2);
                if (animation.Sprite.IsVisible)
                {
                    var textureRegion = animation.Animation.KeyFrames[frame.Value.FrameIndex];
                    var texture = textureRegion.Texture;
                    var sourceRectangle = textureRegion.Bounds;

                    spriteBatch.Draw(texture, frame.Value.Position, sourceRectangle, animation.Sprite.Color * animation.Sprite.Alpha*alpha, animation.Sprite.Rotation,
                        animation.Sprite.Origin,
                        animation.Sprite.Scale,
                        frame.Value.Direction == Direction.Left ? SpriteEffects.FlipHorizontally : SpriteEffects.None,
                        animation.Sprite.Depth);
                }
                lastPosition = frame.Value.Position;
            }

        }
        private void DrawLight(SpriteBatch spriteBatch, GameTime gameTime)
        {
            
            float alpha = (float)((Math.Sin(gameTime.TotalGameTime.TotalSeconds * AnimationConstants.LightBlinkingSpeed) + 1) * 0.5*(AnimationConstants.LightMaxIntensity - AnimationConstants.MinIntensity) + AnimationConstants.MinIntensity);
            var offset = new Vector2(LightTexture.Width / 2, LightTexture.Height / 2);
            var position = ConvertUnits.ToDisplayUnits(body.Position);// - offset * AnimationConstants.LightBlinkingScale;
            spriteBatch.Draw(texture: LightTexture, position: position, origin:offset, scale: AnimationConstants.LightBlinkingScale, color: Color.White* alpha);
        }

        public void Remove(Renderer renderer, bool fromRenderingOnly=false)
        {
            if (!fromRenderingOnly)
            {
                //If the body is not in the world, don't remove it
                if (isGhost && world.GhostCharacter != null)
                {
                    world.RemoveGhostCharacter();
                }
                //else if (!isGhost && world.MainCharacter != null)
                //{
                //    world.RemoveMainCharacter();
                //}
                else
                {
                    Debug.WriteLine("Tried removing a MainCharacter, but failed");
                    return;
                }
            }
            //eventually also remove from renderer
            renderer.RemoveCharacterFromCharacterActionLayer(character: this);
        }

        private (int,int) CreateAnimations()
        {
            if (!isGhost)
            {
                runAnimation = new AnimationState(contentManager: contentManager,
                                                    texturePath: AnimationPath.MainCharacter.Run,
                                                    rowCount: AnimationConstants.FrameCount.Run,
                                                    columnCount: 1,
                                                    frameDuration: AnimationConstants.Duration.Run);
                idleAnimation = new AnimationState(contentManager: contentManager,
                                                    texturePath: AnimationPath.MainCharacter.Idle,
                                                    rowCount: AnimationConstants.FrameCount.Idle,
                                                    columnCount: 1,
                                                    frameDuration: AnimationConstants.Duration.Idle);
                jumpAnimation = new AnimationState(contentManager: contentManager,
                                                    texturePath: AnimationPath.MainCharacter.JumpLoop,
                                                    rowCount: AnimationConstants.FrameCount.JumpLoop,
                                                    columnCount: 1,
                                                    frameDuration: AnimationConstants.Duration.JumpLoop);
            }
            else
            {
                runAnimation = new AnimationState(contentManager: contentManager,
                                                    texturePath: AnimationPath.Ghost.Run,
                                                    rowCount: AnimationConstants.FrameCount.Run,
                                                    columnCount: 1,
                                                    frameDuration: AnimationConstants.Duration.Run);
                idleAnimation = new AnimationState(contentManager: contentManager,
                                                    texturePath: AnimationPath.Ghost.Idle,
                                                    rowCount: AnimationConstants.FrameCount.Idle,
                                                    columnCount: 1,
                                                    frameDuration: AnimationConstants.Duration.Idle);
                jumpAnimation = new AnimationState(contentManager: contentManager,
                                                    texturePath: AnimationPath.Ghost.JumpLoop,
                                                    rowCount: AnimationConstants.FrameCount.JumpLoop,
                                                    columnCount: 1,
                                                    frameDuration: AnimationConstants.Duration.JumpLoop);
            }

            SelectionIndicationTexture = contentManager.Load<Texture2D>(AnimationPath.SelectionIndicator);
            //this.RecordingAnimation = new AnimationState(   contentManager: contentManager,
            //                                                texturePath: AnimationPath.RecordingIndicator,
            //                                                rowCount: AnimationConstants.FrameCount.RecordingIndicator,
            //                                                columnCount: 1,
            //                                                frameDuration: AnimationConstants.Duration.RecordingIndicator);
            //this.SelectionAnimation = new AnimationState(contentManager: contentManager,
            //                                                texturePath: AnimationPath.RecordingIndicator,
            //                                                rowCount: AnimationConstants.FrameCount.RecordingIndicatorRows,
            //                                                columnCount: AnimationConstants.FrameCount.RecordingIndicatorColumns,
            //                                                frameDuration: AnimationConstants.Duration.RecordingIndicator);

            runAnimation.OnFrameChange += OnFrameChange;
            this.CurrentAnimation = idleAnimation;
            return (runAnimation.FrameWidth, runAnimation.FrameHeight);
        }

        public override void Moves(Direction direction)
        {
            SetAnimation(state: State.Running, direction: direction);
            this.LastDirection = direction;
        }
        public override void Jumps()
        {
            OnJump?.Invoke();
            ImpactOnJump();
        }
        public override void Idles()
        {
            SetAnimation(state: State.Idling, direction: LastDirection);
        }
        public override void Lands()
        {
            SetAnimation(state: State.Idling, direction: LastDirection);
            OnLanding?.Invoke();
        }
        public override void TakesOff()
        {
            SetAnimation(state: State.Jumping, direction: LastDirection);
        }

        private void SetAnimation(State state, Direction direction)
        {
            CurrentState = state;
            AnimationState newAnimation = AnimationForState(state: state, isOnGround: IsOnGround);
            

            if (newAnimation != CurrentAnimation)
            {
                CurrentAnimation = newAnimation;
                CurrentAnimation.ResetLastFrameIndex();
            }

            CurrentAnimation.SetDirection(direction: direction);
        }
        private AnimationState AnimationForState(State state, bool isOnGround)
        {
            if (!isOnGround)
            {
                return jumpAnimation;
            }
            else
            {
                switch(state)
                {
                    case State.Running:
                        return runAnimation;
                    case State.Idling:
                        return idleAnimation;
                    case State.Jumping:
                        return jumpAnimation;
                    default:
                        return idleAnimation;
                }
            }

            
        }

        private void OnFrameChange(int frameIndex)
        {
            //if running and a step-on-ground-frame is shown, trigger step
            if (CurrentAnimation == runAnimation && AnimationConstants.RunStepFrameIndices.Contains(frameIndex))
            {
                OnStep?.Invoke();
                ImpactOnStep();
            }
        }

        private void ImpactOnStep()
        {
            ImpactOnGround(PhysicsConstants.MainCharacter.StepImpulse);
            //if (!IsOnGround)
            //{
            //    body.ApplyLinearImpulse(new Vector2(0, PhysicsConstants.MainCharacter.StepImpulse));
            //}
        }
        private void ImpactOnJump()
        {
            ImpactOnGround(PhysicsConstants.MainCharacter.JumpImpulse);
        }
        private void ImpactOnGround(float impulse)
        {
            var groundContacts = this.GroundContacts();// userDataList: new List<string>() { Constants.UserDataGroundSensor});

            foreach (var groundContact in groundContacts)
            {
                Coordinate normal;
                FixedArray2<Coordinate> points;
                groundContact.Item1.GetWorldManifold(normal: out normal, points: out points);
                //if (points[0] != null)
                groundContact.Item2.Body.ApplyLinearImpulse(-impulse * normal, point: points[0]);
                //groundContact.Item2.Body.ApplyForce(StepImpulse);
            }
        }


        public void Dies()
        {
            this.dead = true;
            OnDeath?.Invoke();
        }

        public void Revive()
        {
            this.dead = false;
        }

        public static bool ContactIsToMainCharacter(Contact contact)
        {
            return ContactIsToMainCharacter(fixtureA: contact.FixtureA, fixtureB: contact.FixtureB);
        }
        public static bool ContactIsToMainCharacter(Fixture fixtureA, Fixture fixtureB)
        {
            bool aIsMainCharacter = (string)fixtureA.UserData == Constants.UserDataMainCharacter;
            bool bIsMainCharacter = (string)fixtureB.UserData == Constants.UserDataMainCharacter;
            return aIsMainCharacter || bIsMainCharacter;
        }
        public static bool ContactIsToGhost(Fixture fixtureA, Fixture fixtureB)
        {
            bool aIsMainCharacter = (string)fixtureA.UserData == Constants.UserDataGhost;
            bool bIsMainCharacter = (string)fixtureB.UserData == Constants.UserDataGhost;
            return aIsMainCharacter || bIsMainCharacter;
        }

        private class TrailingList
        {
            private TrailingFrame[] TrailingFrames;
            private int NumberOfFrames;
            private int NextIndex;
            private int LastIndex
            {
                get
                {
                    return (NextIndex == 0? NumberOfFrames: NextIndex) - 1;
                }
            }

            public TrailingList(int numberOfFrames)
            {
                this.NumberOfFrames = numberOfFrames;
                this.TrailingFrames = new TrailingFrame[numberOfFrames];
                this.NextIndex = 0;
                //pre populate the list, so we can reuse the TrailingFrame objects
                for (int i=0; i<NumberOfFrames; i++)
                {
                    TrailingFrames[i] = new TrailingFrame();
                }
            }

            public void Update(TimeSpan totalGameTime, State state, Direction direction, bool isOnGround, int frameIndex, Coordinate position)
            {
                TrailingFrames[NextIndex].State = state;
                TrailingFrames[NextIndex].TotalGameTime = totalGameTime;
                TrailingFrames[NextIndex].FrameIndex = frameIndex;
                TrailingFrames[NextIndex].Position = position;
                TrailingFrames[NextIndex].Direction = direction;
                TrailingFrames[NextIndex].IsOnGround = isOnGround;
                NextIndex = (NextIndex + 1) % NumberOfFrames;
            }

            public TrailingFrame? GetFrameForTime(TimeSpan totalGameTime)
            {
                //int? followingFrameIndex = null;
                int? precedingFrameIndex = null;
                //TimeSpan shortestInFuture = totalGameTime - TrailingFrames[LastIndex].TotalGameTime;
                TimeSpan shortestInPast = totalGameTime - TrailingFrames[NextIndex].TotalGameTime;
                for (int i=0;i<NumberOfFrames; i++)
                {
                    var difference = totalGameTime - TrailingFrames[i].TotalGameTime;
                    //if (difference < TimeSpan.Zero && difference > shortestInFuture)
                    //{
                    //    shortestInFuture = difference;
                    //    followingFrameIndex = i;
                    //}
                    if (difference > TimeSpan.Zero && difference < shortestInPast)
                    {
                        shortestInPast = difference;
                        precedingFrameIndex = i;
                    }
                }
                if (/*!followingFrameIndex.HasValue ||*/ !precedingFrameIndex.HasValue)
                {
                    return null;
                }

                //var position = TrailingFrames[precedingFrameIndex.Value].Position;// + TrailingFrames[followingFrameIndex.Value].Position) / 2f;
                //var state = TrailingFrames[precedingFrameIndex.Value].State;
                //var direction = TrailingFrames[precedingFrameIndex.Value].Direction;
                //int frameIndex = TrailingFrames[precedingFrameIndex.Value].FrameIndex;
                //bool isOnGround = TrailingFrames[precedingFrameIndex.Value].IsOnGround;
                //if (TrailingFrames[precedingFrameIndex.Value].State == TrailingFrames[followingFrameIndex.Value].State)
                //{
                //    frameIndex = (TrailingFrames[precedingFrameIndex.Value].FrameIndex + TrailingFrames[followingFrameIndex.Value].FrameIndex) / 2;
                //}
                return TrailingFrames[precedingFrameIndex.Value];
            }
            
        }

        private struct TrailingFrame
        {
            public TimeSpan TotalGameTime;
            public State State;
            public Direction Direction;
            public bool IsOnGround;
            public int FrameIndex;
            public Coordinate Position;
            public TrailingFrame(TimeSpan totalGameTime, State state, Direction direction, int frameIndex, Coordinate position, bool isOnGround)
            {
                this.TotalGameTime = totalGameTime;
                this.State = state;
                this.Direction = direction;
                this.IsOnGround = isOnGround;
                this.FrameIndex = frameIndex;
                this.Position = position;
            }
        }
    }
}
