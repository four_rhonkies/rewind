﻿/**
* Copyright 2017 Niclas Scheuing, Per Näslung, Tunay Bora, Xiaowei Zeng
* This file is part of REWIND.
*
* REWIND is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* REWIND is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
* GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with REWIND.If not, see<http://www.gnu.org/licenses/>.
*/
using FarseerPhysics;
using FarseerPhysics.Dynamics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;

namespace team8.Physics
{
    using Coordinate = Vector2;
    public class PhysicsObject
    {
        //In texture coordinates. The origin is used for correct mapping of the physical body to the rendered texture
        protected Coordinate origin;
        //Contains the displayed image for rendering
        protected Texture2D texture;
        //Where the texture image is stored relative to the Content Manager root
        protected String texturePath;

        protected PhysicalWorld world;

        //Phyisical body for the physics engine
        public Body body;
        public float Scale;


        public Vector2 Position
        {
            get
            {
                return body.Position;
            }
        }
        public Vector2 PositionInDisplay
        {
            get
            {
                return ConvertUnits.ToDisplayUnits(body.Position);
            }
        }
        public Body Body
        {
            get
            {
                return body;
            }
        }

        //Velocity where if youre below you have stopped.
        public readonly double StopThreshold = 0.02;

        public Direction CurrentDirection
        {
            get
            {
                return this.currentDirection(withTolerance: this.StopThreshold);
            }
        }


        public enum Direction
        {
            Left, Right, Stopped
        }

        protected Direction currentDirection(double withTolerance)
        {
            if (body.LinearVelocity.X > withTolerance)
            {
                return Direction.Right;
            }
            else if (body.LinearVelocity.X < -withTolerance)
            {
                return Direction.Left;
            }
            else
            {
                return Direction.Stopped;
            }
        }

        public PhysicsObject(String texturePath, PhysicalWorld world)
        {
            this.texturePath = texturePath;
            this.world = world;
        }

        virtual protected (int,int) LoadTexture(ContentManager contentManager)
        {
            //load texture
            this.texture = contentManager.Load<Texture2D>(this.texturePath);
            return (texture.Width, texture.Height);
        }

        public virtual void Draw(SpriteBatch spriteBatch, GameTime gameTime, Matrix transform, Action beginAgain)
        {
            spriteBatch.Draw(texture: texture,
                                position: ConvertUnits.ToDisplayUnits(body.Position),
                                sourceRectangle: null,
                                color: Color.White,
                                rotation: body.Rotation,
                                scale: Scale,
                                origin: origin,
                                effects: SpriteEffects.None,
                                layerDepth: 0f);
            //var dotSize = 10;
            //var centerDot = new Texture2D(spriteBatch.GraphicsDevice, dotSize, dotSize);
            //var colors = new Color[dotSize * dotSize];
            //for (int i = 0; i < colors.Length; i++) { colors[i] = Color.White; }
            //centerDot.SetData(colors);
            ////spriteBatch.Draw(texture: centerDot, position: ConvertUnits.ToDisplayUnits(body.Position), color: Color.Red);
            //spriteBatch.Draw(texture: centerDot, position: ConvertUnits.ToDisplayUnits(Position + SpikePeak), color: Color.Red);
            //spriteBatch.Draw(texture: centerDot, position: ConvertUnits.ToDisplayUnits(Position + SensorPeak), color: Color.Green);
        }

        public virtual void Dispose()
        {
            body.Dispose();
        }
    }
}
