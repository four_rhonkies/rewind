﻿/**
* Copyright 2017 Niclas Scheuing, Per Näslung, Tunay Bora, Xiaowei Zeng
* This file is part of REWIND.
*
* REWIND is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* REWIND is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
* GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with REWIND.If not, see<http://www.gnu.org/licenses/>.
*/
using FarseerPhysics.Dynamics;
using FarseerPhysics.Factories;
using FarseerPhysics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using System;
using FarseerPhysics.Collision.Shapes;
using FarseerPhysics.Common;
using System.Collections.Generic;
using FarseerPhysics.Dynamics.Contacts;
using System.Linq;

namespace team8.Physics
{
    using Coordinate = Vector2;
    public abstract class MovableObject : PhysicsObject
    {
        public Fixture GroundSensor;
        public Fixture WallSensor;

        private List<Coordinate> SpikePeaks;
        private List<Coordinate> SensorPeaks;
        private Coordinate WallSensorPeak;
        protected Coordinate OverHeadPosition;
        protected Coordinate LeftChestPosition;
        protected Coordinate RightChestPosition;

        private List<Fixture> MainBodyFixtureList;

        private Texture2D DebugTexture;

        private int SensorGroundCount = 0;
        private int BodyGroundCount = 0;

        public Direction LastDirection;

        protected Coordinate ChestPositionInSim
        {
            get
            {
                return Position + (LastDirection == Direction.Right ? RightChestPosition : LeftChestPosition);
            }
        }

        public bool IsOnGround
        {
            get
            {
                if (SensorGroundCount < 0)
                {
                    //RELEASE: remove
                    throw new Exception("SensorGroundCount is smaller than zero : " + SensorGroundCount);//SensorGroundCount = 0;// 
                }
                return SensorGroundCount > 0;
            }
        }
        public bool AppliesForceOnGround
        {
            get
            {
                if (BodyGroundCount < 0)
                {
                    //RELEASE: remove
                    throw new Exception("SensorGroundCount is smaller than zero : " + SensorGroundCount);//BodyGroundCount = 0;// 
                }
                return BodyGroundCount > 0;
            }
        }

        public Direction RelativeCurrentDirection
        {
            get
            {
                return this.relativeCurrentDirection(withTolerance: this.StopThreshold);
            }
        }

        public MovableObject(String texturePath, PhysicalWorld world): base(texturePath: texturePath, world: world)
        {
            this.texturePath = texturePath;
            this.world = world;
        }

        public virtual void LoadContent(ContentManager contentManager, Coordinate initialPosition, float worldHeight, string userData, float objectDensity = 1, Coordinate? origin = null, BodyType bodyType = BodyType.Static, float friction = 1f, float restitution = 0f)
        {
            //compute scale
            var (width, height) = LoadTexture(contentManager: contentManager);
            float worldWidth = ((float)width / (float)height) * worldHeight;
            Scale = ConvertUnits.ToDisplayUnits(worldHeight) / (float)height;

            OverHeadPosition = new Coordinate(0, -worldHeight * 0.65f);
            RightChestPosition = new Coordinate(-worldWidth *0.2f, -worldHeight * 0.2f);
            LeftChestPosition = new Coordinate(-worldWidth * 0.1f, -worldHeight * 0.2f);


            //compute object width according to the texture ratio and the worldHeight

            var bodyW = worldWidth * 0.2f;

            float spickeHeigth = worldWidth / 8;
            //float halfW = (worldWidth - spickeWidth) / 2;
            float halfH = worldHeight / 2 - spickeHeigth;
            //var bodyPoints = new List<Coordinate>() { new Coordinate(worldWidth / 2, 0), new Coordinate(0, worldHeight / 2), new Coordinate(-worldHeight / 2, 0), new Coordinate(-worldWidth / 2, 0) };
            var spickePoints = new List<Coordinate>() {   new Coordinate(-bodyW, halfH),
                                                        new Coordinate(-bodyW, (halfH+spickeHeigth)),
                                                        new Coordinate(bodyW, (halfH+spickeHeigth)),
                                                        new Coordinate(bodyW, halfH),
                                                        };
            SpikePeaks = spickePoints;// new List<Coordinate>() { new Coordinate(-bodyW, (halfH + spickeHeigth)) , new Coordinate(bodyW, (halfH + spickeHeigth)) };
            Vertices spickeVertices = new Vertices(spickePoints);

            
            //this.body = BodyFactory.CreateRectangle(world, worldWidth, worldHeight, objectDensity, initialPosition);
            this.body = BodyFactory.CreatePolygon(world: world.World, vertices: spickeVertices, density: objectDensity, position: initialPosition);

            //If no origin is given, assume rectangular object and define the origin as its centroid
            if (origin.HasValue)
            {
                this.origin = origin.Value;
            }
            else
            {
                this.origin = new Coordinate(width / 2f, height / 2f);
            }

           
            body.BodyType = bodyType;
            body.Restitution = restitution;
            body.FixedRotation = true;
            body.Friction = friction;
            //Collide with collidors and walls
            body.CollisionCategories = Constants.CategoryMainCharacter;
            body.CollidesWith = Constants.CategoryGround | Constants.CategoryWall | Constants.CategoryCollision | Constants.CategoryRope;

            body.CollisionGroup = Constants.CollisionGroupNegativMainCharacter;
            body.OnCollision += OnBodyCollision;
            body.OnSeparation += OnBodySeparation;
            
            var bodyPoints = new List<Coordinate>() { new Coordinate(-bodyW, -halfH),
                                                        new Coordinate(-bodyW, halfH),
                                                        new Coordinate(bodyW, halfH),
                                                        new Coordinate(bodyW, -halfH)
                                                       };

            Vertices bodyVertices = new Vertices(bodyPoints);
            var bodyShape = new PolygonShape(vertices: bodyVertices, density: 0);
            var frictionlessBody = body.CreateFixture(shape: bodyShape, userData: Constants.UserDataWallSensor);
            frictionlessBody.Friction = 0;
            frictionlessBody.Restitution = 0;
            frictionlessBody.CollisionCategories = Constants.CategoryMainCharacter;
            frictionlessBody.CollidesWith = Constants.CategoryGround | Constants.CategoryWall | Constants.CategoryCollision | Constants.CategorySwitch | Constants.CategoryRope;
            frictionlessBody.CollisionGroup = Constants.CollisionGroupNegativMainCharacter;


            MainBodyFixtureList = new List<Fixture>() {};
            MainBodyFixtureList.AddRange(body.FixtureList);

            SetBodyFixtureUserData(userData: userData);


            //create sensor
            //var sensor = BodyFactory.CreateRectangle(world:world, width: worldWidth/10, height: worldWidth/10, )
            var sensorWidth = worldWidth * 0.4f;
            /*The (0,0) point seems to be the center of the character body, so go down by 0.5 *height plus a bit
            * If the sensor is too long (going down too far), it will trigger the landing animation too soon, and the body will look like falling down a bit after running up a slope
            * If it is too short (not going down far enough), it will trigger OnSeparation too soon (on slopes, moving/bouncing objects)
            * */
            var sensorHeight = worldHeight * 0.55f;
            var sensorPoints = new List<Coordinate>() { new Coordinate(sensorWidth, 0), new Coordinate(sensorWidth, sensorHeight), new Coordinate(-sensorWidth, sensorHeight), new Coordinate(-sensorWidth, 0) };
            SensorPeaks = new List<Coordinate>() { new Coordinate(-sensorWidth, sensorHeight), new Coordinate(sensorWidth, sensorHeight) };
            Vertices sensorVertices = new Vertices(sensorPoints);
            var sensorShape = new PolygonShape(vertices: sensorVertices, density: 0);
            this.GroundSensor = body.CreateFixture(shape: sensorShape, userData: Constants.UserDataGroundSensor);
            this.GroundSensor.IsSensor = true;
            this.GroundSensor.CollisionCategories = Constants.CategoryMainCharacter;
            this.GroundSensor.CollidesWith = Constants.CategoryGround | Constants.CategoryCollision;

            this.GroundSensor.OnCollision += OnSensorCollision;
            this.GroundSensor.OnSeparation += OnSensorSeparation;
            this.GroundSensor.CollisionGroup = Constants.CollisionGroupNegativMainCharacter;



            var wallSensorPoints = new List<Coordinate>() { new Coordinate(sensorWidth, 0), new Coordinate(sensorWidth, sensorHeight), new Coordinate(-sensorWidth, sensorHeight), new Coordinate(-sensorWidth, 0) };
            WallSensorPeak = new Coordinate(-sensorWidth, sensorHeight);
            Vertices wallSensorVertices = new Vertices(wallSensorPoints);
            var wallSensorShape = new PolygonShape(vertices: wallSensorVertices, density: 0);
            this.WallSensor = body.CreateFixture(shape: wallSensorShape, userData: Constants.UserDataWallSensor);
            this.WallSensor.IsSensor = true;
            this.WallSensor.CollisionCategories = Constants.CategoryMainCharacter;
            this.WallSensor.CollidesWith = Constants.CategoryWall;
            this.WallSensor.CollisionGroup = Constants.CollisionGroupNegativMainCharacter;


        }

        protected void SetBodyFixtureUserData(string userData)
        {
            //MainBodyFixture.UserData = userData;
            foreach (var fixture in MainBodyFixtureList)
            {
                fixture.UserData = userData;
            }
        }
        public override void Draw(SpriteBatch spriteBatch, GameTime gameTime, Matrix transform, Action beginAgain)
        {

            if (Configuration.AllowRenderObjectVertices && Configuration.RenderObjectVertices)
            {

                if (DebugTexture == null)
                {
                    var dotSize = 3;
                    DebugTexture = new Texture2D(spriteBatch.GraphicsDevice, dotSize, dotSize);
                    var colors = new Color[dotSize * dotSize];
                    for (int i = 0; i < colors.Length; i++) { colors[i] = Color.White; }
                    DebugTexture.SetData(colors);
                }
            
                foreach (var sensorPeak in SensorPeaks)
                {
                    spriteBatch.Draw(texture: DebugTexture, position: ConvertUnits.ToDisplayUnits(Position + sensorPeak), color: Color.Green);
                }
                foreach (var spike in SpikePeaks)
                {
                    spriteBatch.Draw(texture: DebugTexture, position: ConvertUnits.ToDisplayUnits(Position + spike), color: Color.Red);
                }
                spriteBatch.Draw(texture: DebugTexture, position: ConvertUnits.ToDisplayUnits(Position + WallSensorPeak), color: Color.Blue);
            }
        }


        public void SetToPosition(Coordinate position)
        {
            this.body.Position = position;
            //ResetSensorGroundCount();
        }

        private void ResetSensorGroundCount()
        {
            this.SensorGroundCount = GroundContacts().Count;
            //this.BodyGroundCount = 
        }

        

        private bool OnSensorCollision(Fixture fixtureA, Fixture fixtureB, Contact contact)
        {
            var (isGroundOrCollision, _) = ContactIsToGroundOrCollision(contact, Constants.UserDataGroundSensor);
            if (isGroundOrCollision)
            {
                this.SensorGroundCount++;
                if(SensorGroundCount==1)
                {
                    Lands();
                }
            }
            return true;
        }
        private void OnSensorSeparation(Fixture fixtureA, Fixture fixtureB)
        {
            var (isGroundOrCollision, _) = ContactIsToGroundOrCollision(fixtureA: fixtureA, fixtureB: fixtureB, UserDataFromOtherObject: Constants.UserDataGroundSensor);
            if (isGroundOrCollision)
            {
                this.SensorGroundCount--;
                if (this.SensorGroundCount < 0)
                {
                    var i = 0;
                }
                if (!IsOnGround)
                {
                    TakesOff();
                }
                
            }
        }

        private bool OnBodyCollision(Fixture fixtureA, Fixture fixtureB, Contact contact)
        {
            var (isToGround, _) = ContactIsToGroundOrCollision(fixtureA: fixtureA, fixtureB: fixtureB, UserDataFromOtherObject: new List<string>() { Constants.UserDataMainCharacter, Constants.UserDataGhost });
            if (isToGround)
            {
                this.BodyGroundCount++;
            }
            return true;
        }
        private void OnBodySeparation(Fixture fixtureA, Fixture fixtureB)
        {
            var (isToGround, _) = ContactIsToGroundOrCollision(fixtureA: fixtureA, fixtureB: fixtureB, UserDataFromOtherObject: new List<string>() { Constants.UserDataMainCharacter, Constants.UserDataGhost });
            if (isToGround)
            {
                this.BodyGroundCount--;
            }
        }


        public (bool, Fixture) ContactIsToGroundOrCollision(Contact contact, string UserDataFromOtherObject)
        {
            return ContactIsToGroundOrCollision(fixtureA: contact.FixtureA, fixtureB: contact.FixtureB, UserDataFromOtherObject: UserDataFromOtherObject);
        }
        public (bool, Fixture) ContactIsToGroundOrCollision(Contact contact, List<string> UserDataFromOtherObject)
        {
            return ContactIsToGroundOrCollision(fixtureA: contact.FixtureA, fixtureB: contact.FixtureB, UserDataFromOtherObject: UserDataFromOtherObject);
        }

        public (bool, Fixture) ContactIsToWall(Contact contact, string UserDataFromOtherObject)
        {
            return ContactIsTo(fixtureA: contact.FixtureA, fixtureB: contact.FixtureB, ToUserData: Constants.UserDataWall, UserDataFromOtherObject: UserDataFromOtherObject);
        }
        public (bool, Fixture) ContactIsToWall(Contact contact, List<string> UserDataFromOtherObject)
        {
            return ContactIsTo(fixtureA: contact.FixtureA, fixtureB: contact.FixtureB, ToUserData: Constants.UserDataWall, UserDataFromOtherObject: UserDataFromOtherObject);
        }
        public (bool, Fixture) ContactIsToGround(Fixture fixtureA, Fixture fixtureB, string UserDataFromOtherObject)
        {
            return ContactIsTo(fixtureA: fixtureA, fixtureB: fixtureB, ToUserData: Constants.UserDataGround, UserDataFromOtherObject: UserDataFromOtherObject);
        }

        public (bool, Fixture) ContactIsToGroundOrCollision(Fixture fixtureA, Fixture fixtureB, string UserDataFromOtherObject)
        {
            var (isCollision, collisionFixture) = ContactIsTo(fixtureA: fixtureA, fixtureB: fixtureB, ToUserData: Constants.UserDataCollision, UserDataFromOtherObject: UserDataFromOtherObject);
            var (isGround, groundFixture) = ContactIsTo(fixtureA: fixtureA, fixtureB: fixtureB, ToUserData: Constants.UserDataGround, UserDataFromOtherObject: UserDataFromOtherObject);
            return (isCollision || isGround, (isCollision ? collisionFixture : groundFixture));
        }

        static public(bool, Fixture) ContactIsToGroundOrCollision(Fixture fixtureA, Fixture fixtureB, List<string> UserDataFromOtherObject)
        {
            var (isCollision, collisionFixture) = ContactIsTo(fixtureA: fixtureA, fixtureB: fixtureB, ToUserData: Constants.UserDataCollision, UserDataFromOtherObject: UserDataFromOtherObject);
            var (isGround, groundFixture) = ContactIsTo(fixtureA: fixtureA, fixtureB: fixtureB, ToUserData: Constants.UserDataGround, UserDataFromOtherObject: UserDataFromOtherObject);
            return (isCollision || isGround, (isCollision ? collisionFixture : groundFixture));
        }

        static public(bool, Fixture) ContactIsTo(Fixture fixtureA, Fixture fixtureB, string ToUserData, string UserDataFromOtherObject)
        {
            var charaToGround = (string)fixtureA.UserData == ToUserData && (string)fixtureB.UserData == UserDataFromOtherObject;
            var groundToChara = (string)fixtureB.UserData == ToUserData && (string)fixtureA.UserData == UserDataFromOtherObject;
            //Get the fixture of the ground object
            Fixture groundFixture = charaToGround ? fixtureA : (groundToChara ? fixtureB : null);
            return (charaToGround || groundToChara, groundFixture);
        }

        static public(bool, Fixture) ContactIsTo(Fixture fixtureA, Fixture fixtureB, string ToUserData, List<string> UserDataFromOtherObject)
        {
            var charaToGround = (string)fixtureA.UserData == ToUserData && UserDataFromOtherObject.Contains((string)fixtureB.UserData);
            var groundToChara = (string)fixtureB.UserData == ToUserData && UserDataFromOtherObject.Contains((string)fixtureA.UserData);
            //Get the fixture of the ground object
            Fixture groundFixture = charaToGround ? fixtureA : (groundToChara ? fixtureB : null);
            return (charaToGround || groundToChara, groundFixture);
        }

        //Ground contact of body, not sensor
        public List<(Contact, Fixture)> GroundContacts(List<string> userDataList = null)
        {
            userDataList = userDataList == null ? new List<string>() { Constants.UserDataMainCharacter, Constants.UserDataGhost } : userDataList;
            var contacts = new List<(Contact, Fixture)>();
            for (var contact = body.ContactList; contact != null; contact = contact.Next)
            {
                var (isGround, groundFixture) = ContactIsToGroundOrCollision(contact.Contact, UserDataFromOtherObject: userDataList);
                if (contact.Contact.IsTouching && isGround)
                {
                    //dont know which is me, so add both to the set
                    //bodies.Add(contact.Contact.FixtureA.Body);
                    contacts.Add((contact.Contact, groundFixture));

                }
            }
            return contacts;
        }

        public List<(Contact, Fixture)> WallContacts()
        {
            var contacts = new List<(Contact, Fixture)>();
            for (var contact = body.ContactList; contact != null; contact = contact.Next)
            {
                var (isWall, WallFixture) = ContactIsToWall(contact.Contact, UserDataFromOtherObject: new List<string>() { Constants.UserDataMainCharacter, Constants.UserDataGhost });
                if (contact.Contact.IsTouching && isWall)
                {
                    //dont know which is me, so add both to the set
                    //bodies.Add(contact.Contact.FixtureA.Body);
                    contacts.Add((contact.Contact, WallFixture));

                }
            }
            return contacts;
        }

        public Intersection? PointOnGroundBelowPosition(Vector2? direction = null, float distance = 100)
        {
            Intersection? nearestIntersection = world.GroundBelowPosition(position: Position, direction: direction, distance: distance);
            return nearestIntersection;
        }

        public Intersection? PointOnGroundOrCollisionBelowPosition(Vector2? direction = null, float distance = 100)
        {
            Intersection? nearestIntersection = world.GroundOrCollisionBelowPosition(position: Position, direction: direction, distance: distance);
            return nearestIntersection;
        }

        public Intersection? PointOnWallBelowPosition(Vector2? direction = null, float distance = 100)
        {
            Intersection? nearestIntersection = world.WallBelowPosition(position: Position, direction: direction, distance: distance);
            return nearestIntersection;
        }

        public Vector2 GroundVelocity()
        {
            var groundContacts = GroundContacts();

            //If not on ground, return absolute direction
            if (groundContacts.Count == 0)
            {
                return Vector2.Zero;
            }
            //if there are multiple ground objects, take the first adn warn
            else if (groundContacts.Count > 1)
            {
                //Debug.WriteLine("Multiple ground objects when computing LinearVelocityToGround");
            }
            Fixture groundContact = groundContacts.First().Item2;
            Vector2 groundVelocity = groundContact.Body.GetLinearVelocityFromWorldPoint(worldPoint: this.GroundSensor.Body.Position);
            return groundVelocity;
        }

        public Vector2 LinearVelocityToGround()
        {
            var groundVelocity = GroundVelocity();
            Vector2 relativeVelocity = Body.LinearVelocity - groundVelocity;
            return relativeVelocity;
        }

        protected Direction relativeCurrentDirection(double withTolerance)
        {

            var bodyVelocity = LinearVelocityToGround();
            if (bodyVelocity.X > withTolerance)
            {
                return Direction.Right;
            }
            else if (bodyVelocity.X < -withTolerance)
            {
                return Direction.Left;
            }
            else
            {
                return Direction.Stopped;
            }
        }

        abstract public void Moves(Direction direction);
        abstract public void Jumps();
        abstract public void Idles();
        abstract public void TakesOff();
        abstract public void Lands();
    }
}
