﻿/**
* Copyright 2017 Niclas Scheuing, Per Näslung, Tunay Bora, Xiaowei Zeng
* This file is part of REWIND.
*
* REWIND is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* REWIND is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
* GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with REWIND.If not, see<http://www.gnu.org/licenses/>.
*/
using FarseerPhysics.Dynamics;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace team8
{
    struct Constants
    {
        public const Category CategoryGround = Category.Cat1;
        public const Category CategoryWall = Category.Cat2;
        public const Category CategoryOthers = Category.Cat3;
        public const Category CategoryCollision = Category.Cat4;
        public const Category CategoryMainCharacter = Category.Cat5;
        public const Category CategorySwitch = Category.Cat6;
        public const Category CategoryRope = Category.Cat7;

        public const short CollisionGroupNegativMainCharacter = -1;
        public const short CollisionGroupNegativDefault = -2;
        public const short CollisionGroupDefault = 2;
        public const short CollisionGroupNone = 0;
        public const short CollisionGroupSwitch = 6;

        public const string UserDataGround = "ground";
        public const string UserDataWall = "wall";
        public const string UserDataMainCharacter = "maincharacter";
        public const string UserDataGhost = "ghost";
        public const string UserDataGroundSensor = "groundsensor";
        public const string UserDataWallSensor = "wallsensor";
        public const string UserDataCollision = "collision";
        public const string RopeUserData = "rope";

        public static readonly Vector2 SaveDistanceAboveGround = new Vector2(0, -0.6f);



        public const int FrameWarningLimit = 40;
    }

    public struct AnimationPath
    {
        //Glow
        public const string RecordingIndicator = "sprites/glow_character_dark";

        //Selection indicator
        public const string SelectionIndicator = "sprites/selection_indicator";

        //dissolve outro
        public const string CharacterDissolve = "sprites/dissolve";
        //sprit
        public const string FreeSprits = "levels/level1/animations/freeingspirits";
        public const string Light = "sprites/light";

        public struct MainCharacter
        {
            private const string basePath = "sprites/maincharacter/";
            public const string Run = basePath + "run";
            public const string Idle = basePath + "breath";
            public const string JumpLoop = basePath + "jumploop";
        }
        public struct Ghost
        {
            private const string basePath = "sprites/maincharacter/";
            public const string Run = basePath + "run_ghost";
            public const string Idle = basePath + "breath_ghost";
            public const string JumpLoop = basePath + "jumploop_ghost";
        }
    }

    public struct AnimationConstants
    {
        public struct Duration
        {
            public const float Run = 0.075f;
            public const float Idle = 0.15f;
            public const float JumpLoop = 0.15f;
            //public const float RecordingIndicator = 0.5f;
            public const float RecordingIndicator = 0.2f;
            public const float FreeSpirits = 0.01f;
            public const float CharacterDissolve = 0.1f;
            public const float SelectionIndicator = 1.8f;
        }
        public struct FrameCount
        {
            public const int Run = 8;
            public const int Idle = 6;
            public const int JumpLoop = 6;
            //public const int RecordingIndicator = 2;
            public const int RecordingIndicatorRows = 10;
            public const int RecordingIndicatorColumns = 6;
            public const int FreeSpiritsRows = 4;
            public const int FreeSpiritsColumns = 7;
            public const int FreeSpiritsTotal = 24;
            public const int CharacterDissolveColumns = 7;
            public const int CharacterDissolveRows = 10;
        }

        //The frame index of the running animation in which the character steps on the ground
        public static readonly int[] RunStepFrameIndices = { 7 };
        //higher is faster
        public static float LightBlinkingSpeed = 1.5f;
        public static Vector2 LightBlinkingScale = new Vector2(1, 1)*0.2f;
        public static float MinIntensity = 0.1f;
        public static float LightMaxIntensity = 0.35f;

        public static int GhostTrailingFrames = 40;
        public static int GhostTrailingDuration = 20;
        public static int GhostTrailingMinPixelDistance = 2;

        public static Vector2 SelectionIndicatorScale = Vector2.One * 0.06f;
        public static Color SelectionIndicatorColor = new Color(r: 0.9f, g: 0.9f, b: 0.9f);
    }
    public struct CameraConstants
    {
        public static float HorizontalDistanceToEdge = 0.35f;
        public static float VerticalDistanceToEdge = 0.2f;
        public static float GhostHorizontalDistanceToEdge = 0.1f;
        public static float GhostVerticalDistanceToEdge = 0.05f;
        public static float MaxZoom = 0.98f;
        public static float MinZoom = 0.7f;
        public static float TunnelMinZoom = 0.85f;
        public static float TotalZoom = 0.25f;
        public static Vector2 ReferenceResolution = new Vector2(1680,1050);

        public static float ZoomStep = 0.002f;
        public static float MoveStep = 10f;

    }

    public struct SoundConstants
    {
        public static float DistanceVolumeDecay = 1e5f;
        public struct Path
        {

            //soundtrack
            public struct Soundtrack
            {
                public static string Part1 = "sound/part1";
                public static string Part3Savanna = "sound/part3_savanna";
                public static string Part5Final = "sound/part4_boulder";
                public static string Part4Cave = "sound/part5_final_puzzle";
            }
            public struct Filtered
            {
                public static string Part1 = "sound/part1_filtered";
                public static string Part3Savanna = "sound/part3_savanna_filtered";
                public static string Part5Final = "sound/part4_boulder_filtered";
                public static string Part4Cave = "sound/part5_final_puzzle_filtered";
            }
            public struct Ghost
            {
                public static string Part1 = "sound/part1_ghost";
                public static string Part3Savanna = "sound/part3_savanna_ghost";
                public static string Part5Final = "sound/part4_boulder_ghost";
                public static string Part4Cave = "sound/part5_final_puzzle_ghost";
            }
            public struct Buttons
            {
                public static string ReplayStart = "sound/replaystart";
                public static string RecordStart = "sound/replaystart";
                public static string ReplayToggle = "sound/replaytoggle";
                public static string PressDenied = "sound/xpressdnied";
                public static string ReplayDismissGhost = "sound/replaydeleteghost";
            }
            //Static screens
            public static string StartScreen = "sound/start_screen";
            public static string Credits = "sound/credits";
            public static string Outro = "sound/outro";

            //step
            public static string Step = "sound/step_cut";

            //pipes
            public static string PipeBadMiddle = "sound/pipe_bad_1";
            public static string PipeGoodMiddle = "sound/pipe_good_1";
            public static string PipeBadTop = "sound/pipe_bad_2";
            public static string PipeGoodTop = "sound/pipe_good_2";
            public static string PipeBadBottom = "sound/pipe_bad_3";
            public static string PipeGoodBottom = "sound/pipe_good_3";

            //rock
            public static string RockBumping = "sound/step_cut";
            public static string RockImpact = "sound/rock";

            //spirit
            public static string SpiritsFreed = "sound/safepoint";
            public static string SpiritsCaged = "sound/spirits_caged";

            //death
            public static string Death = "sound/game_over";

            //branch
            public static string BranchCreak = "sound/branch_creak_short";
            public static string BranchBreak = "sound/branch_break";

            //stone
            public static string StoneOpen = "sound/stone_open";
            public static string StoneClose = "sound/stone_close";

            //cable car
            public static string CableCarStarting = "sound/cable_car_starting";
            public static string CableCarRunning = "sound/cable_car_running";

            //outro 
            public static string Dissolve = "sound/dissolve";

            //ambient
            //jungle
            public static string Cricket = "sound/cricket";
            public static string Monkey = "sound/monkey";
            public static string SpookyBird = "sound/spookybird";
            public static string WeirdBird = "sound/weirdbird";
            public static string FunnyBird = "sound/funnybird";
            public static string CheepBird = "sound/cheepbird";
            public static string SingBird = "sound/singbird";

            //savanna
            public static string Lion = "sound/lion";
            public static string Cicada1 = "sound/cicada_1";
            public static string Cicada2 = "sound/cicada_2";
            public static string Cicada3 = "sound/cicada_3";
            public static string CicadaMany = "sound/cicada_many";
            public static string Flies = "sound/flies";

            //cave
            public static string Drop1 = "sound/drop_1";
            public static string Drop2 = "sound/drop_2";
            public static string Drop3 = "sound/drop_3";
            public static string Drop4 = "sound/drop_4";
            public static string DropDelay1 = "sound/drop_1_delay";
            public static string DropDelay2 = "sound/drop_2_delay";
            public static string DropDelay3 = "sound/drop_3_delay";
            public static string DropDelay4 = "sound/drop_4_delay";
            public static string Stone1 = "sound/stone1";
            public static string Stone2 = "sound/stone2";
            public static string Stone3 = "sound/stone3";
            public static string Stone4 = "sound/stone4";
        }
        public static int CableCarStartingDuration = 1985;

        public static Point MaxDistanceToCameraForAmbient = new Point(2500, 1000);

        public struct FadeTimes
        {
            public static int SoundtrackFadeOut = 3000;
            public static int SoundtrackFadeOutSteps = 100;
        }
    }

    public struct FontConstants
    {
        public static string InstructionFont = "Fonts/Font";

        public static string EndScreenFont = "Fonts/FreeSans_regular";
        public static string StartScreenFont = "Fonts/FreeSans_regular";
        public static string TitleFont = "Fonts/FreeSans_bold";
        public static string CreditsFont = "Fonts/FreeSans_regular_20";
        public static string CopyrightFont = "Fonts/FreeSans_regular_small";
    }

    public struct InstructionConstants
    {
        public struct Text
        {
            public static string Move = "Use left joystick for running";
            public static string Jump = "Press A to jump";
            public static string Record = "Press B to start recording";
            public static string Replay = "Press B to end recording and start replaying";
            public static string MarkCharacter = "Press B to change character selection";
            public static string ChooseCharacter = "Press X to choose character";
            public static string Reset = "Press Y to reset to the last save point";
        }
        public struct Duration
        {
            public static int Move = 10 * 1000;
            public static int Jump = 10 * 1000;
            public static int Record = 10 * 1000;
            public static int Replay = 10 * 1000;
            public static int ChooseCharacter = 10 * 1000;
            public static int MarkCharacter = 10 * 1000;
        }
    }

    public struct StaticScreenConstants
    {
        public struct Text
        {
            public static string Title = "REWIND";
            public static string EndText = "Thank you for testing";
            public static string PressToStartController = "Press A to Start";
            public static string PressToStartKeyboard = "Press Space to Start";
            public static string Loading = "Loading";
            public static string Copyright = "Copyright 2017 Niclas Scheuing, Per Näslung, Tunay Bora, Xiaowei Zeng. Attributions and source https://bitbucket.org/four_rhonkies/rewind";
            public static string InstructionController = "Press start for help";
            public static string InstructionKeyboard = "Press H for help";
        }
        public struct Colors
        {
            public static Color EndFontColor = new Color(0xf5f5f5);
            public static Color TitleFontColor = new Color(0xf5f5f5);
            public static Color InstructionFontColor = new Color(0xf5f5f5);
        }
        public struct  Duration
        {
            public static int ShowEndScreenDelay = 3000;
        }
        public static string BackgroundTexturePath = "screens/title_screen";
        public static string InstructionControllerTexturePath = "screens/instructions_screen_controller";
        public static string InstructionKeyboardTexturePath = "screens/instructions_screen_keyboard";
    }

    public struct OutroConstants
    {
        public struct Text
        {
            public static string Title = "REWIND";
            public static string Niclas = "Niclas Scheuing - Lead Programmer and Visual Effects";
            public static string Magenta = "Magenta Zeng - Art and Animation";
            public static string Tunay = "Tunay Bora - Art and Animation";
            public static string Per = "Per Näslund - Programmer and Level Designer";
            public static string Raly = "Janik Scheuing - Music";
        }
        public struct Colors
        {
            public static Color CreditFontColots = new Color(0xf5f5f5);
        }
    }

    public struct PhysicsConstants
    {
        
        public struct MainCharacter
        {
            public static float JumpImpulse = 1f;
            public static float StepImpulse = 0.3f;
        }
    }

    public struct RenderingConstants
    {
        public static float CharacterLayerDepth = 0.0001f;
        public static float WalkableTreeDepth = -0.0001f;
        public static float MinParallaxOffset = 0.01f;
    }

    public struct PuzzleConstants
    {
        public static float CableCarSpeed = 0.75f;
    }
}

