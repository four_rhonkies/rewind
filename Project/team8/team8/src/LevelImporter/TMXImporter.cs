﻿/**
* Copyright 2017 Niclas Scheuing, Per Näslung, Tunay Bora, Xiaowei Zeng
* This file is part of REWIND.
*
* REWIND is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* REWIND is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
* GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with REWIND.If not, see<http://www.gnu.org/licenses/>.
*/
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using team8.Level;

namespace team8.LevelImporter
{
    public class TMXImporter
    {
        private string LevelDirectory;
        private string ContentRoot;

        public TMXImporter(string contentRoot, string levelDirectory)
        {
            this.LevelDirectory = levelDirectory;
            this.ContentRoot = contentRoot;
        }
        public Map ReadMap( string filename)
        {
            var levelPath = ContentRoot + "/" + LevelDirectory + filename;
            var fileContent = File.ReadAllText(levelPath);

            var mapElement = XElement.Parse(fileContent);
            var map = ReadMap(mapElement);

            return map;
        }

        private Map ReadMap(XElement mapElement)
        {
            Map map = new Map(mapElement: mapElement);

            var tilesetElements = mapElement.Elements("tileset");
            map.TileSets = ReadTilesets(tilesetElements: tilesetElements);

            var layerElements = mapElement.Elements("layer");
            map.Layers = ReadLayers(layerElements: layerElements, map: map);

            var objectGroupElements = mapElement.Elements("objectgroup");
            map.ObjectGroups = ReadObjectGroups(objectGroupElements: objectGroupElements);
            
            return map;
        }

        private List<TileSet> ReadTilesets(IEnumerable<XElement> tilesetElements)
        {

            var tilesets = new List<TileSet>();
            foreach (var tilesetElement in tilesetElements)
            {
                TileSet tileSet = new TileSet(tilesetElement: tilesetElement, levelDirectory: this.LevelDirectory);
                tilesets.Add(tileSet);
            }
            return tilesets;
        }
       

        private List<Layer> ReadLayers(IEnumerable<XElement> layerElements, Map map)
        {
            var layers = new List<Layer>();
            foreach (var layerElement in layerElements)
            {
                Layer layer = new Layer(layerElement: layerElement, map:map);
                layers.Add(layer);
            }
            return layers;
        }

       

        private List<ObjectGroup> ReadObjectGroups(IEnumerable<XElement> objectGroupElements)
        {
            var objectGroups = new List<ObjectGroup>();
            foreach (var objectGroupElement in objectGroupElements)
            {
                ObjectGroup objectGroup = ReadObjectGroup(objectGroupElement: objectGroupElement);
                objectGroups.Add(objectGroup);
            }
            return objectGroups;
        }

        private ObjectGroup ReadObjectGroup(XElement objectGroupElement)
        {
            var objectGroup = new ObjectGroup(objectGroupElement);
            return objectGroup;
        }


        public static Dictionary<string, string> ParseProperties(XElement element, string objectType, List<String> keys, bool failOnMissing=true)
        {
            var foundValues = new Dictionary<string, string>();
            if (element.Element("properties") == null)
            {
                if (failOnMissing)
                {
                    Debug.WriteLine("No \"properties\" attribute in " + objectType + ". This is bad.");
                    throw new Exception("No \"properties\" attribute in  " + objectType + ". This is bad.");
                }
                else
                {
                    return foundValues;
                }
            }
            var propertiesElement = element.Element("properties");
            var proteryElements = propertiesElement.Elements("property");

            
            foreach (var propertyElement in proteryElements)
            {
                if (propertyElement.Attribute("name") != null)
                {
                    var foundKey = propertyElement.Attribute("name").Value;
                    foreach (var key in keys)
                    {
                        if(foundKey == key)
                        {
                            foundValues[key] = propertyElement.Attribute("value").Value;
                        }
                    }
                }
            }

            return foundValues;
        }

        public static XAttribute GetPropertyValueAttribute(XElement objectElement, string propertyName)
        {
            var propertiesElement = objectElement.Element("properties");
            if (propertiesElement == null)
            {
                return null;
            }
            var propertyElements = propertiesElement.Elements();
            foreach (var element in propertyElements)
            {
                var nameElement = element.Attribute("name");
                if (nameElement != null && nameElement.Value == propertyName)
                {
                    return element.Attribute("value");
                }
            }
            return null;
        }

        public static List<string> SplitStringList(string listString)
        {
            return listString.Split(new char[] { ',' }).ToList();
        }
    }

    
}
