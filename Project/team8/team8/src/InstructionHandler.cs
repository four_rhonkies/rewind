﻿/**
* Copyright 2017 Niclas Scheuing, Per Näslung, Tunay Bora, Xiaowei Zeng
* This file is part of REWIND.
*
* REWIND is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* REWIND is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
* GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with REWIND.If not, see<http://www.gnu.org/licenses/>.
*/
using Microsoft.Xna.Framework.Content;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using team8.Rendering;

namespace team8
{
    public class InstructionHandler
    {
        private static InstructionHandler PrivateInstance;
        public static InstructionHandler Instance
        {
            get
            {
                if(PrivateInstance == null)
                {
                    PrivateInstance = new InstructionHandler();
                }
                return PrivateInstance;
            }
        }

        private TextRenderLayer TextRenderLayer;

        private Timer StopShowingTimer;

        public void LoadContent(ContentManager contentManager, CameraController cameraController)
        {
            this.TextRenderLayer = new TextRenderLayer(cameraController: cameraController);
            this.TextRenderLayer.LoadContent(contentManager: contentManager);
        }

        public TextRenderLayer GetTextRenderLayer()
        {
            return this.TextRenderLayer;
        }

        public void ShowText(string text, int forMilliSeconds)
        {
            if (!Configuration.ShowInstructions)
            {
                return;
            }
            //If already showing something, abort timer
            if (StopShowingTimer != null)
            {
                StopShowingTimer.Dispose();
            }

            TextRenderLayer.SetText(text: text);
            StopShowingTimer = new Timer(callback: StopShowing, state: null, dueTime: forMilliSeconds, period: Timeout.Infinite);
        }
        private void StopShowing(object state)
        {
            Reset();
        }
        public void Reset()
        {
            if (!Configuration.ShowInstructions)
            {
                return;
            }
            TextRenderLayer.ResetText();
            if (StopShowingTimer != null)
            {
                StopShowingTimer.Dispose();
            }
        }
    }
}
