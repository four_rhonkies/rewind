﻿/**
* Copyright 2017 Niclas Scheuing, Per Näslung, Tunay Bora, Xiaowei Zeng
* This file is part of REWIND.
*
* REWIND is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* REWIND is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
* GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with REWIND.If not, see<http://www.gnu.org/licenses/>.
*/
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Media;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;

namespace team8.Sound
{
    using Coordinate = Vector2;
    public class SoundManager
    {
        private static SoundManager PrivateInstance;
        public static SoundManager Instance
        {
            get
            {
                return PrivateInstance;
            }
        }

        private ContentManager Content;
        private SoundEffect LandingSound;
        private SoundEffectInstance RunningInstance;
        private SoundEffectInstance SoundtrackInstance;
        private SoundEffectInstance GhosttrackInstance;
        private SoundEffectInstance RecordingInstance;

        private SoundFile? CurrentSoundtrack = null;

        private Timer SoundtrackFadeOutTimer;

        public DistanceDelegate DistanceDelegate;
        public MusicRegionDelegate MusicRegionDelegate;
        public ReplayDelegate ReplayDelegate;

        private AmbientSoundEngine AmbientSoundEngine;
        private Semaphore SetSoundtrackSemaphore;

        public delegate float UpdatVolumeDelegate();
        public delegate Coordinate? UpdatPositionDelegate(GameTime gameTime);

        public struct SoundEffectDefinition
        {
            public SoundFile SoundFile;
            public SoundEffectInstance SoundEffectInstance;
            public UpdatVolumeDelegate UpdatVolumeDelegate;
            public UpdatPositionDelegate UpdatPositionDelegate;
            public float InitialVolume;
            public float InitialPan;

            public SoundEffectDefinition(SoundFile soundFile, SoundEffectInstance soundEffectInstance, UpdatVolumeDelegate updateDelegate = null, UpdatPositionDelegate positionDelegate = null, float initialVolume=1, float initialPan = 0)
            {
                this.SoundFile = soundFile;
                this.SoundEffectInstance = soundEffectInstance;
                this.UpdatVolumeDelegate = updateDelegate;
                this.UpdatPositionDelegate = positionDelegate;
                this.InitialVolume = initialVolume;
                this.InitialPan = initialPan;
            }
        }
        private List<SoundFile> SoundFilesForPreloading = new List<SoundFile> {SoundFile.BranchBreak, SoundFile.SpiritFreed,
            SoundFile.Cricket, SoundFile.Monkey, SoundFile.SpookyBird, SoundFile.WeirdBird, SoundFile.FunnyBird, SoundFile.CheepBird, SoundFile.SingBird,
            SoundFile.Lion, SoundFile.Cicada1, SoundFile.Cicada2, SoundFile.Cicada3, SoundFile.CicadaMany, SoundFile.Flies,
            SoundFile.Drop1, SoundFile.Drop2, SoundFile.Drop3, SoundFile.Drop4, SoundFile.DropDelay1, SoundFile.DropDelay2, SoundFile.DropDelay3, SoundFile.DropDelay4,
            SoundFile.Stone1, SoundFile.Stone2, SoundFile.Stone3, SoundFile.Stone4,
            SoundFile.Outro, SoundFile.Credits,SoundFile.Dissolve,
            SoundFile.CableCarRunning, SoundFile.CableCarStarting
        };
        protected ConcurrentDictionary<string, SoundEffectDefinition> Sounds;
        //forward to the configuration
        private bool Mute
        {
            get
            {
                return Configuration.Sound.Mute;
            }
            set
            {
                Configuration.Sound.Mute = value;
            }
        }


        public SoundManager(ContentManager content)
        {
            SetSoundtrackSemaphore = new Semaphore(initialCount: 1, maximumCount: 1);
            PrivateInstance = this;

            Sounds = new ConcurrentDictionary<string, SoundEffectDefinition>();
            Content = content;
            //Disposed = false;
            LoadSoundEffects();
            this.AmbientSoundEngine = new AmbientSoundEngine(soundManager: this);
            PreloadSoundFiles();
        }
        
        private void PreloadSoundFiles()
        {
            foreach(var file in SoundFilesForPreloading)
            {
                this.Content.Load<SoundEffect>(PathForSoundFile(file));
            }
        }

        public void SetSoundtrack(SoundFile track, SoundFile? trackFiltered, SoundFile? trackGhost, float? volume=null, int? fadeOutInMs=null)
        {
            SetSoundtrackSemaphore.WaitOne();
            //Don't do anything, if within same regiopn
            if (CurrentSoundtrack.HasValue && CurrentSoundtrack.Value == track)
            {

                SetSoundtrackSemaphore.Release();
                return;
            }
            //Fade out previouse soundtrack
            if (SoundtrackInstance != null && SoundtrackFadeOutTimer == null)
            {

                
                TimerCallback fadeOut = delegate (object state)
                {
                    SoundtrackInstance.Volume = Math.Max(0,SoundtrackInstance.Volume - 1/(float)SoundConstants.FadeTimes.SoundtrackFadeOutSteps);
                    var otherVolume = 0f;
                    if (RecordingInstance != null)
                    {
                        RecordingInstance.Volume = Math.Max(0, RecordingInstance.Volume - 1 / (float)SoundConstants.FadeTimes.SoundtrackFadeOutSteps);
                        otherVolume = RecordingInstance.Volume;
                    }
                    if (GhosttrackInstance != null)
                    {
                        GhosttrackInstance.Volume = Math.Max(0, GhosttrackInstance.Volume - 1 / (float)SoundConstants.FadeTimes.SoundtrackFadeOutSteps);
                        otherVolume = Math.Max(otherVolume,GhosttrackInstance.Volume);
                    }
                    if (Math.Max(SoundtrackInstance.Volume, otherVolume) == 0 )
                    {
                        SetSoundtrackSemaphore.WaitOne();
                        SoundtrackFadeOutTimer.Dispose();
                        SoundtrackFadeOutTimer = null;
                        
                        //Stop the music
                        SoundtrackInstance.Stop();
                        if (RecordingInstance != null)
                        {
                            RecordingInstance.Stop();
                        }
                        if (GhosttrackInstance != null)
                        {
                            GhosttrackInstance.Stop();
                        }

                        //setup and play
                        StartTracks(track: track, trackFiltered: trackFiltered, trackGhost: trackGhost,volume: volume);
                        SetSoundtrackSemaphore.Release();
                    }
                };
                SoundtrackFadeOutTimer = new Timer(callback: fadeOut, state: null, dueTime: 0, period: fadeOutInMs.GetValueOrDefault(SoundConstants.FadeTimes.SoundtrackFadeOut) / SoundConstants.FadeTimes.SoundtrackFadeOutSteps);
            }
            //There is already a Timer running, don't do anything
            else if (SoundtrackFadeOutTimer != null)
            { 
                SetSoundtrackSemaphore.Release();
                return;
            }
            else
            {
                //setup and play
                StartTracks(track: track, trackFiltered: trackFiltered, trackGhost: trackGhost, volume: volume);
            }

            CurrentSoundtrack = track;
            SetSoundtrackSemaphore.Release();
        }

        private void StartTracks(SoundFile track, SoundFile? trackFiltered, SoundFile? trackGhost, float? volume=null)
        {
            //Start new music
            SetupSoundtrackTimed(track, volume: volume);
            if (trackFiltered.HasValue)
            {
                SetupRecordingTrack(trackFiltered.Value);
            }
            else
            {
                //make sure the recording instance is ready
                SetupRecordingTrack(track, volume: 0f);
            }
            if (trackGhost.HasValue)
            {
                SetupGhostTrack(trackGhost.Value);
            }
            else
            {
                //make sure the recording instance is ready
                SetupGhostTrack(track, volume: 0f);
            }
            StartMusicTimed(soundtrackOnly: trackFiltered == null);
        }


        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private void StartMusicTimed(bool soundtrackOnly=false)
        {
            if (!soundtrackOnly)
            {
                RecordingInstance?.Play();
                GhosttrackInstance?.Play();
            }
            SoundtrackInstance.Play();
        }

        private void SetupSoundtrackTimed(SoundFile track, float? volume=null)
        {
            SoundEffect backgroundMusic = Content.Load<SoundEffect>(PathForSoundFile(track));
            if (SoundtrackInstance != null)
            {
                SoundtrackInstance.Dispose();
            }
            SoundtrackInstance = backgroundMusic.CreateInstance();
            SoundtrackInstance.IsLooped = true;
            SoundtrackInstance.Volume = volume.GetValueOrDefault(SoundtrackVolume());
            SoundtrackInstance.Pitch = 0.0f;
            SoundtrackInstance.Pan = 0.0f;
        }


        private float SoundtrackVolume()
        {
            if (Mute)
            {
                return 0;
            }
            else if (ReplayDelegate != null && ReplayDelegate.GhostExists())
            {
                return Configuration.Sound.MusicVolumeLow;
            }
            else if (ReplayDelegate != null && ReplayDelegate.IsRecording())
            {
                return 0;
            }
            else
            {
                return Configuration.Sound.MusicVolumeNormal;
            }
        }

        public void SetupGhostTrack(SoundFile track, float? volume = null)
        {
            SoundEffect ghostMusic = Content.Load<SoundEffect>(PathForSoundFile(track));
            if (GhosttrackInstance != null)
            {
                GhosttrackInstance.Dispose();
            }
            GhosttrackInstance = ghostMusic.CreateInstance();
            GhosttrackInstance.IsLooped = true;
            GhosttrackInstance.Volume = volume.GetValueOrDefault(GhostTrackVolume());
            GhosttrackInstance.Pitch = 0.0f;
            GhosttrackInstance.Pan = 0.0f;
        }

        private void SetupRecordingTrack(SoundFile track, float? volume=null)
        {
            SoundEffect recordingMusic = Content.Load<SoundEffect>(PathForSoundFile(track));
            if (RecordingInstance != null)
            {
                RecordingInstance.Dispose();
            }
            RecordingInstance = recordingMusic.CreateInstance();
            RecordingInstance.IsLooped = true;
            RecordingInstance.Volume = volume.GetValueOrDefault(RecordingTrackVolume());
            RecordingInstance.Pitch = 0.0f;
            RecordingInstance.Pan = 0.0f;
        }

        private float RecordingTrackVolume()
        {
            if (Mute)
            {
                return 0;
            }
            else if (ReplayDelegate != null && ReplayDelegate.IsRecording())
            {
                return Configuration.Sound.RecordingTrackVolume;
            }
            else
            {
                return 0;
            }
        }
        private float GhostTrackVolume()
        {
            if (Mute)
            {
                return 0;
            }
            else if (ReplayDelegate != null && ReplayDelegate.GhostExists())
            {
                return Configuration.Sound.GhostTrackVolume;
            }
            else
            {
                return 0;
            }
        }

        private void LoadSoundEffects()
        {
            //Landing
            LandingSound = Content.Load<SoundEffect>(SoundConstants.Path.Step);

            //Walking
            SoundEffect stepSound = Content.Load<SoundEffect>(SoundConstants.Path.Step);
            RunningInstance = stepSound.CreateInstance();
            RunningInstance.Volume = Configuration.Sound.CharacterEffectVolume;
            RunningInstance.IsLooped = false;

            //Others

        }


        private void PlayRunningSound()
        {
            if (!Mute)
            {
                RunningInstance.Play();
            }
        }

        private void PlayLandingSound()
        {
            if (!Mute)
            {
                LandingSound.Play(volume: Configuration.Sound.StepSoundVolume, pitch: 0f, pan: 0f);
            }
        }
        private void PlayJumpSound()
        {
            if (!Mute)
            {
                LandingSound.Play(volume: Configuration.Sound.CharacterEffectVolume, pitch: 0f, pan: 0f);
            }
        }

        public void ToggleMuteSound()
        {
            if (Mute)
            {
                //SoundtrackInstance.Volume = Configuration.Sound.MusicVolumeNormal;
                Mute = false;
            }
            else
            {
                //SoundtrackInstance.Volume = 0;
                //RunningInstance.Stop();
                Mute = true;
            }

        }

        /*-----DELEGATED-----*/
        public void OnJump()
        {
            PlayJumpSound();
        }
        public void OnStep()
        {
            PlayRunningSound();
        }
        public void OnLanding()
        {
            PlayLandingSound();
        }
        public void OnDeath()
        {
            PlaySound(file:SoundFile.Death, id: "death", initialVolume: Configuration.Sound.GameOverVolume);
        }
        public void OnGhostAppears()
        {
            if (!Mute)
            {
                GhosttrackInstance.Volume = Configuration.Sound.GhostTrackVolume;
                SoundtrackInstance.Volume = Configuration.Sound.MusicVolumeLow;
                Configuration.Sound.AmbientVolume = Configuration.Sound.ReducedAmbientVolume;
            }
        }

        public void OnGhostDisappears()
        {
            GhosttrackInstance.Volume = 0f;
            if (!Mute)
            {
                SoundtrackInstance.Volume = Configuration.Sound.MusicVolumeNormal;
                Configuration.Sound.AmbientVolume = Configuration.Sound.DefaultAmbientVolume;
            }
        }

        public void OnStartRecording()
        {
            if (!Mute)
            {
                RecordingInstance.Volume = Configuration.Sound.RecordingTrackVolume;
                SoundtrackInstance.Volume = 0;
            }
        }
        public void OnStopRecording()
        {
            RecordingInstance.Volume = 0f;
            if (!Mute)
            {
                SoundtrackInstance.Volume = Configuration.Sound.MusicVolumeNormal;
            }
        }

        public void PlaySound(SoundFile file, string id, float? initialVolume = null, bool loop = false, UpdatVolumeDelegate updateDelegate = null, UpdatPositionDelegate positionDelegate = null)
        {
            //if(Disposed)
            // {
            //     return;
            // }
            var path = PathForSoundFile(file: file);

            //Create instance if not existing
            if (!Sounds.ContainsKey(id))
            {
                var sound = Content.Load<SoundEffect>(path);
                var soundInstance = sound.CreateInstance();
                var volume = initialVolume.GetValueOrDefault(loop ? 0 : 1);
                Sounds[id] = new SoundEffectDefinition(soundFile: file, soundEffectInstance: soundInstance, updateDelegate: updateDelegate, positionDelegate: positionDelegate, initialVolume: volume);
            }
            else
            {
                //Dont interrupt when already playing
                if (Sounds[id].SoundEffectInstance.State == SoundState.Playing)
                {
                    return;
                }
            }

            Sounds[id].SoundEffectInstance.IsLooped = loop;
            ApplyUpdates(Sounds[id], gameTime: null);
            Sounds[id].SoundEffectInstance.Play();



        }
        public void StopSound(string id)
        {
            if (Sounds.ContainsKey(id))
            {
                Sounds[id].SoundEffectInstance.Stop();
            }
        }

        public void SetVolume(string id, float volume)
        {
            if (Sounds.ContainsKey(id))
            {
                Sounds[id].SoundEffectInstance.Volume = volume;
            }
        }

        public float GetVolume(string id)
        {
            if (Sounds.ContainsKey(id))
            {
                return Sounds[id].SoundEffectInstance.Volume;
            }
            else
            {
                //RELAESE
                throw new Exception("Tried to get volume from a SoundFile not present in the dictionary.");
                return 0f;
            }
        }

        public void Reset()
        {
            SoundtrackFadeOutTimer?.Dispose();
            //Disposed = true;
            if (Sounds == null)
            {
                return;
            }
            foreach (var sound in Sounds.Values)
            {
                sound.SoundEffectInstance.Stop();
            }
        }

        /*-----UPDATE-----*/
        public void Update(GameTime gameTime)
        {
            var musicRegionId = UpdateBackgroundMusic();

            foreach (SoundEffectDefinition element in Sounds.Values)
            {
                ApplyUpdates(definition: element, gameTime: gameTime);
            }
            AmbientSoundEngine.Update(gameTime: gameTime, cameraPositionInDisplay: DistanceDelegate.CameraCenterInDisplay(), area: musicRegionId);
        }

        private string UpdateBackgroundMusic()
        {
            if (MusicRegionDelegate == null)
            {
                return null;
            }
            var musicRegionId = this.MusicRegionDelegate.MusicRegionObjectIdForMainCharacter();
            if (musicRegionId == null)
            {
                return null;
            }
            var soundFile = SoundFileForMusicRegionId(musicRegionId: musicRegionId);
            var backgroundSoundFile = BackgroundSoundFileForMusicRegionId(musicRegionId: musicRegionId);
            var ghostSoundFile = GhostSoundFileForMusicRegionId(musicRegionId: musicRegionId);
            if (soundFile.HasValue && backgroundSoundFile.HasValue)
            {
                SetSoundtrack(track: soundFile.Value, trackFiltered: backgroundSoundFile.Value, trackGhost: ghostSoundFile);
            }
            return musicRegionId;

        }

        private void ApplyUpdates(SoundEffectDefinition definition, GameTime gameTime)
        {

           if (definition.SoundEffectInstance == null || definition.SoundEffectInstance.State != SoundState.Playing)
            {
                return;
            }
            var volume = definition.InitialVolume;
            var pan = definition.InitialPan;
            var updatedVolume = definition.UpdatVolumeDelegate?.Invoke();
            volume = updatedVolume.GetValueOrDefault(volume);

            var position = definition.UpdatPositionDelegate?.Invoke(gameTime: gameTime);
            if (position.HasValue)
            {
                var volumeCorrection = VolumeDistanceCorrection(pointInDisplay: position.Value);
                volume = volume * volumeCorrection;
                pan = PanDistanceCorrection(pointInDisplay: position.Value);
            }

            definition.SoundEffectInstance.Volume = volume;
            definition.SoundEffectInstance.Pan = pan;
        }

        private float VolumeDistanceCorrection(Coordinate pointInDisplay)
        {
            if (DistanceDelegate == null)
            {
                return 1;
            }
            Vector2 distanceVector = DistanceDelegate.DistanceFromCameraToPoint(pointInDisplay: pointInDisplay);
            var squaredDistance = distanceVector.LengthSquared();
            if(squaredDistance == 0)
            {
                return 1;
            }
            var correction = SoundConstants.DistanceVolumeDecay / squaredDistance;
            correction = Math.Min(correction, 1);
            return correction;
        }

        private float PanDistanceCorrection(Coordinate pointInDisplay)
        {
            if (DistanceDelegate == null)
            {
                return 0;
            }
            Vector2 distanceVector = DistanceDelegate.DistanceFromCameraCenterToPoint(pointInDisplay: pointInDisplay);
            var distance = distanceVector.X;
            var pan = Math.Sign(distance)* Math.Min(Math.Abs(distance)/1000,1.0);
            return (float)pan;
        }

        public enum SoundFile
        {
            StartScreen,
            Outro,
            Credits,
            SoundtrackPart1,
            SoundtrackPart3Savanna,
            SoundtrackPart4Cave,
            SoundtrackPart5Final,
            FilteredPart1,
            FilteredPart3Savanna,
            FilteredPart4Cave,
            FilteredPart5Final,
            GhostPart1,
            GhostPart3Savanna,
            GhostPart4Cave,
            GhostPart5Final,
            PipeBadTop, PipeGoodTop,
            PipeBadMiddle, PipeGoodMiddle,
            PipeBadBottom, PipeGoodBottom,
            RockBumping, RockImpact,
            SpiritCaged, SpiritFreed,
            Death,
            BranchCreak, BranchBreak,
            StoneOpen, StoneClose,
            CableCarRunning, CableCarStarting,
            ReplayStart, RecordStart, ReplayToggle, PressDenied, ReplayDeleteGhost,
            Cricket, Monkey, SpookyBird, WeirdBird, FunnyBird, CheepBird, SingBird,
            Lion, Cicada1, Cicada2, Cicada3, CicadaMany, Flies,
            Drop1, Drop2, Drop3, Drop4, DropDelay1, DropDelay2, DropDelay3, DropDelay4,
            Stone1, Stone2, Stone3, Stone4,
            Dissolve
        }

        public string PathForSoundFile(SoundFile file)
        {
            switch(file)
            {
                case SoundFile.StartScreen: return SoundConstants.Path.StartScreen;
                case SoundFile.Outro: return SoundConstants.Path.Outro;
                case SoundFile.Credits: return SoundConstants.Path.Credits;
                case SoundFile.SoundtrackPart1: return SoundConstants.Path.Soundtrack.Part1;
                case SoundFile.SoundtrackPart3Savanna: return SoundConstants.Path.Soundtrack.Part3Savanna;
                case SoundFile.SoundtrackPart4Cave: return SoundConstants.Path.Soundtrack.Part4Cave;
                case SoundFile.SoundtrackPart5Final: return SoundConstants.Path.Soundtrack.Part5Final;
                case SoundFile.FilteredPart1: return SoundConstants.Path.Filtered.Part1;
                case SoundFile.FilteredPart3Savanna: return SoundConstants.Path.Filtered.Part3Savanna;
                case SoundFile.FilteredPart4Cave: return SoundConstants.Path.Filtered.Part4Cave;
                case SoundFile.FilteredPart5Final: return SoundConstants.Path.Filtered.Part5Final;
                case SoundFile.GhostPart1: return SoundConstants.Path.Ghost.Part1;
                case SoundFile.GhostPart3Savanna: return SoundConstants.Path.Ghost.Part3Savanna;
                case SoundFile.GhostPart4Cave: return SoundConstants.Path.Ghost.Part4Cave;
                case SoundFile.GhostPart5Final: return SoundConstants.Path.Ghost.Part5Final;
                case SoundFile.PipeBadBottom: return SoundConstants.Path.PipeBadBottom;
                case SoundFile.PipeGoodBottom: return SoundConstants.Path.PipeGoodBottom;
                case SoundFile.PipeBadMiddle: return SoundConstants.Path.PipeBadMiddle;
                case SoundFile.PipeGoodMiddle: return SoundConstants.Path.PipeGoodMiddle;
                case SoundFile.PipeBadTop: return SoundConstants.Path.PipeBadTop;
                case SoundFile.PipeGoodTop: return SoundConstants.Path.PipeGoodTop;
                case SoundFile.RockBumping: return SoundConstants.Path.RockBumping;
                case SoundFile.RockImpact: return SoundConstants.Path.RockImpact;
                case SoundFile.SpiritCaged: return SoundConstants.Path.SpiritsCaged;
                case SoundFile.SpiritFreed: return SoundConstants.Path.SpiritsFreed;
                case SoundFile.Death: return SoundConstants.Path.Death;
                case SoundFile.BranchCreak: return SoundConstants.Path.BranchCreak;
                case SoundFile.BranchBreak: return SoundConstants.Path.BranchBreak;
                case SoundFile.ReplayStart: return SoundConstants.Path.Buttons.ReplayStart;
                case SoundFile.RecordStart: return SoundConstants.Path.Buttons.RecordStart;
                case SoundFile.ReplayDeleteGhost: return SoundConstants.Path.Buttons.ReplayDismissGhost;
                case SoundFile.ReplayToggle: return SoundConstants.Path.Buttons.ReplayToggle;
                case SoundFile.PressDenied: return SoundConstants.Path.Buttons.PressDenied;
                case SoundFile.StoneClose: return SoundConstants.Path.StoneClose;
                case SoundFile.StoneOpen: return SoundConstants.Path.StoneOpen;
                case SoundFile.CableCarRunning: return SoundConstants.Path.CableCarRunning;
                case SoundFile.CableCarStarting: return SoundConstants.Path.CableCarStarting;
                case SoundFile.Cricket: return SoundConstants.Path.Cricket;
                case SoundFile.Monkey: return SoundConstants.Path.Monkey;
                case SoundFile.WeirdBird: return SoundConstants.Path.WeirdBird;
                case SoundFile.SpookyBird: return SoundConstants.Path.SpookyBird;
                case SoundFile.FunnyBird: return SoundConstants.Path.FunnyBird;
                case SoundFile.CheepBird: return SoundConstants.Path.CheepBird;
                case SoundFile.SingBird: return SoundConstants.Path.SingBird;
                case SoundFile.Lion: return SoundConstants.Path.Lion;
                case SoundFile.Cicada1: return SoundConstants.Path.Cicada1;
                case SoundFile.Cicada2: return SoundConstants.Path.Cicada2;
                case SoundFile.Cicada3: return SoundConstants.Path.Cicada3;
                case SoundFile.CicadaMany: return SoundConstants.Path.CicadaMany;
                case SoundFile.Flies: return SoundConstants.Path.Flies;
                case SoundFile.Drop1: return SoundConstants.Path.Drop1;
                case SoundFile.Drop2: return SoundConstants.Path.Drop2;
                case SoundFile.Drop3: return SoundConstants.Path.Drop3;
                case SoundFile.Drop4: return SoundConstants.Path.Drop4;
                case SoundFile.DropDelay1: return SoundConstants.Path.DropDelay1;
                case SoundFile.DropDelay2: return SoundConstants.Path.DropDelay2;
                case SoundFile.DropDelay3: return SoundConstants.Path.DropDelay3;
                case SoundFile.DropDelay4: return SoundConstants.Path.DropDelay4;
                case SoundFile.Stone1: return SoundConstants.Path.Stone1;
                case SoundFile.Stone2: return SoundConstants.Path.Stone2;
                case SoundFile.Stone3: return SoundConstants.Path.Stone3;
                case SoundFile.Stone4: return SoundConstants.Path.Stone4;
                case SoundFile.Dissolve: return SoundConstants.Path.Dissolve;
                default:
                    //RELEASE
                    throw new Exception("No path defined for " + file);
            }
        }

        private SoundFile? SoundFileForMusicRegionId(string musicRegionId)
        {
            switch (musicRegionId)
            {
                case "part1":
                    return SoundFile.SoundtrackPart1;
                case "part3":
                    return SoundFile.SoundtrackPart3Savanna;
                case "part4":
                    return SoundFile.SoundtrackPart4Cave;
                case "part5":
                    return SoundFile.SoundtrackPart5Final;
                default:
                    //RELEASE
                    throw new Exception("No path defined for " + musicRegionId);
                    return null;
            }
        }
        private SoundFile? BackgroundSoundFileForMusicRegionId(string musicRegionId)
        {
            switch (musicRegionId)
            {
                case "part1":
                    return SoundFile.FilteredPart1;
                case "part3":
                    return SoundFile.FilteredPart3Savanna;
                case "part4":
                    return SoundFile.FilteredPart4Cave;
                case "part5":
                    return SoundFile.FilteredPart5Final;
                default:
                    //RELEASE
                    throw new Exception("No path defined for " + musicRegionId);
                    return null;
            }
        }
        
        private SoundFile? GhostSoundFileForMusicRegionId(string musicRegionId)
        {
            switch (musicRegionId)
            {
                case "part1":
                    return SoundFile.GhostPart1;
                case "part3":
                    return SoundFile.GhostPart3Savanna;
                case "part4":
                    return SoundFile.GhostPart4Cave;
                case "part5":
                    return SoundFile.GhostPart5Final;
                default:
                    //RELEASE
                    throw new Exception("No path defined for " + musicRegionId);
                    return null;
            }
        }

        public void Unload(string id)
        {
            if (Sounds.ContainsKey(id))
            {
                Sounds[id].SoundEffectInstance.Dispose();
                SoundEffectDefinition value;
                Sounds.TryRemove(key: id, value: out value);
            }
        }
    }

    public interface DistanceDelegate
    {
        Vector2 DistanceFromCameraToPoint(Coordinate pointInDisplay);
        Vector2 DistanceFromCameraCenterToPoint(Coordinate pointInDisplay);
        Coordinate CameraCenterInDisplay();
        bool IsVisible(Coordinate pointInDisplay, float scaleBoundingRectangel = 1);
    }

    public interface MusicRegionDelegate
    {
        string MusicRegionObjectIdForMainCharacter();
    }
    public interface ReplayDelegate
    {
        bool IsRecording();
        bool GhostExists();
    }
}
