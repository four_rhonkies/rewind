﻿/**
* Copyright 2017 Niclas Scheuing, Per Näslung, Tunay Bora, Xiaowei Zeng
* This file is part of REWIND.
*
* REWIND is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* REWIND is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
* GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with REWIND.If not, see<http://www.gnu.org/licenses/>.
*/
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using static team8.Sound.SoundManager;

namespace team8.Sound
{
    using Coordinate = Vector2;
    public class AmbientSoundEngine
    {
        public class AmbientSound
        {
            public SoundFile SoundFile;
            private double PauseDuration;
            private double Variance;
            private float Speed;
            private Coordinate StartPoint;
            private Vector2 MovementDirection;
            private double NextPlayAtSecond;
            private double LastPlayedAtSecond;
            private Random RandomGenerator;
            public string Id;
            private float Volume;

            public AmbientSound(SoundFile soundFile, double pauseDurationInSeconds, double varianceInSeconds, float speed, float volume=1)
            {
                this.SoundFile = soundFile;
                this.PauseDuration = pauseDurationInSeconds;
                this.Variance = varianceInSeconds;
                this.Speed = speed;
                this.Id = soundFile.ToString();
                this.RandomGenerator = new Random(Id.GetHashCode()+DateTime.Now.Millisecond);
                this.Volume = volume;
                this.NextPlayAtSecond = PauseDuration + (RandomGenerator.NextDouble() - 0.5) * 2 * Variance;
            }

 
            public bool ShouldPlaySound(GameTime gameTime)
            {
                return gameTime.TotalGameTime.TotalSeconds >= NextPlayAtSecond;
            }
            
            public void Reset(GameTime gameTime, Coordinate cameraPositionInDisplay, bool usePause=true)
            {
                var startX = RandomGenerator.Next(minValue: -SoundConstants.MaxDistanceToCameraForAmbient.X, maxValue: SoundConstants.MaxDistanceToCameraForAmbient.X);
                var startY = RandomGenerator.Next(minValue: -SoundConstants.MaxDistanceToCameraForAmbient.Y, maxValue: SoundConstants.MaxDistanceToCameraForAmbient.Y);
                this.StartPoint = new Coordinate(startX, startY)+cameraPositionInDisplay;

                var movementDirection = new Vector2((float)RandomGenerator.NextDouble()-0.5f, (float)RandomGenerator.NextDouble()-0.5f);
                movementDirection.Normalize();
                movementDirection = movementDirection * Speed * (float)RandomGenerator.NextDouble();
                this.MovementDirection = movementDirection;
                LastPlayedAtSecond = gameTime.TotalGameTime.TotalSeconds;
                if (usePause)
                {
                    this.NextPlayAtSecond = gameTime.TotalGameTime.TotalSeconds + PauseDuration + (RandomGenerator.NextDouble() - 0.5) * 2 * Variance;
                }
                else
                {
                    this.NextPlayAtSecond = gameTime.TotalGameTime.TotalSeconds +  RandomGenerator.NextDouble() * Variance;
                }
            }

            public void Play(SoundManager soundManager)
            {
                soundManager.PlaySound(file: SoundFile, id: Id, positionDelegate: UpdatePosition, initialVolume: Configuration.Sound.AmbientVolume*Volume);
            }
            private Coordinate? UpdatePosition(GameTime gameTime)
            {
                if (gameTime == null)
                {
                    return StartPoint;
                }
                return StartPoint + MovementDirection * (float)(gameTime.TotalGameTime.TotalSeconds-LastPlayedAtSecond);
            }


        }


        private List<AmbientSound> AmbientSounds;
        private SoundManager SoundManager;
        private string CurrentArea;

        public AmbientSoundEngine(SoundManager soundManager)
        {
            this.SoundManager = soundManager;
            //InitJungle();
        }


        private void InitJungle()
        {

            this.AmbientSounds = new List<AmbientSound>();
            this.AmbientSounds.Add(new AmbientSound(soundFile: SoundFile.Cricket, pauseDurationInSeconds: 10, varianceInSeconds: 5, speed: 0.8f, volume: 0.6f));//duration 
            this.AmbientSounds.Add(new AmbientSound(soundFile: SoundFile.Monkey, pauseDurationInSeconds: 20, varianceInSeconds: 20, speed: 100, volume: 0.6f));//duration 3s
            this.AmbientSounds.Add(new AmbientSound(soundFile: SoundFile.SpookyBird, pauseDurationInSeconds: 30, varianceInSeconds: 20, speed: 0, volume: 0.6f));//duration 3s
            this.AmbientSounds.Add(new AmbientSound(soundFile: SoundFile.WeirdBird, pauseDurationInSeconds: 20, varianceInSeconds: 10, speed: 0, volume: 0.6f));//duration 2s
            this.AmbientSounds.Add(new AmbientSound(soundFile: SoundFile.FunnyBird, pauseDurationInSeconds: 20, varianceInSeconds: 10, speed: 0, volume: 0.6f));//duration 3s
            this.AmbientSounds.Add(new AmbientSound(soundFile: SoundFile.CheepBird, pauseDurationInSeconds: 20, varianceInSeconds: 10, speed: 0, volume: 0.6f));//duration 6s
            this.AmbientSounds.Add(new AmbientSound(soundFile: SoundFile.SingBird, pauseDurationInSeconds: 30, varianceInSeconds: 40, speed: 0, volume: 0.6f)); //duration 17s
        }
        private void InitSavanna()
        {
            this.AmbientSounds = new List<AmbientSound>();
            this.AmbientSounds.Add(new AmbientSound(soundFile: SoundFile.Lion, pauseDurationInSeconds: 40, varianceInSeconds: 40, speed: 0)); //duration 8s
            this.AmbientSounds.Add(new AmbientSound(soundFile: SoundFile.Cicada1, pauseDurationInSeconds: 20, varianceInSeconds: 20, speed: 0, volume: 0.6f)); //duration 9
            this.AmbientSounds.Add(new AmbientSound(soundFile: SoundFile.Cicada2, pauseDurationInSeconds: 20, varianceInSeconds: 20, speed: 0, volume: 0.6f)); //duration 12
            this.AmbientSounds.Add(new AmbientSound(soundFile: SoundFile.Cicada3, pauseDurationInSeconds: 20, varianceInSeconds: 20, speed: 0, volume: 0.6f)); //duration 15
            this.AmbientSounds.Add(new AmbientSound(soundFile: SoundFile.CicadaMany, pauseDurationInSeconds: 15, varianceInSeconds: 10, speed: 0, volume: 0.6f)); //duration 25
            this.AmbientSounds.Add(new AmbientSound(soundFile: SoundFile.Flies, pauseDurationInSeconds: 30, varianceInSeconds: 10, speed: 0, volume: 0.6f)); //duration 3
        }
        private void InitCave()
        {
            this.AmbientSounds = new List<AmbientSound>();
            this.AmbientSounds.Add(new AmbientSound(soundFile: SoundFile.Drop1, pauseDurationInSeconds: 40, varianceInSeconds: 40, speed: 0)); //duration 1s
            this.AmbientSounds.Add(new AmbientSound(soundFile: SoundFile.Drop2, pauseDurationInSeconds: 40, varianceInSeconds: 40, speed: 0)); //duration 1s
            this.AmbientSounds.Add(new AmbientSound(soundFile: SoundFile.Drop3, pauseDurationInSeconds: 40, varianceInSeconds: 40, speed: 0)); //duration 1s
            this.AmbientSounds.Add(new AmbientSound(soundFile: SoundFile.Drop4, pauseDurationInSeconds: 40, varianceInSeconds: 40, speed: 0)); //duration 1s
            this.AmbientSounds.Add(new AmbientSound(soundFile: SoundFile.DropDelay1, pauseDurationInSeconds: 40, varianceInSeconds: 40, speed: 0)); //duration 2s
            this.AmbientSounds.Add(new AmbientSound(soundFile: SoundFile.DropDelay2, pauseDurationInSeconds: 40, varianceInSeconds: 40, speed: 0)); //duration 2s
            this.AmbientSounds.Add(new AmbientSound(soundFile: SoundFile.DropDelay3, pauseDurationInSeconds: 40, varianceInSeconds: 40, speed: 0)); //duration 2s
            this.AmbientSounds.Add(new AmbientSound(soundFile: SoundFile.DropDelay4, pauseDurationInSeconds: 40, varianceInSeconds: 40, speed: 0)); //duration 2s
            this.AmbientSounds.Add(new AmbientSound(soundFile: SoundFile.Stone1, pauseDurationInSeconds: 60, varianceInSeconds: 60, speed: 0)); //duration 2s
            this.AmbientSounds.Add(new AmbientSound(soundFile: SoundFile.Stone2, pauseDurationInSeconds: 60, varianceInSeconds: 60, speed: 0)); //duration 2s
            this.AmbientSounds.Add(new AmbientSound(soundFile: SoundFile.Stone3, pauseDurationInSeconds: 60, varianceInSeconds: 60, speed: 0)); //duration 2s
            this.AmbientSounds.Add(new AmbientSound(soundFile: SoundFile.Stone4, pauseDurationInSeconds: 60, varianceInSeconds: 60, speed: 0)); //duration 2s
        }

        private void UnloadSounds(SoundManager manager)
        {
            if (AmbientSounds==null)
            {
                return;
            }
            foreach (var ambientSound in AmbientSounds)
            {
                manager.Unload(ambientSound.Id);
            }
        }
        private void SetArea(string area, SoundManager manager)
        {
            UnloadSounds(manager);
            CurrentArea = area;
            switch(area)
            {
                case "part1":
                    InitJungle();
                    break;
                case "part3":
                    InitSavanna();
                    break;
                case "part4":
                case "part5":
                    InitCave();
                    break;

            }
        }

        public void Update(GameTime gameTime, Coordinate cameraPositionInDisplay, string area)
        {
            if(area != CurrentArea)
            {
                SetArea(area: area, manager: SoundManager);
                foreach (var ambientSound in AmbientSounds)
                {
                    ambientSound.Reset(gameTime: gameTime, cameraPositionInDisplay: cameraPositionInDisplay, usePause: false);
                }
            }
            foreach (var ambientSound in AmbientSounds)
            {
                if (ambientSound.ShouldPlaySound(gameTime: gameTime))
                {
                    ambientSound.Play(soundManager: SoundManager);
                    ambientSound.Reset(gameTime: gameTime, cameraPositionInDisplay: cameraPositionInDisplay);
                }
            }
        }

    }
}
