﻿/**
* Copyright 2017 Niclas Scheuing, Per Näslung, Tunay Bora, Xiaowei Zeng
* This file is part of REWIND.
*
* REWIND is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* REWIND is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
* GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with REWIND.If not, see<http://www.gnu.org/licenses/>.
*/
namespace team8
{
    struct Configuration
    {
        public static bool RenderObjectVertices = false;
        public static readonly bool AllowRenderObjectVertices = false;
        public static bool ShowInstructions = false;
        public static bool FullScreen = true;
        public struct Sound
        {
            public static bool Mute = false;
            private static float MusicVolume = 0.7f;//0.7f
            public static float MusicVolumeNormal = 0.75f * MusicVolume;
            public static float MusicVolumeLow = 0.55f * MusicVolume;
            public static float RecordingTrackVolume = 0.5f * MusicVolume;
            public static float GhostTrackVolume = 0.5f * MusicVolume;
            public static float OutroVolume = 0.8f;
            public static float CreditsVolume = 0.8f;
            public static float CharacterEffectVolume = 0.4f;
            public static float StepSoundVolume = 0.2f;
            public static float GameOverVolume = 0.6f;
            public static float BranchVolume = 0.7f;
            public static float StartScreenMusicVolume = 0.7f * MusicVolume;
            public static float EndScreenMusicVolume = 0.75f * MusicVolume;
            public static float ButtonDeniedVolume = 0.5f;
            public static float ReplayButtonVolume = 0.5f;
            public static float DoorVolume = 1f;
            public static float DefaultAmbientVolume = 0.4f;
            public static float ReducedAmbientVolume = 0.2f;
            public static float AmbientVolume = DefaultAmbientVolume;
        }
        public struct Camera
        {
            public static bool FreeCamera = false;
            public static float MaxZoom = 0.98f;
            public static float MinZoom = 0.7f;
            public static float TotalZoom = 0.25f;
            public static float MaxPanSpeed = 5f;
            public static float DefaultMaxPanSpeed = 5f;
            public static void ResetZoom()
            {
                MaxZoom = CameraConstants.MaxZoom;
                MinZoom = CameraConstants.MinZoom;
                TotalZoom = CameraConstants.TotalZoom;
            }
        }   
    }

}
