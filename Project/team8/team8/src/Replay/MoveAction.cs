﻿/**
* Copyright 2017 Niclas Scheuing, Per Näslung, Tunay Bora, Xiaowei Zeng
* This file is part of REWIND.
*
* REWIND is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* REWIND is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
* GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with REWIND.If not, see<http://www.gnu.org/licenses/>.
*/
using Microsoft.Xna.Framework;
using System.Diagnostics;
using team8.Control;

namespace team8.Replay
{
    using Coordinate = Microsoft.Xna.Framework.Vector2;
    class MoveAction: ReplayAction
    {
        private Coordinate? From;
        private Coordinate? To;


        public void StartAction(Coordinate position, GameTime gameTime)
        {
           this.From = position;
        }

        public void EndAction(Coordinate position, GameTime gameTime)
        {
            this.To = position;
        }

        public void Replay(ReplayMovementController replayMovementController, GameTime gameTime)
        {
            if (!this.To.HasValue)
            {
                Debug.WriteLine("MoveAction is replayed but has no To coordinates set.");
            }
            replayMovementController.MoveToPosition(toPosition: this.To.Value, gameTime: gameTime);
        }

        public bool HasCompleted(ReplayMovementController replayMovementController, GameTime gameTime)
        {
            if (!this.To.HasValue)
            {
                Debug.WriteLine("MoveAction is replayed but has no To coordinates set.");
            }
            return replayMovementController.HasReachedPosition(toPosition: this.To.Value);
        }


        override public string ToString()
        {
            return "MoveAction "+(From.HasValue ? From.ToString() : "_") + "->" + (To.HasValue ? To.ToString() : "_");
        }

      
    }


}
