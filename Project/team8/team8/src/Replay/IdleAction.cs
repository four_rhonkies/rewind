﻿/**
* Copyright 2017 Niclas Scheuing, Per Näslung, Tunay Bora, Xiaowei Zeng
* This file is part of REWIND.
*
* REWIND is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* REWIND is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
* GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with REWIND.If not, see<http://www.gnu.org/licenses/>.
*/
using Microsoft.Xna.Framework;
using team8.Control;

namespace team8.Replay
{
    using Coordinate = Microsoft.Xna.Framework.Vector2;
    public class IdleAction : ReplayAction
    {
        //The duration of the action in s
        private double? duration;
        private double startedRecording;
        private double? startedReplaying;
        //private Coordinate startPosition;

        public bool HasCompleted(ReplayMovementController replayMovementController, GameTime gameTime)
        {
            return gameTime.TotalGameTime.TotalMilliseconds - startedReplaying >= duration;
        }

        public bool HasCompletedAndIsStable(ReplayMovementController replayMovementController, GameTime gameTime)
        {
            return HasCompleted(replayMovementController: replayMovementController, gameTime: gameTime);// && replayMovementController.HasStopped();
        }

        public virtual void Replay(ReplayMovementController replayMovementController, GameTime gameTime)
        {
            if (!this.startedReplaying.HasValue)
            {
                this.startedReplaying = gameTime.TotalGameTime.TotalMilliseconds;
            }
            replayMovementController.SlowDown(gameTime: gameTime);

        }

        public void EndAction(Coordinate position, GameTime gameTime)
        {
            duration = gameTime.TotalGameTime.TotalMilliseconds - this.startedRecording;
        }

        public void StartAction(Coordinate position, GameTime gameTime)
        {
            this.startedRecording = gameTime.TotalGameTime.TotalMilliseconds;
            //this.startPosition = position;
        }

        public override string ToString()
        {
            return "IdleAction. Duration: " + (duration.HasValue ? ""+duration : "-");
        }
    }
}
