﻿/**
* Copyright 2017 Niclas Scheuing, Per Näslung, Tunay Bora, Xiaowei Zeng
* This file is part of REWIND.
*
* REWIND is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* REWIND is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
* GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with REWIND.If not, see<http://www.gnu.org/licenses/>.
*/
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using team8.Control;

namespace team8.Replay
{
    using Coordinate = Microsoft.Xna.Framework.Vector2;

    public interface ReplayAction
    {   
        /*Start recording this action*/
        void StartAction(Coordinate position, GameTime gameTime);
        /*End recording this action*/
        void EndAction(Coordinate position, GameTime gameTime);
        /*Replay the recorded action using the replayMovementController*/
        void Replay(ReplayMovementController replayMovementController, GameTime gameTime);
        /*Returns true if the action has completed and false if not completed yet*/
        bool HasCompleted(ReplayMovementController replayMovementController, GameTime gameTime);
    }
}
