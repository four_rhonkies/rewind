﻿/**
* Copyright 2017 Niclas Scheuing, Per Näslung, Tunay Bora, Xiaowei Zeng
* This file is part of REWIND.
*
* REWIND is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* REWIND is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
* GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with REWIND.If not, see<http://www.gnu.org/licenses/>.
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using team8.Control;

namespace team8.Replay
{
    class JumpAction : ReplayAction
    {
        public void EndAction(Vector2 position, GameTime gameTime)
        {
            //TODO implement
            
        }

        public bool HasCompleted(ReplayMovementController replayMovementController, GameTime gameTime)
        {
            return !replayMovementController.ControlledTarget.IsOnGround;
        }

        public void Replay(ReplayMovementController replayMovementController, GameTime gameTime)
        {
            replayMovementController.Jump();
        }

        public void StartAction(Vector2 position, GameTime gameTime)
        {
            //TODO implement
        }

        public override string ToString()
        {
            return "JumpAction";
        }
    }
}
