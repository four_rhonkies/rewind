﻿/**
* Copyright 2017 Niclas Scheuing, Per Näslung, Tunay Bora, Xiaowei Zeng
* This file is part of REWIND.
*
* REWIND is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* REWIND is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
* GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with REWIND.If not, see<http://www.gnu.org/licenses/>.
*/
using Microsoft.Xna.Framework;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using team8.Control;
using team8.Physics;
using team8.Rendering;
using Microsoft.Xna.Framework.Content;
using static team8.Replay.SpecialAction;
using FarseerPhysics;
using System;
using static team8.Physics.PhysicsObject;
using team8.Sound;

namespace team8.Replay
{
    using Coordinate = Vector2;

    /**
     * The states the ReplayEngine can be in.
     **/
    public enum ReplayMode
    {
        Recording, Replaying, None
    }
    /**
     * Handles the replay. It records actions and replays them.
     * The typical program flow is:
     * 1. StartRecording() is called.
     * 2. The RecordingMovementController forwards all movements to the ReplayEngine while it is recording.
     * 3. StopRecording() is called
     * 4. StartReplaying() is called
     * 5. The GameMain calls Replay() in every update, the replay engine performs the replay actions.
     * 6. Replay ends
     **/
    public class ReplayEngine: ReplayDelegate, CharacterResetDelegate
    {
        //private MovableObject mainCharacter;
        private Queue<ReplayAction> actionQueue;
        private ReplayAction recordingAction;
        private ReplayAction replayingAction;
        private Coordinate respawnPosition;
        private Direction RespawnFacingDirection;
        private ReplayMode replayMode;
        private Renderer renderer;
        private PhysicalWorld World;

        private ControlledCharacterDelegate ControlledCharacterDelegate;

        private ReplayMovementController replayMovementController;

        public ReplayMode ReplayMode
        {
            get
            {
                return this.replayMode;
            }
        }
        private static ReplayEngine Instance;

        public RespawnPoint RespawnPoint;

        public Action OnGhostAppears;
        public Action OnGhostDisappears;
        public Action OnStartRecording;
        public Action OnEndRecording;
        public delegate void CharacterSelectionDelegate(MainCharacter chosenCharacter);
        public CharacterSelectionDelegate OnCharacterSelection;

        public ReplayEngine(Renderer renderer, ContentManager content, PhysicalWorld world)
        {
            this.World = world;
            this.actionQueue = new Queue<ReplayAction>();
            this.replayMode = ReplayMode.None;
            this.renderer = renderer;
            Instance = this;

            //respawn point
            this.RespawnPoint = new RespawnPoint(contentManager: content);
            renderer.AddRenderObjectToActionLayer(RespawnPoint);
        }

        public void SetControlledCharacterDelegate(ControlledCharacterDelegate controlledCharacterDelegate)
        {
            this.ControlledCharacterDelegate = controlledCharacterDelegate;
        }
        /*---- Replay----*/
        /**
         * Starts the replay mode. The replay engine will now lis
         **/
        public bool StartReplaying(MainCharacter originalObject)
        {
            
            //If replay already started, don't do anything
            if (this.replayMode == ReplayMode.Replaying)
            {
                return false;
            }

            //InputController.Instance.Vibrate(leftMotor: 0.1f, rightMotor: 0.1f, duration: 100);

            //Call delegate
            OnGhostAppears?.Invoke();

            RespawnPoint.Hidden = true;

            //Reload last world state
            World.StartedReplaying();
            
            var projectedRespawnPoint = World.GetPointOnGroundOrCollision(this.respawnPosition);

            //Create the copy/ghost of this character
            MainCharacter copiedObject = originalObject.GetGhostObject(initalPosition: respawnPosition, initialFacingDirection: RespawnFacingDirection);
            //Add ghost to renderer
            this.renderer.AddCharacterToCharacterLayer(character: copiedObject);
            //Add ghost to world
            this.World.SetGhostCharacter(copiedObject);

            //Create new Movement controller that handles replay movements
            replayMovementController = new ReplayMovementController(controlledTarget: copiedObject);
            //this.animate = new AnimateCharacter(movementController: replayMovementController, content: content, red: true);

            //reset object to respawn point
            //originalObject.SetToPosition(position: projectedRespawnPoint);
            copiedObject.SetSpawnPoint(position: projectedRespawnPoint, facingDirection: RespawnFacingDirection);
            originalObject.SetSpawnPoint(position: projectedRespawnPoint, facingDirection: RespawnFacingDirection);

            //get the first replay action
            if (HasNextReplayAction())
            {
                this.replayingAction = NextReplayAction();
                this.replayMode = ReplayMode.Replaying;
            }
            else
            {
                Debug.WriteLine("Could not start replaying, no actions in queue.");
            }
            return true;
        }

        /**
         * Performs the replay actions.
         * This should be called periodically with the timeElapsed since the last call given as gameTime.
         * This can only be called while the replay mode is set to replaying.
         **/
        public void Replay(GameTime gameTime)
        {
            if (this.replayMode != ReplayMode.Replaying)
            {
                Debug.WriteLine("Tried replaying while not in replay mode. Returning.");
                return;
            }

            //If no more action is available, end replay
            if (!HasNextReplayAction() && (actionQueue.First() is IdleAction) && ((IdleAction) actionQueue.First()).HasCompletedAndIsStable(replayMovementController: this.replayMovementController, gameTime: gameTime) )
            {
                StopReplaying();
                return;
            }
            double forSeconds = gameTime.ElapsedGameTime.TotalSeconds;

            //Ask the action whether it has completed and get the next action if there is any
            var currentActionCompleted = this.replayingAction.HasCompleted(replayMovementController: replayMovementController, gameTime: gameTime);
            if (currentActionCompleted && HasNextReplayAction())
            {
                this.replayingAction = NextReplayAction();
            }
            else if (currentActionCompleted && !HasNextReplayAction())
            {
                this.replayingAction = NextReplayAction();
                StopReplaying();
            }

            //Perform an iteration of replaying
            this.replayingAction.Replay(replayMovementController: replayMovementController, gameTime: gameTime);
        }
        public bool CanDismissCharacter()
        {
            return (World.GhostCharacter != null && World.GhostCharacter != null);
        }
        public void ChooseObjectAndDismissOther(bool dismissGhost)
        {
            if (!CanDismissCharacter())
            {
                return;
            }
            var originalObject = ControlledCharacterDelegate.ControlledCharacter();
            //Stop replaying
            StopReplaying();            

            OnGhostDisappears?.Invoke();
            replayMovementController.Dispose();
            var ghostObject = replayMovementController.GhostableTarget;
            
            //animate = null;
            //Both objects must exist in order to remove one
            //Both objects must not be the same. This is possible when the ghost has been unghosted before.
            if (World.GhostCharacter == null || World.GhostCharacter == null)
            {
                Debug.WriteLine("Tried removing a character, while there was only one in the world");
                return;
            }
            if(dismissGhost)
            {
                ghostObject.Remove(renderer: this.renderer);
                OnCharacterSelection?.Invoke(originalObject);
            }
            else
            {
                originalObject.Remove(renderer: this.renderer, fromRenderingOnly: true);
                ghostObject.UnghostObject();
                OnCharacterSelection?.Invoke(ghostObject);
            }
        }

        public void StopReplaying()
        {

            //If not currently replaying, don't do anything
            if (this.replayMode != ReplayMode.Replaying)
            {
                return;
            }
            this.replayMode = ReplayMode.None;
            World.StopReplaying();
        }
 
        private ReplayAction NextReplayAction()
        {
            return this.actionQueue.Dequeue();
        }
        private bool HasNextReplayAction()
        {
            return this.actionQueue.Count > 1;
        }
        /*---- Records----*/
        /**
         * Ends the recording of the previous action, records a new action and keeps track of all recorded actions. 
         **/
        public bool NewAction(Coordinate position, GameTime gameTime)
        {
            if (this.replayMode != ReplayMode.Recording)
            {
                return false;
            }
            //Ending the previous action
            recordingAction.EndAction(position: position, gameTime: gameTime);
            actionQueue.Enqueue(recordingAction);
            return true;
        }

        public void NewMoveAction(Coordinate position, GameTime gameTime, bool mightBeOldAction = false)
        {
            //If the current action is a MoveAction and we assume it might still be the same action as before, don't do anything.
            //This is needed, because a move/jump/move combination can not be recognized by the direction, but only with the previous action.
            if (mightBeOldAction && recordingAction is MoveAction)
            {
                return;
            }
            if (NewAction(position: position, gameTime: gameTime))
            {
                //Starting the new action
                recordingAction = new MoveAction();
                recordingAction.StartAction(position: position, gameTime: gameTime);
            }
        }

        public void NewJumpAction(Coordinate position, GameTime gameTime)
        {
            if (NewAction(position: position, gameTime: gameTime))
            {
                recordingAction = new JumpAction();
                recordingAction.StartAction(position: position, gameTime: gameTime);
            }
        }

        public void NewIdleAction(Coordinate position, GameTime gameTime)
        {
            if (NewAction(position: position, gameTime: gameTime))
            {
                recordingAction = new IdleAction();
                recordingAction.StartAction(position: position, gameTime: gameTime);
            }
        }

        /**
         * Switches the replay engine into recording mode. It is now taking new actions through NewMoveAction
         **/
        public bool StartRecording(Coordinate position, GameTime gameTime, Direction facingDirection)
        {
            //If recording already started, don't do anything
            if (this.replayMode == ReplayMode.Recording)
            {
                return false;
            }

            OnStartRecording?.Invoke();

            //save current state of the world
            World.StartedRecording();


            this.actionQueue = new Queue<ReplayAction>();
            this.respawnPosition = position;
            this.RespawnFacingDirection = facingDirection;
            var optionalPoint = World.GroundOrCollisionBelowPosition(position: position, distance: 100);
            Coordinate respawnPontPosition = position;
            if (optionalPoint.HasValue)
            {
                respawnPontPosition = optionalPoint.Value.IntersectionPoint;
            }
            this.RespawnPoint.SetPosition(position: ConvertUnits.ToDisplayUnits(respawnPontPosition + new Vector2(0, -0.8f)));
            this.RespawnPoint.Hidden = false;

            this.replayMode = ReplayMode.Recording;
            recordingAction = new IdleAction();
            recordingAction.StartAction(position: position, gameTime: gameTime);
            return true;
        }

        /**
         * Ends the last recorded action and switches into ReplayMode.None. No more actions are recorded.
         **/
        public void StopRecording(Coordinate position, GameTime gameTime)
        {
            //If we're not currently recording, don't do anything
            if (this.replayMode != ReplayMode.Recording)
            {
                return;
            }

            OnEndRecording?.Invoke();

            this.replayMode = ReplayMode.None;
            //Ending the previous action
            recordingAction.EndAction(position: position, gameTime: gameTime);
            actionQueue.Enqueue(recordingAction);
            //Add a final Idle action to make character stand still in the end
            actionQueue.Enqueue(new IdleAction());
        }

        public void AbortRecording()
        {
            //If we're not currently recording, don't do anything
            if (this.replayMode != ReplayMode.Recording)
            {
                return;
            }

            OnEndRecording?.Invoke();

            this.replayMode = ReplayMode.None;

            recordingAction = new IdleAction();
            actionQueue.Enqueue(new IdleAction());

            RespawnPoint.Hidden = true;

            //World.StoppedRecording();

        }

        public static void AddSpecialAction(GameTime gameTime, Coordinate position, SpecialActionDelegate action)
        {
            if (Instance.NewAction(position: position, gameTime: gameTime))
            {
                Instance.recordingAction = new SpecialAction(action: action);
                Instance.recordingAction.StartAction(position: position, gameTime: gameTime);
            }
        }

        protected string ActionQueueString()
        {
            var result = "";
            foreach (ReplayAction action in this.actionQueue)
            {
                result = result + "\n\t" + action.ToString();
            }
            return result;
        }

        public bool ReadyToRecord()
        {
            //Can only start recording, when in None state and the ghost has been dismissed
            return this.replayMode == ReplayMode.None && World.GhostCharacter == null;
        }
        public bool ReadyToReplay()
        {
            //can only start replaying when in recording mode.
            return this.replayMode == ReplayMode.Recording;
        }

        public void Reset()
        {
            switch (ReplayMode)
            {
                case ReplayMode.None: break;
                case ReplayMode.Recording:
                    //When reset during recording, the main character has died and no ghost exists.
                    AbortRecording();
                    break;
                case ReplayMode.Replaying:
                    //When resetting during replaying, the ghost or main character have died. Stop replaying
                    StopReplaying();
                    break;
            }
            
        }

        public bool IsRecording()
        {
            return this.replayMode == ReplayMode.Recording;
        }

        public bool GhostExists()
        {
            return World.GhostCharacter != null;
        }

        public void RespawnAtLastSpirit()
        {
            //Always dismiss ghost when respawning manually
            SelectMainCharacter();
            World.RespawnAtLastSpirit();
        }

        public void SelectMainCharacter()
        {
            Reset();
            ChooseObjectAndDismissOther(dismissGhost: true);
            World.EndChoosing();
        }

        public void CharacterDied(bool isMainCharacter)
        {
            InputController.Instance.Vibrate();
            Reset();
            ChooseObjectAndDismissOther(dismissGhost: !isMainCharacter);
        }

        public void OnGhostOutsideCamera()
        {
            SelectMainCharacter();
        }

    }

    public interface ControlledCharacterDelegate
    {
        MainCharacter ControlledCharacter();
    }
}
