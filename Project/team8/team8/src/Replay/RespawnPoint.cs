﻿/**
* Copyright 2017 Niclas Scheuing, Per Näslung, Tunay Bora, Xiaowei Zeng
* This file is part of REWIND.
*
* REWIND is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* REWIND is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
* GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with REWIND.If not, see<http://www.gnu.org/licenses/>.
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using team8.Animation;
using team8.Rendering;
using Microsoft.Xna.Framework.Content;
using MonoGame.Extended.Sprites;
using team8.Physics;

namespace team8.Replay
{
    using Coordinate = Vector2;
    public class RespawnPoint: RenderObject
    {
        private AnimationState Animation;
        private Coordinate Position;
        public bool Hidden = true;

        public RespawnPoint(ContentManager contentManager)
        {
            CreateAnimation(contentManager: contentManager);
        }

        public void Dispose()
        {
            Animation.Dispose();
        }

        public void Draw(SpriteBatch spriteBatch, GameTime gameTime, Matrix transform, Action beginAgain)
        {
            if (!Hidden && Position != null)
            {
                Animation.Update(gameTime: gameTime, position: Position);
                spriteBatch.Draw(Animation.Sprite);
            }
        }


        private void CreateAnimation(ContentManager contentManager)
        {
            this.Animation = new AnimationState(contentManager: contentManager,
                                                           texturePath: AnimationPath.RecordingIndicator,
                                                           rowCount: AnimationConstants.FrameCount.RecordingIndicatorRows,
                                                           columnCount: AnimationConstants.FrameCount.RecordingIndicatorColumns,
                                                           frameDuration: AnimationConstants.Duration.RecordingIndicator);
            this.Animation.SetScale(0.5f);
        }

        public void SetPosition(Coordinate position)
        {
            this.Position =  position;
        }
    }
}
