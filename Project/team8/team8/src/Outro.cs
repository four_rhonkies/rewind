﻿/**
* Copyright 2017 Niclas Scheuing, Per Näslung, Tunay Bora, Xiaowei Zeng
* This file is part of REWIND.
*
* REWIND is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* REWIND is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
* GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with REWIND.If not, see<http://www.gnu.org/licenses/>.
*/
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Threading;
using team8.Animation;
using team8.Control;
using team8.Physics;
using team8.Rendering;
using team8.Sound;
using MonoGame.Extended.Sprites;
using System.Diagnostics;

namespace team8
{
    public class Outro : RenderObject
    {
        private ReplayMovementController OutroReplayMovementController;
        private Vector2 OutroGoalPosition;
        private Timer OutroStartMovingTimer;
        private Timer StartOutroTimer;
        private Timer StartCreditsTimer;
        private Timer StartDissolveTimer;
        private Timer ShowTitleTimer;
        private Timer ShowCreditsTextTimer;
        private Timer DissolveMusicTimer;
        private CameraController CameraController;
        private PhysicalWorld World;

        private enum State
        {
            Waiting, Walking, Dissolving, Credits
        }
        private State CurrentState;
        private AnimationState DissolveAnimation;
        private float TargetZoomLevel = 2;
        private float AnimationScale = 0.76f;
        private Vector2 AnimationPositionFactor = new Vector2(0.385f, -0.16f);
        private Vector2 CameraFocusOffset = new Vector2(0.3f, 0.65f);
        private Vector2 CreditPanTargetOffset = new Vector2(65000, 0);
        private Vector2 OutroCharacterTargetPositionOffset = new Vector2(8f, 0);
        private double RunSpeedDecay = 0.9;
        private float MinCharacterMaxVelocity = 2f;

        private float ZoomStep = 0.0042f;
        private float MaxWalkingPanSpeed = 5.3f;
        private float MaxCreditPanSpeed = 10;//7;
        private float CameraPanSpeedIncreaseFactor = 10f;
        private float CreditsCameraPanSpeedIncreaseFactor = 0.2f;

        private float FadeOutCharacterFactor = 2f;
        private int FadeOutCharacterAnimationIndex = 15;

        private float MinRunAnimationDurationFactor = 2f;
        private float RunAnimationDureationFactor = 1;
        private float RunAnimationDureationFactorGrow = 0.2f;

        private float MinIdleAnimationDurationFactor = 1.6f;
        private float IdleAnimationDureationFactor = 1;
        private float IdleAnimationDureationFactorGrow = 0.3f;

        private int StartCreditDelay = 100;
        private int StartMovingDelay = 2000;
        private int StartOutroDelay = 1500;
        private int StartDissolveDelayTime = 2600;
        private int ShowTitleDelay = 20000; // 9000
        private int ShowCreditsDelay = 30000;
        private int DissolveMusicDelay = 300;

        private int MusicFadeoutDuration = 1500;

        private SpriteFont TitleFont;
        private SpriteFont CreditsFont;
        private Vector2 TitlePosition = new Vector2(0.5f, 0.45f);
        private Vector2 FirstTextPosition = new Vector2(0.048f, 0.1f);
        private float LineSpace = 1.1f;
        private bool ShowTitle = false;
        private bool ShowCredits = false;
        private double? StartShowingTitle;
        private double? StartShowingCredits;
        private float BlendInTitleDuration = 2500;

        private float DissolveVolume = 0.1f;

        public Outro(PhysicalWorld world, CameraController cameraController, ContentManager contentManager)
        {
            this.CameraController = cameraController;
            this.World = world;
            this.DissolveAnimation = new AnimationState(contentManager: contentManager,
                texturePath: AnimationPath.CharacterDissolve,
                rowCount: AnimationConstants.FrameCount.CharacterDissolveRows,
                columnCount: AnimationConstants.FrameCount.CharacterDissolveColumns,
                frameDuration: AnimationConstants.Duration.CharacterDissolve,
                loop: false
                );

            DissolveAnimation.SetScale(AnimationScale);
            DissolveAnimation.Animation.OnCompleted += OnAnimationCompletion;
            TitleFont = contentManager.Load<SpriteFont>(FontConstants.TitleFont);
            CreditsFont = contentManager.Load<SpriteFont>(FontConstants.CreditsFont);
        }

        public void Update(GameTime gameTime)
        {
            //walk to cliff

            if (CurrentState == State.Walking)
            {
                if (!OutroReplayMovementController.HasReachedPosition(OutroGoalPosition))
                {
                    OutroReplayMovementController.SetMaxVelocity(Math.Max(MinCharacterMaxVelocity, OutroReplayMovementController.GetMaxVelocity()-gameTime.ElapsedGameTime.TotalSeconds*RunSpeedDecay));
                    RunAnimationDureationFactor = RunAnimationDureationFactor + (RunAnimationDureationFactorGrow* (float)gameTime.ElapsedGameTime.TotalSeconds);
                    World.MainCharacter.SetWalkAnimationDuration(AnimationConstants.Duration.Run * Math.Min(MinRunAnimationDurationFactor, RunAnimationDureationFactor));
                    OutroReplayMovementController.MoveToPosition(toPosition: OutroGoalPosition, gameTime: gameTime);
                }
                else
                {
                    OutroReplayMovementController.Stop(gameTime);
                    StartDissolveTimer = new Timer(callback: (object state) => { CurrentState = State.Dissolving; }, state: null, dueTime: StartDissolveDelayTime, period: Timeout.Infinite);
                    SlowDownIdleAnimation(gameTime);
                }
            }
            if (CurrentState == State.Waiting || CurrentState == State.Walking || CurrentState == State.Dissolving)
            {
                if (CameraController.Zoom < TargetZoomLevel)
                {
                    CameraController.ZoomIn(zoomStep: ZoomStep);
                }
                Configuration.Camera.MaxPanSpeed = (float)Math.Min(MaxWalkingPanSpeed, Configuration.Camera.MaxPanSpeed + CameraPanSpeedIncreaseFactor * gameTime.ElapsedGameTime.TotalSeconds);
                CameraController.FocusOnPoint(World.MainCharacterPositionInDisplay, relativePointInScreen: CameraFocusOffset);
            }
            if (CurrentState == State.Dissolving)
            {
                SlowDownIdleAnimation(gameTime);

                var animationSize = DissolveAnimation.Animation.CurrentFrame.Size * DissolveAnimation.AnimationScale;
                var position = World.MainCharacter.PositionInDisplay + animationSize * AnimationPositionFactor;
                DissolveAnimation.Update(gameTime: gameTime, position: position);
                DissolveAnimation.Animation.Play();
                TimerCallback playDissolveSound = delegate (object state)
                {
                    SoundManager.Instance.PlaySound(file: SoundManager.SoundFile.Dissolve, id: "dissolve", initialVolume: DissolveVolume, loop: false);
                };

                DissolveMusicTimer = new Timer(callback: playDissolveSound, state: null, dueTime: DissolveMusicDelay, period: Timeout.Infinite);
                if (DissolveAnimation.CurrentFrameIndex > FadeOutCharacterAnimationIndex)
                {
                    World.MainCharacter.Alpha = (float)Math.Max(0, World.MainCharacter.Alpha - FadeOutCharacterFactor * gameTime.ElapsedGameTime.TotalSeconds);
                }
            }
            if (CurrentState == State.Credits)
            {
                Configuration.Camera.MaxPanSpeed = (float)Math.Min(MaxCreditPanSpeed, Configuration.Camera.MaxPanSpeed + CreditsCameraPanSpeedIncreaseFactor * gameTime.ElapsedGameTime.TotalSeconds);
                CameraController.FocusOnPoint(World.MainCharacterPositionInDisplay + CreditPanTargetOffset, relativePointInScreen: new Vector2(0.5f, 0.5f));
            }
        }

        private void SlowDownIdleAnimation(GameTime gameTime)
        {
            IdleAnimationDureationFactor = IdleAnimationDureationFactor + (IdleAnimationDureationFactorGrow * (float)gameTime.ElapsedGameTime.TotalSeconds);
            World.MainCharacter.SetIdleAnimationDuration(AnimationConstants.Duration.Idle * Math.Min(MinIdleAnimationDurationFactor, IdleAnimationDureationFactor));
            //Debug.WriteLine(AnimationConstants.Duration.Idle * Math.Min(MinIdleAnimationDurationFactor, IdleAnimationDureationFactor));
        }
        public void Draw(SpriteBatch spriteBatch, GameTime gameTime, Matrix transform, Action beginAgain)
        {
            var completed = DissolveAnimation.CurrentFrameIndex >= AnimationConstants.FrameCount.CharacterDissolveColumns * AnimationConstants.FrameCount.CharacterDissolveRows - 1;
            if (CurrentState == State.Dissolving && !completed)
            {
                //var position = World.MainCharacter.Position;// + new Vector2(this.Width / 2, this.Height / 2);
                //AnimationState.Update(gameTime: gameTime, position: position, rotation: RenderRotation());
                
                spriteBatch.Draw(sprite: DissolveAnimation.Sprite);
            }
        }

        private void OnAnimationCompletion()
        {
            //SoundManager.Instance.SetSoundtrack(track: SoundManager.SoundFile.Credits, trackFiltered: null, trackGhost: null, volume: Configuration.Sound.CreditsVolume, fadeOutInMs: 100);
            TimerCallback startCredits = delegate (object state)
            {
                CurrentState = State.Credits;
                Configuration.Camera.MaxPanSpeed = 0;
            };
            StartCreditsTimer = new Timer(callback: startCredits, state: null, dueTime: StartCreditDelay, period: Timeout.Infinite);
        }

       
        public void StartOutro(Action setStateToOutro)
        {
            Renderer.ReduceFog = true;
            
            OutroReplayMovementController = new ReplayMovementController(controlledTarget: World.MainCharacter);
            OutroReplayMovementController.Stop(new GameTime());
            OutroGoalPosition = World.MainCharacter.Position + OutroCharacterTargetPositionOffset;
            Configuration.Camera.MaxPanSpeed = 0f;
            SoundManager.Instance.SetSoundtrack(track: SoundManager.SoundFile.Credits, trackFiltered: null, trackGhost: null, volume: Configuration.Sound.OutroVolume, fadeOutInMs: MusicFadeoutDuration);
            ShowTitleTimer = new Timer(callback: (object state) => { ShowTitle = true; }, state: null, dueTime: ShowTitleDelay, period: Timeout.Infinite);
            ShowCreditsTextTimer = new Timer(callback: (object state) => { ShowCredits = true; }, state: null, dueTime: ShowCreditsDelay, period: Timeout.Infinite);
            //OutroStartMovingTimer = new Timer(callback: (object state) => { CurrentState = State.Walking; }, state: null, dueTime: StartMovingDelay, period: Timeout.Infinite);
            setStateToOutro();
            CurrentState = State.Walking;
            //StartOutroTimer = new Timer(callback: (object state) => { setStateToOutro(); }, state: null, dueTime: StartOutroDelay, period: Timeout.Infinite);
            //CurrentState = State.Waiting;
        }

        public void Dispose()
        {
            DissolveAnimation?.Dispose();
            StartOutroTimer?.Dispose();
            OutroStartMovingTimer?.Dispose();
            StartDissolveTimer?.Dispose();
            StartCreditsTimer?.Dispose();
            ShowTitleTimer?.Dispose();
            ShowCreditsTextTimer?.Dispose();
        }


        public void DrawText(SpriteBatch spriteBatch, GameTime gameTime)
        {
            if (ShowTitle)
            {
                if (!StartShowingTitle.HasValue)
                {
                    StartShowingTitle = gameTime.TotalGameTime.TotalMilliseconds;
                }
                spriteBatch.Begin(blendState: BlendState.AlphaBlend);
                DrawTitle(spriteBatch: spriteBatch, gameTime: gameTime);
            }
            if(ShowCredits)
            {
                if (!StartShowingCredits.HasValue)
                {
                    StartShowingCredits = gameTime.TotalGameTime.TotalMilliseconds;
                }
                DrawCredits(spriteBatch: spriteBatch, gameTime: gameTime);
            }
            spriteBatch.End();
        }
        private void DrawTitle(SpriteBatch spriteBatch, GameTime gameTime)
        {
            var screenBounds = spriteBatch.GraphicsDevice.Viewport.Bounds;
            var fontSize = TitleFont.MeasureString(StaticScreenConstants.Text.Title);
            var textPosition = new Vector2((screenBounds.Width - fontSize.X) * TitlePosition.X, (screenBounds.Height - fontSize.Y) * TitlePosition.Y);
            var titleAlpha = (float)Math.Min(1, (gameTime.TotalGameTime.TotalMilliseconds - StartShowingTitle.Value) / BlendInTitleDuration);
            spriteBatch.DrawString(spriteFont: TitleFont, text: StaticScreenConstants.Text.Title, position: textPosition, color: StaticScreenConstants.Colors.TitleFontColor * titleAlpha);
        }
        private void DrawCredits(SpriteBatch spriteBatch, GameTime gameTime)
        {

            var screenBounds = spriteBatch.GraphicsDevice.Viewport.Bounds;
            var alpha = (float)Math.Min(1, (gameTime.TotalGameTime.TotalMilliseconds - StartShowingCredits.Value) / BlendInTitleDuration);
            var color = OutroConstants.Colors.CreditFontColots * alpha;
            
            var textSize = CreditsFont.MeasureString(OutroConstants.Text.Niclas);

            //Raly
            var textPosition = new Vector2(screenBounds.Width * (1-FirstTextPosition.X)- textSize.X, screenBounds.Height * (1-FirstTextPosition.Y));
            var text = OutroConstants.Text.Raly;
            spriteBatch.DrawString(spriteFont: CreditsFont, text: text, position: textPosition, color: color);

            //Tunay
            textPosition = textPosition - new Vector2(0, LineSpace * textSize.Y);
            text = OutroConstants.Text.Tunay;
            spriteBatch.DrawString(spriteFont: CreditsFont, text: text, position: textPosition, color: color);

            //Magenta
            textPosition = textPosition - new Vector2(0, LineSpace * textSize.Y);
            text = OutroConstants.Text.Magenta;
            spriteBatch.DrawString(spriteFont: CreditsFont, text: text, position: textPosition, color: color);

            //Per
            textPosition = textPosition - new Vector2(0, LineSpace * textSize.Y);
            text = OutroConstants.Text.Per;
            spriteBatch.DrawString(spriteFont: CreditsFont, text: text, position: textPosition, color: color);

            //Niclas
            textPosition = textPosition - new Vector2(0, LineSpace * textSize.Y);
            text = OutroConstants.Text.Niclas;
            spriteBatch.DrawString(spriteFont: CreditsFont, text: text, position: textPosition, color: color);
        }
    }
}
