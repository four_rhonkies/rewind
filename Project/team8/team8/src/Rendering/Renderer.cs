﻿/**
* Copyright 2017 Niclas Scheuing, Per Näslung, Tunay Bora, Xiaowei Zeng
* This file is part of REWIND.
*
* REWIND is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* REWIND is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
* GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with REWIND.If not, see<http://www.gnu.org/licenses/>.
*/
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using team8.Level;
using System.Linq;
using FarseerPhysics;
using team8.Physics;

namespace team8.Rendering
{
    using Coordinate = Vector2;
    public class Renderer
    {
        private Dictionary<RenderLayerLevel,RenderLayer> renderLayers;
        private TextRenderLayer TextRenderLayer;
        private  Map Map;
        private SpriteBatch SpriteBatch;
        private GraphicsDevice GraphicsDevice;
        private CameraController CameraController;
        private Coordinate ParallaxOrigin;
        private CharacterPositionDelegate CharacterPositionDelegate;

        private Effect BlurEffect;
        private Effect BlendEffect;
        private Effect BoxEffect;
        private Effect NoiseEffect;
        private Effect FogEffect;
        private Effect InvertEffect;
        private RenderTarget2D TextureRenderTarget1;
        private RenderTarget2D TextureRenderTarget2;

        private Action BeginDrawing;

        private readonly RenderLayerLevel ActionLevel = new RenderLayerLevel(type: RenderLayerLevelType.ActionLayer);
        private readonly RenderLayerLevel CharacterActionLevel;
        private readonly RenderLayerLevel DebugLevel = new RenderLayerLevel(type: RenderLayerLevelType.Debug, depth: 0f);

        private float BlurScale = 1f;
        private float FogStartDistance = 0f;
        private float FogIntensity = 0.1f;
        private float MaxFogIntensity = 0.5f;
        private float FogEndDistance = 21f;
        private bool UseRecordingFilters = false;
        private float FogChangeSpeed = 0.1f;
        public static bool ReduceFog = false;
        private Random FogIntensityRandom;

        public Renderer(SpriteBatch spriteBatch, GraphicsDevice graphicsDevice, CameraController cameraController)
        {
            this.SpriteBatch = spriteBatch;
            this.GraphicsDevice = graphicsDevice;
            this.CameraController = cameraController;
            this.renderLayers = new Dictionary<RenderLayerLevel, RenderLayer>();
            this.FogIntensityRandom = new Random();

            CharacterActionLevel = new RenderLayerLevel(type: RenderLayerLevelType.Character, depth: RenderingConstants.CharacterLayerDepth);
            renderLayers[CharacterActionLevel] = new RenderLayer(layerLevel: CharacterActionLevel);
        }

        public void LoadContent(ContentManager contentManager)
        {

            BlurEffect = contentManager.Load<Effect>("effects/Blur");
            BlendEffect = contentManager.Load<Effect>("effects/Blend");
            BoxEffect = contentManager.Load<Effect>("effects/Box");
            NoiseEffect = contentManager.Load<Effect>("effects/Noise");
            FogEffect = contentManager.Load<Effect>("effects/Fog");
            InvertEffect = contentManager.Load<Effect>("effects/Invert");

            TextureRenderTarget1 = new RenderTarget2D(graphicsDevice: GraphicsDevice,
                    width: GraphicsDevice.PresentationParameters.BackBufferWidth,
                    height: GraphicsDevice.PresentationParameters.BackBufferHeight,
                    mipMap: false,
                    preferredFormat: GraphicsDevice.PresentationParameters.BackBufferFormat,
                    preferredDepthFormat: DepthFormat.Depth24);
            TextureRenderTarget2 = new RenderTarget2D(graphicsDevice: GraphicsDevice,
                   width: (int)(GraphicsDevice.PresentationParameters.BackBufferWidth / BlurScale),
                   height: (int)(GraphicsDevice.PresentationParameters.BackBufferHeight / BlurScale),
                   mipMap: false,
                   preferredFormat: GraphicsDevice.PresentationParameters.BackBufferFormat,
                   preferredDepthFormat: DepthFormat.Depth24);

        }


        public void OnRecordingStarted()
        {
            this.UseRecordingFilters = true;
        }
        public void OnRecordingStoped()
        {
            this.UseRecordingFilters = false;
        }

        public void SetSpriteBatch(SpriteBatch spriteBatch)
        {
            this.SpriteBatch = spriteBatch;
        }

        public void SetTextRenderLayer(TextRenderLayer textRenderLayer)
        {
            this.TextRenderLayer = textRenderLayer;
        }

        public void AddRenderLayer(RenderLayerLevel level)
        {
            Debug.WriteLine("Adding RenderLayer " + level);

            if (!renderLayers.ContainsKey(key: level))
            {
                this.renderLayers[level] = new RenderLayer(layerLevel: level);
            }
            else
            {
                Debug.WriteLine("Layer for level " + level + " already exists!");
            }
        }

        public void SetMap(Map map)
        {
            this.Map = map;


            //Try to create a RenderLayer for all Layers
            foreach (Layer layer in map.Layers)
            {
                if (layer.RenderLayerLevel.HasValue)
                {
                    //Create a new RenderLayer
                    AddRenderLayer(level: layer.RenderLayerLevel.Value);
                    //Add the Layer as a RenderObject
                    AddRenderObject(renderObject: layer, level: layer.RenderLayerLevel.Value);
                }
                else
                {
                    Debug.WriteLine("Layer has no RenderLayerLevel when added to renderer;");
                }
            }

            //Render Object vertices if required 
            if (Configuration.AllowRenderObjectVertices)
            {

                if (!renderLayers.ContainsKey(DebugLevel))
                {
                    AddRenderLayer(level: DebugLevel);
                }
            }
            
            //Add LevelObjects to renderer
            foreach (var group in map.ObjectGroups)
            {
                //Check if the group defines a level and add it if not already done so
                var groupLevel = group.RenderLayerLevel;
                if (groupLevel.HasValue && !renderLayers.ContainsKey(groupLevel.Value))
                {
                    AddRenderLayer(groupLevel.Value);
                }

                //Iterate all objects and put them into de DebugLayer if they require. Else put them in the groups layer
                foreach (var obj in group.Objects)
                {
                    if (obj.RenderAsDebug)
                    {
                        AddRenderObject(renderObject: obj, level: DebugLevel);
                    }
                    else if (groupLevel.HasValue)
                    {
                        AddRenderObject(renderObject: obj, level: groupLevel.Value);
                    }
                }
            }
            
        }

        public void AddRenderObject(RenderObject renderObject, RenderLayerLevel level)
        {
            //Check if level exists, add if yes, warn if no.
            if (renderLayers.ContainsKey(key: level))
            {
                renderLayers[level].AddRenderObject(renderObject: renderObject);
            }
            else
            {
                Debug.WriteLine("No layer for level" + level);
            }
        }

        public void AddRenderObjectToActionLayer(RenderObject renderObject)
        {

            if (renderLayers.ContainsKey(ActionLevel))
            {
                renderLayers[ActionLevel].AddRenderObject(renderObject: renderObject);
            }
            else
            {
                Debug.WriteLine("No action layer existing in renderer when trying to add render object");
            }
        }

        public void AddCharacterToCharacterLayer(MainCharacter character)
        {

            if (renderLayers.ContainsKey(CharacterActionLevel))
            {
                renderLayers[CharacterActionLevel].AddRenderObject(renderObject: character);
            }
            else
            {
                Debug.WriteLine("No CharacterActionLevel layer existing in renderer when trying to add render object");
            }
        }

        public void RemoveRenderObjectFromActionLayer(RenderObject renderObject)
        {

            if (renderLayers.ContainsKey(ActionLevel))
            {
                renderLayers[ActionLevel].RemoveRenderObject(renderObject: renderObject);
            }
            else
            {
                Debug.WriteLine("No action layer existing in renderer when trying to remove render object");
            }
        }

        public void RemoveCharacterFromCharacterActionLayer(MainCharacter character)
        {

            if (renderLayers.ContainsKey(CharacterActionLevel))
            {
                //CharacterRenderLayer.RemoveCharacter(character: character);
                renderLayers[CharacterActionLevel].RemoveRenderObject(renderObject: character);
            }
            else
            {
                Debug.WriteLine("No action layer existing in renderer when trying to remove render object");
            }
        }

        /**
         *  Removes an object from the renderer.
         *  If fromLevel is given, it tries to remove the object from this level, returning true if it was in this layer and successfully removed.
         *  If fromLevel is not given, it tries all layers, returning with true when the renderObject was successfully removed from one layer.
         **/
        public bool RemoveRenderObject(RenderObject renderObject, RenderLayerLevel? fromLevel = null)
        {
            //If level is given, simply remove
            if (fromLevel.HasValue)
            {

                return this.renderLayers[fromLevel.Value].RemoveRenderObject(renderObject: renderObject);
            }
            //else try all layers and stop once found.
            else
            {
                foreach (RenderLayer layer in renderLayers.Values)
                {
                    if (layer.RemoveRenderObject(renderObject: renderObject))
                    {
                        return true;
                    }
                }
                //If it was not found, return false
                return false;
            }
            

        }
        
        public void Draw(GameTime gameTime)
        {
            if (UseRecordingFilters)
            {
                GraphicsDevice.SetRenderTarget(TextureRenderTarget1);
                GraphicsDevice.DepthStencilState = new DepthStencilState() { DepthBufferEnable = true };
            }
            else
            {
                GraphicsDevice.SetRenderTarget(null);
            }

            GraphicsDevice.Clear(new Color(new Vector3(211f, 219f, 222f)));

            //Call Draw for all objects
            List<RenderLayerLevel> keys = new List<RenderLayerLevel>(renderLayers.Keys);
            keys.Sort(delegate (RenderLayerLevel x, RenderLayerLevel y)
            {
                return (x.Depth < y.Depth ? 1 : -1);
            });

            //Set up some values for shaders
            var characterPosition = CharacterPositionDelegate.CharacterPositionInCamera();
            var screenDiagonal = new Vector2(TextureRenderTarget1.Bounds.Width, TextureRenderTarget1.Bounds.Height);
            var screenDiagonalLength = screenDiagonal.Length();

            foreach (var key in keys)
            {
                //Don't render Debug layers if not configured so
                if (key.Type == RenderLayerLevelType.Debug && !Configuration.RenderObjectVertices)
                {
                    continue;
                }
                var layer = renderLayers[key];
                //if (layer.LayerLevel.Type != RenderLayerLevelType.ActionLayer)
                //{
                //    continue;
                //}

                //the camera produces a view matrix that can be applied to any sprite batch

                //}

                var transform = TransformationForLayer(layer: layer.LayerLevel);

                //((layer.LayerLevel.Depth)*(CameraConstants.MaxZoom-CameraController.Zoom)+1);
                //transformMatrix = Matrix.CreateScale( scaleFactor, scaleFactor, 0) * transformMatrix;
                //SpriteBatch.Begin(transformMatrix: transformMatrix, );

                var depthForFog = layer.LayerLevel.Depth < 1f ? 0f : ConvertUnits.ToDisplayUnits(layer.LayerLevel.Depth);
                FogEffect.Parameters["Depth"].SetValue(depthForFog);
                FogEffect.Parameters["FogStart"].SetValue(FogStartDistance);
                UpdateFogIntensity(gameTime: gameTime);
                FogEffect.Parameters["FogIntensity"].SetValue(FogIntensity);
                FogEffect.Parameters["ScreenDiagonal"].SetValue(screenDiagonalLength);
                FogEffect.Parameters["FogEnd"].SetValue(ConvertUnits.ToDisplayUnits(FogEndDistance)); //screenDiagonalLength + ConvertUnits.ToDisplayUnits(100.0f)); //screen size + max depth of layers
                FogEffect.Parameters["CharacterPosition"].SetValue(characterPosition);

                BeginDrawing = delegate ()
                {
                    if (layer.LayerLevel.Type == RenderLayerLevelType.Character)
                    {
                        SpriteBatch.Begin(transformMatrix: transform);
                    }
                    else
                    {
                        SpriteBatch.Begin(effect: FogEffect, transformMatrix: transform);
                    }
                };
                BeginDrawing();
                layer.Draw(spriteBatch: SpriteBatch, gameTime: gameTime, transform: transform, beginAgain: BeginDrawing);

                SpriteBatch.End();
            }


            //var characterTransform = TransformationForLayer(layer: CharacterActionLevel);
            //SpriteBatch.Begin(transformMatrix: characterTransform);
            //CharacterRenderLayer.Draw(spriteBatch: SpriteBatch, gameTime: gameTime, transform: characterTransform, beginAgain: BeginDrawing);
            //SpriteBatch.End();








            //if (!UseRecordingFilters)
            //{
            //    GraphicsDevice.SetRenderTarget(null);
            //    SpriteBatch.Begin(transformMatrix: Matrix.Identity, effect: InvertEffect);
            //    //CharacterRenderLayer.DrawSelectionIndication(spriteBatch: SpriteBatch, gameTime: gameTime, transform: characterTransform);
            //    SpriteBatch.Draw(TextureRenderTarget1, destinationRectangle: GraphicsDevice.ScissorRectangle, color: Color.White);
            //    SpriteBatch.End();
            //}
            if (UseRecordingFilters)
            {

                //BlurEffect.Parameters["KernelSize"].SetValue(21);
                float[] BlurWeights7 = new float[]
                {
                    0.00598f,
                    0.060626f,
                    0.241843f,
                    0.383103f,
                    0.241843f,
                    0.060626f,
                    0.00598f
                };
                float[] UniformBlurWeights7 = new float[]
                {
                    1/7f,
                    1/7f,
                    1/7f,
                    1/7f,
                    1/7f,
                    1/7f,
                    1/7f,
                };
                //{ 
                //0.000003f,
                //0.00319f,
                //0.178458f,
                //0.636698f,
                //0.178458f,
                //0.00319f,
                //0.000003f
                //};
                float[] BlurWeights5 = new float[]
                {
                0.00319f,
                0.178459f,
                0.636701f,
                0.178459f,
                0.00319f
                };
                float[] BlurWeights21 = new float[]
                {
                0.004481f,
                0.008089f,
                0.013722f,
                0.021874f,
                0.032768f,
                0.046128f,
                0.061021f,
                0.075856f,
                0.088613f,
                0.097274f,
                0.100346f,
                0.097274f,
                0.088613f,
                0.075856f,
                0.061021f,
                0.046128f,
                0.032768f,
                0.021874f,
                0.013722f,
                0.008089f,
                0.004481f
                };


                //BlurEffect.Parameters["BlurWeights"].SetValue(BlurWeights7);
                //BlurEffect.Parameters["BlurStrength"].SetValue(7);

                //BlurEffect.Parameters["ScreenWidth"].SetValue((float)TextureRenderTarget1.Width);
                //BlurEffect.Parameters["ScreenHeight"].SetValue((float)TextureRenderTarget1.Height);
                
                GraphicsDevice.SetRenderTarget(TextureRenderTarget2);
                //Downscale
                //BoxEffect.Parameters["ScreenWidth"].SetValue((float)TextureRenderTarget2.Width);
                //BoxEffect.Parameters["ScreenHeight"].SetValue((float)TextureRenderTarget2.Height);
                //BoxEffect.Parameters["BoxSize"].SetValue((float)BlurScale);
                //BlurEffect.CurrentTechnique = BlurEffect.Techniques.First();
                NoiseEffect.Parameters["RandomSeed"].SetValue((float)(gameTime.TotalGameTime.TotalSeconds));// Math.Sin((double)gameTime.TotalGameTime.TotalSeconds*2)+1)/2);
                NoiseEffect.Parameters["CharacterPosition"].SetValue(characterPosition);
                NoiseEffect.Parameters["EffectAmount"].SetValue(0.05f);
                SpriteBatch.Begin(effect: NoiseEffect, transformMatrix: Matrix.CreateScale(1 / BlurScale));
                SpriteBatch.Draw(TextureRenderTarget1, destinationRectangle: TextureRenderTarget2.Bounds, color: Color.White);
                SpriteBatch.End();

                // Drop the render target
                GraphicsDevice.SetRenderTarget(null);
                GraphicsDevice.Clear(Color.Orange);


                //var transformMatrix = TransformationForLayer(layer: this.ActionLevel);


                //Draw texture to screen
                BlurEffect.Parameters["BlurStrength"].SetValue(21);
                BlurEffect.Parameters["BlurWeights"].SetValue(BlurWeights21);
                //BlurEffect.Parameters["ScreenWidth"].SetValue((float)TextureRenderTarget1.Width);
                //BlurEffect.Parameters["ScreenHeight"].SetValue((float)TextureRenderTarget1.Height);
                BlurEffect.Parameters["CharacterPosition"].SetValue(characterPosition);
                //the smaller the less the effect
                BlurEffect.Parameters["FadeCorrection"].SetValue(1 / (float)TextureRenderTarget1.Height * 0.001f);
                //BlurEffect.CurrentTechnique = BlurEffect.Techniques.First();
                SpriteBatch.Begin(effect: BlurEffect);// , transformMatrix: Matrix.CreateScale(BlurScale));//effect: BlurEffect,
                SpriteBatch.Draw(TextureRenderTarget2, destinationRectangle: GraphicsDevice.ScissorRectangle, color: Color.White);
                SpriteBatch.End();

                SpriteBatch.Begin(effect: BlendEffect);
                //var transformMatrix = TransformationForLayer(layer: this.ActionLevel);
                //var characterPosition = CharacterPositionDelegate.CharacterPositionInCamera();
                //characterPosition = Vector2.Transform(position: characterPosition, matrix: transformMatrix);
                BlendEffect.Parameters["DiagonalLength"].SetValue(screenDiagonal.Length() * 0.25f);
                BlendEffect.Parameters["Radius"].SetValue(screenDiagonal.Length() * 0.06f);
                BlendEffect.Parameters["CharacterPosition"].SetValue(characterPosition);
                BlendEffect.Parameters["Inverted"].SetValue(true);
                SpriteBatch.Draw(TextureRenderTarget1, destinationRectangle: GraphicsDevice.ScissorRectangle, color: Color.White);
                SpriteBatch.End();

                if (TextRenderLayer != null)
                {
                    SpriteBatch.Begin(transformMatrix: Matrix.Identity);
                    TextRenderLayer.Draw(spriteBatch: SpriteBatch, gameTime: gameTime, transform: Matrix.Identity, beginAgain: BeginDrawing);
                    SpriteBatch.End();
                }
            }
        }

        private Matrix TransformationForLayer(RenderLayerLevel layer)
        {
            var parallaxFactor = layer.ParallaxFactor;
            var transformMatrix = CameraController.GetViewMatrix(parallaxFactor: Vector2.One);
            //var transformMatrix = 
            //Matrix.CreateTranslation(new Vector3(-CameraController.CameraPosition * parallaxFactor, 0.0f)) *
            //Matrix.CreateTranslation(new Vector3(-CameraController.CameraOrigin, 0.0f)) *
            //Matrix.CreateRotationZ(CameraController.CameraRotation) *
            //Matrix.CreateScale(CameraController.Zoom, CameraController.Zoom, 1) *

            //Matrix.CreateTranslation(new Vector3(CameraController.CameraOrigin, 0.0f));
            //if (layer.LayerLevel.Type == RenderLayerLevelType.Background)
            //{
            var translationCorrection = Matrix.CreateTranslation(new Vector3(CameraController.CameraPosition * Vector2.One, 0.0f)) *
                                        Matrix.CreateTranslation(new Vector3(-parallaxFactor * (CameraController.CameraPosition - this.ParallaxOrigin), 0.0f)) *
                                         Matrix.CreateTranslation(new Vector3((-this.ParallaxOrigin), 0.0f));

            //var scaleFactor = (1/(layer.LayerLevel.Depth+1)) * (CameraController.Zoom- totalZoom) + totalZoom;
            var resolutionCorrectionFactor = Math.Max(CameraController.ScreenBounds.Width / CameraConstants.ReferenceResolution.X, CameraController.ScreenBounds.Height / CameraConstants.ReferenceResolution.Y);
            var scaleFactor = CameraController.Zoom;
            if (layer.Depth > RenderingConstants.MinParallaxOffset)
            {
                var totalZoom = Configuration.Camera.TotalZoom;
                scaleFactor = totalZoom + (1 / layer.Depth) * (CameraController.Zoom - totalZoom);
            }
            //scaleFactor = scaleFactor * resolutionCorrectionFactor;
            //scaleFactor =  1 / (0.5f*layer.LayerLevel.Depth + 1) * CameraController.Zoom;
            //scaleFactor = CameraController.Zoom;


            var translationCorrection2 =
                Matrix.CreateTranslation(new Vector3(-parallaxFactor * (CameraController.CameraPosition - this.ParallaxOrigin), 0.0f)) *
                Matrix.CreateTranslation(new Vector3((-this.ParallaxOrigin), 0.0f)) *
                Matrix.CreateTranslation(new Vector3(-CameraController.CameraOrigin, 0.0f)) *
                Matrix.CreateScale(scaleFactor, scaleFactor, 0) *
                Matrix.CreateScale(1 / CameraController.Zoom, 1 / CameraController.Zoom, 1) *
                Matrix.CreateTranslation(new Vector3(CameraController.CameraOrigin, 0.0f)) *
                //Matrix.CreateScale(resolutionCorrectionFactor, resolutionCorrectionFactor, 0) *
                Matrix.CreateTranslation(new Vector3(CameraController.CameraPosition, 0.0f));


            transformMatrix = translationCorrection2 * transformMatrix * Matrix.CreateScale(resolutionCorrectionFactor);
            return transformMatrix;
        }

        public void Dispose()
        {
            SpriteBatch.Dispose();

            foreach(RenderLayer layer in renderLayers.Values)
            {
                layer.Dispose();
            }
            
        }

        public void SetParallaxOrigin(Coordinate parallaxOrigin)
        {
            this.ParallaxOrigin = parallaxOrigin;
        }
        public void SetCharacterPositionDelegate(CharacterPositionDelegate characterPositionDelegate)
        {
            this.CharacterPositionDelegate = characterPositionDelegate;
        }
        private void UpdateFogIntensity(GameTime gameTime)
        {
            var randomFactor = (float)-FogIntensityRandom.NextDouble();
            if (!ReduceFog)
            {
                randomFactor = (randomFactor + 0.5f) * 2;
            }
            var newIntensity = FogIntensity + randomFactor * FogChangeSpeed* (float)gameTime.ElapsedGameTime.TotalSeconds;
            this.FogIntensity = Math.Min(MaxFogIntensity, Math.Max(0, newIntensity));
        }
    }   

    public interface CharacterPositionDelegate
    {
        Coordinate CharacterPositionInCamera();
        float DistanceToFurthestPointOnScreenEdge();
    }

}
