﻿/**
* Copyright 2017 Niclas Scheuing, Per Näslung, Tunay Bora, Xiaowei Zeng
* This file is part of REWIND.
*
* REWIND is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* REWIND is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
* GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with REWIND.If not, see<http://www.gnu.org/licenses/>.
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using System.Threading;

namespace team8.Rendering
{
    using Coordinate = Vector2;
    public class TextRenderLayer : IRenderLayer
    {
        private CameraController CameraController;
        private SpriteFont Font;
        private Coordinate Position;
        private string Text;

        public TextRenderLayer(CameraController cameraController)
        {
            this.CameraController = cameraController;
            //var margin = 50;
            Position = new Coordinate(CameraController.ScreenBounds.Width * 0.025f, CameraController.ScreenBounds.Height * 0.9f);// + new Coordinate(margin, -margin);
        }

        public void LoadContent(ContentManager contentManager)
        {
            Font = contentManager.Load<SpriteFont>(FontConstants.InstructionFont);

        }
        public void Dispose()
        {
            
        }

        public void SetText(string text)
        {
            this.Text = text;
        }

        public void ResetText()
        {
            this.Text = null;
        }

        public void Draw(SpriteBatch spriteBatch, GameTime gameTime, Matrix transform, Action beginAgain)
        {
            if (Text != null)
            {
                spriteBatch.DrawString(spriteFont: Font, text: Text, position: Position, color: Color.White);
            }
        }
    }
}
