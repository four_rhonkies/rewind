﻿/**
* Copyright 2017 Niclas Scheuing, Per Näslung, Tunay Bora, Xiaowei Zeng
* This file is part of REWIND.
*
* REWIND is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* REWIND is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
* GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with REWIND.If not, see<http://www.gnu.org/licenses/>.
*/
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using team8.Physics;

namespace team8.Rendering
{

    public enum RenderLayerLevelType
    {
        Foreground, ActionLayer, Background, Debug, Character

    }

    public struct RenderLayerLevel
    {
        public RenderLayerLevelType Type;
        //the Depth in meters behind or in front of the action layer
        private float depth;
        public float Depth
        {
            set
            {
                this.depth = value;
                SetParallaxFactor(value);
            }
            get
            {
                return this.depth;
            }
        }
        public Vector2 ParallaxFactor;

        public RenderLayerLevel(RenderLayerLevelType type, float depth=0)
        {
            this.Type = type;
            this.depth = depth;
            //must init ParallaxFactor to something, otherwise compiler complains.
            this.ParallaxFactor = Vector2.One;
            SetParallaxFactor(depth);
        }

        private void SetParallaxFactor(float layerDepth)
        {
            var factor = 1/(layerDepth + 1);
            this.ParallaxFactor = new Vector2(factor, factor);
        }

        override public string ToString()
        {
            return Type.ToString() + " " + Depth.ToString();
        }
    }

    public class RenderLayer: IRenderLayer
    {
        public RenderLayerLevel LayerLevel;
        private List<RenderObject> renderObjects;

        public RenderLayer(RenderLayerLevel layerLevel)
        {
            this.LayerLevel = layerLevel;
            this.renderObjects = new List<RenderObject>();
        }

        public void AddRenderObject(RenderObject renderObject)
        {
            this.renderObjects.Add(renderObject);
        }

        public virtual void Draw(SpriteBatch spriteBatch, GameTime gameTime, Matrix transform, Action beginAgain)
        {
            //Call Draw for all objects
            foreach (RenderObject renderObject in renderObjects)
            {
                renderObject.Draw(spriteBatch: spriteBatch, gameTime: gameTime, transform: transform, beginAgain: beginAgain);
            }
        }

        public bool RemoveRenderObject(RenderObject renderObject)
        {
            var success = this.renderObjects.Remove(renderObject);
            return success;
        }

        public void Dispose()
        {
            foreach(var obj in this.renderObjects)
            {
                obj.Dispose();
            }
        }

    }

    public class CharacterRenderLayer : RenderLayer
    {
        private List<MainCharacter> Characters;
        public CharacterRenderLayer(RenderLayerLevel layerLevel) : base(layerLevel)
        {
            this.Characters = new List<MainCharacter>();
        }
        //public void DrawSelectionIndication(SpriteBatch spriteBatch, GameTime gameTime, Matrix transform)
        //{
        //    foreach(var character in Characters)
        //    {
        //        character.DrawSelectionIndicator(spriteBatch: spriteBatch, gameTime: gameTime, transform: transform);
        //    }
        //}

        public override void Draw(SpriteBatch spriteBatch, GameTime gameTime, Matrix transform, Action beginAgain)
        {
            base.Draw(spriteBatch: spriteBatch, gameTime: gameTime, transform: transform, beginAgain: beginAgain);
            foreach(var character in Characters)
            {
                character.Draw(spriteBatch: spriteBatch, gameTime: gameTime, transform: transform, beginAgain: beginAgain);
            }
        }

        public void AddCharacter(MainCharacter character)
        {
            this.Characters.Add(character);
        }

        public bool RemoveCharacter(MainCharacter character)
        {
            var success = this.Characters.Remove(character);
            return success;
        }
    }

    public interface IRenderLayer
    {
        void Draw(SpriteBatch spriteBatch, GameTime gameTime, Matrix transform, Action beginAgain);
        void Dispose();
    }
}
