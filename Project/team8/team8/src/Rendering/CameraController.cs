﻿/**
* Copyright 2017 Niclas Scheuing, Per Näslung, Tunay Bora, Xiaowei Zeng
* This file is part of REWIND.
*
* REWIND is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* REWIND is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
* GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with REWIND.If not, see<http://www.gnu.org/licenses/>.
*/
using FarseerPhysics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoGame.Extended;
using MonoGame.Extended.ViewportAdapters;
using System;
using team8.Physics;
using team8.Sound;

namespace team8.Rendering
{
    using Coordinate = Vector2;
    public class CameraController: DistanceDelegate, CharacterPositionDelegate
    {
        private Camera2D Camera;
        private PhysicalWorld World;

        private GameWindow Window;
        public Action OnWindowSizeChanged;

        public Action OnGhostOutsideCamera;

        public Rectangle ScreenBounds
        {
            get
            {
                return Window.ClientBounds;
            }
        }

        public Coordinate CameraPosition
        {
            get
            {
                return Camera.Position;
            }
        }
        public Coordinate CameraOrigin
        {
            get
            {
                return Camera.Origin;
            }
        }
        public float Zoom
        {
            get
            {
                return Camera.Zoom;
            }
        }
        
        public float CameraRotation
        {
            get
            {
                return Camera.Rotation;
            }
        }

        public CameraController(GameWindow window, GraphicsDevice graphicsDevice)
        {
            this.Window = window;
            UpdateMargins();
            window.ClientSizeChanged += WindowSizeChanged;
            //var viewportAdapter = graphicsDevice.Viewport;//new BoxingViewportAdapter(window, graphicsDevice, window.ClientBounds.Width, window.ClientBounds.Height);
            this.Camera = new Camera2D(graphicsDevice);
        }

        private void WindowSizeChanged(object sender, EventArgs e)
        {
            //var viewportAdapter = new BoxingViewportAdapter(window: Window, graphicsDevice: grap, window.ClientBounds.Width, window.ClientBounds.Height);
            //this.Camera = new Camera2D(viewportAdapter);
            OnWindowSizeChanged?.Invoke();
            UpdateMargins();
        }
        private void UpdateMargins()
        {
            //HorizontalMarginToBoundingRectangle = Window.ClientBounds.Width * CameraConstants.HorizontalDistanceToEdge;
            //VerticalMarginToBoundingRectangle = Window.ClientBounds.Height * CameraConstants.VerticalDistanceToEdge;
            //GhostHorizontalMarginToBoundingRectangle = Window.ClientBounds.Width * CameraConstants.GhostHorizontalDistanceToEdge;
            //GhostVerticalMarginToBoundingRectangle = Window.ClientBounds.Height * CameraConstants.GhostVerticalDistanceToEdge;
        }

        private void ZoomOut(float zoomStep)
        {
            var step = zoomStep;// Math.Min(CameraConstants.ZoomStep, zoomStep);
            if (Camera.Zoom - step < Configuration.Camera.MinZoom)
            {
                step = Camera.Zoom - Configuration.Camera.MinZoom;
            }
            else if (Camera.Zoom - step > Configuration.Camera.MaxZoom)
            {
                step = Camera.Zoom - Configuration.Camera.MaxZoom;
            }
            Camera.ZoomOut(deltaZoom: step);
        }

        public void FocusOnPoint(Coordinate focusPoint, Vector2 relativePointInScreen)
        {
            var rect = BoundingRectangle();
            var direction = focusPoint - new Coordinate(rect.Left+ rect.Width*relativePointInScreen.X, rect.Top + rect.Height * relativePointInScreen.Y);
            var speed = Math.Min(direction.Length(), Configuration.Camera.MaxPanSpeed);
            if (speed < 1)
            {
                return;
            }
            direction.Normalize();
            MoveCamera(direction: direction* speed);
        }

        public void Update()
        {
            if (Configuration.Camera.FreeCamera)
            {
                return;
            }

            var mainCharaPosition = World.MainCharacterPositionInDisplay;
            var ghostPosition = World.GhostCharacterPositionInDisplay;
            if (ghostPosition.HasValue) // && !WithinView(ghostPosition.Value, ghosty: true))
            {
                //Use normal margin and ghosty on one side each, because ghost can go closer to the edge
                var viewRect = HalfGhostyActionBoundingRectangle();
                var distanceBetweenCharas = mainCharaPosition - ghostPosition.Value;
                var distanceBetweenCharasX = Math.Abs(mainCharaPosition.X - ghostPosition.Value.X);
                var distanceBetweenCharasY = Math.Abs(mainCharaPosition.Y - ghostPosition.Value.Y);
                 
                //if ()
                //if (distanceBetweenCharasX > viewRect.Width || distanceBetweenCharasY > viewRect.Height)
                
                var xFactor = distanceBetweenCharasX / viewRect.Width;
                var yFactor = distanceBetweenCharasY / viewRect.Height;
                var maxFactor = Math.Max(xFactor, yFactor);

                ZoomOut(Camera.Zoom*(maxFactor - 1));
                
                var ghostDirection = FromEdgeToPoint(ghostPosition.Value, ghosty: true);
                MoveCamera(direction: ghostDirection);

            }
            else
            {
                ZoomOut(Camera.Zoom * (Configuration.Camera.MaxZoom - 1));
                //Camera.Zoom = Configuration.Camera.MaxZoom;
            }

            //Adjust for main character
            
            if (!WithinView(mainCharaPosition))
            {
                var direction = FromEdgeToPoint(mainCharaPosition);
                MoveCamera(direction: direction);
            }

            //If the ghost is not visible anymore, it has to be removed
            if (ghostPosition.HasValue && Camera.Zoom == Configuration.Camera.MinZoom && !WithinView(ghostPosition.Value, ghosty: true) )
            {
                OnGhostOutsideCamera?.Invoke();
            }
        }

        private bool WithinView(Coordinate point, bool ghosty=false)
        {
            
            return ghosty? GhostyActionBoundingRectangle().Contains(point) : ActionBoundingRectangle().Contains(point);
        }

     
        private Vector2 FromEdgeToPoint(Coordinate point, bool ghosty=false)
        {
            var rect = ghosty ? GhostyActionBoundingRectangle() : ActionBoundingRectangle();
            Coordinate closestOnEdge = rect.ClosestPointTo(point);
            return point - closestOnEdge;
        }

        private RectangleF ActionBoundingRectangle()
        {
            var viewRect = BoundingRectangle();
            var xMargin = viewRect.Width * CameraConstants.HorizontalDistanceToEdge;
            var yMargin = viewRect.Height * CameraConstants.VerticalDistanceToEdge;
            return new RectangleF(  x: viewRect.X + xMargin ,
                                    y: viewRect.Y + yMargin ,
                                    width: viewRect.Width - 2 * xMargin ,
                                    height: viewRect.Height - 2 * yMargin ) ;
        }

        private RectangleF HalfGhostyActionBoundingRectangle()
        {
            var viewRect = BoundingRectangle();
            var xMargin = viewRect.Width * CameraConstants.HorizontalDistanceToEdge;
            var yMargin = viewRect.Height * CameraConstants.VerticalDistanceToEdge;
            var xMarginGhost = viewRect.Width * CameraConstants.GhostHorizontalDistanceToEdge;
            var yMarginGhost = viewRect.Height * CameraConstants.GhostVerticalDistanceToEdge;
            return new RectangleF(x: viewRect.X + (xMargin + xMarginGhost) *0.5f ,
                                    y: viewRect.Y + (yMargin + yMarginGhost) *0.5f,
                                    width: viewRect.Width - (xMargin + xMarginGhost),
                                    height: viewRect.Height - (yMargin + yMarginGhost));
        }

        private RectangleF GhostyActionBoundingRectangle()
        {
            var viewRect = BoundingRectangle();

            var xMarginGhost = viewRect.Width * CameraConstants.GhostHorizontalDistanceToEdge;
            var yMarginGhost = viewRect.Height * CameraConstants.GhostVerticalDistanceToEdge;
            return new RectangleF(x: viewRect.X + xMarginGhost,
                                    y: viewRect.Y + yMarginGhost,
                                    width: viewRect.Width - 2 * xMarginGhost,
                                    height: viewRect.Height - 2 * yMarginGhost);
        }

        private RectangleF BoundingRectangle()
        {
            var viewRect = Camera.BoundingRectangle;
            //var widthCorrection = ScreenBounds.Width / CameraConstants.ReferenceResolution.X;
            //var heightCorrection = ScreenBounds.Height / CameraConstants.ReferenceResolution.Y;
            var correction = Math.Max(ScreenBounds.Width / CameraConstants.ReferenceResolution.X, ScreenBounds.Height / CameraConstants.ReferenceResolution.Y);
            var newViewRect = new RectangleF(x: viewRect.X,
                y: viewRect.Y,
                width: viewRect.Width / correction,
                height: viewRect.Height / correction
                );

            return newViewRect;
        }

        public void ZoomIn(float? zoomStep = null)
        {
            Camera.ZoomIn(zoomStep.GetValueOrDefault(CameraConstants.ZoomStep));
        }
        public void ZoomOut()
        {
            Camera.ZoomOut(CameraConstants.ZoomStep);
        }

        public Matrix GetViewMatrix(Vector2 parallaxFactor)
        {
            return this.Camera.GetViewMatrix(parallaxFactor: parallaxFactor);
        }
        public void MoveCamera(Vector2 direction)
        {
            Camera.Move(direction: direction);
        }
        public void MoveCameraRight()
        {
            this.MoveCamera(new Vector2(CameraConstants.MoveStep,0));
        }
        public void MoveCameraLeft()
        {
            this.MoveCamera(new Vector2(-CameraConstants.MoveStep, 0));
        }
        public void MoveCameraUp()
        {
            this.MoveCamera(new Vector2(0,-CameraConstants.MoveStep));
        }
        public void MoveCameraDown()
        {
            this.MoveCamera(new Vector2(0, CameraConstants.MoveStep));
        }
        public void SetWorld(PhysicalWorld world)
        {
            this.World = world;
        }

        public Coordinate DistanceFromCameraToPoint(Coordinate pointInDisplay)
        {
            if (WithinView(point: pointInDisplay, ghosty: true))
            {
                return Vector2.Zero;
            }
            return FromEdgeToPoint(point: pointInDisplay, ghosty: true);
        }

        public Coordinate DistanceFromCameraCenterToPoint(Coordinate pointInDisplay)
        {
            var distance = pointInDisplay - new Coordinate(BoundingRectangle().Center.X, BoundingRectangle().Center.Y);
            return distance;
        }
        public Coordinate CameraCenterInDisplay()
        {
            return Camera.BoundingRectangle.Center;
        }

        public bool IsVisible(Coordinate pointInDisplay, float scaleBoundingRectangel=1f)
        {
            var rect = BoundingRectangle();
            var scaledRect = new RectangleF(
                x: rect.X - rect.Width * 0.5f * scaleBoundingRectangel,
                y: rect.Y - rect.Height * 0.5f * scaleBoundingRectangel,
                width: rect.Width*scaleBoundingRectangel,
                height: rect.Height*scaleBoundingRectangel
                );
            return scaledRect.Contains(pointInDisplay);
        }

        public Coordinate CharacterPositionInCamera()
        {
            return Camera.WorldToScreen(World.MainCharacterPositionInDisplay);
        }
        public float DistanceToFurthestPointOnScreenEdge()
        {
            var rect = BoundingRectangle();
            var pointOnEdge = World.MainCharacterPositionInDisplay;
            var x = (pointOnEdge.X - rect.Left) < rect.Width/2 ? rect.Right : rect.Left;
            var y = (pointOnEdge.Y - rect.Top) < rect.Height/2 ? rect.Bottom: rect.Top;
            var distanceVector = new Coordinate(x, y) - World.MainCharacterPositionInDisplay;
            return distanceVector.Length();
        }
    }

}
