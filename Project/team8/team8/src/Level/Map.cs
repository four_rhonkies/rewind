﻿/**
* Copyright 2017 Niclas Scheuing, Per Näslung, Tunay Bora, Xiaowei Zeng
* This file is part of REWIND.
*
* REWIND is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* REWIND is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
* GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with REWIND.If not, see<http://www.gnu.org/licenses/>.
*/
using FarseerPhysics;
using FarseerPhysics.Dynamics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using team8.Physics;

namespace team8.Level
{
    using Coordinate = Vector2;
    public class Map
    {
        private int WidthInTiles;
        private int HeightInTiles;
        public int TileWidth;
        public int TileHeight;

        public List<TileSet> TileSets;
        public List<Layer> Layers;
        public List<ObjectGroup> ObjectGroups;

        private static Matrix MapToDisplay;

        private static readonly float MapToDisplayScale = 1f;

        //public Map(int widthInTiles, int heightInTiles, int tileWidth, int tileHeight)
        //{
        //    this.WidthInTiles = widthInTiles;
        //    this.HeightInTiles = heightInTiles;
        //    this.TileWidth = tileWidth;
        //    this.TileHeight = tileHeight;
        //}

        public Map(XElement mapElement)
        {
            MapToDisplay = Matrix.Identity;
            MapToDisplay.Scale = new Vector3(MapToDisplayScale, MapToDisplayScale, MapToDisplayScale);
            Parse(mapElement: mapElement);
        }

        private void Parse(XElement mapElement)
        {
            this.WidthInTiles = int.Parse(mapElement.Attribute("width").Value);
            this.HeightInTiles = int.Parse(mapElement.Attribute("height").Value);
            this.TileWidth = int.Parse(mapElement.Attribute("tilewidth").Value);
            this.TileHeight = int.Parse(mapElement.Attribute("tileheight").Value);
            
        }

        public TileSet TileSetForId(int id)
        {
            return this.TileSets.Find(set => set.Id == id);
        }

       
        public void LoadContent(ContentManager contentManager, PhysicalWorld world)
        {
            foreach(TileSet tileSet in TileSets)
            {
                tileSet.LoadContent(contentManager: contentManager, world: world);
            }
            foreach(ObjectGroup group in ObjectGroups)
            {
                group.LoadContent(world: world, map: this, contentManager: contentManager);
            }
        }

        public void Dispose()
        {
            foreach (TileSet tileSet in TileSets)
            {
                tileSet.Dispose();
            }
            foreach (ObjectGroup group in ObjectGroups)
            {
                group.Dispose();
            }
        }

        /*----- STATIC-----*/
        public static Coordinate TransformFromMapToDisplay(Coordinate point)
        {
            return Vector2.Transform(point, Map.MapToDisplay);
        }

        public static Coordinate TransformFromMapToSim(Coordinate point)
        {
            return ConvertUnits.ToSimUnits(Vector2.Transform(point, Map.MapToDisplay));
        }

        public static float TransformFromMapToDisplay(float distance)
        {
            return distance * MapToDisplayScale;
        }

        public static IEnumerable<Coordinate> TransformFromMapToDisplay(IEnumerable<Coordinate> points)
        {
            var transformedPoints = new List<Coordinate>();
            foreach(Coordinate point in points)
            {
                transformedPoints.Add(TransformFromMapToDisplay(point: point));
            }
            return transformedPoints;
        }

        public static IEnumerable<Coordinate> TransformFromMapToSim(IEnumerable<Coordinate> points)
        {
            var transformedPoints = new List<Coordinate>();
            foreach (Coordinate point in points)
            {
                var transPoint = TransformFromMapToDisplay(point: point);
                transPoint = ConvertUnits.ToSimUnits(transPoint);
                transformedPoints.Add(transPoint);
            }
            return transformedPoints;
        }
    }
}
