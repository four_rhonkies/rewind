﻿/**
* Copyright 2017 Niclas Scheuing, Per Näslung, Tunay Bora, Xiaowei Zeng
* This file is part of REWIND.
*
* REWIND is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* REWIND is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
* GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with REWIND.If not, see<http://www.gnu.org/licenses/>.
*/
using FarseerPhysics.Dynamics;
using FarseerPhysics.Dynamics.Contacts;
using Microsoft.Xna.Framework.Content;
using System.Linq;
using team8.Physics;

namespace team8.Level
{
    public class SwitchLevelObject: PhysicsLevelObject
    {
        public readonly string SwitchId;
        protected PhysicalWorld World;
        private int CurrentCollisionCount = 0;
        private SwitchEvent SwitchEvent;

        public SwitchLevelObject(string switchId, string id): base(id:id)
        {
            this.SwitchId = switchId;
        }

        public override void LoadContent(PhysicalWorld world, Map map, ContentManager contentManager)
        {
            this.World = world;
            this.SwitchEvent = GetSwitchEvent();
            base.LoadContent(world: world, map: map, contentManager: contentManager);
            Body.IsSensor = true;
            Body.FixtureList.First().CollidesWith = Constants.CategoryMainCharacter | Constants.CategoryOthers | Constants.CategoryGround | Constants.CategoryWall;// | Constants.CategoryCollision;
            Body.FixtureList.First().CollisionCategories = Constants.CategorySwitch;
            Body.FixtureList.First().CollisionGroup = Constants.CollisionGroupSwitch;

            Body.OnCollision += OnCollision;
            Body.OnSeparation += OnSeparation;

            World.AddSwitchObject(this);
        }

        protected virtual SwitchEvent GetSwitchEvent()
        {
            return SwitchEvent.GetSwitchEvent(switchId: this.SwitchId, world: World);
        }

        virtual protected bool OnCollision(Fixture fixtureA, Fixture fixtureB, Contact contact)
        {
           if (SwitchEvent.ReactToContact(fixtureA: fixtureA, fixtureB: fixtureB))
            {
                if (!SwitchEvent.CountContacts() || CurrentCollisionCount == 0)
                {
                    SwitchEvent.OnCollision(contact: contact);
                    Body.OnSeparation += SwitchEvent.KillEvent;
                }
                CurrentCollisionCount++;
            }
            return true;
        }
        virtual protected void OnSeparation(Fixture fixtureA, Fixture fixtureB)
        {
            if (SwitchEvent.ReactToContact(fixtureA: fixtureA, fixtureB: fixtureB) )
            { 
                if(!SwitchEvent.CountContacts() || CurrentCollisionCount == 1)
                {
                    SwitchEvent.OnSeparation();
                }
                CurrentCollisionCount--;
            }
        }

        public void Reset()
        {
            SwitchEvent?.Reset();
        }

    }
}
