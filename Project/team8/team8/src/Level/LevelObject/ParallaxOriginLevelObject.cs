﻿/**
* Copyright 2017 Niclas Scheuing, Per Näslung, Tunay Bora, Xiaowei Zeng
* This file is part of REWIND.
*
* REWIND is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* REWIND is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
* GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with REWIND.If not, see<http://www.gnu.org/licenses/>.
*/
using FarseerPhysics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using team8.Physics;
using static team8.Control.MovementController;
using static team8.Physics.PhysicsObject;

namespace team8.Level
{
    using Coordinate = Vector2;
    public class ParallaxOriginLevelObject : LevelObject
    {
        public Coordinate ParallaxOrigin;

        public ParallaxOriginLevelObject(string id) : base(id: id)
        {

        }

        protected override void Parse(XElement objectElement)
        {
            var polyline = ParsePolyline(objectElement: objectElement);
            List<Coordinate> points = polyline.Points.ToList();

            ParallaxOrigin = points.First() + this.Origin;
        }

        public override void LoadContent(PhysicalWorld world, Map map, ContentManager contentManager)
        {
            world.SetParallaxOrigin(parallaxOrigin: this.ParallaxOrigin);
        }


    }
}