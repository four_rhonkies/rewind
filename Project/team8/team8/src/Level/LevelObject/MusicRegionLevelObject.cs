﻿/**
* Copyright 2017 Niclas Scheuing, Per Näslung, Tunay Bora, Xiaowei Zeng
* This file is part of REWIND.
*
* REWIND is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* REWIND is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
* GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with REWIND.If not, see<http://www.gnu.org/licenses/>.
*/
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace team8.Level
{
    using Microsoft.Xna.Framework.Content;
    using MonoGame.Extended.Shapes;
    using team8.Physics;
    using Coordinate = Vector2;

    public class MusicRegionLevelObject : GeometryLevelObject
    {
        public string MusicRegionId;
        private Polygon PolygonInDisplay;

        public MusicRegionLevelObject(string id, string musicRegionId) : base(id)
        {
            this.MusicRegionId = musicRegionId;
        }

        public bool PointWithinRegion(Coordinate pointInDisplay)
        {
            return PolygonInDisplay.Contains(pointInDisplay);
        }

        public override void LoadContent(PhysicalWorld world, Map map, ContentManager contentManager)
        {
            base.LoadContent(world, map, contentManager);
            var points = this.PointsInDisplay();
            this.PolygonInDisplay = new Polygon(points);
            world.AddMusicRegionObject(this);
        }
    }
}
