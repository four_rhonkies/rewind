﻿/**
* Copyright 2017 Niclas Scheuing, Per Näslung, Tunay Bora, Xiaowei Zeng
* This file is part of REWIND.
*
* REWIND is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* REWIND is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
* GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with REWIND.If not, see<http://www.gnu.org/licenses/>.
*/
using FarseerPhysics;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Dynamics.Contacts;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using team8.Animation;
using team8.LevelImporter;
using team8.Physics;
using team8.Sound;
using static team8.Physics.PhysicsObject;
using static team8.Sound.SoundManager;

namespace team8.Level
{
    using Coordinate = Vector2;
    public class SpiritLevelObject : PhysicsLevelObject
    {
        private PhysicalWorld World;
        private bool Collected = false;
        public Coordinate SpawnPosition;
        public Direction FacingDirection;

        private string TriggerRenderGroupId;

        public SpiritLevelObject(string id): base(id:id)
        {
        }

        public override void LoadContent(PhysicalWorld world, Map map, ContentManager contentManager)
        {
            this.World = world;
            base.LoadContent(world, map, contentManager: contentManager);
            Body.IsSensor = true;
            Body.FixtureList.First().CollidesWith = Constants.CategoryMainCharacter | Constants.CategoryOthers | Constants.CategoryGround | Constants.CategoryWall;
            Body.FixtureList.First().CollisionCategories = Constants.CategorySwitch;
            Body.FixtureList.First().CollisionGroup = Constants.CollisionGroupSwitch;

            Body.OnCollision += OnCollision;

            //Preload free animation
            contentManager.Load<Texture2D>(AnimationPath.FreeSprits);
            //Body.OnSeparation += OnSeparation;
        }

        protected override void ParseProperties(XElement objectElement)
        {
            var rendergroupidString = "trigger_rendergroupid";

            var keys = new List<string>() { rendergroupidString };

            //parse the Xelement
            var foundValues = TMXImporter.ParseProperties(element: objectElement, objectType: "SpiritLevelObject", keys: keys, failOnMissing: true);

            //convert the found values. If the value was not found, keep the current (default) value
            var rendergroupid = foundValues.ContainsKey(rendergroupidString) ? foundValues[rendergroupidString] : null;
            //Set values
            this.TriggerRenderGroupId = rendergroupid;

            base.ParseProperties(objectElement);
        }

        private bool OnCollision(Fixture fixtureA, Fixture fixtureB, Contact contact)
        {

            bool contactToMain = MainCharacter.ContactIsToMainCharacter(fixtureA: fixtureA, fixtureB: fixtureB);
            bool contactToGhost = MainCharacter.ContactIsToGhost(fixtureA: fixtureA, fixtureB: fixtureB);
            
            if (!Collected && (contactToMain || contactToGhost))
            {
                Collected = true;

                UpdatPositionDelegate updatePosition = delegate (GameTime gameTime)
                {
                    return DisplayPosition();
                };

                SoundManager.Instance.PlaySound(file: SoundFile.SpiritFreed, id: this.TriggerRenderGroupId, initialVolume: 1f, positionDelegate: updatePosition);
                this.SpawnPosition = contactToMain ? World.MainCharacter.Position : World.GhostCharacter.Position;
                this.FacingDirection = contactToMain ? World.MainCharacter.LastDirection : World.GhostCharacter.LastDirection;
                World.SetLastSpiritObject(this);

                var renderGroup = World.GetRenderPhysicsGroup(renderGroupId: TriggerRenderGroupId);
                var animationObject = ((AnimationLevelObject)renderGroup.VisualLevelObject);
                var oldFrameWidth = animationObject.AnimationState.FrameWidth;
                
                animationObject.AnimationState.SetAnimation(texturePath: AnimationPath.FreeSprits,
                    rowCount: AnimationConstants.FrameCount.FreeSpiritsRows,
                    columnCount: AnimationConstants.FrameCount.FreeSpiritsColumns,
                    frameDuration: AnimationConstants.Duration.FreeSpirits,
                    startAndEndIndex: (0, AnimationConstants.FrameCount.FreeSpiritsTotal),
                    loop: false
                    );

                var newFrameWidth = animationObject.AnimationState.FrameWidth;

                animationObject.AnimationState.MultiplayScale((float)oldFrameWidth / (float)newFrameWidth);
            }
            return true;
        }
        //private void OnSeparation(Fixture fixtureA, Fixture fixtureB)
        //{
        //    if (SwitchEvent.ReactToContact(fixtureA: fixtureA, fixtureB: fixtureB) && (!SwitchEvent.CountContacts() || CurrentCollisionCount == 1))
        //    {
        //        SwitchEvent.OnSeparation();
        //    }
        //    CurrentCollisionCount--;
        //}

    }
}
