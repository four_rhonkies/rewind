﻿/**
* Copyright 2017 Niclas Scheuing, Per Näslung, Tunay Bora, Xiaowei Zeng
* This file is part of REWIND.
*
* REWIND is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* REWIND is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
* GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with REWIND.If not, see<http://www.gnu.org/licenses/>.
*/
using FarseerPhysics;
using FarseerPhysics.Common;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Factories;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using MonoGame.Extended;
using MonoGame.Extended.Shapes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using team8.LevelImporter;
using team8.Physics;

namespace team8.Level
{
    using Coordinate = Vector2;
    public class GeometryLevelObject: LevelObject
    {
        protected Polyline Polyline;
        protected RectangleF Rectangle;
        //private Ellipse Ellipse;
        protected Polygon Polygon;
        protected GeometryType GeometryType;

        public GeometryLevelObject(string id): base(id: id)
        {

        }

        protected override void Parse(XElement objectElement)
        {
            //get geometry type. If non is set, it is a rectangle
            string type = null;
            if (objectElement.HasElements)
            {
                foreach (var ele in objectElement.Elements())
                {
                    if (ele.Name.ToString() != "properties")
                    {
                        type = ele.Name.ToString();
                        break;
                    }
                }
            }
            if (type == null)
            {
                type = "rectangle";
            }


            //Parse geometry
            ParseGeometry(geometryType: type, objectElement: objectElement);
        }

        protected void ParseGeometry(string geometryType, XElement objectElement)
        {
            switch (geometryType)
            {
                case "polyline":
                    this.GeometryType = GeometryType.polyline;
                    this.Polyline = ParsePolyline(objectElement: objectElement);
                    break;
                //case "ellipse":
                //    this.GeometryType = GeometryType.ellipse;
                //    ParseEllipse(objectElement: objectElement);
                //    break;
                case "polygon":
                    this.GeometryType = GeometryType.polygon;
                    this.Polygon = ParsePolygon(objectElement: objectElement);
                    break;
                case "rectangle":
                    this.GeometryType = GeometryType.rectangle;
                    this.Rectangle = ParseRectangle(objectElement: objectElement);
                    break;
                default:
                    throw new Exception("Invalid geometry type when parsing object from level.");
            }
        }

        public IEnumerable<Coordinate> PointsInDisplay(bool relativeToPosition = false)
        {
            List<Coordinate> points = null;
            switch (GeometryType)
            {
                case GeometryType.polyline:
                    points = Polyline.Points.ToList();
                    break;
                case GeometryType.polygon:
                    points = Polygon.Vertices.ToList();
                    break;
                case GeometryType.rectangle:
                    points = Rectangle.GetCorners().ToList();
                    break;
            }
            if (points != null)
            {
                Matrix2D rotationMatrix = Matrix2D.CreateRotationZ(Rotation());

               var positionInDisplayUnit = DisplayPosition();
                for (int i = 0; i < points.Count; i++)
                {
                    var point = rotationMatrix.Transform(points[i]);
                    points[i] = Map.TransformFromMapToDisplay(point) + (relativeToPosition ? Vector2.Zero : positionInDisplayUnit);
                }
            }
            return points.AsEnumerable();
        }

        public IEnumerable<Coordinate> PointsInSim(bool relativeToPosition = false)
        {
            var points = PointsInDisplay(relativeToPosition: relativeToPosition).ToList();
            for (int i = 0; i < points.Count; i++)
            {
                points[i] = ConvertUnits.ToSimUnits(points[i]);
            }
            return points.AsEnumerable();
        }


        virtual public float Rotation()
        {
            //TODO: implement
            //var rotation = Body == null ? 0f : Body.Rotation;

            return 0;
        }

        virtual public Coordinate DisplayPosition()
        {
            //TODO: implement
            //var positionInDisplayUnit = Body == null ? Map.TransformFromMapToDisplay(this.Origin) : ConvertUnits.ToDisplayUnits(Body.Position);
            return this.OriginInDisplayCoordinates;
        }
    }
}
