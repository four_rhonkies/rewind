﻿/**
* Copyright 2017 Niclas Scheuing, Per Näslung, Tunay Bora, Xiaowei Zeng
* This file is part of REWIND.
*
* REWIND is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* REWIND is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
* GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with REWIND.If not, see<http://www.gnu.org/licenses/>.
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Content;
using team8.Physics;
using FarseerPhysics.Dynamics.Joints;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Factories;
using team8.LevelImporter;
using System.Xml.Linq;
using Microsoft.Xna.Framework;
using FarseerPhysics.Common;
using Microsoft.Xna.Framework.Graphics;
using FarseerPhysics;
using FarseerPhysics.Dynamics.Contacts;

namespace team8.Level
{
    using Coordinate = Vector2;
    class RopeLevelObject : GeometryLevelObject
    {
        private float Density = 5f;
        private readonly float SegmentWidth = 0.02f;
        private float TextureWidth = 5;
        private List<RopeSegment> Segments;
        private Texture2D SegmentTexture;
        private Texture2D DebugTexture;
        private float MaxAngle = 10;
        private bool FixStart = true;
        private bool FixEnd = false;
        private float ImpulseFactor = 0.007f;

        private struct RopeSegment
        {
            public Body Body;
            public float Length;
            public float Angle;
            public Coordinate StartPoint;
            public Coordinate EndPoint;
            public RopeSegment(Body body, float length, float angle, Coordinate startPoint, Coordinate endPoint)
            {
                this.Body = body;
                this.Length = length;
                this.Angle = angle;
                this.StartPoint = startPoint;
                this.EndPoint = endPoint;
            }
        }

        public RopeLevelObject(string id) : base(id)
        {
        }
        override protected void ParseProperties(XElement objectElement)
        {
            //define the keys we are looking for
            var densityString = "density";
            var widthString = "width";
            var fixStartString = "fix_start";
            var fixEndString = "fix_end";
            var keys = new List<string>() { densityString, widthString, fixStartString, fixEndString };

            //parse the Xelement
            var foundValues = TMXImporter.ParseProperties(element: objectElement, objectType: "LayerObject", keys: keys, failOnMissing: false);

            //convert the found values. If the value was not found, keep the current (default) value
            var density = foundValues.ContainsKey(densityString) ? float.Parse(foundValues[densityString]) : this.Density;
            this.TextureWidth = foundValues.ContainsKey(widthString) ? float.Parse(foundValues[widthString]) : this.TextureWidth;
            this.FixEnd = foundValues.ContainsKey(fixEndString) ? bool.Parse(foundValues[fixEndString]) : this.FixEnd;
            this.FixStart = foundValues.ContainsKey(fixStartString) ? bool.Parse(foundValues[fixStartString]) : this.FixStart;
            //Set values
            this.Density = density;
        }

        public override void LoadContent(PhysicalWorld world, Map map, ContentManager contentManager)
        {
            base.LoadContent(world, map, contentManager);
            Segments = new List<RopeSegment>();
            var points = Map.TransformFromMapToSim(Polyline.Points).ToList();
            var simOrigin = Map.TransformFromMapToSim(this.Origin);

            //Create segment bodies
            for (int i = 0; i < points.Count() - 1; i++)
            {
                var directionBetweenPoints = points[i + 1] - points[i];
                var normDirectionBetweenPoints = points[i + 1] - points[i];
                normDirectionBetweenPoints.Normalize();
                var segmentNormal = Vector2.Transform(directionBetweenPoints, Matrix.CreateRotationZ(MathHelper.ToRadians(90)));

                var bodyPoints = new List<Coordinate>() {   (segmentNormal+normDirectionBetweenPoints)*SegmentWidth,
                                                            (- segmentNormal+normDirectionBetweenPoints)*SegmentWidth,
                                                            directionBetweenPoints + (segmentNormal-normDirectionBetweenPoints)*SegmentWidth,
                                                            directionBetweenPoints  + (- segmentNormal-normDirectionBetweenPoints)*SegmentWidth
                                                        };
                //var center = bodyPoints.First() + bodyPoints.Last();
                Vertices vertices = new Vertices(bodyPoints);
                var segment = BodyFactory.CreatePolygon(world: world.World, vertices: vertices, density: this.Density, position: points[i] + simOrigin, bodyType: BodyType.Dynamic);
                //var sensor = FixtureFactory.AttachPolygon(vertices: vertices, density: 0.001f, body: segment, userData: Constants.RopeUserData);
                
                //segment.CollisionCategories = Constants.CategoryOthers;
                //segment.CollidesWith = Constants.CategoryGround | Constants.CategoryOthers | Constants.CategoryCollision;
                //segment.CollisionGroup = Constants.CollisionGroupNone;
                //sensor.CollisionCategories = Constants.CategoryRope;
                //sensor.CollidesWith = Constants.CategoryMainCharacter;
                //sensor.IsSensor = true;
                //sensor.OnCollision += OnCollision;

                segment.CollisionCategories = Constants.CategoryRope;
                segment.CollidesWith = Constants.CategoryMainCharacter;
                segment.IsSensor = true;
                segment.OnCollision += OnCollision;

                float angle = (float)Math.Atan2(normDirectionBetweenPoints.Y , normDirectionBetweenPoints.X);
                Segments.Add(new RopeSegment(body: segment, length: directionBetweenPoints.Length(), angle:angle, startPoint: points[i] + simOrigin, endPoint: points[i] + simOrigin + directionBetweenPoints));
            }
            //Crete joints
                float totalLength = 0;
            for (int i = 0; i < Segments.Count - 1; i++)
            {
                var revoluteJoint = new RevoluteJoint(bodyA: Segments[i].Body, bodyB: Segments[i + 1].Body, anchor: Segments[i + 1].Body.Position, useWorldCoordinates: true);
                revoluteJoint.ReferenceAngle = MathHelper.ToRadians(Segments[i].Angle - Segments[i + 1].Angle);
                revoluteJoint.LowerLimit = MathHelper.ToRadians(-MaxAngle);
                revoluteJoint.UpperLimit = MathHelper.ToRadians(MaxAngle);
                //revoluteJoint.LimitEnabled = true;
                world.World.AddJoint(revoluteJoint);

                //for (int j = i+1; j < Segments.Count; j++)
                //{
                //if (i == j)
                //{
                //    continue;
                //}
                var j = i + 1;
                var ropeJoint = new RopeJoint(bodyA: Segments[0].Body, bodyB: Segments[j].Body, anchorA: Segments[0].EndPoint, anchorB: Segments[j].StartPoint, useWorldCoordinates: true);
                ropeJoint.MaxLength = totalLength + 0.1f; ;
                world.World.AddJoint(ropeJoint);

                totalLength = totalLength + Segments[j].Length;
            }
            totalLength = 0;
            for (int i = Segments.Count - 2; i >0 ; i--)
            {
                var ropeJointEnd = new RopeJoint(bodyA: Segments.Last().Body, bodyB: Segments[i].Body, anchorA: Segments.Last().StartPoint, anchorB: Segments[i].EndPoint, useWorldCoordinates: true);
                ropeJointEnd.MaxLength = totalLength + 0.1f; ;
                world.World.AddJoint(ropeJointEnd);
                totalLength = totalLength + Segments[i].Length;
                //}
            }
            //Creat anchors
            //float AnchorSize = 0.1f;
            //var startAnchorPoints = new List<Coordinate>() {   new Coordinate(AnchorSize,AnchorSize),
            //                                                    new Coordinate(-AnchorSize,AnchorSize),
            //                                                    new Coordinate(-AnchorSize,-AnchorSize),
            //                                                    new Coordinate(AnchorSize,-AnchorSize),
            //                                     };
            //var startAnchor = BodyFactory.CreatePolygon(world: world.World, vertices: new Vertices(startAnchorPoints), density: 0.1f, position: Segments[0].Body.Position, bodyType: BodyType.Kinematic);
            //startAnchor.FixedRotation = true;
            //startAnchor.CollidesWith = Category.None;
            ////foreach(var outeregment in Segments)
            //{
            //    //var startAnchorJoint = new RevoluteJoint(bodyA: Segments[0].Body, bodyB: startAnchor, anchor: Segments[0].Body.Position + new Coordinate(0.001f, 0), useWorldCoordinates: true);
            //    //world.World.AddJoint(startAnchorJoint);
            //    var startDistanceJoint = new DistanceJoint(bodyA: segment.Body, bodyB: startAnchor, anchorA: segment.Body.Position, anchorB: startAnchor.Position, useWorldCoordinates: true);
            //    startDistanceJoint.CollideConnected = false;
            //    startDistanceJoint.DampingRatio = 10;
            //    startDistanceJoint.Frequency = 100;
            //    world.World.AddJoint(startDistanceJoint);
            //}

            //var endAnchor = BodyFactory.CreatePolygon(world: world.World, vertices: new Vertices(startAnchorPoints), density: 0.1f, position: Segments.Last().Body.Position, bodyType: BodyType.Static);
            //endAnchor.FixedRotation = true;
            //endAnchor.CollidesWith = Category.None;
            //var endAnchorJoint = new RevoluteJoint(bodyA: Segments.Last().Body, bodyB: endAnchor, anchor: Segments.Last().Body.Position + new Coordinate(0.001f, 0), useWorldCoordinates: true);
            //world.World.AddJoint(endAnchorJoint);
            Segments.First().Body.IsStatic = this.FixStart;
            Segments.Last().Body.IsStatic = this.FixEnd;


            SegmentTexture = contentManager.Load<Texture2D>("levels/level1/rope_segment");
        }

        private bool OnCollision(Fixture fixtureA, Fixture fixtureB, Contact contact)
        {
            var aIsMainCharacter = (string)fixtureA.UserData == Constants.UserDataMainCharacter;
            //var bIsMainCharacter = (string)fixtureB.UserData == Constants.UserDataMainCharacter;
            var character = aIsMainCharacter ? fixtureA : fixtureB;
            var rope = aIsMainCharacter ? fixtureB : fixtureA;
            if (MainCharacter.ContactIsToMainCharacter(contact) || MainCharacter.ContactIsToGhost(fixtureA: fixtureA, fixtureB: fixtureB))
            {
                rope.Body.ApplyLinearImpulse(character.Body.LinearVelocity * ImpulseFactor);
                //rope.IgnoreCollisionWith(character);
            }

            return true;
        }

        public override void Draw(SpriteBatch spriteBatch, GameTime gameTime, Matrix transform, Action beginAgain)
        {
            if (IsHidden)
            {
                return;
            }




            foreach (var segment in Segments)
            {
                var displayPosition = ConvertUnits.ToDisplayUnits(segment.Body.Position);
                var scale = new Vector2(ConvertUnits.ToDisplayUnits(segment.Length)*1.15f / SegmentTexture.Width, TextureWidth/SegmentTexture.Height);
                spriteBatch.Draw(texture: SegmentTexture, position: displayPosition, rotation: segment.Body.Rotation+segment.Angle, scale: scale);
            }


            if (Configuration.RenderObjectVertices)
            {
                if (DebugTexture == null)
                {
                    SetupDebugRendering(graphicsDevice: spriteBatch.GraphicsDevice);
                }

                var points = PointsInSim(relativeToPosition: false).ToList();
                for (int i = 0; i < points.Count() - 1; i++)
                {
                    spriteBatch.Draw(texture: DebugTexture, position: ConvertUnits.ToDisplayUnits(points[i]), color: new Color(i / 5f, 1 - i / 5f, 0));
                    var directionBetweenPoints = points[i + 1] - points[i];
                    directionBetweenPoints.Normalize();
                    var segmentNormal = Vector2.Transform(directionBetweenPoints, Matrix.CreateRotationZ(MathHelper.ToRadians(90)));
                    var bodyPoints = new List<Coordinate>() {   points[i] + (segmentNormal-directionBetweenPoints)*SegmentWidth,
                                                                points[i] + (- segmentNormal-directionBetweenPoints)*SegmentWidth,
                                                                points[i+1] + (segmentNormal+directionBetweenPoints)*SegmentWidth,
                                                                points[i+1]  + (- segmentNormal+directionBetweenPoints)*SegmentWidth
                                                            };

                    foreach (var point in bodyPoints)
                    {
                        spriteBatch.Draw(texture: DebugTexture, position: ConvertUnits.ToDisplayUnits(point), color: new Color(i / 5f, 1 - 1 - i / 5f, 1));
                    }
                }
                foreach (var segment in Segments)
                {
                    var displayPosition = ConvertUnits.ToDisplayUnits(segment.Body.Position);
                    spriteBatch.Draw(texture: DebugTexture, position: displayPosition, rotation: segment.Body.Rotation);
                }
            }
        }

        private void SetupDebugRendering(GraphicsDevice graphicsDevice)
        {
            var dotSize = 3;
            DebugTexture = new Texture2D(graphicsDevice, dotSize, dotSize);
            //Use different Colors for different objects
            Color color = Color.White;
            
            var colors = new Color[dotSize * dotSize];
            for (int i = 0; i < colors.Length; i++) { colors[i] = color; }
            DebugTexture.SetData(colors);
        }
    }
}
