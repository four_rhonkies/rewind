﻿/**
* Copyright 2017 Niclas Scheuing, Per Näslung, Tunay Bora, Xiaowei Zeng
* This file is part of REWIND.
*
* REWIND is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* REWIND is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
* GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with REWIND.If not, see<http://www.gnu.org/licenses/>.
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using FarseerPhysics.Dynamics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using team8.Physics;
using team8.LevelImporter;

namespace team8.Level
{
    using Coordinate = Vector2;
    public class ImageLevelObject : VisualLevelObject
    {
        //Id used for identifying the corresponding TileSet
        private int TileSetId;
        private TileSet TileSet;
        private float Alpha=1;

        //public Coordinate RotationOriginInDisplay
        //{
        //    get
        //    {
        //        return Map.TransformFromMapToDisplay(this.Origin+TileSet.RotationOrigin*Scale);
        //    }
        //}

        public ImageLevelObject(string id): base(id:id)
        {
        }


        override protected void ParseProperties(XElement objectElement)
        {
            base.ParseProperties(objectElement: objectElement);
            //define the keys we are looking for
            var alphaString = "alpha";
            var keys = new List<string>() { alphaString };

            //parse the Xelement
            var foundValues = TMXImporter.ParseProperties(element: objectElement, objectType: "LayerObject", keys: keys, failOnMissing: false);

            //convert the found values. If the value was not found, keep the current (default) value
            this.Alpha = foundValues.ContainsKey(alphaString) ? float.Parse(foundValues[alphaString]) : this.Alpha;
        }

        protected override void Parse(XElement objectElement)
        {
            base.Parse(objectElement: objectElement);
            this.TileSetId = int.Parse(objectElement.Attribute("gid").Value);
        }

        public override void Dispose()
        {
            TileSet.Dispose();
        }

        public override void Draw(SpriteBatch spriteBatch, GameTime gameTime, Matrix transform, Action beginAgain)
        {
            if (!this.IsHidden)
            {
                var totalRotation = RenderRotation();// + MathHelper.ToRadians(this.Rotation);
                this.TileSet.Draw(spriteBatch: spriteBatch, position: RenderPosition(), transform: transform, rotation: totalRotation, scale: this.Scale, alpha: Alpha);
            }
        }

        public override void LoadContent(PhysicalWorld world, Map map, ContentManager contentManager)
        {
            this.TileSet = map.TileSetForId(id: this.TileSetId);
            this.Scale = new Vector2(this.Width / TileSet.OriginalTextureWidth, this.Height / TileSet.OriginalTextureHeight);
            base.LoadContent(world: world, map: map, contentManager: contentManager);
        }
    }
}