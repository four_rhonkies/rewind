﻿/**
* Copyright 2017 Niclas Scheuing, Per Näslung, Tunay Bora, Xiaowei Zeng
* This file is part of REWIND.
*
* REWIND is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* REWIND is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
* GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with REWIND.If not, see<http://www.gnu.org/licenses/>.
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using FarseerPhysics.Dynamics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using team8.Physics;
using team8.LevelImporter;

namespace team8.Level
{
    using Coordinate = Vector2;
    public class VisualLevelObject : LevelObject
    {
        protected float Width;
        protected float Height;
        //Scale factor for rendering the texture
        protected Vector2 Scale;

        public Func<Coordinate> RenderPosition;
        public Func<float> RenderRotation;

        public VisualLevelObject(string id) : base(id: id)
        {
            this.RenderPosition = RenderPositionInDisplayUnits;
            this.RenderRotation = GetRotation;
        }

        protected override void Parse(XElement objectElement)
        {
            this.Width = float.Parse(objectElement.Attribute("width").Value);
            this.Height = float.Parse(objectElement.Attribute("height").Value);
            this.Origin = this.Origin - new Vector2(0, Height);


        }

        override protected void ParseProperties(XElement objectElement)
        {
            //define the keys we are looking for
            var renderGroupIdString = "rendergroupid";
            var keys = new List<string>() { renderGroupIdString };

            //parse the Xelement
            var foundValues = TMXImporter.ParseProperties(element: objectElement, objectType: "LayerObject", keys: keys, failOnMissing: false);

            //convert the found values. If the value was not found, keep the current (default) value
            var renderGroupId = foundValues.ContainsKey(renderGroupIdString) ? foundValues[renderGroupIdString] : this.RenderGroupId;

            //Set values
            this.RenderGroupId = renderGroupId;
        }

        override protected void ReadProperties(LevelObjectProperties? properties)
        {
            if (properties.HasValue)
            {
                this.RenderGroupId = properties.Value.RenderGroupId != null ? properties.Value.RenderGroupId : this.RenderGroupId;
            }
        }
        public override void LoadContent(PhysicalWorld world, Map map, ContentManager contentManager)
        {
            base.LoadContent(world, map, contentManager);
            world.AddVisualLevelObject(this);
        }


        protected virtual Coordinate RenderPositionInDisplayUnits()
        {
            return Map.TransformFromMapToDisplay(this.Origin);
        }
        protected float GetRotation()
        {
            return 0;
        }
    }
}
