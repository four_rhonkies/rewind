﻿/**
* Copyright 2017 Niclas Scheuing, Per Näslung, Tunay Bora, Xiaowei Zeng
* This file is part of REWIND.
*
* REWIND is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* REWIND is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
* GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with REWIND.If not, see<http://www.gnu.org/licenses/>.
*/

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using MonoGame.Extended.Sprites;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using team8.Animation;
using team8.LevelImporter;
using team8.Physics;

namespace team8.Level
{
    using Coordinate = Vector2;
    public class AnimationLevelObject: VisualLevelObject
    {
        public AnimationState AnimationState;
        private string AnimationPath;
        private int RowCount;
        private int ColumnCount;
        private float FrameDuration;
        private int StartFrame = 0;
        private int EndFrame;
        private int CurrentFrame;
        private bool Loop = false;
        public bool Playing = false;
        private bool Reversed = false;

        public AnimationLevelObject(string id): base(id:id)
        {
            
        }

        protected override void ParseProperties(XElement objectElement)
        {
            base.ParseProperties(objectElement);

            //define the keys we are looking for
            var animationString = "animation";
            var columnCountString = "column_count";
            var rowCountString = "row_count";
            var frameDurationString = "frame_duration";
            var loopString = "loop";
            var startFrameString = "start_frame";
            var endFrameString = "end_frame";
            var reversedString = "reversed";
            var keys = new List<string>() { animationString, columnCountString, rowCountString, frameDurationString, loopString, startFrameString, reversedString, endFrameString };

            //parse the Xelement
            var foundValues = TMXImporter.ParseProperties(element: objectElement, objectType: "LayerObject", keys: keys, failOnMissing: false);

            //convert the found values. If the value was not found, keep the current (default) value
            var animation = foundValues.ContainsKey(animationString) ? foundValues[animationString] : null;
            var columnCount = foundValues.ContainsKey(columnCountString) ? (int?)int.Parse(foundValues[columnCountString]) : null;
            var rowCount = foundValues.ContainsKey(rowCountString) ? (int?)int.Parse(foundValues[rowCountString]) : null;
            var frameDuration = foundValues.ContainsKey(frameDurationString) ? (float?)float.Parse(foundValues[frameDurationString]) : null;
            this.StartFrame = foundValues.ContainsKey(startFrameString) ? int.Parse(foundValues[startFrameString]) : this.StartFrame;
            var endFrame = foundValues.ContainsKey(endFrameString) ? (int?)int.Parse(foundValues[endFrameString]) : null;
            this.Loop = foundValues.ContainsKey(loopString) ? bool.Parse(foundValues[loopString]) : this.Loop;
            this.Reversed = foundValues.ContainsKey(reversedString) ? bool.Parse(foundValues[reversedString]) : this.Reversed;

            //Set values
            if (!columnCount.HasValue || !rowCount.HasValue || !frameDuration.HasValue)
            {
                //RELEASE
                throw new Exception("No row_count or column_count set in level file for animation " + animation);
            }

            this.RowCount = rowCount.Value;
            this.ColumnCount = columnCount.Value;
            this.AnimationPath = animation;
            this.EndFrame = endFrame.HasValue ? endFrame.Value : RowCount * ColumnCount-1;
            this.FrameDuration = frameDuration.Value;
            //If loop, then autoplay
            this.Playing = this.Loop;
            this.CurrentFrame =  this.Reversed ? EndFrame - StartFrame: 0;
        }
        
        public override void Dispose()
        {
            this.AnimationState.Dispose();
        }

        public void Play()
        {
            Playing = true;
            this.AnimationState.Animation.OnCompleted = OnCompletion;
        }

        private void OnCompletion()
        {
            Playing = false;
            //AnimationState.Rewind();

            this.AnimationState.Animation.OnCompleted -= OnCompletion;
        }

        public override void Draw(SpriteBatch spriteBatch, GameTime gameTime, Matrix transform, Action beginAgain)
        {
            if (!this.IsHidden && Playing)
            {
                //var totalRotation = RenderRotation();// + MathHelper.ToRadians(this.Rotation);
                var position = RenderPosition() + new Vector2(this.Width / 2, this.Height / 2);
                AnimationState.Update(gameTime: gameTime, position: position, rotation: RenderRotation());
                spriteBatch.Draw(sprite: AnimationState.Sprite);
                CurrentFrame = AnimationState.CurrentFrameIndex;
                //this.TileSet.Draw(spriteBatch: spriteBatch, position: RenderPosition(), rotation: totalRotation, scale: this.Scale);
            }
            else if (!this.IsHidden)
            {
                AnimationState.SetFrame(CurrentFrame, position: RenderPosition() + new Vector2(this.Width / 2, this.Height / 2), rotation: RenderRotation());
                spriteBatch.Draw(sprite: AnimationState.Sprite);
            }
        }

        public void SetAnimationDirection(bool forward)
        {
            this.AnimationState.SetAnimationDirection(forward: forward);
        }

        public void InvertAnimationDirection()
        {
            this.AnimationState.InvertAnimationDirection();
        }

        public void Rewind()
        {
            this.AnimationState.Rewind();
        }

        public override void LoadContent(PhysicalWorld world, Map map, ContentManager contentManager)
        {
            var frameCount = RowCount * ColumnCount;
            this.AnimationState = new AnimationState(contentManager: contentManager, texturePath: AnimationPath, rowCount: RowCount, columnCount: ColumnCount, frameDuration: FrameDuration, loop: this.Loop, reversed: this.Reversed, startAndEndIndex: (StartFrame, EndFrame));
            this.Scale = new Vector2(this.Width / AnimationState.FrameWidth, this.Height / AnimationState.FrameHeight);
            AnimationState.SetScale(scale: Scale);
            base.LoadContent(world: world, map: map, contentManager: contentManager);
        }

        override protected Coordinate RenderPositionInDisplayUnits()
        {
            return Map.TransformFromMapToDisplay(this.Origin);
        }
    }
}
