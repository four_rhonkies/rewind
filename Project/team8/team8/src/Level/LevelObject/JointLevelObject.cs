﻿/**
* Copyright 2017 Niclas Scheuing, Per Näslung, Tunay Bora, Xiaowei Zeng
* This file is part of REWIND.
*
* REWIND is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* REWIND is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
* GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with REWIND.If not, see<http://www.gnu.org/licenses/>.
*/
using FarseerPhysics;
using FarseerPhysics.Common;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Dynamics.Joints;
using FarseerPhysics.Factories;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using team8.LevelImporter;
using team8.Physics;

namespace team8.Level
{
    using Coordinate = Vector2;
    public class JointLevelObject: GeometryLevelObject
    {
        private enum JointType
        {
            Distance, Prismatic, Revolute
        }

        public struct JointState
        {
            public bool Enabled;
            public Coordinate AnchorA;
            public Coordinate AnchorB;
            public JointState(bool enabled, Coordinate anchorA, Coordinate anchorB)
            {
                this.Enabled = enabled;
                this.AnchorA = anchorA;
                this.AnchorB = anchorB;
            }
        }

        private JointType jointType;
        private World World;
        public string JointId;

        //joint parameters
        //distance joint
        private float Frequency = 0;
        private float DampingRatio = 0;

        private Joint joint;
        public Joint Joint
        {
            get
            {
                return joint;
            }
        }

        private Dictionary<string,JointState> LastStates;

        public JointLevelObject(string id, string jointType): base(id:id)
        {
            this.jointType = JointTypeForString(jointTypeString: jointType);
            this.LastStates = new Dictionary<string, JointState>();
        }

        override protected void ReadProperties(LevelObjectProperties? properties)
        {
            if (properties.HasValue)
            {
                this.JointGroupIds = properties.Value.JointGroupIds != null ? properties.Value.JointGroupIds : this.JointGroupIds;
            }
        }

        override protected void ParseProperties(XElement objectElement)
        {
            //define the keys we are looking for
            var jointGroupIdString = "jointgroupid";
            var frequencyString = "frequency";
            var dampingString = "damping";
            var keys = new List<string>() { jointGroupIdString, frequencyString, dampingString};

            //parse the Xelement
            var foundValues = TMXImporter.ParseProperties(element: objectElement, objectType: "LayerObject", keys: keys, failOnMissing: false);

            //convert the found values. If the value was not found, keep the current (default) value
            if (foundValues.ContainsKey(jointGroupIdString))
            {
                var jointGroupIds = TMXImporter.SplitStringList(foundValues[jointGroupIdString]);
                if (jointGroupIds.Count != 1)
                {
                    throw new Exception("Invalid number of jointgroupid defined for joint (oid " + Id + "). 1 expected, given " + jointGroupIds.Count);
                }
                this.JointGroupIds = jointGroupIds;
                JointId = jointGroupIds.First();
            }
            this.Frequency = foundValues.ContainsKey(frequencyString) ? float.Parse(foundValues[frequencyString]) : this.Frequency;
            this.DampingRatio = foundValues.ContainsKey(dampingString) ? float.Parse(foundValues[dampingString]) : this.DampingRatio;

            //Set values

        }

        public override void LoadContent(PhysicalWorld world, Map map, ContentManager contentManager)
        {
            this.World = world.World;
            base.LoadContent(world: world, map: map, contentManager: contentManager);
            
        }

        public void CreateJoint(Body bodyA, Body bodyB)
        {
            switch (jointType)
            {
                case JointType.Distance:
                    var distancePoints = GetTwoPolylinePoints();
                    var firstPoint = distancePoints.First();
                    var secondPoint = distancePoints.Last();

                    Coordinate anchorA, anchorB;
                    //Check which point is in which body
                    Transform transformA, transformB;
                    bodyA.GetTransform(transform: out transformA);
                    bodyB.GetTransform(transform: out transformB);

                    var firstPointIsInBodyA = bodyA.FixtureList.First().Shape.TestPoint(transform: ref transformA, point: ref firstPoint);
                    var secondPointIsInBodyA = bodyA.FixtureList.First().Shape.TestPoint(transform: ref transformA, point: ref secondPoint);
                    var firstPointIsInBodyB = bodyB.FixtureList.First().Shape.TestPoint(transform: ref transformB, point: ref firstPoint);
                    var secondPointIsInBodyB = bodyB.FixtureList.First().Shape.TestPoint(transform: ref transformB, point: ref secondPoint);
                    if (firstPointIsInBodyA && secondPointIsInBodyB)
                    {
                        anchorA = firstPoint;
                        anchorB = secondPoint;
                    }
                    else if (firstPointIsInBodyB && secondPointIsInBodyA)
                    {
                        anchorA = secondPoint;
                        anchorB = firstPoint;
                    }
                    else
                    {
                        throw new Exception("Anchors of distance joint (oid:"+this.ObjectId+") must be in labeled anchor objects and the joint points must intersect with one of the anchros.");
                    }
                    //Create joint
                    var distanceJoint = new DistanceJoint(bodyA: bodyA, bodyB: bodyB, anchorA: anchorA, anchorB: anchorB, useWorldCoordinates: true);
                    distanceJoint.CollideConnected = false;
                    distanceJoint.DampingRatio = this.DampingRatio;
                    distanceJoint.Frequency = this.Frequency;
                    World.AddJoint(distanceJoint);
                    this.joint = distanceJoint;
                    break;
                case JointType.Prismatic:
                    var prismaticPoints = GetTwoPolylinePoints();

                    var upperPoint = prismaticPoints.First().Y < prismaticPoints.Last().Y ? prismaticPoints.First() : prismaticPoints.Last();
                    var lowerPoint = prismaticPoints.First().Y > prismaticPoints.Last().Y ? prismaticPoints.First() : prismaticPoints.Last();


                    Vector2 axis = upperPoint - lowerPoint;// upperPoint - lowerPoint;
                    var axisLength = axis.Length();
                    axis.Normalize();
                    var prismaticJoint = new PrismaticJoint(bodyA: bodyA, bodyB: bodyB, anchorB: upperPoint, anchorA: lowerPoint, axis: axis, useWorldCoordinates: true);
                    World.AddJoint(prismaticJoint);
                    //prismaticJoint.LimitEnabled = true;
                    //prismaticJoint.SetLimits(-1 * axisLength, 2 * axisLength);
                    //var prismaticJoint = JointFactory.CreatePrismaticJoint(world: World, bodyA: bodyA, bodyB: bodyB, anchor: anchor, axis: axis);
                    this.joint = prismaticJoint;
                    break;
                case JointType.Revolute:
                    var revolutePoints = PointsInSim();
                    Coordinate revoluteAnchor = revolutePoints.First();
                    var revoluteJoint = new RevoluteJoint(bodyA: bodyA, bodyB: bodyB, anchor: revoluteAnchor, useWorldCoordinates: true);
                    World.AddJoint(revoluteJoint);
                    this.joint = revoluteJoint;
                    break;
            }
        }

        private IEnumerable<Coordinate> GetTwoPolylinePoints()
        {
            var polylinePoints = PointsInSim().ToList();
            if (this.Polyline == null || polylinePoints.Count() != 2)
            {
                throw new Exception("Joint gemoetry (oid:" + this.Id + ", "+this.jointType+") must be polyline with 2 points");
            }
            return polylinePoints;
        }

        private JointType JointTypeForString(string jointTypeString)
        {
            switch(jointTypeString)
            {
                case "distance": return JointType.Distance;
                case "prismatic": return JointType.Prismatic;
                case "revolute": return JointType.Revolute;
                default: throw new Exception("Invalid joint type "+jointTypeString+". Forgot to implement a joint type in the JointType enum?");
            }
        }
        public void SaveState(string id)
        {
            JointState state = GetState();
            this.LastStates[id] = state;
        }

        public void LoadLastState(string id)
        {
            if (LastStates.ContainsKey(id))
            {
                SetState(state: LastStates[id]);
            }
        }
        public void SetState(JointState state)
        {
            this.joint.Enabled = state.Enabled;
            this.joint.WorldAnchorA = state.AnchorA;
            this.joint.WorldAnchorB = state.AnchorB;
        }


        public JointState GetState()
        {
            return new JointState(enabled: this.joint.Enabled, anchorA: joint.WorldAnchorA, anchorB: joint.WorldAnchorB);
        }

        public void SetEnabled(bool enabled)
        {
            if (this.Joint.Enabled != enabled)
            {
                this.Joint.Enabled = enabled;
            }
        }
        override public Coordinate DisplayPosition()
        {
            //TODO: implement
            //var positionInDisplayUnit = Body == null ? Map.TransformFromMapToDisplay(this.Origin) : ConvertUnits.ToDisplayUnits(Body.Position);
            return Joint != null ?ConvertUnits.ToDisplayUnits(Joint.BodyB.Position) : this.OriginInDisplayCoordinates;
        }
    }
}
