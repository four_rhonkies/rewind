﻿/**
* Copyright 2017 Niclas Scheuing, Per Näslung, Tunay Bora, Xiaowei Zeng
* This file is part of REWIND.
*
* REWIND is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* REWIND is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
* GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with REWIND.If not, see<http://www.gnu.org/licenses/>.
*/


using FarseerPhysics;
using FarseerPhysics.Common;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Factories;
using Microsoft.Xna.Framework;
using MonoGame.Extended.Shapes;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using team8.LevelImporter;

namespace team8.Level
{
    using Microsoft.Xna.Framework.Content;
    using Microsoft.Xna.Framework.Graphics;
    using MonoGame.Extended;
    using System;
    using team8.Physics;
    using Coordinate = Vector2;

   
    public class PhysicsLevelObject : GeometryLevelObject
    {
        //Density is needed for the physical objects
        private float Density = 1f;
        private float? Friction = null;

        //The element is a wall. Note that a if IsWall is true, Collsion should also be true
        private bool IsWall = false;
        //The element acts as a collidor
        private bool Collision = false;
        private bool IsGround = false;
        private bool FixRotation = false;
        private bool IsSensor = false;
        private bool OnlyCharacterCollision = false;
        private float AngularDamping = 1;

        private Texture2D DebugTexture;
        private Texture2D DebugTexturePosition;
        private short CollisionGroup = Constants.CollisionGroupNone;
        private Body body;

        public Body Body
        {
            get
            {
                return body;
            }
        }
        private BodyType? UsedBodyType;

        private Dictionary<string,State> LastStates;

        public PhysicsLevelObject(string id): base(id: id)
        {
            this.LastStates = new Dictionary<string, State>();
        }

        override protected void ReadProperties(LevelObjectProperties? properties)
        {
            if (properties.HasValue)
            {
                this.IsWall = properties.Value.IsWall.GetValueOrDefault(this.IsWall);
                this.Collision = properties.Value.Collision.GetValueOrDefault(this.Collision);
                this.RenderGroupId = properties.Value.RenderGroupId != null ? properties.Value.RenderGroupId : this.RenderGroupId;
                this.JointGroupIds = properties.Value.JointGroupIds != null ? properties.Value.JointGroupIds : this.JointGroupIds;
                this.IsGround = properties.Value.IsGround.GetValueOrDefault(this.IsGround);
                this.FixRotation = properties.Value.FixRotation.GetValueOrDefault(this.FixRotation);
                if (properties.Value.BodyType != null)
                {
                    this.UsedBodyType = BodyTypeForString(bodyString: properties.Value.BodyType);
                }
                
            }
        }

        

        override protected void ParseProperties(XElement objectElement)
        {
            //define the keys we are looking for
            var collisionString = "collision";
            var wallString = "wall";
            var renderGroupIdString = "rendergroupid";
            var bodyTypeString = "bodytype";
            var groundString = "ground";
            var fixRotationString = "fixrotation";
            var jointGroupIdString = "jointgroupid";
            var densityString = "density";
            var frictionString = "friction";
            var angularDampingString= "angulardamping";
            var sensorString = "sensor";
            var onlyCharacterCollisionString = "only_character_collision";
            var keys = new List<string>() { collisionString,
                                            wallString,
                                            renderGroupIdString,
                                            bodyTypeString,
                                            groundString,
                                            fixRotationString,
                                            jointGroupIdString,
                                            densityString,
                                            frictionString,
                                            angularDampingString,sensorString,
                                            onlyCharacterCollisionString
                                        };

            //parse the Xelement
            var foundValues = TMXImporter.ParseProperties(element: objectElement, objectType: "LayerObject", keys: keys, failOnMissing: false);

            //convert the found values. If the value was not found, keep the current (default) value
            var collision = foundValues.ContainsKey(collisionString) ? bool.Parse(foundValues[collisionString]) : this.Collision;
            var isWall = foundValues.ContainsKey(wallString) ? bool.Parse(foundValues[wallString]) : this.IsWall;
            var isGround = foundValues.ContainsKey(groundString) ? bool.Parse(foundValues[groundString]) : this.IsGround;
            var renderGroupId = foundValues.ContainsKey(renderGroupIdString) ? foundValues[renderGroupIdString] : this.RenderGroupId;
            var bodyType = foundValues.ContainsKey(bodyTypeString) ? foundValues[bodyTypeString] : null;
            var fixRotation = foundValues.ContainsKey(fixRotationString) ? bool.Parse(foundValues[fixRotationString]) : this.FixRotation;
            var density = foundValues.ContainsKey(densityString) ? float.Parse(foundValues[densityString]) : this.Density;
            var friction = foundValues.ContainsKey(frictionString) ? float.Parse(foundValues[frictionString]) : this.Friction;
            var angularDamping = foundValues.ContainsKey(angularDampingString) ? float.Parse(foundValues[angularDampingString]) : this.AngularDamping;
            var isSensor = foundValues.ContainsKey(sensorString) ? bool.Parse(foundValues[sensorString]) : this.IsSensor;
            var onlyCharacterCollision = foundValues.ContainsKey(onlyCharacterCollisionString) ? bool.Parse(foundValues[onlyCharacterCollisionString]) : this.OnlyCharacterCollision;
            //Set values
            this.IsWall = isWall;
            this.Collision = collision || this.IsWall;
            this.RenderGroupId = renderGroupId;
            this.IsGround = isGround;
            this.FixRotation = fixRotation;
            this.Friction = friction;
            this.Density = density;
            this.AngularDamping = angularDamping;
            this.IsSensor = isSensor;
            this.OnlyCharacterCollision = onlyCharacterCollision;

            if (foundValues.ContainsKey(jointGroupIdString))
            {
                this.JointGroupIds = TMXImporter.SplitStringList(foundValues[jointGroupIdString]);
            }
            if (bodyType != null)
            {
                this.UsedBodyType = BodyTypeForString(bodyString: bodyType);
            }


        }

        private BodyType BodyTypeForString(string bodyString)
        {
            switch (bodyString)
            {
                case "static":
                    return BodyType.Static;
                case "dynamic":
                    return BodyType.Dynamic;
                case "kinematic":
                    return BodyType.Kinematic;
                default:
                    //RELEASE
                    throw new Exception("Invaid bodytype " + bodyString);
            }
        }
      

        public override void LoadContent(PhysicalWorld world, Map map, ContentManager contentManager)
        {
            Body body = null;
            var origin = Map.TransformFromMapToSim(point: this.Origin);
            
            switch (this.GeometryType)
            {
             
                case GeometryType.polyline:
                    var linePoints = PointsInSim(relativeToPosition: true);
                    if (linePoints.Count() == 2)
                    {
                        body = BodyFactory.CreateEdge(world: world.World, start: linePoints.First(), end: linePoints.Last());
                    }
                    else
                    {
                        body = BodyFactory.CreateChainShape(world: world.World, vertices: new Vertices(linePoints) );
                    }
                    break;
                case GeometryType.rectangle:
                case GeometryType.polygon:
                    var polyVertices = new Vertices(PointsInSim(relativeToPosition: true));
                    //Dont add the position for polygons, because the vertices are already transformed.
                    body = BodyFactory.CreatePolygon(world: world.World,
                        vertices: polyVertices,
                        density: Density
                        );
                    break;
            }
            
            // Add body to world
            if (body != null)
            {
                body.Position = origin;
                body.FixedRotation = this.FixRotation;
                if(IsWall)
                {
                    this.body = world.CreateAndAddWallObject(obj: this, body: body);
                }
                else if (IsGround)
                {
                    this.body = world.CreateAndAddGroundObject(obj: this, body: body);
                }
                else if (Collision)
                {
                    this.body = world.CreateAndAddCollisionObject(obj: this, body: body, Friction: this.Friction);
                }
                else
                {
                   this.body = world.CreateAndAddOtherObject(obj: this, body: body);
                }
            }
            base.LoadContent(world: world, map: map, contentManager: contentManager);
            if (this.OnlyCharacterCollision)
            {
                body.CollidesWith = Constants.CategoryMainCharacter;
            }
            if (this.UsedBodyType.HasValue)
            {
                body.BodyType = this.UsedBodyType.Value;
            }
            body.CollisionGroup = this.CollisionGroup;
            body.AngularDamping = this.AngularDamping;
            body.IsSensor = this.IsSensor;
            //world.AddPhysicsObject(this);
        }

        private void SetupDebugRendering(GraphicsDevice graphicsDevice)
        {
            var dotSize = 5;
            DebugTexture = new Texture2D(graphicsDevice, dotSize, dotSize);
            //Use different Colors for different objects
            Color color = Color.Purple;
            if(IsWall)
            {
                color = Color.Green;
            }
            else if (IsGround)
            {
                color = Color.Orange;
            }
            else if(Collision)
            {
                color = Color.HotPink;
            }
            else if(this is SwitchLevelObject)
            {
                color = Color.Yellow;
            }
            var colors = new Color[dotSize * dotSize];
            for (int i = 0; i < colors.Length; i++) { colors[i] = color; }
            DebugTexture.SetData(colors);


            DebugTexturePosition = new Texture2D(graphicsDevice, dotSize, dotSize);
            var colorsPosition = new Color[dotSize * dotSize];
            for (int i = 0; i < colorsPosition.Length; i++) { colorsPosition[i] = Color.CadetBlue; }
            DebugTexturePosition.SetData(colorsPosition);

        }
        override public void Dispose()
        {
            Body.Dispose();
        }

        public override void Draw(SpriteBatch spriteBatch, GameTime gameTime, Matrix transform, Action beginAgain)
        {
            if(IsHidden)
            {
                return;
            }

            if (DebugTexture == null)
            {
                SetupDebugRendering(graphicsDevice: spriteBatch.GraphicsDevice);
            }
            foreach (var point in PointsInDisplay())
            {
                spriteBatch.Draw(texture: DebugTexture, position: point, rotation: body.Rotation);
            }

            spriteBatch.Draw(texture: DebugTexturePosition, position: DisplayPosition());

        }

        override public float Rotation()
        {
            return Body == null ? 0f : Body.Rotation;
        }

        override public Coordinate DisplayPosition()
        {
            return Body == null ? Map.TransformFromMapToDisplay(this.Origin) : ConvertUnits.ToDisplayUnits(Body.Position);
        }

        protected override void SetHidden(bool isHidden)
        {
            base.SetHidden(isHidden);

            this.CollisionGroup = IsHidden ? Constants.CollisionGroupNegativMainCharacter : Constants.CollisionGroupNone;
            if (body != null)
            {
                body.CollisionGroup = this.CollisionGroup;
            }
        }

        public void SetEnabled(bool enabled)
        {
            if (this.Body.Enabled != enabled)
            {
                this.Body.Enabled = enabled;
                
            }
        }

        /*-----Save State-----*/

        public void SaveState(string id)
        {
            State state = GetState();
            this.LastStates[id] = state;
        }

        public void LoadLastState(string id)
        {
            if (LastStates.ContainsKey(id))
            {
                SetState(state: LastStates[id]);
            }
        }
        public void SetState(State state)
        {
            this.body.Position = state.Position;
            this.body.Rotation = state.Rotation;
            this.body.LinearVelocity = state.LinearVelocity;
            this.body.AngularVelocity = state.AngularVelocity;
            this.body.BodyType = state.BodyType;
            this.body.Enabled = state.Enabled;
        }

        public State GetState()
        {
            return new State(   rotation: body.Rotation,
                                position: this.body.Position,
                                linearVelocity: this.body.LinearVelocity,
                                angularVelocity: this.body.AngularVelocity,
                                bodyType: this.body.BodyType,
                                enabled: this.body.Enabled);
        }

        public struct State
        {
            public float Rotation;
            public Coordinate Position;
            public Vector2 LinearVelocity;
            public float AngularVelocity;
            public BodyType BodyType;
            public bool Enabled;

            public State(float rotation, Coordinate position, Vector2 linearVelocity, float angularVelocity, BodyType bodyType, bool enabled)
            {
                this.Rotation = rotation;
                this.Position = position;
                this.LinearVelocity = linearVelocity;
                this.AngularVelocity = angularVelocity;
                this.BodyType = bodyType;
                this.Enabled = enabled;
            }
        }
    }
}
