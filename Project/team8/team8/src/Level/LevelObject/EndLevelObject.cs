﻿/**
* Copyright 2017 Niclas Scheuing, Per Näslung, Tunay Bora, Xiaowei Zeng
* This file is part of REWIND.
*
* REWIND is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* REWIND is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
* GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with REWIND.If not, see<http://www.gnu.org/licenses/>.
*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Dynamics.Contacts;
using Microsoft.Xna.Framework.Content;
using team8.Physics;

namespace team8.Level
{
    public class EndLevelObject : SwitchLevelObject
    {
        public EndLevelObject(string id) : base(id: id, switchId: "end")
        {
        }

        protected override bool OnCollision(Fixture fixtureA, Fixture fixtureB, Contact contact)
        {
            World.EndLevel();
            return true;
        }
        protected override void OnSeparation(Fixture fixtureA, Fixture fixtureB)
        {
            
        }
        protected override SwitchEvent GetSwitchEvent()
        {
            return null;
        }
    }
}
