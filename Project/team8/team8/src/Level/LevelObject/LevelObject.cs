﻿/**
* Copyright 2017 Niclas Scheuing, Per Näslung, Tunay Bora, Xiaowei Zeng
* This file is part of REWIND.
*
* REWIND is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* REWIND is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
* GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with REWIND.If not, see<http://www.gnu.org/licenses/>.
*/
using FarseerPhysics;
using FarseerPhysics.Common;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Factories;
using Microsoft.Xna.Framework;
using MonoGame.Extended.Shapes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using team8.Rendering;
using Microsoft.Xna.Framework.Graphics;
using System.Diagnostics;
using team8.LevelImporter;
using Microsoft.Xna.Framework.Content;
using team8.Physics;
using MonoGame.Extended;

namespace team8.Level
{
    using Coordinate = Vector2;

    public struct LevelObjectProperties
    {
        public bool? Collision;
        public bool? IsWall;
        public bool? IsGround;
        public string RenderGroupId;
        public List<string> JointGroupIds;
        public bool? IsHidden;
        public string BodyType;
        public bool? FixRotation;
        public bool? IsLethal;

        public LevelObjectProperties(bool? collision=null,
            bool? isGround=null,
            bool? isWall=null,
            string renderGroupId = null,
            List<string> jointId = null,
            bool? isHidden=null,
            string bodyType=null,
            bool? fixRotation=null,
            bool? isLethal=null)
        {
            this.Collision = collision;
            this.IsWall = isWall;
            this.RenderGroupId = renderGroupId;
            this.IsGround = isGround;
            this.JointGroupIds = jointId;
            this.IsHidden = isHidden;
            this.BodyType = bodyType;
            this.FixRotation = fixRotation;
            this.IsLethal = isLethal;
        }
    }

    public enum GeometryType
    {
        polyline, rectangle, polygon
    }

    abstract public class LevelObject: RenderObject
    {
        protected int ObjectId;
        protected string Name;
        //In level coordinates
        protected Coordinate Origin;
        public Coordinate OriginInDisplayCoordinates
        {
            get
            {
                return Map.TransformFromMapToDisplay(this.Origin);
            }
        }
        protected string RenderGroupId;
        public List<string> JointGroupIds;
        public readonly string Id;

        protected bool isHidden;
        public bool IsHidden
        {
            get
            {
                return this.isHidden;
            }
            set
            {
                SetHidden(isHidden: value);
            }
        }

        public virtual bool RenderAsDebug
        {
            get
            {
                return (this is PhysicsLevelObject);
            }
        }

        protected LevelObject(string id)
        {
            this.Id = id;
            JointGroupIds = new List<string>();
            this.isHidden = false;
        }

        public static LevelObject Create(XElement objectElement, Coordinate offset, LevelObjectProperties? properties)
        {
            var levelObject = StaticParse(objectElement: objectElement, offset: offset, properties: properties);

            levelObject.ReadProperties(properties: properties);
            //Call subclasses parse method
            levelObject.Parse(objectElement: objectElement);
            //Parse properties
            levelObject.ParseProperties(objectElement: objectElement);

            return levelObject;

        }

        private static LevelObject StaticParse(XElement objectElement, Coordinate offset, LevelObjectProperties? properties)
        {
            //Figure out if image or physics
            LevelObject levelObject;
            var isSpawnPointAttribute = TMXImporter.GetPropertyValueAttribute(objectElement: objectElement, propertyName: "spawnpoint");
            var isParallaxOriginAttribute = TMXImporter.GetPropertyValueAttribute(objectElement: objectElement, propertyName: "parallaxorigin");
            var switchIdAttribute = TMXImporter.GetPropertyValueAttribute(objectElement: objectElement, propertyName: "switchid");
            var jointTypeAttribute = TMXImporter.GetPropertyValueAttribute(objectElement: objectElement, propertyName: "jointtype");
            var spiritAttribute = TMXImporter.GetPropertyValueAttribute(objectElement: objectElement, propertyName: "spirit");
            var lethalAttribute = TMXImporter.GetPropertyValueAttribute(objectElement: objectElement, propertyName: "lethal");
            var animationAttribute = TMXImporter.GetPropertyValueAttribute(objectElement: objectElement, propertyName: "animation");
            var musicRegionAttribute = TMXImporter.GetPropertyValueAttribute(objectElement: objectElement, propertyName: "music_region_id");
            var backgroundRegionIdAttribute = TMXImporter.GetPropertyValueAttribute(objectElement: objectElement, propertyName: "background_region_id");
            var endAttribute = TMXImporter.GetPropertyValueAttribute(objectElement: objectElement, propertyName: "end");
            var ropeAttribute = TMXImporter.GetPropertyValueAttribute(objectElement: objectElement, propertyName: "rope");

            //get id
            var idAttribute = TMXImporter.GetPropertyValueAttribute(objectElement: objectElement, propertyName: "id");
            var id = idAttribute == null ? "" : idAttribute.Value;

            var gidAttirbute = objectElement.Attribute("gid");
            //i fhas gid Attribute, it is image
            if (gidAttirbute != null)
            {
                if (animationAttribute != null)
                {
                    levelObject = new AnimationLevelObject(id);
                }
                else
                {
                    var gid = gidAttirbute.Value;
                    levelObject = new ImageLevelObject(id);
                }
            }
            else if(isSpawnPointAttribute != null && bool.Parse(isSpawnPointAttribute.Value))
            {
                levelObject = new SpawnPointLevelObject(id);

            }
            else if (isParallaxOriginAttribute != null && bool.Parse(isParallaxOriginAttribute.Value))
            {
                levelObject = new ParallaxOriginLevelObject(id);

            }
            else if(switchIdAttribute != null)
            {
                var switchId = switchIdAttribute.Value;
                levelObject = new SwitchLevelObject(switchId: switchId, id: id);
            }
            else if(jointTypeAttribute != null)
            {
                var jointType = jointTypeAttribute.Value;
                levelObject = new JointLevelObject(jointType: jointType, id: id);
            }
            else if (spiritAttribute != null && bool.Parse(spiritAttribute.Value))
            {
                levelObject = new SpiritLevelObject(id: id);
            }
            else if (lethalAttribute != null && bool.Parse(lethalAttribute.Value) || (properties.HasValue && properties.Value.IsLethal.GetValueOrDefault(false)))
            {
                levelObject = new LethalSwitchObject(id: id);
            }
            else if(musicRegionAttribute != null)
            {
                var musicRegionId = musicRegionAttribute.Value;
                levelObject = new MusicRegionLevelObject(id: id, musicRegionId: musicRegionId);
            }
            else if (backgroundRegionIdAttribute != null)
            {
                var backgroundRegionId = backgroundRegionIdAttribute.Value;
                levelObject = new BackgroundRegionLevelObject(id: id, backgroundRegionId: backgroundRegionId);
            }
            else if (endAttribute != null && bool.Parse(endAttribute.Value))
            {
                levelObject = new EndLevelObject(id: id);
            }
            else if (ropeAttribute != null && bool.Parse(ropeAttribute.Value))
            {
                levelObject = new RopeLevelObject(id: id);
            }
            //Else it is physics
            else
            {
                levelObject = new PhysicsLevelObject(id);
            }

            //Set Name
            levelObject.Name = objectElement.Attribute("name") != null ? objectElement.Attribute("name").Value : "";
            //Set Id
            levelObject.ObjectId = int.Parse(objectElement.Attribute("id").Value);

            //Get origin
            float xOrigin = float.Parse(objectElement.Attribute("x").Value);
            float yOrigin = float.Parse(objectElement.Attribute("y").Value);
            levelObject.Origin = offset + new Coordinate(xOrigin, yOrigin);

            //is hidden
            var hiddenAttribute = TMXImporter.GetPropertyValueAttribute(objectElement: objectElement, propertyName: "hidden");
            var hidden = hiddenAttribute != null ? bool.Parse(hiddenAttribute.Value) : (properties.HasValue && properties.Value.IsHidden.HasValue ? properties.Value.IsHidden.Value : levelObject.IsHidden);
            levelObject.SetHidden(hidden);

            return levelObject;
        }

        public virtual void Draw(SpriteBatch spriteBatch, GameTime gameTime, Matrix transform, Action beginAgain)
        {
            //Can be override by subclasses
        }

        protected Polyline ParsePolyline(XElement objectElement)
        {
            var polylineElement = objectElement.Element("polyline");

            string pointsString = polylineElement.Attribute("points").Value;
            var points = ParsePoints(pointsString: pointsString);
            return new Polyline(points: points);
        }

        protected Polygon ParsePolygon(XElement objectElement)
        {
            var polygonElement = objectElement.Element("polygon");

            string pointsString = polygonElement.Attribute("points").Value;
            var points = ParsePoints(pointsString: pointsString);
            var polygon = new Polygon(vertices: points);
            return polygon;
        }

        protected List<Coordinate> ParsePoints(string pointsString)
        {
            //get points
            var pointStringList = pointsString.Split(' ');
            var points = new List<Coordinate>();
            foreach (string pointString in pointStringList)
            {
                string[] xy = pointString.Split(',');
                var x = float.Parse(xy[0]);
                var y = float.Parse(xy[1]);
                var point = new Coordinate(x, y);
                points.Add(point);
            }
            return points;
        }

        protected RectangleF ParseRectangle(XElement objectElement)
        {
            float width = float.Parse(objectElement.Attribute("width").Value);
            float heigth = float.Parse(objectElement.Attribute("height").Value);
            var rectangle = new RectangleF(x:0, y: 0, width: width, height: heigth);
            return rectangle;
        }


        //Must be overriden by subclasses
        public virtual void Dispose() { }
        protected virtual void Parse(XElement objectElement) { }
        protected virtual void ParseProperties(XElement objectElement) {}
        protected virtual void ReadProperties(LevelObjectProperties? properties) { }

        public virtual void LoadContent(PhysicalWorld world, Map map, ContentManager contentManager)
        {
            //is collision object set to collision object
            if (RenderGroupId != null)
            {
                var renderGroup = world.GetRenderPhysicsGroup(renderGroupId: RenderGroupId);
                if (renderGroup == null)
                {
                    renderGroup = world.CreateAndAddRenderGroup(renderGroupId: RenderGroupId);
                }

                if (this is VisualLevelObject)
                {
                    renderGroup.SetImageLayerObject((VisualLevelObject) this);
                }
                else if (this is PhysicsLevelObject)
                {
                    renderGroup.SetPhysicsLevelObject((PhysicsLevelObject) this);
                }
            }

            if(JointGroupIds.Count > 0)
            {
                foreach (var jointGroupId in JointGroupIds)
                {
                    var jointGroup = world.GetJointGroup(jointGroupId: jointGroupId);
                    if (jointGroup == null)
                    {
                        jointGroup = world.CreateAndAddJointGroup(jointGroupId: jointGroupId);
                    }
                    if (this is PhysicsLevelObject)
                    {
                        jointGroup.AddPhysicsObject((PhysicsLevelObject)this);
                    }
                    else if (this is JointLevelObject)
                    {
                        jointGroup.AddJointObject((JointLevelObject)this);
                    }
                }
            }
        }


        protected virtual void SetHidden(bool isHidden)
        {
            this.isHidden = isHidden;
        }

    }
}
