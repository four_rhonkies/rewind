﻿/**
* Copyright 2017 Niclas Scheuing, Per Näslung, Tunay Bora, Xiaowei Zeng
* This file is part of REWIND.
*
* REWIND is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* REWIND is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
* GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with REWIND.If not, see<http://www.gnu.org/licenses/>.
*/
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using team8.LevelImporter;
using team8.Rendering;

namespace team8.Level
{
using Coordinate = Vector2;
    public class Layer: RenderObject
    {
        private struct GridIndex
        {
            public int Row, Column;
            public GridIndex(int row, int column)
            {
                this.Row = row;
                this.Column = column;
            }
        }

        private string Name;
        private int WidthInTiles;
        private int HeightInTiles;
        //private int[,] LevelGrid;
        private Dictionary<GridIndex, int> SparseLevelGrid;
        private Map Map;
        private float Depth;
        private string TypeString;
        public RenderLayerLevel? RenderLayerLevel;

        //public Layer(string name, int widthInTiles, int heightInTiles, int[,] levelGrid)
        //{
        //    this.Name = name;
        //    this.WidthInTiles = widthInTiles;
        //    this.HeightInTiles = heightInTiles;
        //    this.LevelGrid = levelGrid;
        //}
        public Layer(XElement layerElement, Map map)
        {
            Parse(layerElement: layerElement);
            this.Map = map;
        }

        private void Parse(XElement layerElement)
        {
            //Set Name
            this.Name = layerElement.Attribute("name") != null ? layerElement.Attribute("name").Value : "";
            ParseProperties(layerElement: layerElement);
            //Set dimensino
            this.WidthInTiles = int.Parse(layerElement.Attribute("width").Value);
            this.HeightInTiles = int.Parse(layerElement.Attribute("height").Value);

            //parse the layer grid
            var data = layerElement.Element("data").Value;

            //Set LevelGrid
            this.SparseLevelGrid = ParseLevelGrid(data: data);
        }

        private void ParseProperties(XElement layerElement)
        {
            //define the keys we are looking for
            var depthString = "depth";
            var typeString = "type";
            var keys = new List<string>() { depthString, typeString };

            //parse the Xelement
            var foundValues = TMXImporter.ParseProperties(element: layerElement, objectType: "Layer", keys: keys);

            //convert the found values. If the value was not found, keep the current (default) value
            var type = foundValues[typeString];
            var depth = foundValues.ContainsKey(depthString) ? float.Parse(foundValues[depthString]) : 0f;
            

            //Set values
            this.Depth = depth;
            this.TypeString = type;
            this.RenderLayerLevel = GetLevelForLayerType(type: this.TypeString, depth: this.Depth);
        }

        public static RenderLayerLevel? GetLevelForLayerType(string type, float depth)
        {
            if (type.Contains("background"))
            {
                return new RenderLayerLevel(type: RenderLayerLevelType.Background, depth: depth);
            }
            else if (type.Contains("action"))
            {
                return new RenderLayerLevel(type: RenderLayerLevelType.ActionLayer);
            }
            else if (type.Contains("foreground"))
            {
                return new RenderLayerLevel(type: RenderLayerLevelType.Foreground, depth: depth);
            }
            return null;
        }

        /**
         * expected name example "background10.4_comment", "foreground2_somethingelse"
         **/
        private float ParseDepthFromLayerName(string name, string beginsWith)
        {
            int startIndex = name.IndexOf(beginsWith) + beginsWith.Length;
            int endIndex = name.Contains("_") ? name.IndexOf("_") : name.Length;
            int length = endIndex - startIndex;
            string depthString = name.Substring(startIndex: startIndex, length: length);
            return float.Parse(depthString);
        }

        private Dictionary<GridIndex, int> ParseLevelGrid(string data)
        {

            //int[,] levelGrid = new int[HeightInTiles,WidthInTiles];
            Dictionary<GridIndex, int> sparseLevelGrid = new Dictionary<GridIndex, int>();
            var lines = data.Split('\n').ToList();
            //remove the first and last line
            lines.RemoveAt(index: 0);
            lines.RemoveAt(index: lines.Count - 1);

            for (int row = 0; row < HeightInTiles; row++)
            {
                var line = lines.ElementAt(index: row).Split(',').ToList();
                for (int column = 0; column < WidthInTiles; column++)
                {
                    var element = int.Parse(line.ElementAt(index: column));
                    if (element != 0)
                    {
                        sparseLevelGrid[new GridIndex(column: column, row: row)] = element;
                    }

                    //levelGrid[row, column] = element;
                }
            }

            return sparseLevelGrid;
        }

        /**
         * The top left corner of the grid element when indext from the top-left corner
         **/
        private Coordinate CoordinateForGridElement(int column, int row)
        {
            var x = column * Map.TileWidth;
            var y = row * Map.TileHeight;
            return new Coordinate(x, y);
        }

        private int LevelGridElement(int row, int column)
        {
            return this.SparseLevelGrid[new GridIndex(column: column, row: row)];
        }

        public void Draw(SpriteBatch spriteBatch, GameTime gameTime, Matrix transform, Action beginAgain)
        {
            foreach(KeyValuePair<GridIndex, int> pair in SparseLevelGrid)
            {
                var index = pair.Key;
                var tileSetId = LevelGridElement(row: index.Row, column:index.Column);
                var tileSet = Map.TileSetForId(id: tileSetId);
                        
                //Get the origin of the tileSet from the grid
                var tileOrigin = CoordinateForGridElement(column: index.Column, row: index.Row);
                //Rendering origin is top left
                //tileOrigin = tileOrigin - new Vector2(0, tileSet.TextureHeight);
                //transform the origin the display coordinates
                var displayTileOrigin = Map.TransformFromMapToDisplay(tileOrigin);

                tileSet.Draw(spriteBatch: spriteBatch, position: displayTileOrigin, transform: transform);
            }
        }

        public void Dispose()
        {
            //TODO: clean up
        }
    }
}
