﻿/**
* Copyright 2017 Niclas Scheuing, Per Näslung, Tunay Bora, Xiaowei Zeng
* This file is part of REWIND.
*
* REWIND is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* REWIND is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
* GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with REWIND.If not, see<http://www.gnu.org/licenses/>.
*/
using FarseerPhysics.Dynamics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using team8.LevelImporter;
using team8.Physics;
using team8.Rendering;

namespace team8.Level
{
    using Coordinate = Vector2;
    public class ObjectGroup
    {
        public List<LevelObject> Objects;
        private string Name;
        private Coordinate Offset;
        public RenderLayerLevel? RenderLayerLevel = null;

        public ObjectGroup(XElement element)
        {
            Parse(element: element);
        }

        private void Parse(XElement element)
        {
            this.Name = element.Attribute("name") != null ? element.Attribute("name").Value : "";
            //offset
            float offsetX = element.Attribute("offsetx") != null ? float.Parse(element.Attribute("offsetx").Value) : 0f;
            float offsetY = element.Attribute("offsety") != null ? float.Parse(element.Attribute("offsety").Value) : 0f;
            this.Offset = new Coordinate(offsetX, offsetY);

            var properties = ParseProperties(element);
            var objectElements = element.Elements("object");
            this.Objects = ReadObjects(objectElements: objectElements, properties: properties);
        }

        private LevelObjectProperties ParseProperties(XElement objectElement)
        {
            
            
            //define the keys we are looking for
            var depthString = "depth";
            var typeString = "type";
            var collisionString = "collision";
            var groundString = "ground";
            var wallString = "wall";
            var renderGroupIdString = "rendergroupid";
            var jointGroupIdString = "jointgroupid";
            var hiddenString = "hidden";
            var bodyTypeString = "bodytype";
            var fixRotationString = "fixrotation";
            var lethalString = "lethal";
            var keys = new List<string>() { depthString,
                                            typeString,
                                            collisionString,
                                            wallString,
                                            renderGroupIdString,
                                            groundString,
                                            jointGroupIdString,
                                            hiddenString,
                                            bodyTypeString,
                                            fixRotationString,
                                            lethalString,};

            //parse the Xelement
            var foundValues = TMXImporter.ParseProperties(element: objectElement, objectType: "LayerObject", keys: keys, failOnMissing: false);

            //convert the found values. If the value was not found, keep the current (default) value
            bool? collision = foundValues.ContainsKey(collisionString) ? (bool?) bool.Parse(foundValues[collisionString]) : null;
            bool? isGround = foundValues.ContainsKey(groundString) ? (bool?)bool.Parse(foundValues[groundString]) : null;
            bool? isWall = foundValues.ContainsKey(wallString) ? (bool?) bool.Parse(foundValues[wallString]) : null;
            string renderGroupId = foundValues.ContainsKey(renderGroupIdString) ? foundValues[renderGroupIdString] : null;
            bool? hidden = foundValues.ContainsKey(hiddenString) ? (bool?) bool.Parse(foundValues[hiddenString]) : null;
            string bodyType = foundValues.ContainsKey(bodyTypeString) ? foundValues[bodyTypeString] : null;
            var fixRotation = foundValues.ContainsKey(fixRotationString) ? (bool?) bool.Parse(foundValues[fixRotationString]) : null;
            var isLethal = foundValues.ContainsKey(lethalString) ? (bool?)bool.Parse(foundValues[lethalString]) : null;

            if (foundValues.ContainsKey(typeString))
            {
                var type = foundValues[typeString];
                var depth = foundValues.ContainsKey(depthString) ? float.Parse(foundValues[depthString]) : 0f;
                this.RenderLayerLevel = Layer.GetLevelForLayerType(type: type, depth: depth);
            }
            var jointGroupIds = foundValues.ContainsKey(jointGroupIdString) ? TMXImporter.SplitStringList(foundValues[jointGroupIdString]) :  null ;
            //Create properties
            var properties = new LevelObjectProperties(collision: collision,
                isGround: isGround,
                isWall: isWall,
                renderGroupId: renderGroupId,
                jointId: jointGroupIds,
                isHidden: hidden,
                bodyType: bodyType,
                fixRotation: fixRotation,
                isLethal: isLethal
                );
            return properties;
        }

        private List<LevelObject> ReadObjects(IEnumerable<XElement> objectElements, LevelObjectProperties properties)
        {
            var objects = new List<LevelObject>();
            foreach (var objectElement in objectElements)
            {
                LevelObject obj = LevelObject.Create(objectElement: objectElement, offset: this.Offset, properties: properties);
                objects.Add(obj);
            }
            return objects;
        }

        public void LoadContent(PhysicalWorld world, Map map, ContentManager contentManager)
        {
            foreach(LevelObject obj in Objects)
            {
                obj.LoadContent(world: world, map:map, contentManager: contentManager);
            }
        }

        public void Dispose()
        {
            foreach (LevelObject obj in Objects)
            {
                obj.Dispose();
            }
        }
        
    }
}
