﻿/**
* Copyright 2017 Niclas Scheuing, Per Näslung, Tunay Bora, Xiaowei Zeng
* This file is part of REWIND.
*
* REWIND is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* REWIND is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
* GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with REWIND.If not, see<http://www.gnu.org/licenses/>.
*/
//using FarseerPhysics.Dynamics;
//using FarseerPhysics.Dynamics.Contacts;
//using Microsoft.Xna.Framework;
//using Microsoft.Xna.Framework.Audio;
//using System;
//using System.Collections.Generic;
//using System.Threading;
//using team8.Control;
//using team8.Physics;
//using team8.Replay;
//using team8.Sound;
//using static team8.Control.InputController;
//using static team8.Level.SwitchEvent;
//using static team8.Replay.SpecialAction;
//using static team8.Sound.SoundManager;

//namespace team8.Level
//{
//    public class WindPipeSwitch : SwitchEvent
//    {
//        private String WindPipeId;
//        private SoundFile GoodSound;
//        private SoundFile BadSound;
//        private float MaxVolume = 1;
//        private PhysicsLevelObject Pipe;
//        public WindPipeSwitch(PhysicalWorld world, String windPipeId, SoundFile goodSound, SoundFile badSound): base(world)
//        {
//            this.WindPipeId = windPipeId;
//            this.GoodSound = goodSound;
//            this.BadSound = badSound;
//        }

//        public override void OnCollision(Contact contact)
//        {

//            if(Pipe == null)
//            {

//                this.Pipe = World.GetRenderPhysicsGroup(renderGroupId: this.WindPipeId).PhysicsLevelObject;
//            }

//            MaxVolume = 1f;
//            PlaySound();
//            float rotationAngle = 2f;

//            //Hijack right joystick
//            KeyPressDelegate rightKeyPressed = delegate (GameTime gameTime)
//            {
//                var state = RotatePipe(angle: -rotationAngle);
//                SpecialActionDelegate action = delegate ()
//                {
//                    SetPipeToState(state: state);
//                };
//                ReplayEngine.AddSpecialAction(gameTime: gameTime, position: World.MainCharacter.Position, action: action);
//            };
//            KeyPressDelegate leftKeyPressed = delegate (GameTime gameTime)
//            {
//                var state = RotatePipe(angle: rotationAngle);
//                SpecialActionDelegate action = delegate ()
//                {
//                    SetPipeToState(state: state);
//                };
//                ReplayEngine.AddSpecialAction(gameTime: gameTime, position: World.MainCharacter.Position, action: action);
//            };

//            InputController.HijackControl(keyDelegate: rightKeyPressed, key: KeyDefinition.RightStickRight, blocking: true);
//            InputController.HijackControl(keyDelegate: leftKeyPressed, key: KeyDefinition.RightStickLeft, blocking: true);
//        }

//        public override bool CountContacts()
//        {
//            return false;
//        }

//        public override void OnSeparation()
//        {
//            base.OnSeparation();
//            MaxVolume = 0.5f;
//            PlaySound();
//            InputController.ReleaseHijackedControl(key: KeyDefinition.RightStickRight);
//            InputController.ReleaseHijackedControl(key: KeyDefinition.RightStickLeft);
//        }

//        private void PlaySound()
//        {

//            UpdatVolumeDelegate badVolumeUpdate = delegate ()
//            {
//                var (_, badVolume) = GetUpdatedVolume();
//                return badVolume;
//            };
//            UpdatVolumeDelegate goodVolumeUpdate = delegate ()
//            {
//                var (goodVolume, _) = GetUpdatedVolume();
//                return goodVolume;
//            };
//            UpdatPositionDelegate positionUpdate = delegate ()
//            {
//                return PipePosition();
//            };
//            SoundManager.Instance.PlaySound(BadSound, id: WindPipeId+"_bad", loop: true, updateDelegate: badVolumeUpdate, positionDelegate: positionUpdate);
//            SoundManager.Instance.PlaySound(GoodSound, id: WindPipeId + "_good", loop: true, updateDelegate: goodVolumeUpdate, positionDelegate: positionUpdate);
//        }
//        private Vector2 PipePosition()
//        {
//            return Pipe.DisplayPosition();
//        }

//        private (float, float) GetUpdatedVolume()
//        {
//            var targetRotation = 0;
//            var currentRotation = Pipe.Body.Rotation;
//            var value = Math.Cos(targetRotation - currentRotation);
//            var goodVolume = (float)Math.Max(value, 0);
//            float badVolume = MaxVolume * (1 - goodVolume);
//            goodVolume = MaxVolume * goodVolume;
//            return (goodVolume, badVolume);
//        }

//        private PhysicsLevelObject.State RotatePipe(float angle)
//        {
//            Pipe.Body.AngularVelocity = angle;
//            return Pipe.GetState();
//        }

//        private void SetPipeToState( PhysicsLevelObject.State state)
//        {
//            Pipe.SetState(state: state);
//        }

//        public override bool ReactToContact(Fixture fixtureA, Fixture fixtureB)
//        {
//            bool contactToMain = MainCharacter.ContactIsToMainCharacter(fixtureA: fixtureA, fixtureB: fixtureB);
//            return contactToMain;
//        }
//    }

//    public class WindPipeTopSensor : AnimatedSwitchBranch
//    {
//        private Timer Timer;
//        private Timer CloseTimer;
//        public bool Open = false;
//        public delegate void OnActionDelegate();
//        public OnActionDelegate OnAction;

//        public WindPipeTopSensor(PhysicalWorld world, List<string> branchRenderGroupIds, List<string> closedCollisionIds = null, List<string> closedWallIds = null, List<string> openCollisionIds = null, bool reverted = false) :
//            base(world:world,
//                closedCollisionIds: closedCollisionIds==null ?  new List<string>() : closedCollisionIds,
//                closedWallIds: closedWallIds == null ? new List<string>(): closedWallIds,
//                openCollisionIds: openCollisionIds==null ? new List<string>() : openCollisionIds,
//                branchRenderGroupIds: branchRenderGroupIds, reverted: reverted)
//        {
//        }

//        public override void OnCollision(Contact contact)
//        {
//            if (Open)
//            {
//                return;
//            }
//            TimerCallback completionHandler = delegate (object state)
//            {
//                Timer.Dispose();
//                if (CloseTimer != null)
//                {
//                    CloseTimer.Dispose();
//                }
//                this.Open = true;
//                Action();
//            };
//            Timer = new Timer(callback: completionHandler, state: null, dueTime: 100, period: Timeout.Infinite);
//        }
//        public override void KillEvent(Fixture fixtureA, Fixture fixtureB)
//        {
//            if (Timer != null)
//            {
//                Timer.Dispose();
//            }
//        }
//        virtual protected void Action()
//        {
//            Change(toOpen: !Reverted);
//            OnAction?.Invoke();
//        }
//        public override void OnSeparation()
//        {
//            TimerCallback closeBranch = delegate (object state)
//            {
//                this.Open = false;
//                Change(toOpen: Reverted);
//            };
//            CloseTimer = new Timer(callback: closeBranch, state: null, dueTime: 1000, period: Timeout.Infinite);
//        }

//        public override bool ReactToContact(Fixture fixtureA, Fixture fixtureB)
//        {
//            ////React to all except characters
//            bool contactToMain = MainCharacter.ContactIsToMainCharacter(fixtureA: fixtureA, fixtureB: fixtureB);
//            bool contactToGhost = MainCharacter.ContactIsToGhost(fixtureA: fixtureA, fixtureB: fixtureB);
//            return !contactToMain && !contactToGhost;
//        }
//    }

//    public class BottomWindPipTopSensor : WindPipeTopSensor
//    {
//        private WindPipeTopSensor MiddleEvent;

//        public BottomWindPipTopSensor(PhysicalWorld world, List<string> branchRenderGroupIds, List<string> closedCollisionIds=null, List<string> closedWallIds=null, List<string> openCollisionIds=null, bool reverted = false) : base(world: world, openCollisionIds: openCollisionIds, branchRenderGroupIds: branchRenderGroupIds, reverted:reverted)
//        {
//            MiddleEvent = (WindPipeTopSensor)GetSwitchEvent(switchId: "windpipe_middle_top", world: World);
//            MiddleEvent.OnAction += Action;
//        }

//        //public BottomWindPipTopSensor(PhysicalWorld world, List<string> closedGroupIds, List<string> openGroupIds) : base(world, closedGroupIds, openGroupIds)
//        //{
//        //    MiddleEvent = (WindPipeTopSensor)GetSwitchEvent(switchId: "windpipe_middle_top", world: World);
//        //    MiddleEvent.OnAction += Action;
//        //}

//        override protected void Action()
//        {

//            if (this.Open && MiddleEvent.Open)
//            {

//            }
//        }

//        //protected override void PlaySound()
//        //{

//        //}
//    }

//}
