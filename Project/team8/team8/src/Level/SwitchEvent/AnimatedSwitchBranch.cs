﻿/**
* Copyright 2017 Niclas Scheuing, Per Näslung, Tunay Bora, Xiaowei Zeng
* This file is part of REWIND.
*
* REWIND is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* REWIND is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
* GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with REWIND.If not, see<http://www.gnu.org/licenses/>.
*/
using FarseerPhysics.Dynamics.Contacts;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using team8.Physics;
using team8.Sound;
using static team8.Sound.SoundManager;

namespace team8.Level
{
    public class AnimatedSwitchBranch : SwitchEvent
    {
        private List<AnimatedBranchDefinition> BranchDefinitions;
        //private Semaphore ChangeSemaphore;

        public class AnimatedBranchDefinition
        {
            public string OpenCollisionId;
            public string ClosedCollisionId;
            public string ClosedWallId;
            public string OpenWallId;
            public string BranchRenderGroupId;
            public bool Reverted;
            public bool? Open;
            public int ControlledByOtherSwitchCount;

            public AnimatedBranchDefinition(string branchRanderGroupId, string openCollisionid = null, string closedCollisionId = null, string closedWallId = null, string openWallId = null, bool reverted = false)
            {
                this.OpenCollisionId = openCollisionid;
                this.ClosedCollisionId = closedCollisionId;
                this.ClosedWallId = closedWallId;
                this.BranchRenderGroupId = branchRanderGroupId;
                this.Reverted = reverted;
                this.OpenWallId = openWallId;
                this.Open = null;
                this.ControlledByOtherSwitchCount = 0;
            }
        }
        public delegate void OnBranchChangeDelegate(bool toOpen, string branchRanderGroupId);
        public delegate void OnControlDelegate(string branchRanderGroupId);
        public OnBranchChangeDelegate OnBranchChange;
        public OnControlDelegate OnControlTaken;
        public OnControlDelegate OnControlReleased;

        public AnimatedSwitchBranch(PhysicalWorld world, List<AnimatedBranchDefinition> definition) : base(world)
        {
            this.BranchDefinitions = definition;
            //this.ChangeSemaphore = new Semaphore(initialCount: 1, maximumCount: 1);
        }

        override public void OnCollision(Contact contact)
        {

            HandleControll(taken: true);
            Change(toOpen: true);
        }

        override public void OnSeparation()
        {
            Change(toOpen: false);
            HandleControll(taken: false);
        }
        private void HandleControll(bool taken)
        {
            foreach (var definition in this.BranchDefinitions)
            {
                var renderGroupClosed = World.GetRenderPhysicsGroup(renderGroupId: definition.BranchRenderGroupId);
                if (renderGroupClosed != null)
                {
                    if (taken)
                    {
                        OnControlTaken?.Invoke(branchRanderGroupId: definition.BranchRenderGroupId);
                    }
                    else
                    {
                        OnControlReleased?.Invoke(branchRanderGroupId: definition.BranchRenderGroupId);
                    }
                }
            }
        }
        public void BranchChanged(bool toOpen, string branchRanderGroupId)
        {
            if (this.BranchDefinitions.Exists(def => def.BranchRenderGroupId == branchRanderGroupId))
            {
                var definition = this.BranchDefinitions.Find(def => def.BranchRenderGroupId == branchRanderGroupId);
                definition.Open = toOpen;
            }
        }
        public void OtherSwitchCollision(string branchRanderGroupId)
        {
            if (this.BranchDefinitions.Exists(def => def.BranchRenderGroupId == branchRanderGroupId))
            {
                var definition = this.BranchDefinitions.Find(def => def.BranchRenderGroupId == branchRanderGroupId);
                definition.ControlledByOtherSwitchCount += 1;
            }
        }
        public void OtherSwitchRelease(string branchRanderGroupId)
        {
            if (this.BranchDefinitions.Exists(def => def.BranchRenderGroupId == branchRanderGroupId))
            {
                var definition = this.BranchDefinitions.Find(def => def.BranchRenderGroupId == branchRanderGroupId);
                definition.ControlledByOtherSwitchCount -= 1;
            }
        }

        protected void Change(bool toOpen)
        {
            //ChangeSemaphore.WaitOne();
            foreach (var definition in this.BranchDefinitions)
            {
                var setOpen = definition.Reverted ? !toOpen : toOpen;

                var renderGroupClosed = World.GetRenderPhysicsGroup(renderGroupId: definition.BranchRenderGroupId);
                if (renderGroupClosed != null)
                {
                    var animatedBranch = (AnimationLevelObject)renderGroupClosed.VisualLevelObject;
                    var moveBranch = (!definition.Open.HasValue || setOpen != definition.Open.Value) && definition.ControlledByOtherSwitchCount == 0;
                    if (!moveBranch)
                    {
                        continue;
                    }
                    animatedBranch.SetAnimationDirection(forward: setOpen);
                    OnBranchChange?.Invoke(toOpen: setOpen, branchRanderGroupId: definition.BranchRenderGroupId);
                    BranchChanged(toOpen: setOpen, branchRanderGroupId: definition.BranchRenderGroupId);
                    animatedBranch.Play();
                    animatedBranch.Rewind();


                }
                if (definition.ClosedCollisionId != null)
                {
                    var collisionClosed = World.GetOtherObject(id: definition.ClosedCollisionId);
                    if (collisionClosed != null)
                    {
                        collisionClosed.IsHidden = setOpen;
                    }
                }

                if (definition.ClosedWallId != null)
                {
                    var wallClosed = World.GetWallObject(id: definition.ClosedWallId);
                    if (wallClosed != null)
                    {
                        wallClosed.IsHidden = setOpen;
                    }
                }

                if (definition.OpenCollisionId != null)
                {
                    var collisionOpen = World.GetOtherObject(id: definition.OpenCollisionId);
                    if (collisionOpen != null)
                    {
                        collisionOpen.IsHidden = !setOpen;
                    }
                }

                if (definition.OpenWallId != null)
                {
                    var wallOpen = World.GetWallObject(id: definition.OpenWallId);
                    if (wallOpen != null)
                    {
                        wallOpen.IsHidden = !setOpen;
                    }
                }

                PlaySound(definition);
            }
            //ChangeSemaphore.Release();

        }



        protected virtual void PlaySound(AnimatedBranchDefinition definition)
        {
            var renderGroupClosed = World.GetRenderPhysicsGroup(renderGroupId: definition.BranchRenderGroupId);
            if (renderGroupClosed != null && renderGroupClosed.Ready)
            {
                UpdatPositionDelegate positionUpdate = delegate (GameTime gameTime)
                {
                    return renderGroupClosed.DisplayPosition();
                };
                SoundManager.Instance.PlaySound(file: SoundFile.BranchCreak, id: renderGroupClosed.RenderGroupId, initialVolume: Configuration.Sound.BranchVolume, positionDelegate: positionUpdate);
            }

        }
    }
}
