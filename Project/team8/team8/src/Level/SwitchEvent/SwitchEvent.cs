/**
* Copyright 2017 Niclas Scheuing, Per N�slung, Tunay Bora, Xiaowei Zeng
* This file is part of REWIND.
*
* REWIND is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* REWIND is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
* GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with REWIND.If not, see<http://www.gnu.org/licenses/>.
*/
using FarseerPhysics.Dynamics;
using FarseerPhysics.Dynamics.Contacts;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using team8.Control;
using team8.Physics;
using team8.Sound;
using static team8.Control.InputController;
using static team8.Level.DoorEvent;
using static team8.Sound.SoundManager;

namespace team8.Level
{
    using Coordinate = Vector2;
    public abstract class SwitchEvent
    {
        protected PhysicalWorld World;
        protected static Dictionary<string, SwitchEvent> Archive = new Dictionary<string, SwitchEvent>();
        public SwitchEvent(PhysicalWorld world)
        {
            this.World = world;
        }
        static public SwitchEvent GetSwitchEvent(string switchId, PhysicalWorld world)
        {
            switch (switchId)
            {
                case "switchBranch1":
                    return new AnimatedSwitchBranch(world: world, definition: new List<AnimatedSwitchBranch.AnimatedBranchDefinition>
                    {
                        new AnimatedSwitchBranch.AnimatedBranchDefinition(openCollisionid: "open_branch_top_1", closedCollisionId: "close_branch_top_1", closedWallId: "branch_close_wall_1", branchRanderGroupId: "branchclosed1" )
                    });
                case "switchBranch2":
                    return new AnimatedSwitchBranch(world: world, definition: new List<AnimatedSwitchBranch.AnimatedBranchDefinition>
                    { 
                        new AnimatedSwitchBranch.AnimatedBranchDefinition(openCollisionid: "open_branch_top_2", closedWallId: "branch_close_wall_2", branchRanderGroupId: "branchclosed2" )
                    });
                    
                case "switchBranch3":
                    return new AnimatedSwitchBranch(world: world, definition: new List<AnimatedSwitchBranch.AnimatedBranchDefinition>
                    {
                        new AnimatedSwitchBranch.AnimatedBranchDefinition(openCollisionid: "open_branch_top_3", branchRanderGroupId: "branchclosed3", reverted: true )
                    });
                case "switchBranch6":
                    return new AnimatedSwitchBranch(world: world, definition: new List<AnimatedSwitchBranch.AnimatedBranchDefinition>
                    {
                        new AnimatedSwitchBranch.AnimatedBranchDefinition(openCollisionid: "open_branch_top_6", closedWallId: "branch_close_wall_6", branchRanderGroupId: "branchclosed6" )
                    });
                    
                case "switchBranch7":
                    return new AnimatedSwitchBranch(world: world, definition: new List<AnimatedSwitchBranch.AnimatedBranchDefinition>
                    {
                        new AnimatedSwitchBranch.AnimatedBranchDefinition(openCollisionid: "open_branch_top_7", closedCollisionId: "close_branch_top_7" ,branchRanderGroupId: "branchclosed7", reverted: true)
                    });
                case "dead_tree_bottom":
                    if (!Archive.ContainsKey(switchId))
                    {
                        Archive[switchId] = new AnimatedSwitchBranch(world: world, definition: new List<AnimatedSwitchBranch.AnimatedBranchDefinition>
                        {
                            new AnimatedSwitchBranch.AnimatedBranchDefinition(openCollisionid: "branch_pipe_top_open", closedCollisionId: "branch_pipe_top_close", branchRanderGroupId: "branch_pipe_top", reverted: true)
                        });

                    }

                    var bottomDeadTreeEvent = (AnimatedSwitchBranch)Archive[switchId];

                    if (Archive.ContainsKey("dead_tree_top"))
                    {
                        var topDeadTreeEventLoaded = (AnimatedSwitchBranch)Archive["dead_tree_top"];
                        bottomDeadTreeEvent.OnBranchChange += topDeadTreeEventLoaded.BranchChanged;
                        bottomDeadTreeEvent.OnControlReleased += topDeadTreeEventLoaded.OtherSwitchRelease;
                        bottomDeadTreeEvent.OnControlTaken += topDeadTreeEventLoaded.OtherSwitchCollision;

                        topDeadTreeEventLoaded.OnBranchChange += bottomDeadTreeEvent.BranchChanged;
                        topDeadTreeEventLoaded.OnControlReleased += bottomDeadTreeEvent.OtherSwitchRelease;
                        topDeadTreeEventLoaded.OnControlTaken += bottomDeadTreeEvent.OtherSwitchCollision;
                    }
                    return bottomDeadTreeEvent;
                case "dead_tree_top":

                    if (!Archive.ContainsKey(switchId))
                    {
                        Archive[switchId] = new AnimatedSwitchBranch(world: world, definition: new List<AnimatedSwitchBranch.AnimatedBranchDefinition>
                        {
                            new AnimatedSwitchBranch.AnimatedBranchDefinition(openCollisionid: "branch_pipe_top_open", closedCollisionId: "branch_pipe_top_close", branchRanderGroupId: "branch_pipe_top", reverted: true),
                            new AnimatedSwitchBranch.AnimatedBranchDefinition(openCollisionid: "branch_pipe_bottom_open", closedWallId: "branch_pipe_bottom_close", branchRanderGroupId: "branch_pipe_bottom", reverted: false)
                        });
                    }
                    var topDeadTreeEvent = (AnimatedSwitchBranch)Archive[switchId];

                    if( Archive.ContainsKey("dead_tree_bottom") )
                    {
                        var bottomDeadTreeEventLoaded = (AnimatedSwitchBranch)Archive["dead_tree_bottom"];
                        bottomDeadTreeEventLoaded.OnBranchChange += topDeadTreeEvent.BranchChanged;
                        bottomDeadTreeEventLoaded.OnControlReleased += topDeadTreeEvent.OtherSwitchRelease;
                        bottomDeadTreeEventLoaded.OnControlTaken += topDeadTreeEvent.OtherSwitchCollision;

                        topDeadTreeEvent.OnBranchChange += bottomDeadTreeEventLoaded.BranchChanged;
                        topDeadTreeEvent.OnControlReleased += bottomDeadTreeEventLoaded.OtherSwitchRelease;
                        topDeadTreeEvent.OnControlTaken += bottomDeadTreeEventLoaded.OtherSwitchCollision;
                    }
                    return topDeadTreeEvent;

                case "catch":
                    return new AnimatedSwitchBranch(world: world, definition: new List<AnimatedSwitchBranch.AnimatedBranchDefinition>
                    {
                        new AnimatedSwitchBranch.AnimatedBranchDefinition(openCollisionid: "open_catch", branchRanderGroupId: "catchbranch", reverted: false)
                    });
                case "fall_down":
                    return new AnimatedSwitchBranch(world: world, definition: new List<AnimatedSwitchBranch.AnimatedBranchDefinition>
                    {
                        new AnimatedSwitchBranch.AnimatedBranchDefinition(openCollisionid: "fall_down_closed_branch", branchRanderGroupId: "fall_down_branch", reverted: true)
                    });
                case "ground_boulder":
                    return new BoulderEvent(world: world, shakingObjectId: "shake_ground_1", impulse: 0, soundFile: SoundFile.RockImpact);
                case "boulder_stopper":
                    return new RemoveStopperEvent(world: world);
                case "instruction_jump":
                    return new InstructionEvent(world: world,
                                                    texts: new Queue<string>(new[] { InstructionConstants.Text.Jump }) ,
                                                    displayTimes: new Queue<int> (new []{InstructionConstants.Duration.Jump }));
                case "instruction_replay":
                    return new ReplayInstructionEvent(world: world,
                                                    texts: new Queue<string>(new[] { InstructionConstants.Text.Record, InstructionConstants.Text.Replay, InstructionConstants.Text.MarkCharacter, InstructionConstants.Text.ChooseCharacter }),
                                                    displayTimes: new Queue<int>(new[] { InstructionConstants.Duration.Record, InstructionConstants.Duration.Replay, InstructionConstants.Duration.MarkCharacter, InstructionConstants.Duration.ChooseCharacter }));

                //Doors
                case "door_after_boulder":
                    return new DoorEvent(world: world, doorDefinitions: new List<DoorDefinition>() {
                        new DoorDefinition(doorRenderGroupId: "door_after_boulder", velocity: new Vector2(0,-1), validAreaObjectId: "door_after_boulder_area")
                    });
                

                case "cable_car_start":
                    return new CableCarEvent(world: world, doorDefinitions: new List<DoorDefinition>() {
                        new DoorDefinition(doorRenderGroupId: "cable_car_top", velocity: new Vector2(PuzzleConstants.CableCarSpeed, 0), onCollisionOnly: true, validAreaObjectId: "valid_area_cable_car")
                    },
                    soundSourceRenderGroupId: "car_balance"
                    );
                

                case "door_switch_puzzle_1":
                    return new DoorEvent(world: world, doorDefinitions: new List<DoorDefinition>() {
                        new DoorDefinition(doorRenderGroupId: "door_puzzle_1", velocity: new Vector2(0, 2f), validAreaObjectId: "valid_area_puzzle_door_1")
                    });

                case "door_switch_puzzle_2-4":
                    var doorSpeed = 2;
                    return new DoorEvent(world: world, doorDefinitions: new List<DoorDefinition>() {
                        new DoorDefinition(doorRenderGroupId: "door_puzzle_2", velocity: new Vector2(0, -doorSpeed), validAreaObjectId: "valid_area_puzzle_door_2"),
                        new DoorDefinition(doorRenderGroupId: "door_puzzle_3", velocity: new Vector2(0, -doorSpeed), validAreaObjectId: "valid_area_puzzle_door_3"),
                        new DoorDefinition(doorRenderGroupId: "door_puzzle_4", velocity: new Vector2(0, -doorSpeed), validAreaObjectId: "valid_area_puzzle_door_4"),
                    });

                case "light_switch_on_tunnel1":
                    return new LightSwitch(world:world);
                case "turn_off_fog":
                    return new TurnOffFog(world: world);
                default:
                    throw new Exception("Switch has no valid switchid defined. This is invalid");
            }
        }


        public virtual void OnCollision(Contact contact) { }
        public virtual void OnSeparation() { }
        public virtual void KillEvent(Fixture fixtureA, Fixture fixtureB) { }
        public virtual void Reset() { }

        //Returns true if OnCollision should be triggered for this contact
        public virtual bool ReactToContact(Fixture fixtureA, Fixture fixtureB)
        {
            bool contactToMain = MainCharacter.ContactIsToMainCharacter(fixtureA: fixtureA, fixtureB: fixtureB);
            bool contactToGhost = MainCharacter.ContactIsToGhost(fixtureA: fixtureA, fixtureB: fixtureB);
            return contactToMain || contactToGhost;
        }

        public virtual bool CountContacts()
        {
            return true;
        }

        public static void Dispose()
        {
            Archive = new Dictionary<string, SwitchEvent>();
        }
        public class TurnOffFog : SwitchEvent
        {
            public TurnOffFog(PhysicalWorld world) : base(world)
            {
            }

            public override void OnCollision(Contact contact)
            {
                Rendering.Renderer.ReduceFog = true;
            }

        }

            public class InstructionEvent : SwitchEvent
        {
            private bool Triggered = false;

            protected Queue<string> Texts;
            private Queue<int> DisplayTimes;
            //private Timer Timer;
            public InstructionEvent(PhysicalWorld world, Queue<string> texts, Queue<int> displayTimes) : base(world)
            {
                this.Texts = texts;
                this.DisplayTimes = displayTimes;
            }

            public override bool ReactToContact(Fixture fixtureA, Fixture fixtureB)
            {
                return base.ReactToContact(fixtureA, fixtureB) && !Triggered;
            }
            

            public override void OnCollision(Contact contact)
            {
                this.Triggered = true;
                ShowTimedText(null);
                InputController.Instance.HijackControl(key: KeyDefinition.ReplayKey, keyDelegate: ShowTimedText, blocking: false, singlePress: true, onRelease: true);
            }

            protected virtual void ShowTimedText(object state)
            {

                if (Texts.Count > 0)
                {
                    var displayTime = this.DisplayTimes.Dequeue();
                    var text = this.Texts.Dequeue();
                    InstructionHandler.Instance.ShowText(text: text, forMilliSeconds: displayTime);
                }
                else
                {
                    InputController.Instance.ReleaseHijackedControl(key: KeyDefinition.ReplayKey);
                }
            }
        }

        public class ReplayInstructionEvent : InstructionEvent
        {
            public ReplayInstructionEvent(PhysicalWorld world, Queue<string> texts, Queue<int> displayTimes) : base(world, texts, displayTimes)
            {
            }

            public override void OnCollision(Contact contact)
            {
                base.OnCollision(contact);
                InputController.Instance.HijackControl(key: KeyDefinition.ReplayKey, keyDelegate: ShowTimedText, blocking: false, singlePress: true, onRelease: true);
            }

            override protected void ShowTimedText(object state)
            {
                switch(Texts.Count)
                {
                    case 1:
                        InputController.Instance.ReleaseHijackedControl(key: KeyDefinition.ReplayKey);
                        InputController.Instance.HijackControl(key: KeyDefinition.ChooseCharacterKey, keyDelegate: ShowTimedText, blocking: false);
                        break;
                    case 0:
                        InputController.Instance.ReleaseHijackedControl(key: KeyDefinition.ChooseCharacterKey);
                        InstructionHandler.Instance.Reset();
                        break;
                }
                base.ShowTimedText(state);
            }
        }

        public class BoulderEvent : SwitchEvent
        {
            private string ShakingObjectId;
            private float Impulse;
            private SoundFile SoundFile;

            public BoulderEvent(PhysicalWorld world, string shakingObjectId, float impulse, SoundFile soundFile) : base(world)
            {
                this.ShakingObjectId = shakingObjectId;
                this.Impulse = impulse;
                this.SoundFile = soundFile;
            }

            public override void OnCollision(Contact contact)
            {
                var shakableGround = World.GetOtherObject(id: ShakingObjectId);
                if (shakableGround != null)
                {
                    shakableGround.Body.ApplyLinearImpulse(new Vector2(0, Impulse));
                }

                SoundManager.Instance.PlaySound(this.SoundFile, id: "boulder", initialVolume: 1f);
            }

            public override bool ReactToContact(Fixture fixtureA, Fixture fixtureB)
            {
                ////React to all except characters
                bool contactToMain = MainCharacter.ContactIsToMainCharacter(fixtureA: fixtureA, fixtureB: fixtureB);
                bool contactToGhost = MainCharacter.ContactIsToGhost(fixtureA: fixtureA, fixtureB: fixtureB);
                return !contactToMain && !contactToGhost;
            }
            public override bool CountContacts()
            {
                return false;
            }
        }

        public class RemoveStopperEvent : SwitchEvent
        {
            private int CollisionCount = 0;
            public RemoveStopperEvent(PhysicalWorld world) : base(world)
            {
            }

            public override void OnCollision(Contact contact)
            {

                Coordinate position = Vector2.Zero;
                if (CollisionCount == 0)
                {
                    var rightBranchGroup = World.GetRenderPhysicsGroup(renderGroupId: "boulder_branch_right");
                    if (rightBranchGroup != null)
                    {
                        rightBranchGroup.PhysicsLevelObject.Body.Rotation = MathHelper.ToRadians(-10);
                        rightBranchGroup.PhysicsLevelObject.Body.Position = rightBranchGroup.PhysicsLevelObject.Body.Position + new Vector2(0, 0.1f);
                        position = rightBranchGroup.DisplayPosition().Value;
                    }

                    var leftBranchGroup = World.GetRenderPhysicsGroup(renderGroupId: "boulder_branch_left");
                    if (leftBranchGroup != null)
                    {
                        leftBranchGroup.PhysicsLevelObject.Body.Rotation = MathHelper.ToRadians(10);
                        leftBranchGroup.PhysicsLevelObject.Body.Position = leftBranchGroup.PhysicsLevelObject.Body.Position + new Vector2(0, 0.1f);
                    }
                    var boulder = World.GetRenderPhysicsGroup(renderGroupId: "boulder");
                    if (boulder != null)
                    {
                        boulder.PhysicsLevelObject.Body.Position = boulder.PhysicsLevelObject.Body.Position + new Vector2(0, 0.25f);
                        //boulder.PhysicsLevelObject.Body.BodyType = BodyType.Dynamic;
                        //boulder.PhysicsLevelObject.Body.ApplyTorque(1000);
                    }
                    var anchor = World.GetOtherObject(id: "boulder_top_anchor");
                    if (anchor != null)
                    {
                        anchor.Body.Position = anchor.Body.Position + new Vector2(0, 0.25f);

                        //boulder.PhysicsLevelObject.Body.BodyType = BodyType.Dynamic;
                        //boulder.PhysicsLevelObject.Body.ApplyTorque(1000);
                    }
                    var balanceGroup = World.GetRenderPhysicsGroup(renderGroupId: "boulder_balance");
                    if (balanceGroup != null)
                    {
                        balanceGroup.PhysicsLevelObject.Body.ApplyLinearImpulse(new Vector2(0, 1));
                        //leftBranchGroup.PhysicsLevelObject.Body.Position = leftBranchGroup.PhysicsLevelObject.Body.Position + new Vector2(0, 0.1f);
                    }

                    CollisionCount = 1;
                }
                // second impact
                else if(CollisionCount == 1)
                {

                    var smallBoulderBlocker = World.GetOtherObject(id: "small_boulder_blocker");
                    if (smallBoulderBlocker != null)
                    {
                        smallBoulderBlocker.Body.Enabled = false;

                        //boulder.PhysicsLevelObject.Body.BodyType = BodyType.Dynamic;
                        //boulder.PhysicsLevelObject.Body.ApplyTorque(1000);
                    }
                    var distanceJoint = World.GetJointGroup(jointGroupId: "boulder_rope_top");
                    if (distanceJoint != null)
                    {
                        distanceJoint.JointObject.Joint.Enabled = false;
                    }

                    var boulder = World.GetRenderPhysicsGroup(renderGroupId: "boulder");
                    if (boulder != null)
                    {
                        boulder.PhysicsLevelObject.Body.BodyType = BodyType.Dynamic;
                        //boulder.PhysicsLevelObject.Body.ApplyTorque(1000);
                    }
                    var rightBranchGroup = World.GetRenderPhysicsGroup(renderGroupId: "boulder_branch_right");
                    if (rightBranchGroup != null)
                    {
                        rightBranchGroup.PhysicsLevelObject.Body.BodyType = BodyType.Dynamic;
                        position = rightBranchGroup.DisplayPosition().Value;
                    }

                    var leftBranchGroup = World.GetRenderPhysicsGroup(renderGroupId: "boulder_branch_left");
                    if (leftBranchGroup != null)
                    {
                        leftBranchGroup.PhysicsLevelObject.Body.BodyType = BodyType.Dynamic;
                    }
                    var balanceGroup = World.GetRenderPhysicsGroup(renderGroupId: "boulder_balance");
                    if (balanceGroup != null)
                    {
                        balanceGroup.PhysicsLevelObject.Body.ApplyLinearImpulse(new Vector2(0, 1));
                    }
                    var anchor = World.GetOtherObject(id: "boulder_top_anchor");
                    if (anchor != null)
                    {
                        anchor.IsHidden = true;
                        
                    }
                    
                    
                    CollisionCount = 2;
                }
                if (CollisionCount < 2)
                {
                    UpdatPositionDelegate positionDelegate = delegate (GameTime gameTime)
                    {
                        return position;
                    };
                    SoundManager.Instance.PlaySound(file: SoundFile.BranchBreak, id: "boulder_branch", positionDelegate: positionDelegate);
                }
            }
            public override void Reset()
            {
                CollisionCount = 0;
            }
        }

        public class LightSwitch : SwitchEvent
        {
            public LightSwitch(PhysicalWorld world) : base(world)
            {
            }

            public override void OnCollision(Contact contact)
            {
                World.MainCharacter.ShowLight = true;
                if (World.GhostCharacter != null)
                {
                    World.GhostCharacter.ShowLight = true;
                }
            }
            public override void OnSeparation()
            {
                World.MainCharacter.ShowLight = false;
                if (World.GhostCharacter != null)
                {
                    World.GhostCharacter.ShowLight = false;
                }
            }
        }
    }
}
