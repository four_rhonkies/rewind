﻿/**
* Copyright 2017 Niclas Scheuing, Per Näslung, Tunay Bora, Xiaowei Zeng
* This file is part of REWIND.
*
* REWIND is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* REWIND is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
* GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with REWIND.If not, see<http://www.gnu.org/licenses/>.
*/
using FarseerPhysics.Dynamics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using team8.Physics;
using FarseerPhysics.Dynamics.Contacts;
using Microsoft.Xna.Framework;
using System.Threading;
using MonoGame.Extended;
using MonoGame.Extended.Shapes;
using FarseerPhysics;
using team8.Sound;
using static team8.Sound.SoundManager;

namespace team8.Level
{
    using Coordinate = Vector2;
    public class DoorEvent : SwitchEvent
    {
        protected List<DoorDefinition> DoorDefinitions;
        public struct DoorDefinition
        {
            public string DoorRenderGroupId;
            public Vector2 Velocity;
            public bool OnCollisionOnly;
            public RectangleF? ValidArea;
            public string ValidAreaObjectId;
            public string SoundId;
            //public Coordinate ForwardLimit;
            //public Coordinate BackwardLimit;

            public DoorDefinition(string doorRenderGroupId, Vector2 velocity, string validAreaObjectId, bool onCollisionOnly = false)
            {
                this.DoorRenderGroupId = doorRenderGroupId;
                this.Velocity = velocity;
                this.OnCollisionOnly = onCollisionOnly;
                this.ValidArea = null;
                this.ValidAreaObjectId = validAreaObjectId;
                this.SoundId = "door_" + doorRenderGroupId;
                //this.ForwardLimit = forwardLimit;
                //this.BackwardLimit = backwardLimit;
            }

            public bool InValidRange(PhysicalWorld world)
            {
                var doorGroup = world.GetRenderPhysicsGroup(renderGroupId: DoorRenderGroupId);
                if (doorGroup != null && doorGroup.DisplayPosition().HasValue)
                {
                    var position = doorGroup.DisplayPosition().Value;
                    //var xSmaller = position.X < Math.Max(ForwardLimit.X, BackwardLimit.X);
                    //var xLarger = position.X > Math.Min(ForwardLimit.X, BackwardLimit.X);
                    //var ySmaller = position.Y < Math.Max(ForwardLimit.Y, BackwardLimit.Y);
                    //var yLarger = position.Y > Math.Min(ForwardLimit.Y, BackwardLimit.Y);
                    //return xSmaller && xLarger && ySmaller && yLarger;
                    if (!ValidArea.HasValue)
                    {
                        this.ValidArea = RectangleFromId(world: world, id: ValidAreaObjectId);
                    }
                    return ValidArea.Value.Contains(position);
                }
                return false;
            }


            private RectangleF RectangleFromId(PhysicalWorld world, string id)
            {
                var validAreaObject = (GeometryLevelObject)world.GetOtherObject(id: id);
                var polygon = new Polygon(validAreaObject.PointsInDisplay());
                return polygon.BoundingRectangle;
            }
        }

        public DoorEvent(PhysicalWorld world, List<DoorDefinition> doorDefinitions) : base(world)
        {
            this.DoorDefinitions = doorDefinitions;
            World.OnUpdate += Update;
        }

        public override void OnCollision(Contact contact)
        {
            MoveDoor(isCollision: true);
            World.OnRespawn += OnRespawn;
        }


        public override void OnSeparation()
        {
            MoveDoor(isCollision: false);
        }

        protected void MoveDoor(bool isCollision)
        {
            foreach (var definition in DoorDefinitions)
            {
                if (definition.OnCollisionOnly && !isCollision)
                {
                    continue;
                }
                var doorGroup = World.GetRenderPhysicsGroup(renderGroupId: definition.DoorRenderGroupId);
                if (doorGroup != null)
                {
                    //doorGroup.PhysicsLevelObject.Body.Position = doorGroup.PhysicsLevelObject.Body.Position + new Vector2(0, -3f);
                    doorGroup.PhysicsLevelObject.Body.LinearVelocity = definition.Velocity * (isCollision ? 1 : -1);
                    

                    //SoundManager.Instance.StopSound(id: definition.SoundId);
                    StartPlayingSound(definition: definition, doorGroup: doorGroup);
                }
            }
        }

        protected virtual void StartPlayingSound(DoorDefinition definition, RenderPhysicsGroup doorGroup)
        {
            UpdatPositionDelegate positionDelegate = delegate (GameTime gameTime)
            {
                return doorGroup.DisplayPosition();
            };
            SoundManager.Instance.PlaySound(file: SoundFile.StoneClose, id: definition.SoundId, positionDelegate: positionDelegate, loop: true, initialVolume: Configuration.Sound.DoorVolume);
        }



        private void Update(GameTime gameTime)
        {
            foreach (var definition in DoorDefinitions)
            {
                if (!definition.InValidRange(world: World))
                {
                    var doorGroup = World.GetRenderPhysicsGroup(renderGroupId: definition.DoorRenderGroupId);
                    if (doorGroup != null)
                    {
                        doorGroup.PhysicsLevelObject.Body.LinearVelocity = Vector2.Zero;
                        var closePoint = definition.ValidArea.Value.ClosestPointTo(doorGroup.DisplayPosition().Value);
                        var offset = (definition.ValidArea.Value.Center - closePoint) * new Vector2(Math.Abs(definition.Velocity.X), Math.Abs(definition.Velocity.Y));
                        closePoint = closePoint + offset*0.01f;
                        doorGroup.PhysicsLevelObject.Body.Position = ConvertUnits.ToSimUnits(closePoint);
                        SoundManager.Instance.StopSound(id: definition.SoundId);
                    }
                }
            }
        }

        protected virtual void OnRespawn()
        {
            StopSound();
        }

        private void StopSound()
        {
            foreach (var definition in DoorDefinitions)
            {
                SoundManager.Instance.StopSound(id: definition.SoundId);
            }
        }



        //public void TouchedSensor()
        //{
        //    var doorGroup = World.GetRenderPhysicsGroup(renderGroupId: DoorRenderGroupId);
        //    if (doorGroup != null)
        //    {
        //        //doorGroup.PhysicsLevelObject.Body.Position = doorGroup.PhysicsLevelObject.Body.Position + new Vector2(0, -3f);
        //        doorGroup.PhysicsLevelObject.Body.LinearVelocity = Vector2.Zero;
        //    }
        //}
    }

    public class CableCarEvent : DoorEvent
    {
        private Timer SoundStartTimer;
        private Timer CarStartTimer;
        private string SoundSourceRenderGroupId;
        public CableCarEvent(PhysicalWorld world, List<DoorDefinition> doorDefinitions, string soundSourceRenderGroupId) : base(world, doorDefinitions)
        {
            this.SoundSourceRenderGroupId = soundSourceRenderGroupId;
        }

        //public override void OnCollision(Contact contact)
        //{
        //    World.OnRespawn += OnRespawn;
        //    TimerCallback startCar = delegate (object state)
        //    {
        //        MoveDoor(isCollision: true);
        //    };
        //    CarStartTimer = new Timer(callback: startCar, state: null, dueTime: SoundConstants.CableCarStartingDuration, period: Timeout.Infinite);
        //    foreach (var definition in DoorDefinitions)
        //    {
        //        PlaySound(definition: definition);
        //    }

        //}

        protected override void StartPlayingSound(DoorDefinition definition, RenderPhysicsGroup doorGroup)
        {
            var soundGroup = World.GetRenderPhysicsGroup(renderGroupId: SoundSourceRenderGroupId);
            if (soundGroup != null)
            {
                UpdatPositionDelegate positionDelegate = delegate (GameTime gameTime)
                {
                    var soundSourceGroup = World.GetRenderPhysicsGroup(renderGroupId: SoundSourceRenderGroupId);
                    return soundSourceGroup.DisplayPosition();
                };
                TimerCallback startSound = delegate (object state)
                {
                    SoundManager.Instance.PlaySound(file: SoundFile.CableCarRunning, id: definition.SoundId, positionDelegate: positionDelegate, loop: true, initialVolume: Configuration.Sound.DoorVolume);
                };
                SoundStartTimer = new Timer(callback: startSound, state: null, dueTime: SoundConstants.CableCarStartingDuration, period: Timeout.Infinite);
                SoundManager.Instance.PlaySound(file: SoundFile.CableCarStarting, id: definition.SoundId + "_starting", positionDelegate: positionDelegate, initialVolume: Configuration.Sound.DoorVolume);
            }
        }

        protected override void OnRespawn()
        {
            SoundStartTimer?.Dispose();
            CarStartTimer?.Dispose();
            base.OnRespawn();
        }

        //private void PlaySound(DoorDefinition definition)
        //{ 
        //}
    }

    //public class DoorStopperEvent: SwitchEvent
    //{
    //    //public Action Collided;
    //    private string DoorRenderGroupId;
    //    private Timer RebounceTimer;
    //    private int ReboundsTime = 15;
    //    public DoorStopperEvent(PhysicalWorld world, string doorRenderGroupId) : base(world)
    //    {
    //        this.DoorRenderGroupId = doorRenderGroupId;
    //    }

    //    public override bool ReactToContact(Fixture fixtureA, Fixture fixtureB)
    //    {
    //        ////React to all except characters
    //        bool contactToMain = MainCharacter.ContactIsToMainCharacter(fixtureA: fixtureA, fixtureB: fixtureB);
    //        bool contactToGhost = MainCharacter.ContactIsToGhost(fixtureA: fixtureA, fixtureB: fixtureB);
    //        return !contactToMain && !contactToGhost;
    //    }

    //    public override void OnCollision(Contact contact)
    //    {
    //        var doorGroup = World.GetRenderPhysicsGroup(renderGroupId: DoorRenderGroupId);
    //        if (doorGroup != null)
    //        {
    //            var velocity = doorGroup.PhysicsLevelObject.Body.LinearVelocity;
    //            TimerCallback stop = delegate (object status)
    //            {
    //                doorGroup.PhysicsLevelObject.Body.LinearVelocity = Vector2.Zero;
    //            };
    //            RebounceTimer = new Timer(callback: stop, state: null, dueTime: ReboundsTime, period: Timeout.Infinite);
    //            doorGroup.PhysicsLevelObject.Body.LinearVelocity = -velocity;
    //        }
    //    }

}
