﻿/**
* Copyright 2017 Niclas Scheuing, Per Näslung, Tunay Bora, Xiaowei Zeng
* This file is part of REWIND.
*
* REWIND is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* REWIND is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
* GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with REWIND.If not, see<http://www.gnu.org/licenses/>.
*/
using FarseerPhysics;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace team8.Level
{
    using Coordinate = Vector2;
    public class RenderPhysicsGroup
    {
        public VisualLevelObject VisualLevelObject;
        public PhysicsLevelObject PhysicsLevelObject;
        public readonly string RenderGroupId;
        private Vector2 RenderPositionOffset;
        public bool Ready
        {
            get
            {
                return this.VisualLevelObject != null && this.PhysicsLevelObject != null;
            }
        }

        public RenderPhysicsGroup(string renderGroupId)
        {
            this.RenderGroupId = renderGroupId;
        }

        public void SetImageLayerObject(VisualLevelObject imageObject)
        {
            this.VisualLevelObject = imageObject;
            if (this.PhysicsLevelObject != null)
            {
                SetRenderPositionFunction();
            }
        }
        public void SetPhysicsLevelObject(PhysicsLevelObject physicsLevelObject)
        {
            this.PhysicsLevelObject = physicsLevelObject;
            if (this.VisualLevelObject != null)
            {
                SetRenderPositionFunction();
            }
        }

        private void SetRenderPositionFunction()
        {
            var offset = this.VisualLevelObject.OriginInDisplayCoordinates - this.PhysicsLevelObject.DisplayPosition();
            this.RenderPositionOffset = offset;
            this.VisualLevelObject.RenderPosition = this.ImageRenderPosition;
            this.VisualLevelObject.RenderRotation = this.ImageRenderRotation;
        }

        public Coordinate ImageRenderPosition()
        {
            var displayPosition = this.PhysicsLevelObject.DisplayPosition();
            displayPosition = displayPosition + this.RenderPositionOffset;
            return displayPosition;
        }
        public float ImageRenderRotation()
        {
            return PhysicsLevelObject.Rotation();
        }

        public void SetHidden(bool isHidden)
        {
            this.VisualLevelObject.IsHidden = isHidden;
            this.PhysicsLevelObject.IsHidden = isHidden;
        }

        //Angle in degrees
        public void Rotate(float angle)
        {
            var radianAngle = MathHelper.ToRadians(angle);
            this.PhysicsLevelObject.Body.Rotation = this.PhysicsLevelObject.Body.Rotation + radianAngle;
        }
        public Coordinate? DisplayPosition()
        {
            return this.PhysicsLevelObject?.DisplayPosition();
        }
    }
}
