﻿/**
* Copyright 2017 Niclas Scheuing, Per Näslung, Tunay Bora, Xiaowei Zeng
* This file is part of REWIND.
*
* REWIND is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* REWIND is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
* GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with REWIND.If not, see<http://www.gnu.org/licenses/>.
*/

using System;

namespace team8.Level
{
    public class JointGroup
    {
        public readonly string JointId;
        private PhysicsLevelObject ObjectA;
        private PhysicsLevelObject ObjectB;
        public JointLevelObject JointObject;

        public JointGroup(string jointId)
        {
            this.JointId = jointId;
        }

        public void AddPhysicsObject(PhysicsLevelObject physicsObject)
        {
            if(ObjectA == null)
            {
                this.ObjectA = physicsObject;
            }
            else if(ObjectB == null)
            {
                this.ObjectB = physicsObject;
            }
            else
            {
                throw new Exception("Tried adding too many PhysicsLevelObject to JointGroup "+JointId);
            }
            AllObjectsSet();
        }
        public void AddJointObject(JointLevelObject jointObject)
        {
            if (JointObject == null)
            {
                this.JointObject = jointObject;
            }
            else
            {
                throw new Exception("Tried adding too many JointLevelObject to JointGroup " + JointId);
            }
            AllObjectsSet();
        }

        private void AllObjectsSet()
        {
            if(this.ObjectA != null && ObjectB != null && JointObject != null)
            {
                JointObject.CreateJoint(bodyA: ObjectA.Body, bodyB: ObjectB.Body);
            }
        }
    }

}
