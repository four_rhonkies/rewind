﻿/**
* Copyright 2017 Niclas Scheuing, Per Näslung, Tunay Bora, Xiaowei Zeng
* This file is part of REWIND.
*
* REWIND is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* REWIND is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.See the
* GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with REWIND.If not, see<http://www.gnu.org/licenses/>.
*/
using FarseerPhysics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using team8.LevelImporter;
using team8.Physics;
using team8.Rendering;

namespace team8.Level
{
    using Coordinate = Vector2;
    public class TileSet
    {
        private string Name;
        private int TileWidth;
        private int TileHeight;
        private string TexturePath;
        public float OriginalTextureWidth;
        public float OriginalTextureHeight;
        public float CompressedTextureWidth;
        public float CompressedTextureHeight;
        public Vector2 ScaleFromCompressedToOriginalSize;
        public int Id;
        public Texture2D Texture;
        private string LevelDirectory;
        private Texture2D DebugTexture;
        //The rotation center in sim coordinates relativ to the render object
        private Coordinate CompressedRotationOrigin;
        //public string ImageId;
        //public bool IsHidden = false;

    

        public TileSet(XElement tilesetElement, string levelDirectory)
        {
            this.LevelDirectory = levelDirectory;
            Parse(tilesetElement: tilesetElement);
        }

        private void Parse(XElement tilesetElement)
        {
            //set Name
            this.Name = tilesetElement.Attribute("name") != null ? tilesetElement.Attribute("name").Value : "";
            //set Id
            this.Id = int.Parse(tilesetElement.Attribute("firstgid").Value);
            //Set dimension
            this.TileWidth = int.Parse(tilesetElement.Attribute("tilewidth").Value);
            this.TileHeight = int.Parse(tilesetElement.Attribute("tileheight").Value);

            //Set Texture properties
            var textureElement = tilesetElement.Element("image");
            this.TexturePath = ParseTexturePath(path: textureElement.Attribute("source").Value);
            this.OriginalTextureWidth = int.Parse(textureElement.Attribute("width").Value);
            this.OriginalTextureHeight = int.Parse(textureElement.Attribute("height").Value);
            ParseProperties(objectElement: tilesetElement);
        }

        private string ParseTexturePath(string path)
        {
            var texturePath = path.Substring(startIndex: 0, length: path.IndexOf('.'));
            texturePath = LevelDirectory + texturePath;
            //texturePath = texturePath.Replace(oldChar: '/', newChar:'\\');
            return texturePath;
        }

        private void ParseProperties(XElement objectElement)
        {
            //define the keys we are looking for
            var imageIdString = "image_id";
            var isHiddenString = "hidden";

            var keys = new List<string>() { imageIdString, isHiddenString };

            //parse the Xelement
            var foundValues = TMXImporter.ParseProperties(element: objectElement, objectType: "LayerObject", keys: keys, failOnMissing: false);
            //convert the found values. If the value was not found, keep the current (default) value
            
            //this.ImageId = foundValues.ContainsKey(imageIdString) ? foundValues[imageIdString] : this.ImageId;
            //this.IsHidden = foundValues.ContainsKey(isHiddenString) ? bool.Parse(foundValues[isHiddenString]) : this.IsHidden;
            //Set values

        }

        public void LoadContent(ContentManager contentManager, PhysicalWorld world)
        {
            //load texture
            this.Texture = contentManager.Load<Texture2D>(this.TexturePath);
            this.CompressedTextureWidth = Texture.Width;
            this.CompressedTextureHeight = Texture.Height;

            this.CompressedRotationOrigin = new Coordinate(CompressedTextureWidth / 2, CompressedTextureHeight / 2);
            this.ScaleFromCompressedToOriginalSize = new Vector2(OriginalTextureWidth / Texture.Width, OriginalTextureHeight / Texture.Height);

            //if (ImageId != null)
            //{
            //    world.AddTileSet(this);
            //}
        }

        public void Draw(SpriteBatch spriteBatch, Coordinate position, Matrix transform, float rotation =0f, Vector2? scale=null, float alpha=1)
        {
            //if (IsHidden)
            //{
            //    return;
            //}
            var realScale = scale.GetValueOrDefault(Vector2.One) * ScaleFromCompressedToOriginalSize;
            var rotationOrigin = new Coordinate(this.Texture.Width/2 * realScale.X, Texture.Height/2 * realScale.Y);
            //var NewRotationOrigin = new Coordinate(0, 0);
            var renderPosition = position + rotationOrigin;
            spriteBatch.Draw(texture: Texture, position: renderPosition, origin: CompressedRotationOrigin, rotation: rotation, scale: realScale, color: Color.White * alpha);

            if (Configuration.RenderObjectVertices && Configuration.AllowRenderObjectVertices)
            {
                if (DebugTexture == null)
                {
                    SetupDebugRendering(graphicsDevice: spriteBatch.GraphicsDevice);
                }
                spriteBatch.Draw(texture: DebugTexture, position: renderPosition, rotation: rotation);
            }
        }

        private void SetupDebugRendering(GraphicsDevice graphicsDevice)
        {
            var dotSize = 5;
            DebugTexture = new Texture2D(graphicsDevice, dotSize, dotSize);
             Color color = Color.Green;

            var colors = new Color[dotSize * dotSize];
            for (int i = 0; i < colors.Length; i++) { colors[i] = color; }
            DebugTexture.SetData(colors);


        }

        public void Dispose()
        {
            Texture.Dispose();
        }
    }
}
