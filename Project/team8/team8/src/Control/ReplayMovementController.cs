﻿using Microsoft.Xna.Framework;
using team8.Physics;
using static team8.Physics.PhysicsObject;

namespace team8.Control
{
    using Coordinate = Microsoft.Xna.Framework.Vector2;
    public class ReplayMovementController: MovementController
    {
        private readonly double positionThreshold = 0.1;
        public MainCharacter GhostableTarget
        {
            get
            {
                return base.ControlledTarget as MainCharacter;
            }
        }

        public ReplayMovementController(MainCharacter controlledTarget) : base(controlledTarget)
        {
            //this.controlledTarget = controlledTarget;
        }

        /*-----replay related-----*/
        public void MoveToPosition(Coordinate toPosition, GameTime gameTime)
        {
           //Get the direction to move to
            var direction = this.PositionIsInDirection(position: toPosition);
            switch (direction) {
                case Direction.Right:
                    Move(direction: direction, gameTime: gameTime);
                    break;
                case Direction.Left:
                    Move(direction: direction, gameTime: gameTime);
                    break;
                case Direction.Stopped:
                    break;
            }
            //DebugMovement("MoveTo");
        }

        public bool HasReachedPosition(Coordinate toPosition, double? withTolerance = null)
        {
            var tolerance = withTolerance.GetValueOrDefault(this.positionThreshold);
            //If the direction towards the toPosition is Stopped (with given tolerance), it has reached its goal
            return PositionIsInDirection(position: toPosition, withTolerance: tolerance) == Direction.Stopped;
        }

        override public void Stop(GameTime gameTime)
        {
            base.Stop(gameTime: gameTime);
            //DebugMovement("Stop");
        }
        
        override public void SlowDown(GameTime gameTime)
        {
            base.SlowDown(gameTime: gameTime);
            //DebugMovement("SlowDown");

        }

        //private void DebugMovement(string actionName)
        //{
        //    Debug.WriteLine(actionName + " Dir: "+ ControlledTarget.RelativeCurrentDirection + " vel:" + ControlledTarget.LinearVelocityToGround());
        //}

        public bool HasStopped()
        {
            return ControlledTarget.RelativeCurrentDirection == Direction.Stopped;
        }
    }
}
