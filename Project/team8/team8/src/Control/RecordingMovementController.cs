﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using team8.Physics;
using team8.Replay;
using static team8.Physics.PhysicsObject;

namespace team8.Control
{
    public class RecordingMovementController : MovementController, ControlledCharacterDelegate
    {
        private ReplayEngine replayEngine;

        public MainCharacter MainCharacter
        {
            get
            {
                return base.ControlledTarget as MainCharacter;
            }
        }

        public RecordingMovementController(MainCharacter controlledTarget, ReplayEngine replayEngine) : base(controlledTarget)
        {
            this.replayEngine = replayEngine;
            this.replayEngine.OnCharacterSelection += OnCharacterSelection;
        }

        override public void Move(Direction direction, GameTime gameTime, double? acceleration = null)
        {

            //If the movement direction has not changed, it might still be the same moveAction as before, but we don't know for sure. The ReplayEngine will decide.
            var mightBeSameAsPreviousAction = !this.changeInMovement(direction: direction);
            replayEngine.NewMoveAction(position: body.Position, gameTime: gameTime, mightBeOldAction: mightBeSameAsPreviousAction);


            base.Move(direction: direction, gameTime: gameTime, acceleration: acceleration);
        }

        override public void SlowDown(GameTime gameTime)
        {
            base.SlowDown(gameTime: gameTime);
            if (ControlledTarget.RelativeCurrentDirection == Direction.Stopped && this.stoppedMoving(direction: Direction.Stopped, debugging: true) )
            {
                replayEngine.NewIdleAction(position: body.Position, gameTime: gameTime);
            }
        }

                
        public void Jump(GameTime gameTime)
        {
            replayEngine.NewJumpAction(position: body.Position, gameTime: gameTime);
            base.Jump();
            replayEngine.NewIdleAction(position: body.Position, gameTime: gameTime);
        }

        override public void Stop(GameTime gameTime)
        {
           
            if (this.changeInMovement(direction: Direction.Stopped, debugging: true))
            {
                replayEngine.NewIdleAction(position: body.Position, gameTime: gameTime);
                //replayEngine.NewMoveAction(position: body.Position, gameTime: gameTime);
            }
            base.Stop(gameTime: gameTime);
        }


        /**
         * Start replaying actions
         **/
        public bool Replay(GameTime gameTime)
        {
            //Can not replay while Replaying or None
            if (replayEngine.ReplayMode != ReplayMode.Recording)
            {
                return false;
            }
            //Finish last action
            replayEngine.StopRecording(ControlledTarget.Position, gameTime: gameTime);

            //Trigger replay
            return replayEngine.StartReplaying(originalObject: this.MainCharacter);
        }

        /**
         * Start Recording actions
         **/
        public bool Record(GameTime gameTime)
        {
            return replayEngine.StartRecording(position: this.ControlledTarget.Position, gameTime: gameTime, facingDirection: ControlledTarget.LastDirection);
        }

        public void ChooseObject(bool dismissGhost)
        {
           //remove one of the characters
            replayEngine.ChooseObjectAndDismissOther(dismissGhost: dismissGhost);
        }

        public void OnCharacterSelection(MainCharacter chosenCharacter)
        {
            if (chosenCharacter != null)
            {
                this.SetControlledTarget(chosenCharacter);
            }
        }

        public bool ReadyToRecord()
        {
            return replayEngine.ReadyToRecord();
        }
        public bool ReadyToReplay()
        {
            return replayEngine.ReadyToReplay();
        }
        public bool ReadyToChooseCharacter()
        {
            return replayEngine.CanDismissCharacter();
        }

        public MainCharacter ControlledCharacter()
        {
            return this.MainCharacter ;
        }
    }
}
