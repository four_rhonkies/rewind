﻿using FarseerPhysics.Common;
using FarseerPhysics.Dynamics;
using FarseerPhysics.Dynamics.Contacts;
using Microsoft.Xna.Framework;
using System;
using System.Diagnostics;
using System.Linq;
using team8.Physics;
using static team8.Physics.PhysicsObject;

namespace team8.Control
{
    using Coordinate = Microsoft.Xna.Framework.Vector2;
    public abstract class MovementController
    {
        //constants
        // Horizontal acceleration on ground.
        private readonly double defaultAcceleration = 60;
        private readonly double defaultDeacceleeration = 100;

        private readonly double accelerationReductionInAir = 0.6;
        private readonly double deaccelerationReductionInAir = 0.1;

        private double acceleration
        {
            get
            {
                return this.defaultAcceleration * (controlledTarget.IsOnGround ? 1: this.accelerationReductionInAir);
            }
        }
       private double deacceleration
        {
            get
            {
                return this.defaultDeacceleeration * (controlledTarget.IsOnGround ? 1 : this.deaccelerationReductionInAir);
            }
        }
        
        private Vector2 accelerationVector
        {
            get
            {
                var direction = new Vector2((float)acceleration, 0);
                direction = Vector2.Transform(direction, Matrix.CreateRotationZ(1f));
                return direction;

            }
        }

       


        //max horizontal velocity
        private double maxVelocity = 4;
        private readonly float MaxXVelocityAir = 2.5f;

        private readonly float jumpVelocity = -8;

        private bool JumpInNextFrame = false;
      
        protected Direction lastDirection = Direction.Stopped;

        private MovableObject controlledTarget;
        public MovableObject ControlledTarget { get { return controlledTarget; } }

        public Body body
        {
            get
            {
                if(controlledTarget == null)
                {
                    return null;
                }
                return controlledTarget.Body;
            }
        }


        public MovementController(MovableObject controlledTarget)
        {
            SetControlledTarget(controlledTarget);
            body.OnSeparation += OnSeparation;
            body.OnCollision += OnCollision;
        }

        public void SetMaxVelocity(double velo)
        {
            this.maxVelocity = velo;
        }
        public double GetMaxVelocity()
        {
            return this.maxVelocity;
        }
        /*-----private methods-----*/



        private void OnSeparation(Fixture fixtureA, Fixture fixtureB)
        {
            var angle = DirectedAngleFromGround(controlledTarget.LinearVelocityToGround());
            if (!controlledTarget.AppliesForceOnGround && !JumpInNextFrame && angle > -45 && angle < 45.0)
            {

                var searchDirection = new Vector2(DirectionSign(controlledTarget.RelativeCurrentDirection) * 0.1f, -1);
                searchDirection.Normalize();
                var intersection = ControlledTarget.PointOnGroundOrCollisionBelowPosition(direction: searchDirection, distance: 0.7f);
                if (intersection.HasValue)
                {

                    var normal = intersection.Value.IntersectionNormal;
                    body.ApplyLinearImpulse(new Vector2(normal.X, -normal.Y) *0.5f); // new Vector2(0, 0.5f)

                }
            }
        }


        private bool OnCollision(Fixture fixtureA, Fixture fixtureB, Contact contact)
        {
            if (JumpInNextFrame && controlledTarget.IsOnGround)
            {
                 JumpInNextFrame = false;
            }
            return true;
        }

        private bool TouchesWall()
        {
            return this.controlledTarget.WallContacts().Count > 0;
        }


        private (Vector2, Coordinate)  GroundDirection(Direction direction)
        {
            var allContacts = controlledTarget.GroundContacts();
            var contacts = allContacts.FindAll(cont => cont.Item1.Manifold.LocalNormal.Y < 0);

            Vector2 groundDirection;    
            Coordinate contactPoint;


            //If no contact, we are in air and move vertically only
            if (contacts.Count == 0)
            {
                groundDirection = new Vector2(1, 0);
                contactPoint = body.Position;
            }

            else
            {
                //Take the first contact from the list
                Contact groundContact = contacts.First().Item1;
                Vector2 groundNormal;
                FixedArray2<Coordinate> points;
                groundContact.GetWorldManifold(normal: out groundNormal, points: out points);
                contactPoint = body.Position;

                //IF two ground object, Take the one not touched last
                if (contacts.Count > 1)
                {
                    Contact steepestContact = groundContact;
                    var steepestAngle = DirectedAngleFromGround(new Vector2(-groundNormal.Y, groundNormal.X)); ;
                    foreach ((Contact,Fixture) cont in contacts)
                    {
                        cont.Item1.GetWorldManifold(normal: out groundNormal, points: out points);
                        double angle = DirectedAngleFromGround(new Vector2(-groundNormal.Y, groundNormal.X));
                        if (steepestAngle < angle)
                        {
                            steepestAngle = angle;
                            steepestContact = cont.Item1;
                        }
                    }
                    steepestContact.GetWorldManifold(normal: out groundNormal, points: out points);
                    
                }
                else if(contacts.Count > 2)
                {
                    //Debug.WriteLine("Touching more than 2 ground objects");
                }
                //The ground normal should be oriented in positive direction
                groundDirection = new Vector2(-groundNormal.Y, groundNormal.X);
            }
            
            return (groundDirection, contactPoint);
            
        }

        private int DirectionSign(Direction direction)
        {
            int directionSign = 0;
            switch (direction)
            {
                case Direction.Left:
                    directionSign = -1;
                    break;
                case Direction.Right:
                    directionSign = 1;
                    break;
                case Direction.Stopped:
                    directionSign = 0;
                    break;
            }
            return directionSign;
        }
        private double AngleFromGround(Vector2 direction)
        {
            Vector2 normalDirection = new Vector2(direction.X, direction.Y);
            normalDirection.Normalize();
            var angle = Math.Atan(-normalDirection.Y / normalDirection.X) / (2 * Math.PI) * 360;
            return angle;
            
        }

        //Is depending on current direction and always positive for upwards movement and negative for downwards movement
        private double DirectedAngleFromGround(Vector2 direction)
        {
            var charcterDirection = ControlledTarget.RelativeCurrentDirection == Direction.Stopped ? ControlledTarget.LastDirection : ControlledTarget.RelativeCurrentDirection;
            var sign = DirectionSign(charcterDirection);
            var angle = (sign == 0 ? 1 : sign ) * AngleFromGround(direction);
            return angle;
            
        }
        /* -----Methods handling the phyisical movement-----*/
        protected void accelerate(Direction direction, double forSeconds, double? acceleration = null)
        {
             var (groundDirection, groundPoint) = GroundDirection(direction: direction);

            //Get signum for direction. + is Right, - is Left
            //Increase acceleration when moving right, decrease when moving left
            var directionSign = DirectionSign(direction);

            //In case of direction changing->Stop. But only while on ground
            if ( (stoppedMoving(direction: direction) && controlledTarget.AppliesForceOnGround ) )// )
            {
                stop();
                //Return to avoid "overshooting" (changing direciton again after stopping)
                return;
            }
            //On ground consider normal of velocity, in air only horizontal direction
            Vector2 currentVelocity = controlledTarget.LinearVelocityToGround();
            var currentXVelocity = controlledTarget.AppliesForceOnGround ? Math.Sign(currentVelocity.X) * currentVelocity.Length() : currentVelocity.X;
            var xVelocity = currentXVelocity + directionSign * forSeconds * acceleration.GetValueOrDefault(this.acceleration);

            //If faster than max, set maxVelocity
            if (maxVelocity - Math.Abs(xVelocity) <= 0)
            {
                xVelocity = directionSign * maxVelocity;
            }

            var newVelocity = (float)xVelocity * groundDirection;

            //first check if we are going to steep.
            var velocityAngle = DirectedAngleFromGround(newVelocity);
            if (velocityAngle > 45f)
            {
                //Walk straight agains wall
                newVelocity =   new Vector2(-groundDirection.Y, groundDirection.X);
            }

            newVelocity = newVelocity + controlledTarget.GroundVelocity();
            //If in air, only apply horizontal velocity
            if (!controlledTarget.AppliesForceOnGround || JumpInNextFrame || DirectedAngleFromGround(newVelocity) > 80)
            {

                newVelocity = new Vector2((float)xVelocity, body.LinearVelocity.Y);
                //Make sure we are not too fast in air
                if (newVelocity.X > MaxXVelocityAir)
                {
                    newVelocity.X = MaxXVelocityAir;
                }

            }
            if (TouchesWall() && !controlledTarget.AppliesForceOnGround)
            {
                //only do this if trying to go towards wall. Let the player fly away from the wall
                var wallDirection = DirectionOfTouchingWall();
                var wallAngle = DirectedAngleFromGround(direction: wallDirection);
                if (wallAngle > 0)
                {
                    var xVelocityOnWall = wallDirection.X / wallDirection.Y * body.LinearVelocity.Y;
                    var velocityReductionOnWall = 0.95f;
                    newVelocity = new Vector2(xVelocityOnWall, body.LinearVelocity.Y) * velocityReductionOnWall;

                }
            }

            //Store the new velocity
            body.LinearVelocity = newVelocity;
        }

        private Vector2 DirectionOfTouchingWall()
        {
            var wallContacts = controlledTarget.WallContacts();
            Vector2 wallNormal;
            FixedArray2<Coordinate> points;
            wallContacts.First().Item1.GetWorldManifold(normal: out wallNormal, points: out points);
            var wallDirection = new Vector2(-wallNormal.Y, wallNormal.X);
            return wallDirection;
        }

        protected void deaccelerate(double forSeconds)
        {
            Direction? direction = null;
            switch (controlledTarget.RelativeCurrentDirection)
            {
            case Direction.Left:
                direction = Direction.Right;
                break;
            case Direction.Right:
                direction = Direction.Left;
                break;
            case Direction.Stopped:
                return;
            }
            //On ground consider normal of velocity, in air only horizontal direction
            double currentAbsVelocity = controlledTarget.IsOnGround ? controlledTarget.LinearVelocityToGround().Length() : Math.Abs(controlledTarget.LinearVelocityToGround().X);
            //deaccelerate with the current acceleration (Math.Abs(this.velocity/forSeconds)) at most.
            var deacc = Math.Min(currentAbsVelocity / forSeconds, deacceleration);

            if (direction.HasValue)
            {
                accelerate(direction: direction.Value, forSeconds: forSeconds, acceleration: deacc);
            }
        }

        protected void jump()
        {
            //Don't allow double jumps/jumps in the air
            if (controlledTarget.IsOnGround && !JumpInNextFrame)
            {
                var jumpImpulse = jumpVelocity * body.Mass;
                body.LinearVelocity = new Vector2(body.LinearVelocity.X, jumpVelocity);
                JumpInNextFrame = true;
                
            }
            else if (controlledTarget.IsOnGround)
            {
                JumpInNextFrame = false;
                body.Position = body.Position + new Vector2(0, -0.1f);
            }
        }

        protected void stop()
        {
            var groundVelocityX = controlledTarget.GroundVelocity().X;
            var newVelocity = new Vector2(groundVelocityX, body.LinearVelocity.Y);
            body.LinearVelocity = newVelocity;
        }


        /*----- Methods for checking the character state -----*/

        protected bool directionChanged(Direction direction, bool debugging = false)
        {
            var changed = this.lastDirection != Direction.Stopped && direction != this.lastDirection;
            if (changed && debugging)
            {
                Debug.WriteLine("Direction changed. From " + this.lastDirection + " to " + direction);
            }
            return changed;
        }

        protected bool startedMoving(Direction direction, bool debugging = false)
        {
            var started = this.lastDirection == Direction.Stopped && direction != Direction.Stopped;
            if (started && debugging)
            {
                Debug.WriteLine("Started moving. From "+ this.lastDirection + " to "+direction);
            }
            return started;
        }

        protected bool stoppedMoving(Direction direction, bool debugging = false)
        {
            //TODO: What if physics engine stopped the character and the player also stopped? Then this will return false...
            var stopped = this.lastDirection != Direction.Stopped && direction == Direction.Stopped;
            if (stopped && debugging)
            {
                Debug.WriteLine("Stopped moving. From " + this.lastDirection + " to " + direction);
            }

            
            return stopped;
        }

        protected bool changeInMovement(Direction direction, bool debugging=false)
        {
            return directionChanged(direction: direction, debugging: debugging) || startedMoving(direction: direction, debugging: debugging) || stoppedMoving(direction: direction, debugging: debugging);
        }

        /**
         * Indicates the direction in which the given position is from the body's courrent position
         **/
        protected Direction PositionIsInDirection(Coordinate position, double? withTolerance=null)
        {
            var tolerance = withTolerance.GetValueOrDefault(this.ControlledTarget.StopThreshold); 

            //First make sure we are still far enough for the given tolerance
            if (Math.Abs(position.X - body.Position.X) > tolerance)
            {   
                //Then check for the direction
                if (position.X > body.Position.X)
                {
                    return Direction.Right;
                }
                else if (position.X < body.Position.X)
                {
                    return Direction.Left;
                }
            }
            
            //If we are close enough, stopped
            return Direction.Stopped;
        }

        protected void SetControlledTarget(MovableObject newControlledTarget)
        {
            if (body != null)
            {
                body.OnCollision -= this.OnCollision;
                body.OnSeparation -= this.OnSeparation;
            }
            newControlledTarget.body.OnCollision += this.OnCollision;
            newControlledTarget.body.OnSeparation += this.OnSeparation;
            this.controlledTarget = newControlledTarget;
        }

        public void Dispose()
        {
            if (body != null)
            {
                body.OnCollision -= this.OnCollision;
                body.OnSeparation -= this.OnSeparation;
            }
        }

        public virtual void Move(Direction direction, GameTime gameTime, double? acceleration = null)
        {
            //Move character
            this.accelerate(direction: direction, forSeconds: gameTime.ElapsedGameTime.TotalSeconds, acceleration: acceleration);
            this.lastDirection = direction;
            controlledTarget.Moves(direction: direction);
        }

        virtual public void Stop(GameTime gameTime)
        {
            this.stop();
            this.lastDirection = Direction.Stopped;
            controlledTarget.Idles();
        }

        public virtual void SlowDown(GameTime gameTime)
        {
            if (ControlledTarget.IsOnGround)
            {
                Stop(gameTime: gameTime);
            }
            else {
                if (ControlledTarget.CurrentDirection != Direction.Stopped)
                {
                    this.deaccelerate(forSeconds: gameTime.ElapsedGameTime.TotalSeconds);
                }
                else
                {
                    if (this.stoppedMoving(direction: Direction.Stopped, debugging: true))
                    {
                        this.lastDirection = Direction.Stopped;
                        controlledTarget.Idles();
                    }
                }
            }
        }
        public virtual void Jump()
        {
            this.jump();
            controlledTarget.Jumps();
        }
    }
}
