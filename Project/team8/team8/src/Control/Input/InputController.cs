﻿using Microsoft.Xna.Framework;
using System.Collections.Generic;
using System.Linq;
using team8.Rendering;
using team8.Sound;
using static team8.Physics.PhysicsObject;

namespace team8.Control
{
    public class InputController
    {
        public enum KeyDefinition
        {
            LeftKey,
            RightKey,
            JumpKey,
            DebugKey,
            ReplayKey,
            ChooseCharacterKey,
            ZoomInKey,
            ZoomOutKey,
            MoveCameraUpKey,
            MoveCameraDownKey,
            ReloadLevelKey,
            RespawnKey,
            MuteKey,
            DebugRenderingKey,
            FreeCameraKey,
            RightStickRight,
            RightStickLeft,
            ShowInstructionsKey
        }

        public struct HijackedKeyDefinition
        {
            public KeyPressDelegate KeyPressDelegate;
            public bool Blocking;
            public bool SinglePress;
            public bool OnRelease;

            public HijackedKeyDefinition(KeyPressDelegate keyPressDelegate, bool blocking, bool singlePress, bool onRelease=false)
            {
                this.KeyPressDelegate = keyPressDelegate;
                this.Blocking = blocking;
                this.SinglePress = singlePress;
                this.OnRelease = onRelease;
            }
        }

        private List<IInputHandler> InputHandlers;
       
        private RecordingMovementController movementController;
        private CameraController CameraController;
        private SoundManager soundManager;

        public ReloadGameDelegate ReloadDelegate;
        public ChooseCharacterDelegate ChooseCharacterDelegate;
        public CharacterResetDelegate CharacterResetDelegate;
        public ChangeScreenDelegate ChangeScreenDelegate;

        private static InputController PrivateInstance;
        public static InputController Instance
        {
            get
            {
                return PrivateInstance;
            }
        }

        private Dictionary<KeyDefinition, HijackedKeyDefinition> HijackedKeys = new Dictionary<KeyDefinition, HijackedKeyDefinition>();
        public delegate void KeyPressDelegate(GameTime gameTime);
        protected List<KeyDefinition> IgnoredKeys;
        private List<KeyDefinition> UnblockedKeys;
        private bool Blocked=false;

        public InputController( CameraController cameraController,
                                SoundManager soundManager)
        {
            this.CameraController = cameraController;
            this.soundManager = soundManager;
            this.InputHandlers = new List<IInputHandler>();
            this.UnblockedKeys = new List<KeyDefinition>();
            PrivateInstance = this;
        }

        public void SetMovementController(RecordingMovementController movementController)
        {
            this.movementController = movementController;

        }

        public void AddInputHandler(IInputHandler handler)
        {
            this.InputHandlers.Add(handler);
        }


        public virtual void HandleInput(GameTime gameTime)
        {
            
            foreach(var handler in InputHandlers)
            {
                handler.Update(gameTime);
            }
            this.IgnoredKeys = HandleHijackedKeys(gameTime: gameTime);
            HandleSpecialKeys(gameTime: gameTime);
            HandleRunning(gameTime);
            HandleJumping(gameTime);
            HandleCamera();
        }

        public void BlockAllKeys(List<KeyDefinition> exceptKeys=null)
        {
            if (exceptKeys != null)
            {
                this.UnblockedKeys = exceptKeys;
            }
            this.Blocked = true;
        }
        public void UnblockKeys()
        {
            this.Blocked = false;
        }

        public void HijackControl(KeyDefinition key, KeyPressDelegate keyDelegate, bool blocking, bool singlePress=false, bool onRelease = false)
        {
            HijackedKeys[key] = new HijackedKeyDefinition(keyPressDelegate: keyDelegate, blocking: blocking, singlePress: singlePress, onRelease: onRelease);
            
        }
        public void HijackControl(KeyDefinition key, HijackedKeyDefinition hijackedKeyDefintion)
        {
            HijackedKeys[key] = hijackedKeyDefintion;

        }
        public void ReleaseHijackedControl(KeyDefinition key)
        {
            HijackedKeys.Remove(key);

        }
        private List<KeyDefinition> HandleHijackedKeys(GameTime gameTime)
        {
            List<KeyDefinition> pressedKeys = new List<KeyDefinition>();
            foreach (KeyDefinition key in HijackedKeys.Keys.ToList())
            {
                if (HijackedKeys[key].Blocking)
                {
                    pressedKeys.Add(key);
                }
                var pressed = false;
                if (HijackedKeys[key].OnRelease)
                {
                    pressed = FirstKeyReleaseRaw(key: key);
                }
                else
                {
                    pressed = HijackedKeys[key].SinglePress ? FirstKeyPressRaw(key: key) : KeyPressedRaw(key);
                }
                if (pressed)
                {

                    HijackedKeys[key].KeyPressDelegate(gameTime: gameTime);
                }
            }
            return pressedKeys;
        }

        private void HandleChooseCharacter()
        {
            if (movementController != null && movementController.ReadyToChooseCharacter() && ChooseCharacterDelegate.ReadyToSelect())
            {
                ReleaseHijackedControl(KeyDefinition.JumpKey);
                movementController.ChooseObject(dismissGhost: !ChooseCharacterDelegate.IsGhostMarked());
                ChooseCharacterDelegate?.EndChoosing();
                soundManager?.PlaySound(file: SoundManager.SoundFile.ReplayDeleteGhost, id: "ChooseCharacter", initialVolume: Configuration.Sound.ReplayButtonVolume);
            }
        }

        private void HandleSpecialKeys(GameTime gameTime)
        {
            //Single fire
            if ( FirstKeyPress(key: KeyDefinition.ReplayKey) && movementController != null)
            {
                if (movementController.ReadyToRecord())
                {
                    var success = this.movementController.Record(gameTime);
                    if (success)
                    {
                        soundManager.PlaySound(file: SoundManager.SoundFile.RecordStart, id: "RecordStart", initialVolume: Configuration.Sound.ReplayButtonVolume);
                    }
                    else
                    {
                        soundManager.PlaySound(file: SoundManager.SoundFile.PressDenied, id: "PressDenied", initialVolume: Configuration.Sound.ButtonDeniedVolume);
                    }
                }
                else if (movementController.ReadyToReplay())
                {
                    var success = this.movementController.Replay(gameTime: gameTime);
                    if (success)
                    {
                        soundManager?.PlaySound(file: SoundManager.SoundFile.ReplayStart, id: "ReplayStart", initialVolume: Configuration.Sound.ReplayButtonVolume);
                    }
                    else
                    {
                        soundManager?.PlaySound(file: SoundManager.SoundFile.PressDenied, id: "PressDenied", initialVolume: Configuration.Sound.ButtonDeniedVolume);
                    }
                }
                else if (movementController.ReadyToChooseCharacter())
                {
                    ChooseCharacterDelegate?.MarkOtherCharacter();
                    soundManager?.PlaySound(file: SoundManager.SoundFile.ReplayToggle, id: "ReplayToggle");
                }
                else
                {
                    soundManager?.PlaySound(file: SoundManager.SoundFile.PressDenied, id: "PressDenied", initialVolume: Configuration.Sound.ButtonDeniedVolume);
                }
                
            }

            if (FirstKeyPress(key: KeyDefinition.ChooseCharacterKey))
            {
                HandleChooseCharacter();
            }
            if (FirstKeyPress(key: KeyDefinition.ReloadLevelKey))
            {
                ReloadDelegate?.ReloadWorld();
            }
            if (FirstKeyPress(key: KeyDefinition.RespawnKey))
            {
                CharacterResetDelegate?.RespawnAtLastSpirit();
            }
            if (FirstKeyPress(key: KeyDefinition.MuteKey) )
            {
                soundManager?.ToggleMuteSound();
            }
            if (FirstKeyPress(key: KeyDefinition.DebugRenderingKey))
            {
                Configuration.RenderObjectVertices = !Configuration.RenderObjectVertices;
            }
            if (FirstKeyPress(key: KeyDefinition.FreeCameraKey))
            {
                Configuration.Camera.FreeCamera = !Configuration.Camera.FreeCamera;
            }
            if(FirstKeyPress(key: KeyDefinition.ShowInstructionsKey))
            {
                ChangeScreenDelegate?.ShowInstructionScreen();
            }
        }

        private void EndChoosingCharacter()
        {
            ChooseCharacterDelegate?.EndChoosing();
            IgnoredKeys.Remove(KeyDefinition.LeftKey);
            IgnoredKeys.Remove(KeyDefinition.RightKey);
        }

        private void StartChoosingCharacter()
        {
            IgnoredKeys.Add(KeyDefinition.LeftKey);
            IgnoredKeys.Add(KeyDefinition.RightKey);
            ChooseCharacterDelegate?.StartChoosing();
        }

        private void HandleJumping(GameTime gameTime)
        {
            if (FirstKeyPress(KeyDefinition.JumpKey) )
            {
                this.movementController?.Jump(gameTime: gameTime);
            }
        }

        private void HandleRunning(GameTime gameTime)
        {
            var secondsElapsed = gameTime.ElapsedGameTime.TotalSeconds;

            Direction? direction = null;
            if (KeyPressed(KeyDefinition.RightKey))
            {
                direction = Direction.Right;
            }
            else if (KeyPressed(KeyDefinition.LeftKey))
            {
                direction = Direction.Left;
            }
            else if ( !Blocked && movementController != null && (!KeyPressed(key: KeyDefinition.RightKey) && !KeyPressed(key:KeyDefinition.LeftKey) ))
            {
                this.movementController?.SlowDown(gameTime: gameTime);
                return;
            }
            if (movementController != null && direction.HasValue)
            {
                this.movementController.Move(direction: direction.Value, gameTime: gameTime);
            }
       }

        private void HandleCamera()
        {
            //Zoom in
            if (KeyPressed(KeyDefinition.ZoomInKey))
            {
                CameraController?.ZoomIn();
            }
            //zoom out
            else if (KeyPressed(KeyDefinition.ZoomOutKey))
            {
                CameraController?.ZoomOut();
            }
            //down
            else if (KeyPressed(KeyDefinition.MoveCameraDownKey))
            {
                CameraController?.MoveCameraDown();
            }
            //up
            else if (KeyPressed(KeyDefinition.MoveCameraUpKey))
            {
                CameraController?.MoveCameraUp();
            }
            //right
            else if (KeyPressed(KeyDefinition.RightStickRight))
            {
                CameraController?.MoveCameraRight();
            }
            //left
            else if (KeyPressed(KeyDefinition.RightStickLeft))
            {
                CameraController?.MoveCameraLeft();
            }
        }

        private bool DisallowKey(KeyDefinition key)
        {
            return this.IgnoredKeys.Contains(key) || (Blocked && !this.UnblockedKeys.Contains(key));
        }
        private bool KeyPressed(KeyDefinition key)
        {
            if (DisallowKey(key))
            {
                return false;
            }
            else
            {
                return KeyPressedRaw(key: key);
            }
        }
        private bool FirstKeyPress(KeyDefinition key)
        {
            if (DisallowKey(key))
            {
                return false;
            }
            else
            {
                return FirstKeyPressRaw(key: key);
            }
        }
        private bool FirstKeyRelease(KeyDefinition key)
        {
            if (DisallowKey(key))
            {
                return false;
            }
            else
            {
                return FirstKeyReleaseRaw(key: key);
            }
        }

        private bool KeyPressedRaw(KeyDefinition key)
        {
            var pressed = false;
            foreach (var handler in InputHandlers)
            {
                pressed = pressed || handler.KeyPressedRaw(key: key);
            }
            return pressed;
        }

        private bool FirstKeyPressRaw(KeyDefinition key)
        {
            var pressed = false;
            foreach (var handler in InputHandlers)
            {
                pressed = pressed || handler.FirstKeyPressRaw(key: key);
            }
            return pressed;
        }

        private bool FirstKeyReleaseRaw(KeyDefinition key)
        {
            var released = false;
            foreach (var handler in InputHandlers)
            {
                released = released || handler.FirstKeyReleaseRaw(key: key);
            }
            return released;
        }


        public void Vibrate(float leftMotor=1f, float rightMotor = 1f, int? duration=null)
        {
            foreach (var handler in InputHandlers)
            {
                handler.Vibrate(leftMotor: leftMotor, rightMotor:rightMotor, duration: duration);
            }
        }
    }

    public interface ChooseCharacterDelegate
    {
        void StartChoosing();
        void EndChoosing();
        bool ReadyToSelect();
        bool IsGhostMarked();
        void MarkOtherCharacter();
    }
}
