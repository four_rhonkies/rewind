﻿using Microsoft.Xna.Framework;
using static team8.Control.InputController;

namespace team8.Control
{
    public interface IInputHandler
    {

        bool FirstKeyPressRaw(KeyDefinition key);
        bool FirstKeyReleaseRaw(KeyDefinition key);
        bool KeyPressedRaw(KeyDefinition key);
        void Update(GameTime gameTime);
        void Vibrate(float leftMotor, float rightMotor, int? duration=null);
    }
}
