﻿using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using team8.Rendering;
using team8.Sound;
using Microsoft.Xna.Framework;
using static team8.Control.InputController;

namespace team8.Control
{
    public class KeyboardInputHandler : IInputHandler
    {
        private KeyboardState KeyboardState;
        private KeyboardState LastKeyboardState;

        void IInputHandler.Update(GameTime gameTime)
        {
            LastKeyboardState = KeyboardState;
            KeyboardState = Keyboard.GetState();
        }

        bool IInputHandler.FirstKeyPressRaw(KeyDefinition key)
        {
            return KeyPressed(key: key, state: KeyboardState) && !KeyPressed(key: key, state: LastKeyboardState);
            //return KeyboardState.IsKeyDown(key) && LastKeyboardState.IsKeyUp(key);
        }
        bool IInputHandler.FirstKeyReleaseRaw(KeyDefinition key)
        {
            return !KeyPressed(key: key, state: KeyboardState) && KeyPressed(key: key, state: LastKeyboardState);
        }

            bool IInputHandler.KeyPressedRaw(KeyDefinition key)
        {
            return KeyPressed(key: key, state: this.KeyboardState);
        }

        private bool KeyPressed(KeyDefinition key, KeyboardState state)
        {
            var pressedKey = GetKeyForKeyDefinition(definition: key);
            if (pressedKey.HasValue)
            {
                return state.IsKeyDown(key: pressedKey.Value);
            }
            else
            {
                return false;
            }
        }

        private Keys? GetKeyForKeyDefinition(KeyDefinition definition)
        {
            switch (definition)
            {
                case KeyDefinition.LeftKey:
                    return Keys.A;
                case KeyDefinition.RightKey:
                    return Keys.D;
                case KeyDefinition.JumpKey:
                    return Keys.Space;
                case KeyDefinition.DebugKey:
                    return Keys.P;
                case KeyDefinition.ReplayKey:
                    return Keys.R;
                case KeyDefinition.RespawnKey:
                    return Keys.T;
                //case KeyDefinition.DismissGhostKey:
                //    return Keys.D1;
                //case KeyDefinition.DismissOriginalKey:
                //    return Keys.D2;
                case KeyDefinition.ChooseCharacterKey:
                    return Keys.E;
                case KeyDefinition.ZoomInKey:
                    return Keys.I;
                case KeyDefinition.ZoomOutKey:
                    return Keys.O;
                case KeyDefinition.MoveCameraUpKey:
                    return Keys.Up;
                case KeyDefinition.MoveCameraDownKey:
                    return Keys.Down;
                case KeyDefinition.ReloadLevelKey:
                    return Keys.F5;
                case KeyDefinition.MuteKey:
                    return Keys.M;
                case KeyDefinition.DebugRenderingKey:
                    return Keys.U;
                case KeyDefinition.FreeCameraKey:
                    return Keys.F;
                case KeyDefinition.RightStickLeft:
                    return Keys.Left;
                case KeyDefinition.RightStickRight:
                    return Keys.Right;
                case KeyDefinition.ShowInstructionsKey:
                    return Keys.H;
                default:
                    return null;
            }
        }

        public void Vibrate(float leftMotor, float rightMotor, int? duration)
        {

        }
    }
}
