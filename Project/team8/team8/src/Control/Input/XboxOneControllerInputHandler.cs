﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using static team8.Control.InputController;
using System.Threading;

namespace team8.Control
{
    public class XboxOneControllerInputHandler : IInputHandler
    {
        private GamePadState GamePadState;
        private GamePadState LastGamePadState;
        private PlayerIndex PlayerIndex = PlayerIndex.One;
        private Timer VibrationTimer;
        private int VibrationTime = 200;

        void IInputHandler.Update(GameTime gameTime)
        {
            LastGamePadState = GamePadState;
            GamePadState = GamePad.GetState(PlayerIndex);
        }

        bool IInputHandler.FirstKeyPressRaw(KeyDefinition key)
        {
            return KeyPressed(key: key, state: GamePadState) && !KeyPressed(key: key, state: LastGamePadState);
        }

        bool IInputHandler.FirstKeyReleaseRaw(KeyDefinition key)
        {
            return !KeyPressed(key: key, state: GamePadState) && KeyPressed(key: key, state: LastGamePadState);
        }

        bool IInputHandler.KeyPressedRaw(KeyDefinition key)
        {
            return KeyPressed(key: key, state: GamePadState);
        }

        private bool KeyPressed(KeyDefinition key, GamePadState state)
        {
            var button = GetButtonForKeyDefinition(definition: key);
            if (button.HasValue)
            {
                return state.IsButtonDown(button: button.Value);
            }
            else
            {
                return false;
            }
        }

        protected virtual Buttons? GetButtonForKeyDefinition(KeyDefinition definition)
        {
            switch (definition)
            {
                case KeyDefinition.LeftKey:
                    return Buttons.LeftThumbstickLeft;
                case KeyDefinition.RightKey:
                    return Buttons.LeftThumbstickRight;
                case KeyDefinition.JumpKey:
                    return Buttons.A;
                case KeyDefinition.ReplayKey:
                    return Buttons.B;
                case KeyDefinition.ChooseCharacterKey:
                    return Buttons.X;
                case KeyDefinition.MoveCameraUpKey:
                    return Buttons.RightThumbstickUp;
                case KeyDefinition.MoveCameraDownKey:
                    return Buttons.RightThumbstickDown;
                case KeyDefinition.RightStickRight:
                    return Buttons.RightThumbstickRight;
                case KeyDefinition.RightStickLeft:
                    return Buttons.RightThumbstickLeft;
                case KeyDefinition.RespawnKey:
                    return Buttons.Y;
                case KeyDefinition.ShowInstructionsKey:
                    return Buttons.Start;
                default:
                    return null;
            }
        }

        public void Vibrate(float leftMotor = 0.3f, float rightMotor = 0.3f, int? duration = null)
        {
            TimerCallback stop = delegate (object state)
            {
                GamePad.SetVibration(playerIndex: PlayerIndex, leftMotor: 0, rightMotor: 0);
            };
            VibrationTimer = new Timer(callback: stop, state: null, dueTime: duration.GetValueOrDefault(this.VibrationTime), period: Timeout.Infinite);

            GamePad.SetVibration(playerIndex: PlayerIndex, leftMotor: leftMotor, rightMotor: rightMotor);

        }
    }
    public class AltXboxOneControllerInputHandler : XboxOneControllerInputHandler
    {
        protected override  Buttons? GetButtonForKeyDefinition(KeyDefinition definition)
        {
            switch (definition)
            {
                case KeyDefinition.LeftKey:
                    return Buttons.DPadLeft;
                case KeyDefinition.RightKey:
                    return Buttons.DPadRight;
                default:
                    return base.GetButtonForKeyDefinition(definition);
            }
        }
    }
}
