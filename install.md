## Install the AppPackage
1. Unzip
2. Double-click the *.cer file.
3. Click "Install Certificate..."
4. Choose "Local Machine", "Next"
5. Check "Place all certificates in the following store" and click "Browse"
6. Select "Trusted People" and "Ok"
7. Click Next
6. "Finish"
7. (short) "Yes"
8. (long) A Seccurity Warning will pop up asking you whether you want to install a certificate from an unknow CA called "team8". Since we cannot issue certificates through a truseted CA, you will have to accept that risk. You can uninstall the certificate after testing the game.
9. Now you should see: "The import was successful"
10. Double click the .appxbundle file
11. Select "Install"

### Troubleshoot
Error: 
_The current user has already installed an unpackaged version of this app. A packaged version cannot replace this. The conflicting package is Rewind and it was published by CN=team8._
Solution: 
1. Press windows key
2. type team8
3. right click on the found app
4. uninstall

### Helpful link
https://stackoverflow.com/questions/23812471/installing-appx-without-trusted-certificate
